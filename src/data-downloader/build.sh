#!/usr/bin/env bash

set -o xtrace

echo "Starting build..."

# Check that all dependencies are met
hash protoc 2>/dev/null || { echo >&2 "protoc not found on path. Obtain latest version from https://github.com/google/protobuf/releases"; exit 1; }

# Define constants
SRC=datadownloader.proto

GEN=generated_code
GEN_JAVA=${GEN}/java
GEN_PYTHON=${GEN}/python

# Remove old generated code
rm -rf ${GEN}

# Create directories for newly generated code
mkdir -p ${GEN_JAVA}
mkdir -p ${GEN_PYTHON}

echo "Compiling ${SRC} with protoc"
protoc --java_out=./${GEN_JAVA} \
       --python_out=./${GEN_PYTHON} \
       ${SRC}

echo "Done."

set +o xtrace
