#!/bin/bash

set -o xtrace

echo "Compiling analysis.proto"
rm -rf java
rm -rf python
rm -rf javascript
mkdir -p java
mkdir -p python
mkdir -p javascript
protoc --java_out=./java --python_out=./python analysis.proto
pbjs --target static-module --wrap ./jswrapper.js --root redvoxanalysis --out ./javascript/analysis.pb.js analysis.proto # Requires node, npm, protobufjs to be installed globally

# requires protobufjs@6.7.0!!!!!! Someone, please update me.

set +o xtrace
