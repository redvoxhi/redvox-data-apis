/*eslint-disable block-scoped-var, no-redeclare, no-control-regex, no-prototype-builtins*/
var $protobuf = protobuf;

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots.redvoxanalysis || ($protobuf.roots.redvoxanalysis = {});

$root.protobuf = (function() {

    /**
     * Namespace protobuf.
     * @exports protobuf
     * @namespace
     */
    var protobuf = {};

    protobuf.Response = (function() {

        /**
         * Properties of a Response.
         * @typedef protobuf.Response$Properties
         * @type {Object}
         * @property {protobuf.Response.ResponseType} [responseType] Response responseType.
         * @property {string} [message] Response message.
         * @property {string} [error] Response error.
         */

        /**
         * Constructs a new Response.
         * @exports protobuf.Response
         * @constructor
         * @param {protobuf.Response$Properties=} [properties] Properties to set
         */
        function Response(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * Response responseType.
         * @type {protobuf.Response.ResponseType|undefined}
         */
        Response.prototype.responseType = 0;

        /**
         * Response message.
         * @type {string|undefined}
         */
        Response.prototype.message = "";

        /**
         * Response error.
         * @type {string|undefined}
         */
        Response.prototype.error = "";

        /**
         * Creates a new Response instance using the specified properties.
         * @param {protobuf.Response$Properties=} [properties] Properties to set
         * @returns {protobuf.Response} Response instance
         */
        Response.create = function create(properties) {
            return new Response(properties);
        };

        /**
         * Encodes the specified Response message. Does not implicitly {@link protobuf.Response.verify|verify} messages.
         * @param {protobuf.Response$Properties} message Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Response.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.responseType != null && message.hasOwnProperty("responseType"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.responseType);
            if (message.message != null && message.hasOwnProperty("message"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.message);
            if (message.error != null && message.hasOwnProperty("error"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.error);
            return writer;
        };

        /**
         * Encodes the specified Response message, length delimited. Does not implicitly {@link protobuf.Response.verify|verify} messages.
         * @param {protobuf.Response$Properties} message Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Response.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Response message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.Response} Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Response.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.Response();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.responseType = reader.uint32();
                    break;
                case 2:
                    message.message = reader.string();
                    break;
                case 3:
                    message.error = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Response message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.Response} Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Response.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Response message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        Response.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.responseType != null)
                switch (message.responseType) {
                default:
                    return "responseType: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.message != null)
                if (!$util.isString(message.message))
                    return "message: string expected";
            if (message.error != null)
                if (!$util.isString(message.error))
                    return "error: string expected";
            return null;
        };

        /**
         * Creates a Response message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.Response} Response
         */
        Response.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.Response)
                return object;
            var message = new $root.protobuf.Response();
            switch (object.responseType) {
            case "SUCCESS":
            case 0:
                message.responseType = 0;
                break;
            case "ERROR":
            case 1:
                message.responseType = 1;
                break;
            case "DONE":
            case 2:
                message.responseType = 2;
                break;
            }
            if (object.message != null)
                message.message = String(object.message);
            if (object.error != null)
                message.error = String(object.error);
            return message;
        };

        /**
         * Creates a Response message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.Response.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.Response} Response
         */
        Response.from = Response.fromObject;

        /**
         * Creates a plain object from a Response message. Also converts values to other types if specified.
         * @param {protobuf.Response} message Response
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Response.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.responseType = options.enums === String ? "SUCCESS" : 0;
                object.message = "";
                object.error = "";
            }
            if (message.responseType != null && message.hasOwnProperty("responseType"))
                object.responseType = options.enums === String ? $root.protobuf.Response.ResponseType[message.responseType] : message.responseType;
            if (message.message != null && message.hasOwnProperty("message"))
                object.message = message.message;
            if (message.error != null && message.hasOwnProperty("error"))
                object.error = message.error;
            return object;
        };

        /**
         * Creates a plain object from this Response message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Response.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this Response to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        Response.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * ResponseType enum.
         * @name ResponseType
         * @memberof protobuf.Response
         * @enum {number}
         * @property {number} SUCCESS=0 SUCCESS value
         * @property {number} ERROR=1 ERROR value
         * @property {number} DONE=2 DONE value
         */
        Response.ResponseType = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "SUCCESS"] = 0;
            values[valuesById[1] = "ERROR"] = 1;
            values[valuesById[2] = "DONE"] = 2;
            return values;
        })();

        return Response;
    })();

    /**
     * PlotType enum.
     * @name PlotType
     * @memberof protobuf
     * @enum {number}
     * @property {number} MICROPHONE_FFT=0 MICROPHONE_FFT value
     * @property {number} MICROPHONE_MULTIRES=1 MICROPHONE_MULTIRES value
     * @property {number} BAROMETER_FFT=2 BAROMETER_FFT value
     * @property {number} BAROMETER_MULTIRES=3 BAROMETER_MULTIRES value
     * @property {number} LATENCY=4 LATENCY value
     * @property {number} LATITUDE_PLOT=5 LATITUDE_PLOT value
     * @property {number} LONGITUDE_PLOT=6 LONGITUDE_PLOT value
     * @property {number} ALTITUDE_PLOT=7 ALTITUDE_PLOT value
     * @property {number} ACCURACY_PLOT=8 ACCURACY_PLOT value
     * @property {number} MAGNETOMETER_PLOT=9 MAGNETOMETER_PLOT value
     * @property {number} GYROSCOPE_PLOT=10 GYROSCOPE_PLOT value
     * @property {number} LIGHT_PLOT=11 LIGHT_PLOT value
     * @property {number} ACCELEROMETER_PLOT=12 ACCELEROMETER_PLOT value
     * @property {number} MICROPHONE_FFT_LINEAR=13 MICROPHONE_FFT_LINEAR value
     * @property {number} MICROPHONE_FFT_LOG=14 MICROPHONE_FFT_LOG value
     * @property {number} AUDIO_PICKER=15 AUDIO_PICKER value
     */
    protobuf.PlotType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "MICROPHONE_FFT"] = 0;
        values[valuesById[1] = "MICROPHONE_MULTIRES"] = 1;
        values[valuesById[2] = "BAROMETER_FFT"] = 2;
        values[valuesById[3] = "BAROMETER_MULTIRES"] = 3;
        values[valuesById[4] = "LATENCY"] = 4;
        values[valuesById[5] = "LATITUDE_PLOT"] = 5;
        values[valuesById[6] = "LONGITUDE_PLOT"] = 6;
        values[valuesById[7] = "ALTITUDE_PLOT"] = 7;
        values[valuesById[8] = "ACCURACY_PLOT"] = 8;
        values[valuesById[9] = "MAGNETOMETER_PLOT"] = 9;
        values[valuesById[10] = "GYROSCOPE_PLOT"] = 10;
        values[valuesById[11] = "LIGHT_PLOT"] = 11;
        values[valuesById[12] = "ACCELEROMETER_PLOT"] = 12;
        values[valuesById[13] = "MICROPHONE_FFT_LINEAR"] = 13;
        values[valuesById[14] = "MICROPHONE_FFT_LOG"] = 14;
        values[valuesById[15] = "AUDIO_PICKER"] = 15;
        return values;
    })();

    /**
     * DataType enum.
     * @name DataType
     * @memberof protobuf
     * @enum {number}
     * @property {number} WAVE_MIC=0 WAVE_MIC value
     * @property {number} WAVE_BAR=1 WAVE_BAR value
     */
    protobuf.DataType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "WAVE_MIC"] = 0;
        values[valuesById[1] = "WAVE_BAR"] = 1;
        return values;
    })();

    protobuf.WebBasedReportRequest = (function() {

        /**
         * Properties of a WebBasedReportRequest.
         * @typedef protobuf.WebBasedReportRequest$Properties
         * @type {Object}
         * @property {number|Long} [uuid] WebBasedReportRequest uuid.
         * @property {number|Long} [startTimestampSecondsUtc] WebBasedReportRequest startTimestampSecondsUtc.
         * @property {number|Long} [endTimestampSecondsUtc] WebBasedReportRequest endTimestampSecondsUtc.
         * @property {Array.<number|Long>} [deviceIds] WebBasedReportRequest deviceIds.
         * @property {Array.<protobuf.PlotType>} [plotTypes] WebBasedReportRequest plotTypes.
         * @property {number} [originLatitudeDecimalDegrees] WebBasedReportRequest originLatitudeDecimalDegrees.
         * @property {number} [originLongitudeDecimcalDegrees] WebBasedReportRequest originLongitudeDecimcalDegrees.
         * @property {number} [originHeightMeters] WebBasedReportRequest originHeightMeters.
         * @property {number|Long} [originTimestampSecondsUtc] WebBasedReportRequest originTimestampSecondsUtc.
         * @property {string} [requestee] WebBasedReportRequest requestee.
         * @property {string} [reportId] WebBasedReportRequest reportId.
         * @property {Array.<string>} [deviceIdsApi900] WebBasedReportRequest deviceIdsApi900.
         * @property {boolean} [performTimeSynch] WebBasedReportRequest performTimeSynch.
         * @property {string} [params] WebBasedReportRequest params.
         */

        /**
         * Constructs a new WebBasedReportRequest.
         * @exports protobuf.WebBasedReportRequest
         * @constructor
         * @param {protobuf.WebBasedReportRequest$Properties=} [properties] Properties to set
         */
        function WebBasedReportRequest(properties) {
            this.deviceIds = [];
            this.plotTypes = [];
            this.deviceIdsApi900 = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * WebBasedReportRequest uuid.
         * @type {number|Long|undefined}
         */
        WebBasedReportRequest.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * WebBasedReportRequest startTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        WebBasedReportRequest.prototype.startTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * WebBasedReportRequest endTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        WebBasedReportRequest.prototype.endTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * WebBasedReportRequest deviceIds.
         * @type {Array.<number|Long>|undefined}
         */
        WebBasedReportRequest.prototype.deviceIds = $util.emptyArray;

        /**
         * WebBasedReportRequest plotTypes.
         * @type {Array.<protobuf.PlotType>|undefined}
         */
        WebBasedReportRequest.prototype.plotTypes = $util.emptyArray;

        /**
         * WebBasedReportRequest originLatitudeDecimalDegrees.
         * @type {number|undefined}
         */
        WebBasedReportRequest.prototype.originLatitudeDecimalDegrees = 0;

        /**
         * WebBasedReportRequest originLongitudeDecimcalDegrees.
         * @type {number|undefined}
         */
        WebBasedReportRequest.prototype.originLongitudeDecimcalDegrees = 0;

        /**
         * WebBasedReportRequest originHeightMeters.
         * @type {number|undefined}
         */
        WebBasedReportRequest.prototype.originHeightMeters = 0;

        /**
         * WebBasedReportRequest originTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        WebBasedReportRequest.prototype.originTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * WebBasedReportRequest requestee.
         * @type {string|undefined}
         */
        WebBasedReportRequest.prototype.requestee = "";

        /**
         * WebBasedReportRequest reportId.
         * @type {string|undefined}
         */
        WebBasedReportRequest.prototype.reportId = "";

        /**
         * WebBasedReportRequest deviceIdsApi900.
         * @type {Array.<string>|undefined}
         */
        WebBasedReportRequest.prototype.deviceIdsApi900 = $util.emptyArray;

        /**
         * WebBasedReportRequest performTimeSynch.
         * @type {boolean|undefined}
         */
        WebBasedReportRequest.prototype.performTimeSynch = false;

        /**
         * WebBasedReportRequest params.
         * @type {string|undefined}
         */
        WebBasedReportRequest.prototype.params = "";

        /**
         * Creates a new WebBasedReportRequest instance using the specified properties.
         * @param {protobuf.WebBasedReportRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.WebBasedReportRequest} WebBasedReportRequest instance
         */
        WebBasedReportRequest.create = function create(properties) {
            return new WebBasedReportRequest(properties);
        };

        /**
         * Encodes the specified WebBasedReportRequest message. Does not implicitly {@link protobuf.WebBasedReportRequest.verify|verify} messages.
         * @param {protobuf.WebBasedReportRequest$Properties} message WebBasedReportRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WebBasedReportRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.startTimestampSecondsUtc);
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint64(message.endTimestampSecondsUtc);
            if (message.deviceIds && message.deviceIds.length) {
                writer.uint32(/* id 4, wireType 2 =*/34).fork();
                for (var i = 0; i < message.deviceIds.length; ++i)
                    writer.uint64(message.deviceIds[i]);
                writer.ldelim();
            }
            if (message.plotTypes && message.plotTypes.length) {
                writer.uint32(/* id 5, wireType 2 =*/42).fork();
                for (var i = 0; i < message.plotTypes.length; ++i)
                    writer.uint32(message.plotTypes[i]);
                writer.ldelim();
            }
            if (message.originLatitudeDecimalDegrees != null && message.hasOwnProperty("originLatitudeDecimalDegrees"))
                writer.uint32(/* id 6, wireType 1 =*/49).double(message.originLatitudeDecimalDegrees);
            if (message.originLongitudeDecimcalDegrees != null && message.hasOwnProperty("originLongitudeDecimcalDegrees"))
                writer.uint32(/* id 7, wireType 1 =*/57).double(message.originLongitudeDecimcalDegrees);
            if (message.originHeightMeters != null && message.hasOwnProperty("originHeightMeters"))
                writer.uint32(/* id 8, wireType 1 =*/65).double(message.originHeightMeters);
            if (message.originTimestampSecondsUtc != null && message.hasOwnProperty("originTimestampSecondsUtc"))
                writer.uint32(/* id 9, wireType 0 =*/72).uint64(message.originTimestampSecondsUtc);
            if (message.requestee != null && message.hasOwnProperty("requestee"))
                writer.uint32(/* id 10, wireType 2 =*/82).string(message.requestee);
            if (message.reportId != null && message.hasOwnProperty("reportId"))
                writer.uint32(/* id 11, wireType 2 =*/90).string(message.reportId);
            if (message.deviceIdsApi900 && message.deviceIdsApi900.length)
                for (var i = 0; i < message.deviceIdsApi900.length; ++i)
                    writer.uint32(/* id 12, wireType 2 =*/98).string(message.deviceIdsApi900[i]);
            if (message.performTimeSynch != null && message.hasOwnProperty("performTimeSynch"))
                writer.uint32(/* id 13, wireType 0 =*/104).bool(message.performTimeSynch);
            if (message.params != null && message.hasOwnProperty("params"))
                writer.uint32(/* id 14, wireType 2 =*/114).string(message.params);
            return writer;
        };

        /**
         * Encodes the specified WebBasedReportRequest message, length delimited. Does not implicitly {@link protobuf.WebBasedReportRequest.verify|verify} messages.
         * @param {protobuf.WebBasedReportRequest$Properties} message WebBasedReportRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WebBasedReportRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a WebBasedReportRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.WebBasedReportRequest} WebBasedReportRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WebBasedReportRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.WebBasedReportRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.startTimestampSecondsUtc = reader.uint64();
                    break;
                case 3:
                    message.endTimestampSecondsUtc = reader.uint64();
                    break;
                case 4:
                    if (!(message.deviceIds && message.deviceIds.length))
                        message.deviceIds = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.deviceIds.push(reader.uint64());
                    } else
                        message.deviceIds.push(reader.uint64());
                    break;
                case 5:
                    if (!(message.plotTypes && message.plotTypes.length))
                        message.plotTypes = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.plotTypes.push(reader.uint32());
                    } else
                        message.plotTypes.push(reader.uint32());
                    break;
                case 6:
                    message.originLatitudeDecimalDegrees = reader.double();
                    break;
                case 7:
                    message.originLongitudeDecimcalDegrees = reader.double();
                    break;
                case 8:
                    message.originHeightMeters = reader.double();
                    break;
                case 9:
                    message.originTimestampSecondsUtc = reader.uint64();
                    break;
                case 10:
                    message.requestee = reader.string();
                    break;
                case 11:
                    message.reportId = reader.string();
                    break;
                case 12:
                    if (!(message.deviceIdsApi900 && message.deviceIdsApi900.length))
                        message.deviceIdsApi900 = [];
                    message.deviceIdsApi900.push(reader.string());
                    break;
                case 13:
                    message.performTimeSynch = reader.bool();
                    break;
                case 14:
                    message.params = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a WebBasedReportRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.WebBasedReportRequest} WebBasedReportRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WebBasedReportRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a WebBasedReportRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        WebBasedReportRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.startTimestampSecondsUtc != null)
                if (!$util.isInteger(message.startTimestampSecondsUtc) && !(message.startTimestampSecondsUtc && $util.isInteger(message.startTimestampSecondsUtc.low) && $util.isInteger(message.startTimestampSecondsUtc.high)))
                    return "startTimestampSecondsUtc: integer|Long expected";
            if (message.endTimestampSecondsUtc != null)
                if (!$util.isInteger(message.endTimestampSecondsUtc) && !(message.endTimestampSecondsUtc && $util.isInteger(message.endTimestampSecondsUtc.low) && $util.isInteger(message.endTimestampSecondsUtc.high)))
                    return "endTimestampSecondsUtc: integer|Long expected";
            if (message.deviceIds != null) {
                if (!Array.isArray(message.deviceIds))
                    return "deviceIds: array expected";
                for (var i = 0; i < message.deviceIds.length; ++i)
                    if (!$util.isInteger(message.deviceIds[i]) && !(message.deviceIds[i] && $util.isInteger(message.deviceIds[i].low) && $util.isInteger(message.deviceIds[i].high)))
                        return "deviceIds: integer|Long[] expected";
            }
            if (message.plotTypes != null) {
                if (!Array.isArray(message.plotTypes))
                    return "plotTypes: array expected";
                for (var i = 0; i < message.plotTypes.length; ++i)
                    switch (message.plotTypes[i]) {
                    default:
                        return "plotTypes: enum value[] expected";
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                        break;
                    }
            }
            if (message.originLatitudeDecimalDegrees != null)
                if (typeof message.originLatitudeDecimalDegrees !== "number")
                    return "originLatitudeDecimalDegrees: number expected";
            if (message.originLongitudeDecimcalDegrees != null)
                if (typeof message.originLongitudeDecimcalDegrees !== "number")
                    return "originLongitudeDecimcalDegrees: number expected";
            if (message.originHeightMeters != null)
                if (typeof message.originHeightMeters !== "number")
                    return "originHeightMeters: number expected";
            if (message.originTimestampSecondsUtc != null)
                if (!$util.isInteger(message.originTimestampSecondsUtc) && !(message.originTimestampSecondsUtc && $util.isInteger(message.originTimestampSecondsUtc.low) && $util.isInteger(message.originTimestampSecondsUtc.high)))
                    return "originTimestampSecondsUtc: integer|Long expected";
            if (message.requestee != null)
                if (!$util.isString(message.requestee))
                    return "requestee: string expected";
            if (message.reportId != null)
                if (!$util.isString(message.reportId))
                    return "reportId: string expected";
            if (message.deviceIdsApi900 != null) {
                if (!Array.isArray(message.deviceIdsApi900))
                    return "deviceIdsApi900: array expected";
                for (var i = 0; i < message.deviceIdsApi900.length; ++i)
                    if (!$util.isString(message.deviceIdsApi900[i]))
                        return "deviceIdsApi900: string[] expected";
            }
            if (message.performTimeSynch != null)
                if (typeof message.performTimeSynch !== "boolean")
                    return "performTimeSynch: boolean expected";
            if (message.params != null)
                if (!$util.isString(message.params))
                    return "params: string expected";
            return null;
        };

        /**
         * Creates a WebBasedReportRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.WebBasedReportRequest} WebBasedReportRequest
         */
        WebBasedReportRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.WebBasedReportRequest)
                return object;
            var message = new $root.protobuf.WebBasedReportRequest();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.startTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.startTimestampSecondsUtc = $util.Long.fromValue(object.startTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.startTimestampSecondsUtc === "string")
                    message.startTimestampSecondsUtc = parseInt(object.startTimestampSecondsUtc, 10);
                else if (typeof object.startTimestampSecondsUtc === "number")
                    message.startTimestampSecondsUtc = object.startTimestampSecondsUtc;
                else if (typeof object.startTimestampSecondsUtc === "object")
                    message.startTimestampSecondsUtc = new $util.LongBits(object.startTimestampSecondsUtc.low >>> 0, object.startTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.endTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.endTimestampSecondsUtc = $util.Long.fromValue(object.endTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.endTimestampSecondsUtc === "string")
                    message.endTimestampSecondsUtc = parseInt(object.endTimestampSecondsUtc, 10);
                else if (typeof object.endTimestampSecondsUtc === "number")
                    message.endTimestampSecondsUtc = object.endTimestampSecondsUtc;
                else if (typeof object.endTimestampSecondsUtc === "object")
                    message.endTimestampSecondsUtc = new $util.LongBits(object.endTimestampSecondsUtc.low >>> 0, object.endTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.deviceIds) {
                if (!Array.isArray(object.deviceIds))
                    throw TypeError(".protobuf.WebBasedReportRequest.deviceIds: array expected");
                message.deviceIds = [];
                for (var i = 0; i < object.deviceIds.length; ++i)
                    if ($util.Long)
                        (message.deviceIds[i] = $util.Long.fromValue(object.deviceIds[i])).unsigned = true;
                    else if (typeof object.deviceIds[i] === "string")
                        message.deviceIds[i] = parseInt(object.deviceIds[i], 10);
                    else if (typeof object.deviceIds[i] === "number")
                        message.deviceIds[i] = object.deviceIds[i];
                    else if (typeof object.deviceIds[i] === "object")
                        message.deviceIds[i] = new $util.LongBits(object.deviceIds[i].low >>> 0, object.deviceIds[i].high >>> 0).toNumber(true);
            }
            if (object.plotTypes) {
                if (!Array.isArray(object.plotTypes))
                    throw TypeError(".protobuf.WebBasedReportRequest.plotTypes: array expected");
                message.plotTypes = [];
                for (var i = 0; i < object.plotTypes.length; ++i)
                    switch (object.plotTypes[i]) {
                    default:
                    case "MICROPHONE_FFT":
                    case 0:
                        message.plotTypes[i] = 0;
                        break;
                    case "MICROPHONE_MULTIRES":
                    case 1:
                        message.plotTypes[i] = 1;
                        break;
                    case "BAROMETER_FFT":
                    case 2:
                        message.plotTypes[i] = 2;
                        break;
                    case "BAROMETER_MULTIRES":
                    case 3:
                        message.plotTypes[i] = 3;
                        break;
                    case "LATENCY":
                    case 4:
                        message.plotTypes[i] = 4;
                        break;
                    case "LATITUDE_PLOT":
                    case 5:
                        message.plotTypes[i] = 5;
                        break;
                    case "LONGITUDE_PLOT":
                    case 6:
                        message.plotTypes[i] = 6;
                        break;
                    case "ALTITUDE_PLOT":
                    case 7:
                        message.plotTypes[i] = 7;
                        break;
                    case "ACCURACY_PLOT":
                    case 8:
                        message.plotTypes[i] = 8;
                        break;
                    case "MAGNETOMETER_PLOT":
                    case 9:
                        message.plotTypes[i] = 9;
                        break;
                    case "GYROSCOPE_PLOT":
                    case 10:
                        message.plotTypes[i] = 10;
                        break;
                    case "LIGHT_PLOT":
                    case 11:
                        message.plotTypes[i] = 11;
                        break;
                    case "ACCELEROMETER_PLOT":
                    case 12:
                        message.plotTypes[i] = 12;
                        break;
                    case "MICROPHONE_FFT_LINEAR":
                    case 13:
                        message.plotTypes[i] = 13;
                        break;
                    case "MICROPHONE_FFT_LOG":
                    case 14:
                        message.plotTypes[i] = 14;
                        break;
                    case "AUDIO_PICKER":
                    case 15:
                        message.plotTypes[i] = 15;
                        break;
                    }
            }
            if (object.originLatitudeDecimalDegrees != null)
                message.originLatitudeDecimalDegrees = Number(object.originLatitudeDecimalDegrees);
            if (object.originLongitudeDecimcalDegrees != null)
                message.originLongitudeDecimcalDegrees = Number(object.originLongitudeDecimcalDegrees);
            if (object.originHeightMeters != null)
                message.originHeightMeters = Number(object.originHeightMeters);
            if (object.originTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.originTimestampSecondsUtc = $util.Long.fromValue(object.originTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.originTimestampSecondsUtc === "string")
                    message.originTimestampSecondsUtc = parseInt(object.originTimestampSecondsUtc, 10);
                else if (typeof object.originTimestampSecondsUtc === "number")
                    message.originTimestampSecondsUtc = object.originTimestampSecondsUtc;
                else if (typeof object.originTimestampSecondsUtc === "object")
                    message.originTimestampSecondsUtc = new $util.LongBits(object.originTimestampSecondsUtc.low >>> 0, object.originTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.requestee != null)
                message.requestee = String(object.requestee);
            if (object.reportId != null)
                message.reportId = String(object.reportId);
            if (object.deviceIdsApi900) {
                if (!Array.isArray(object.deviceIdsApi900))
                    throw TypeError(".protobuf.WebBasedReportRequest.deviceIdsApi900: array expected");
                message.deviceIdsApi900 = [];
                for (var i = 0; i < object.deviceIdsApi900.length; ++i)
                    message.deviceIdsApi900[i] = String(object.deviceIdsApi900[i]);
            }
            if (object.performTimeSynch != null)
                message.performTimeSynch = Boolean(object.performTimeSynch);
            if (object.params != null)
                message.params = String(object.params);
            return message;
        };

        /**
         * Creates a WebBasedReportRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.WebBasedReportRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.WebBasedReportRequest} WebBasedReportRequest
         */
        WebBasedReportRequest.from = WebBasedReportRequest.fromObject;

        /**
         * Creates a plain object from a WebBasedReportRequest message. Also converts values to other types if specified.
         * @param {protobuf.WebBasedReportRequest} message WebBasedReportRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        WebBasedReportRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.deviceIds = [];
                object.plotTypes = [];
                object.deviceIdsApi900 = [];
            }
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.startTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.startTimestampSecondsUtc = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.endTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.endTimestampSecondsUtc = options.longs === String ? "0" : 0;
                object.originLatitudeDecimalDegrees = 0;
                object.originLongitudeDecimcalDegrees = 0;
                object.originHeightMeters = 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.originTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.originTimestampSecondsUtc = options.longs === String ? "0" : 0;
                object.requestee = "";
                object.reportId = "";
                object.performTimeSynch = false;
                object.params = "";
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                if (typeof message.startTimestampSecondsUtc === "number")
                    object.startTimestampSecondsUtc = options.longs === String ? String(message.startTimestampSecondsUtc) : message.startTimestampSecondsUtc;
                else
                    object.startTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.startTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.startTimestampSecondsUtc.low >>> 0, message.startTimestampSecondsUtc.high >>> 0).toNumber(true) : message.startTimestampSecondsUtc;
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                if (typeof message.endTimestampSecondsUtc === "number")
                    object.endTimestampSecondsUtc = options.longs === String ? String(message.endTimestampSecondsUtc) : message.endTimestampSecondsUtc;
                else
                    object.endTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.endTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.endTimestampSecondsUtc.low >>> 0, message.endTimestampSecondsUtc.high >>> 0).toNumber(true) : message.endTimestampSecondsUtc;
            if (message.deviceIds && message.deviceIds.length) {
                object.deviceIds = [];
                for (var j = 0; j < message.deviceIds.length; ++j)
                    if (typeof message.deviceIds[j] === "number")
                        object.deviceIds[j] = options.longs === String ? String(message.deviceIds[j]) : message.deviceIds[j];
                    else
                        object.deviceIds[j] = options.longs === String ? $util.Long.prototype.toString.call(message.deviceIds[j]) : options.longs === Number ? new $util.LongBits(message.deviceIds[j].low >>> 0, message.deviceIds[j].high >>> 0).toNumber(true) : message.deviceIds[j];
            }
            if (message.plotTypes && message.plotTypes.length) {
                object.plotTypes = [];
                for (var j = 0; j < message.plotTypes.length; ++j)
                    object.plotTypes[j] = options.enums === String ? $root.protobuf.PlotType[message.plotTypes[j]] : message.plotTypes[j];
            }
            if (message.originLatitudeDecimalDegrees != null && message.hasOwnProperty("originLatitudeDecimalDegrees"))
                object.originLatitudeDecimalDegrees = message.originLatitudeDecimalDegrees;
            if (message.originLongitudeDecimcalDegrees != null && message.hasOwnProperty("originLongitudeDecimcalDegrees"))
                object.originLongitudeDecimcalDegrees = message.originLongitudeDecimcalDegrees;
            if (message.originHeightMeters != null && message.hasOwnProperty("originHeightMeters"))
                object.originHeightMeters = message.originHeightMeters;
            if (message.originTimestampSecondsUtc != null && message.hasOwnProperty("originTimestampSecondsUtc"))
                if (typeof message.originTimestampSecondsUtc === "number")
                    object.originTimestampSecondsUtc = options.longs === String ? String(message.originTimestampSecondsUtc) : message.originTimestampSecondsUtc;
                else
                    object.originTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.originTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.originTimestampSecondsUtc.low >>> 0, message.originTimestampSecondsUtc.high >>> 0).toNumber(true) : message.originTimestampSecondsUtc;
            if (message.requestee != null && message.hasOwnProperty("requestee"))
                object.requestee = message.requestee;
            if (message.reportId != null && message.hasOwnProperty("reportId"))
                object.reportId = message.reportId;
            if (message.deviceIdsApi900 && message.deviceIdsApi900.length) {
                object.deviceIdsApi900 = [];
                for (var j = 0; j < message.deviceIdsApi900.length; ++j)
                    object.deviceIdsApi900[j] = message.deviceIdsApi900[j];
            }
            if (message.performTimeSynch != null && message.hasOwnProperty("performTimeSynch"))
                object.performTimeSynch = message.performTimeSynch;
            if (message.params != null && message.hasOwnProperty("params"))
                object.params = message.params;
            return object;
        };

        /**
         * Creates a plain object from this WebBasedReportRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        WebBasedReportRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this WebBasedReportRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        WebBasedReportRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return WebBasedReportRequest;
    })();

    protobuf.WebBasedReportResponse = (function() {

        /**
         * Properties of a WebBasedReportResponse.
         * @typedef protobuf.WebBasedReportResponse$Properties
         * @type {Object}
         * @property {number|Long} [uuid] WebBasedReportResponse uuid.
         * @property {string} [reportId] WebBasedReportResponse reportId.
         * @property {protobuf.Response$Properties} [response] WebBasedReportResponse response.
         */

        /**
         * Constructs a new WebBasedReportResponse.
         * @exports protobuf.WebBasedReportResponse
         * @constructor
         * @param {protobuf.WebBasedReportResponse$Properties=} [properties] Properties to set
         */
        function WebBasedReportResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * WebBasedReportResponse uuid.
         * @type {number|Long|undefined}
         */
        WebBasedReportResponse.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * WebBasedReportResponse reportId.
         * @type {string|undefined}
         */
        WebBasedReportResponse.prototype.reportId = "";

        /**
         * WebBasedReportResponse response.
         * @type {protobuf.Response$Properties|undefined}
         */
        WebBasedReportResponse.prototype.response = null;

        /**
         * Creates a new WebBasedReportResponse instance using the specified properties.
         * @param {protobuf.WebBasedReportResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.WebBasedReportResponse} WebBasedReportResponse instance
         */
        WebBasedReportResponse.create = function create(properties) {
            return new WebBasedReportResponse(properties);
        };

        /**
         * Encodes the specified WebBasedReportResponse message. Does not implicitly {@link protobuf.WebBasedReportResponse.verify|verify} messages.
         * @param {protobuf.WebBasedReportResponse$Properties} message WebBasedReportResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WebBasedReportResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.reportId != null && message.hasOwnProperty("reportId"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.reportId);
            if (message.response && message.hasOwnProperty("response"))
                $root.protobuf.Response.encode(message.response, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified WebBasedReportResponse message, length delimited. Does not implicitly {@link protobuf.WebBasedReportResponse.verify|verify} messages.
         * @param {protobuf.WebBasedReportResponse$Properties} message WebBasedReportResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WebBasedReportResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a WebBasedReportResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.WebBasedReportResponse} WebBasedReportResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WebBasedReportResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.WebBasedReportResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.reportId = reader.string();
                    break;
                case 3:
                    message.response = $root.protobuf.Response.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a WebBasedReportResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.WebBasedReportResponse} WebBasedReportResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WebBasedReportResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a WebBasedReportResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        WebBasedReportResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.reportId != null)
                if (!$util.isString(message.reportId))
                    return "reportId: string expected";
            if (message.response != null) {
                var error = $root.protobuf.Response.verify(message.response);
                if (error)
                    return "response." + error;
            }
            return null;
        };

        /**
         * Creates a WebBasedReportResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.WebBasedReportResponse} WebBasedReportResponse
         */
        WebBasedReportResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.WebBasedReportResponse)
                return object;
            var message = new $root.protobuf.WebBasedReportResponse();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.reportId != null)
                message.reportId = String(object.reportId);
            if (object.response != null) {
                if (typeof object.response !== "object")
                    throw TypeError(".protobuf.WebBasedReportResponse.response: object expected");
                message.response = $root.protobuf.Response.fromObject(object.response);
            }
            return message;
        };

        /**
         * Creates a WebBasedReportResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.WebBasedReportResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.WebBasedReportResponse} WebBasedReportResponse
         */
        WebBasedReportResponse.from = WebBasedReportResponse.fromObject;

        /**
         * Creates a plain object from a WebBasedReportResponse message. Also converts values to other types if specified.
         * @param {protobuf.WebBasedReportResponse} message WebBasedReportResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        WebBasedReportResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.reportId = "";
                object.response = null;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.reportId != null && message.hasOwnProperty("reportId"))
                object.reportId = message.reportId;
            if (message.response != null && message.hasOwnProperty("response"))
                object.response = $root.protobuf.Response.toObject(message.response, options);
            return object;
        };

        /**
         * Creates a plain object from this WebBasedReportResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        WebBasedReportResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this WebBasedReportResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        WebBasedReportResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return WebBasedReportResponse;
    })();

    protobuf.ProgressUpdate = (function() {

        /**
         * Properties of a ProgressUpdate.
         * @typedef protobuf.ProgressUpdate$Properties
         * @type {Object}
         * @property {number|Long} [uuid] ProgressUpdate uuid.
         * @property {number} [currentStage] ProgressUpdate currentStage.
         * @property {number} [totalStages] ProgressUpdate totalStages.
         * @property {string} [messageStage] ProgressUpdate messageStage.
         * @property {number} [currentItem] ProgressUpdate currentItem.
         * @property {number} [totalItems] ProgressUpdate totalItems.
         * @property {string} [messageItem] ProgressUpdate messageItem.
         * @property {string} [meta] ProgressUpdate meta.
         */

        /**
         * Constructs a new ProgressUpdate.
         * @exports protobuf.ProgressUpdate
         * @constructor
         * @param {protobuf.ProgressUpdate$Properties=} [properties] Properties to set
         */
        function ProgressUpdate(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * ProgressUpdate uuid.
         * @type {number|Long|undefined}
         */
        ProgressUpdate.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * ProgressUpdate currentStage.
         * @type {number|undefined}
         */
        ProgressUpdate.prototype.currentStage = 0;

        /**
         * ProgressUpdate totalStages.
         * @type {number|undefined}
         */
        ProgressUpdate.prototype.totalStages = 0;

        /**
         * ProgressUpdate messageStage.
         * @type {string|undefined}
         */
        ProgressUpdate.prototype.messageStage = "";

        /**
         * ProgressUpdate currentItem.
         * @type {number|undefined}
         */
        ProgressUpdate.prototype.currentItem = 0;

        /**
         * ProgressUpdate totalItems.
         * @type {number|undefined}
         */
        ProgressUpdate.prototype.totalItems = 0;

        /**
         * ProgressUpdate messageItem.
         * @type {string|undefined}
         */
        ProgressUpdate.prototype.messageItem = "";

        /**
         * ProgressUpdate meta.
         * @type {string|undefined}
         */
        ProgressUpdate.prototype.meta = "";

        /**
         * Creates a new ProgressUpdate instance using the specified properties.
         * @param {protobuf.ProgressUpdate$Properties=} [properties] Properties to set
         * @returns {protobuf.ProgressUpdate} ProgressUpdate instance
         */
        ProgressUpdate.create = function create(properties) {
            return new ProgressUpdate(properties);
        };

        /**
         * Encodes the specified ProgressUpdate message. Does not implicitly {@link protobuf.ProgressUpdate.verify|verify} messages.
         * @param {protobuf.ProgressUpdate$Properties} message ProgressUpdate message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ProgressUpdate.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.currentStage != null && message.hasOwnProperty("currentStage"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.currentStage);
            if (message.totalStages != null && message.hasOwnProperty("totalStages"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.totalStages);
            if (message.messageStage != null && message.hasOwnProperty("messageStage"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.messageStage);
            if (message.currentItem != null && message.hasOwnProperty("currentItem"))
                writer.uint32(/* id 5, wireType 0 =*/40).uint32(message.currentItem);
            if (message.totalItems != null && message.hasOwnProperty("totalItems"))
                writer.uint32(/* id 6, wireType 0 =*/48).uint32(message.totalItems);
            if (message.messageItem != null && message.hasOwnProperty("messageItem"))
                writer.uint32(/* id 7, wireType 2 =*/58).string(message.messageItem);
            if (message.meta != null && message.hasOwnProperty("meta"))
                writer.uint32(/* id 8, wireType 2 =*/66).string(message.meta);
            return writer;
        };

        /**
         * Encodes the specified ProgressUpdate message, length delimited. Does not implicitly {@link protobuf.ProgressUpdate.verify|verify} messages.
         * @param {protobuf.ProgressUpdate$Properties} message ProgressUpdate message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ProgressUpdate.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a ProgressUpdate message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.ProgressUpdate} ProgressUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ProgressUpdate.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.ProgressUpdate();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.currentStage = reader.uint32();
                    break;
                case 3:
                    message.totalStages = reader.uint32();
                    break;
                case 4:
                    message.messageStage = reader.string();
                    break;
                case 5:
                    message.currentItem = reader.uint32();
                    break;
                case 6:
                    message.totalItems = reader.uint32();
                    break;
                case 7:
                    message.messageItem = reader.string();
                    break;
                case 8:
                    message.meta = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a ProgressUpdate message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.ProgressUpdate} ProgressUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ProgressUpdate.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a ProgressUpdate message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        ProgressUpdate.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.currentStage != null)
                if (!$util.isInteger(message.currentStage))
                    return "currentStage: integer expected";
            if (message.totalStages != null)
                if (!$util.isInteger(message.totalStages))
                    return "totalStages: integer expected";
            if (message.messageStage != null)
                if (!$util.isString(message.messageStage))
                    return "messageStage: string expected";
            if (message.currentItem != null)
                if (!$util.isInteger(message.currentItem))
                    return "currentItem: integer expected";
            if (message.totalItems != null)
                if (!$util.isInteger(message.totalItems))
                    return "totalItems: integer expected";
            if (message.messageItem != null)
                if (!$util.isString(message.messageItem))
                    return "messageItem: string expected";
            if (message.meta != null)
                if (!$util.isString(message.meta))
                    return "meta: string expected";
            return null;
        };

        /**
         * Creates a ProgressUpdate message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.ProgressUpdate} ProgressUpdate
         */
        ProgressUpdate.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.ProgressUpdate)
                return object;
            var message = new $root.protobuf.ProgressUpdate();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.currentStage != null)
                message.currentStage = object.currentStage >>> 0;
            if (object.totalStages != null)
                message.totalStages = object.totalStages >>> 0;
            if (object.messageStage != null)
                message.messageStage = String(object.messageStage);
            if (object.currentItem != null)
                message.currentItem = object.currentItem >>> 0;
            if (object.totalItems != null)
                message.totalItems = object.totalItems >>> 0;
            if (object.messageItem != null)
                message.messageItem = String(object.messageItem);
            if (object.meta != null)
                message.meta = String(object.meta);
            return message;
        };

        /**
         * Creates a ProgressUpdate message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.ProgressUpdate.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.ProgressUpdate} ProgressUpdate
         */
        ProgressUpdate.from = ProgressUpdate.fromObject;

        /**
         * Creates a plain object from a ProgressUpdate message. Also converts values to other types if specified.
         * @param {protobuf.ProgressUpdate} message ProgressUpdate
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ProgressUpdate.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.currentStage = 0;
                object.totalStages = 0;
                object.messageStage = "";
                object.currentItem = 0;
                object.totalItems = 0;
                object.messageItem = "";
                object.meta = "";
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.currentStage != null && message.hasOwnProperty("currentStage"))
                object.currentStage = message.currentStage;
            if (message.totalStages != null && message.hasOwnProperty("totalStages"))
                object.totalStages = message.totalStages;
            if (message.messageStage != null && message.hasOwnProperty("messageStage"))
                object.messageStage = message.messageStage;
            if (message.currentItem != null && message.hasOwnProperty("currentItem"))
                object.currentItem = message.currentItem;
            if (message.totalItems != null && message.hasOwnProperty("totalItems"))
                object.totalItems = message.totalItems;
            if (message.messageItem != null && message.hasOwnProperty("messageItem"))
                object.messageItem = message.messageItem;
            if (message.meta != null && message.hasOwnProperty("meta"))
                object.meta = message.meta;
            return object;
        };

        /**
         * Creates a plain object from this ProgressUpdate message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ProgressUpdate.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this ProgressUpdate to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        ProgressUpdate.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return ProgressUpdate;
    })();

    protobuf.DataRequest = (function() {

        /**
         * Properties of a DataRequest.
         * @typedef protobuf.DataRequest$Properties
         * @type {Object}
         * @property {number|Long} [uuid] DataRequest uuid.
         * @property {number|Long} [startTimestampSecondsUtc] DataRequest startTimestampSecondsUtc.
         * @property {number|Long} [endTimestampSecondsUtc] DataRequest endTimestampSecondsUtc.
         * @property {Array.<number|Long>} [deviceIds] DataRequest deviceIds.
         * @property {Array.<protobuf.DataType>} [dataTypes] DataRequest dataTypes.
         */

        /**
         * Constructs a new DataRequest.
         * @exports protobuf.DataRequest
         * @constructor
         * @param {protobuf.DataRequest$Properties=} [properties] Properties to set
         */
        function DataRequest(properties) {
            this.deviceIds = [];
            this.dataTypes = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * DataRequest uuid.
         * @type {number|Long|undefined}
         */
        DataRequest.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * DataRequest startTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        DataRequest.prototype.startTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * DataRequest endTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        DataRequest.prototype.endTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * DataRequest deviceIds.
         * @type {Array.<number|Long>|undefined}
         */
        DataRequest.prototype.deviceIds = $util.emptyArray;

        /**
         * DataRequest dataTypes.
         * @type {Array.<protobuf.DataType>|undefined}
         */
        DataRequest.prototype.dataTypes = $util.emptyArray;

        /**
         * Creates a new DataRequest instance using the specified properties.
         * @param {protobuf.DataRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.DataRequest} DataRequest instance
         */
        DataRequest.create = function create(properties) {
            return new DataRequest(properties);
        };

        /**
         * Encodes the specified DataRequest message. Does not implicitly {@link protobuf.DataRequest.verify|verify} messages.
         * @param {protobuf.DataRequest$Properties} message DataRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DataRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.startTimestampSecondsUtc);
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint64(message.endTimestampSecondsUtc);
            if (message.deviceIds && message.deviceIds.length) {
                writer.uint32(/* id 4, wireType 2 =*/34).fork();
                for (var i = 0; i < message.deviceIds.length; ++i)
                    writer.uint64(message.deviceIds[i]);
                writer.ldelim();
            }
            if (message.dataTypes && message.dataTypes.length) {
                writer.uint32(/* id 5, wireType 2 =*/42).fork();
                for (var i = 0; i < message.dataTypes.length; ++i)
                    writer.uint32(message.dataTypes[i]);
                writer.ldelim();
            }
            return writer;
        };

        /**
         * Encodes the specified DataRequest message, length delimited. Does not implicitly {@link protobuf.DataRequest.verify|verify} messages.
         * @param {protobuf.DataRequest$Properties} message DataRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DataRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DataRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.DataRequest} DataRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DataRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.DataRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.startTimestampSecondsUtc = reader.uint64();
                    break;
                case 3:
                    message.endTimestampSecondsUtc = reader.uint64();
                    break;
                case 4:
                    if (!(message.deviceIds && message.deviceIds.length))
                        message.deviceIds = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.deviceIds.push(reader.uint64());
                    } else
                        message.deviceIds.push(reader.uint64());
                    break;
                case 5:
                    if (!(message.dataTypes && message.dataTypes.length))
                        message.dataTypes = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.dataTypes.push(reader.uint32());
                    } else
                        message.dataTypes.push(reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DataRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.DataRequest} DataRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DataRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DataRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        DataRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.startTimestampSecondsUtc != null)
                if (!$util.isInteger(message.startTimestampSecondsUtc) && !(message.startTimestampSecondsUtc && $util.isInteger(message.startTimestampSecondsUtc.low) && $util.isInteger(message.startTimestampSecondsUtc.high)))
                    return "startTimestampSecondsUtc: integer|Long expected";
            if (message.endTimestampSecondsUtc != null)
                if (!$util.isInteger(message.endTimestampSecondsUtc) && !(message.endTimestampSecondsUtc && $util.isInteger(message.endTimestampSecondsUtc.low) && $util.isInteger(message.endTimestampSecondsUtc.high)))
                    return "endTimestampSecondsUtc: integer|Long expected";
            if (message.deviceIds != null) {
                if (!Array.isArray(message.deviceIds))
                    return "deviceIds: array expected";
                for (var i = 0; i < message.deviceIds.length; ++i)
                    if (!$util.isInteger(message.deviceIds[i]) && !(message.deviceIds[i] && $util.isInteger(message.deviceIds[i].low) && $util.isInteger(message.deviceIds[i].high)))
                        return "deviceIds: integer|Long[] expected";
            }
            if (message.dataTypes != null) {
                if (!Array.isArray(message.dataTypes))
                    return "dataTypes: array expected";
                for (var i = 0; i < message.dataTypes.length; ++i)
                    switch (message.dataTypes[i]) {
                    default:
                        return "dataTypes: enum value[] expected";
                    case 0:
                    case 1:
                        break;
                    }
            }
            return null;
        };

        /**
         * Creates a DataRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DataRequest} DataRequest
         */
        DataRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.DataRequest)
                return object;
            var message = new $root.protobuf.DataRequest();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.startTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.startTimestampSecondsUtc = $util.Long.fromValue(object.startTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.startTimestampSecondsUtc === "string")
                    message.startTimestampSecondsUtc = parseInt(object.startTimestampSecondsUtc, 10);
                else if (typeof object.startTimestampSecondsUtc === "number")
                    message.startTimestampSecondsUtc = object.startTimestampSecondsUtc;
                else if (typeof object.startTimestampSecondsUtc === "object")
                    message.startTimestampSecondsUtc = new $util.LongBits(object.startTimestampSecondsUtc.low >>> 0, object.startTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.endTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.endTimestampSecondsUtc = $util.Long.fromValue(object.endTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.endTimestampSecondsUtc === "string")
                    message.endTimestampSecondsUtc = parseInt(object.endTimestampSecondsUtc, 10);
                else if (typeof object.endTimestampSecondsUtc === "number")
                    message.endTimestampSecondsUtc = object.endTimestampSecondsUtc;
                else if (typeof object.endTimestampSecondsUtc === "object")
                    message.endTimestampSecondsUtc = new $util.LongBits(object.endTimestampSecondsUtc.low >>> 0, object.endTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.deviceIds) {
                if (!Array.isArray(object.deviceIds))
                    throw TypeError(".protobuf.DataRequest.deviceIds: array expected");
                message.deviceIds = [];
                for (var i = 0; i < object.deviceIds.length; ++i)
                    if ($util.Long)
                        (message.deviceIds[i] = $util.Long.fromValue(object.deviceIds[i])).unsigned = true;
                    else if (typeof object.deviceIds[i] === "string")
                        message.deviceIds[i] = parseInt(object.deviceIds[i], 10);
                    else if (typeof object.deviceIds[i] === "number")
                        message.deviceIds[i] = object.deviceIds[i];
                    else if (typeof object.deviceIds[i] === "object")
                        message.deviceIds[i] = new $util.LongBits(object.deviceIds[i].low >>> 0, object.deviceIds[i].high >>> 0).toNumber(true);
            }
            if (object.dataTypes) {
                if (!Array.isArray(object.dataTypes))
                    throw TypeError(".protobuf.DataRequest.dataTypes: array expected");
                message.dataTypes = [];
                for (var i = 0; i < object.dataTypes.length; ++i)
                    switch (object.dataTypes[i]) {
                    default:
                    case "WAVE_MIC":
                    case 0:
                        message.dataTypes[i] = 0;
                        break;
                    case "WAVE_BAR":
                    case 1:
                        message.dataTypes[i] = 1;
                        break;
                    }
            }
            return message;
        };

        /**
         * Creates a DataRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.DataRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DataRequest} DataRequest
         */
        DataRequest.from = DataRequest.fromObject;

        /**
         * Creates a plain object from a DataRequest message. Also converts values to other types if specified.
         * @param {protobuf.DataRequest} message DataRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DataRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.deviceIds = [];
                object.dataTypes = [];
            }
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.startTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.startTimestampSecondsUtc = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.endTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.endTimestampSecondsUtc = options.longs === String ? "0" : 0;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                if (typeof message.startTimestampSecondsUtc === "number")
                    object.startTimestampSecondsUtc = options.longs === String ? String(message.startTimestampSecondsUtc) : message.startTimestampSecondsUtc;
                else
                    object.startTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.startTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.startTimestampSecondsUtc.low >>> 0, message.startTimestampSecondsUtc.high >>> 0).toNumber(true) : message.startTimestampSecondsUtc;
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                if (typeof message.endTimestampSecondsUtc === "number")
                    object.endTimestampSecondsUtc = options.longs === String ? String(message.endTimestampSecondsUtc) : message.endTimestampSecondsUtc;
                else
                    object.endTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.endTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.endTimestampSecondsUtc.low >>> 0, message.endTimestampSecondsUtc.high >>> 0).toNumber(true) : message.endTimestampSecondsUtc;
            if (message.deviceIds && message.deviceIds.length) {
                object.deviceIds = [];
                for (var j = 0; j < message.deviceIds.length; ++j)
                    if (typeof message.deviceIds[j] === "number")
                        object.deviceIds[j] = options.longs === String ? String(message.deviceIds[j]) : message.deviceIds[j];
                    else
                        object.deviceIds[j] = options.longs === String ? $util.Long.prototype.toString.call(message.deviceIds[j]) : options.longs === Number ? new $util.LongBits(message.deviceIds[j].low >>> 0, message.deviceIds[j].high >>> 0).toNumber(true) : message.deviceIds[j];
            }
            if (message.dataTypes && message.dataTypes.length) {
                object.dataTypes = [];
                for (var j = 0; j < message.dataTypes.length; ++j)
                    object.dataTypes[j] = options.enums === String ? $root.protobuf.DataType[message.dataTypes[j]] : message.dataTypes[j];
            }
            return object;
        };

        /**
         * Creates a plain object from this DataRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DataRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this DataRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        DataRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DataRequest;
    })();

    protobuf.DataResponse = (function() {

        /**
         * Properties of a DataResponse.
         * @typedef protobuf.DataResponse$Properties
         * @type {Object}
         * @property {number|Long} [uuid] DataResponse uuid.
         * @property {string} [dataKey] DataResponse dataKey.
         * @property {protobuf.Response$Properties} [response] DataResponse response.
         */

        /**
         * Constructs a new DataResponse.
         * @exports protobuf.DataResponse
         * @constructor
         * @param {protobuf.DataResponse$Properties=} [properties] Properties to set
         */
        function DataResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * DataResponse uuid.
         * @type {number|Long|undefined}
         */
        DataResponse.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * DataResponse dataKey.
         * @type {string|undefined}
         */
        DataResponse.prototype.dataKey = "";

        /**
         * DataResponse response.
         * @type {protobuf.Response$Properties|undefined}
         */
        DataResponse.prototype.response = null;

        /**
         * Creates a new DataResponse instance using the specified properties.
         * @param {protobuf.DataResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.DataResponse} DataResponse instance
         */
        DataResponse.create = function create(properties) {
            return new DataResponse(properties);
        };

        /**
         * Encodes the specified DataResponse message. Does not implicitly {@link protobuf.DataResponse.verify|verify} messages.
         * @param {protobuf.DataResponse$Properties} message DataResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DataResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.dataKey);
            if (message.response && message.hasOwnProperty("response"))
                $root.protobuf.Response.encode(message.response, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified DataResponse message, length delimited. Does not implicitly {@link protobuf.DataResponse.verify|verify} messages.
         * @param {protobuf.DataResponse$Properties} message DataResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DataResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DataResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.DataResponse} DataResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DataResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.DataResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.dataKey = reader.string();
                    break;
                case 3:
                    message.response = $root.protobuf.Response.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DataResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.DataResponse} DataResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DataResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DataResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        DataResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.dataKey != null)
                if (!$util.isString(message.dataKey))
                    return "dataKey: string expected";
            if (message.response != null) {
                var error = $root.protobuf.Response.verify(message.response);
                if (error)
                    return "response." + error;
            }
            return null;
        };

        /**
         * Creates a DataResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DataResponse} DataResponse
         */
        DataResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.DataResponse)
                return object;
            var message = new $root.protobuf.DataResponse();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.dataKey != null)
                message.dataKey = String(object.dataKey);
            if (object.response != null) {
                if (typeof object.response !== "object")
                    throw TypeError(".protobuf.DataResponse.response: object expected");
                message.response = $root.protobuf.Response.fromObject(object.response);
            }
            return message;
        };

        /**
         * Creates a DataResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.DataResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DataResponse} DataResponse
         */
        DataResponse.from = DataResponse.fromObject;

        /**
         * Creates a plain object from a DataResponse message. Also converts values to other types if specified.
         * @param {protobuf.DataResponse} message DataResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DataResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.dataKey = "";
                object.response = null;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                object.dataKey = message.dataKey;
            if (message.response != null && message.hasOwnProperty("response"))
                object.response = $root.protobuf.Response.toObject(message.response, options);
            return object;
        };

        /**
         * Creates a plain object from this DataResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DataResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this DataResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        DataResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DataResponse;
    })();

    protobuf.RealTimePlotRequest = (function() {

        /**
         * Properties of a RealTimePlotRequest.
         * @typedef protobuf.RealTimePlotRequest$Properties
         * @type {Object}
         * @property {number|Long} [uuid] RealTimePlotRequest uuid.
         * @property {number|Long} [deviceId] RealTimePlotRequest deviceId.
         * @property {protobuf.PlotType} [plotType] RealTimePlotRequest plotType.
         * @property {number|Long} [secondsBeforeNow] RealTimePlotRequest secondsBeforeNow.
         */

        /**
         * Constructs a new RealTimePlotRequest.
         * @exports protobuf.RealTimePlotRequest
         * @constructor
         * @param {protobuf.RealTimePlotRequest$Properties=} [properties] Properties to set
         */
        function RealTimePlotRequest(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RealTimePlotRequest uuid.
         * @type {number|Long|undefined}
         */
        RealTimePlotRequest.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RealTimePlotRequest deviceId.
         * @type {number|Long|undefined}
         */
        RealTimePlotRequest.prototype.deviceId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RealTimePlotRequest plotType.
         * @type {protobuf.PlotType|undefined}
         */
        RealTimePlotRequest.prototype.plotType = 0;

        /**
         * RealTimePlotRequest secondsBeforeNow.
         * @type {number|Long|undefined}
         */
        RealTimePlotRequest.prototype.secondsBeforeNow = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * Creates a new RealTimePlotRequest instance using the specified properties.
         * @param {protobuf.RealTimePlotRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.RealTimePlotRequest} RealTimePlotRequest instance
         */
        RealTimePlotRequest.create = function create(properties) {
            return new RealTimePlotRequest(properties);
        };

        /**
         * Encodes the specified RealTimePlotRequest message. Does not implicitly {@link protobuf.RealTimePlotRequest.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequest$Properties} message RealTimePlotRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.deviceId != null && message.hasOwnProperty("deviceId"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.deviceId);
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.plotType);
            if (message.secondsBeforeNow != null && message.hasOwnProperty("secondsBeforeNow"))
                writer.uint32(/* id 4, wireType 0 =*/32).uint64(message.secondsBeforeNow);
            return writer;
        };

        /**
         * Encodes the specified RealTimePlotRequest message, length delimited. Does not implicitly {@link protobuf.RealTimePlotRequest.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequest$Properties} message RealTimePlotRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RealTimePlotRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RealTimePlotRequest} RealTimePlotRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RealTimePlotRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.deviceId = reader.uint64();
                    break;
                case 3:
                    message.plotType = reader.uint32();
                    break;
                case 4:
                    message.secondsBeforeNow = reader.uint64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RealTimePlotRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RealTimePlotRequest} RealTimePlotRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RealTimePlotRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RealTimePlotRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.deviceId != null)
                if (!$util.isInteger(message.deviceId) && !(message.deviceId && $util.isInteger(message.deviceId.low) && $util.isInteger(message.deviceId.high)))
                    return "deviceId: integer|Long expected";
            if (message.plotType != null)
                switch (message.plotType) {
                default:
                    return "plotType: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    break;
                }
            if (message.secondsBeforeNow != null)
                if (!$util.isInteger(message.secondsBeforeNow) && !(message.secondsBeforeNow && $util.isInteger(message.secondsBeforeNow.low) && $util.isInteger(message.secondsBeforeNow.high)))
                    return "secondsBeforeNow: integer|Long expected";
            return null;
        };

        /**
         * Creates a RealTimePlotRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequest} RealTimePlotRequest
         */
        RealTimePlotRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RealTimePlotRequest)
                return object;
            var message = new $root.protobuf.RealTimePlotRequest();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.deviceId != null)
                if ($util.Long)
                    (message.deviceId = $util.Long.fromValue(object.deviceId)).unsigned = true;
                else if (typeof object.deviceId === "string")
                    message.deviceId = parseInt(object.deviceId, 10);
                else if (typeof object.deviceId === "number")
                    message.deviceId = object.deviceId;
                else if (typeof object.deviceId === "object")
                    message.deviceId = new $util.LongBits(object.deviceId.low >>> 0, object.deviceId.high >>> 0).toNumber(true);
            switch (object.plotType) {
            case "MICROPHONE_FFT":
            case 0:
                message.plotType = 0;
                break;
            case "MICROPHONE_MULTIRES":
            case 1:
                message.plotType = 1;
                break;
            case "BAROMETER_FFT":
            case 2:
                message.plotType = 2;
                break;
            case "BAROMETER_MULTIRES":
            case 3:
                message.plotType = 3;
                break;
            case "LATENCY":
            case 4:
                message.plotType = 4;
                break;
            case "LATITUDE_PLOT":
            case 5:
                message.plotType = 5;
                break;
            case "LONGITUDE_PLOT":
            case 6:
                message.plotType = 6;
                break;
            case "ALTITUDE_PLOT":
            case 7:
                message.plotType = 7;
                break;
            case "ACCURACY_PLOT":
            case 8:
                message.plotType = 8;
                break;
            case "MAGNETOMETER_PLOT":
            case 9:
                message.plotType = 9;
                break;
            case "GYROSCOPE_PLOT":
            case 10:
                message.plotType = 10;
                break;
            case "LIGHT_PLOT":
            case 11:
                message.plotType = 11;
                break;
            case "ACCELEROMETER_PLOT":
            case 12:
                message.plotType = 12;
                break;
            case "MICROPHONE_FFT_LINEAR":
            case 13:
                message.plotType = 13;
                break;
            case "MICROPHONE_FFT_LOG":
            case 14:
                message.plotType = 14;
                break;
            case "AUDIO_PICKER":
            case 15:
                message.plotType = 15;
                break;
            }
            if (object.secondsBeforeNow != null)
                if ($util.Long)
                    (message.secondsBeforeNow = $util.Long.fromValue(object.secondsBeforeNow)).unsigned = true;
                else if (typeof object.secondsBeforeNow === "string")
                    message.secondsBeforeNow = parseInt(object.secondsBeforeNow, 10);
                else if (typeof object.secondsBeforeNow === "number")
                    message.secondsBeforeNow = object.secondsBeforeNow;
                else if (typeof object.secondsBeforeNow === "object")
                    message.secondsBeforeNow = new $util.LongBits(object.secondsBeforeNow.low >>> 0, object.secondsBeforeNow.high >>> 0).toNumber(true);
            return message;
        };

        /**
         * Creates a RealTimePlotRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RealTimePlotRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequest} RealTimePlotRequest
         */
        RealTimePlotRequest.from = RealTimePlotRequest.fromObject;

        /**
         * Creates a plain object from a RealTimePlotRequest message. Also converts values to other types if specified.
         * @param {protobuf.RealTimePlotRequest} message RealTimePlotRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.deviceId = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.deviceId = options.longs === String ? "0" : 0;
                object.plotType = options.enums === String ? "MICROPHONE_FFT" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.secondsBeforeNow = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.secondsBeforeNow = options.longs === String ? "0" : 0;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.deviceId != null && message.hasOwnProperty("deviceId"))
                if (typeof message.deviceId === "number")
                    object.deviceId = options.longs === String ? String(message.deviceId) : message.deviceId;
                else
                    object.deviceId = options.longs === String ? $util.Long.prototype.toString.call(message.deviceId) : options.longs === Number ? new $util.LongBits(message.deviceId.low >>> 0, message.deviceId.high >>> 0).toNumber(true) : message.deviceId;
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                object.plotType = options.enums === String ? $root.protobuf.PlotType[message.plotType] : message.plotType;
            if (message.secondsBeforeNow != null && message.hasOwnProperty("secondsBeforeNow"))
                if (typeof message.secondsBeforeNow === "number")
                    object.secondsBeforeNow = options.longs === String ? String(message.secondsBeforeNow) : message.secondsBeforeNow;
                else
                    object.secondsBeforeNow = options.longs === String ? $util.Long.prototype.toString.call(message.secondsBeforeNow) : options.longs === Number ? new $util.LongBits(message.secondsBeforeNow.low >>> 0, message.secondsBeforeNow.high >>> 0).toNumber(true) : message.secondsBeforeNow;
            return object;
        };

        /**
         * Creates a plain object from this RealTimePlotRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RealTimePlotRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RealTimePlotRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RealTimePlotRequest;
    })();

    protobuf.RealTimePlotResponse = (function() {

        /**
         * Properties of a RealTimePlotResponse.
         * @typedef protobuf.RealTimePlotResponse$Properties
         * @type {Object}
         * @property {number|Long} [uuid] RealTimePlotResponse uuid.
         * @property {number|Long} [deviceId] RealTimePlotResponse deviceId.
         * @property {protobuf.PlotType} [plotType] RealTimePlotResponse plotType.
         * @property {protobuf.Response$Properties} [response] RealTimePlotResponse response.
         */

        /**
         * Constructs a new RealTimePlotResponse.
         * @exports protobuf.RealTimePlotResponse
         * @constructor
         * @param {protobuf.RealTimePlotResponse$Properties=} [properties] Properties to set
         */
        function RealTimePlotResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RealTimePlotResponse uuid.
         * @type {number|Long|undefined}
         */
        RealTimePlotResponse.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RealTimePlotResponse deviceId.
         * @type {number|Long|undefined}
         */
        RealTimePlotResponse.prototype.deviceId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RealTimePlotResponse plotType.
         * @type {protobuf.PlotType|undefined}
         */
        RealTimePlotResponse.prototype.plotType = 0;

        /**
         * RealTimePlotResponse response.
         * @type {protobuf.Response$Properties|undefined}
         */
        RealTimePlotResponse.prototype.response = null;

        /**
         * Creates a new RealTimePlotResponse instance using the specified properties.
         * @param {protobuf.RealTimePlotResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.RealTimePlotResponse} RealTimePlotResponse instance
         */
        RealTimePlotResponse.create = function create(properties) {
            return new RealTimePlotResponse(properties);
        };

        /**
         * Encodes the specified RealTimePlotResponse message. Does not implicitly {@link protobuf.RealTimePlotResponse.verify|verify} messages.
         * @param {protobuf.RealTimePlotResponse$Properties} message RealTimePlotResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.deviceId != null && message.hasOwnProperty("deviceId"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.deviceId);
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.plotType);
            if (message.response && message.hasOwnProperty("response"))
                $root.protobuf.Response.encode(message.response, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified RealTimePlotResponse message, length delimited. Does not implicitly {@link protobuf.RealTimePlotResponse.verify|verify} messages.
         * @param {protobuf.RealTimePlotResponse$Properties} message RealTimePlotResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RealTimePlotResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RealTimePlotResponse} RealTimePlotResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RealTimePlotResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.deviceId = reader.uint64();
                    break;
                case 3:
                    message.plotType = reader.uint32();
                    break;
                case 4:
                    message.response = $root.protobuf.Response.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RealTimePlotResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RealTimePlotResponse} RealTimePlotResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RealTimePlotResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RealTimePlotResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.deviceId != null)
                if (!$util.isInteger(message.deviceId) && !(message.deviceId && $util.isInteger(message.deviceId.low) && $util.isInteger(message.deviceId.high)))
                    return "deviceId: integer|Long expected";
            if (message.plotType != null)
                switch (message.plotType) {
                default:
                    return "plotType: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    break;
                }
            if (message.response != null) {
                var error = $root.protobuf.Response.verify(message.response);
                if (error)
                    return "response." + error;
            }
            return null;
        };

        /**
         * Creates a RealTimePlotResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotResponse} RealTimePlotResponse
         */
        RealTimePlotResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RealTimePlotResponse)
                return object;
            var message = new $root.protobuf.RealTimePlotResponse();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.deviceId != null)
                if ($util.Long)
                    (message.deviceId = $util.Long.fromValue(object.deviceId)).unsigned = true;
                else if (typeof object.deviceId === "string")
                    message.deviceId = parseInt(object.deviceId, 10);
                else if (typeof object.deviceId === "number")
                    message.deviceId = object.deviceId;
                else if (typeof object.deviceId === "object")
                    message.deviceId = new $util.LongBits(object.deviceId.low >>> 0, object.deviceId.high >>> 0).toNumber(true);
            switch (object.plotType) {
            case "MICROPHONE_FFT":
            case 0:
                message.plotType = 0;
                break;
            case "MICROPHONE_MULTIRES":
            case 1:
                message.plotType = 1;
                break;
            case "BAROMETER_FFT":
            case 2:
                message.plotType = 2;
                break;
            case "BAROMETER_MULTIRES":
            case 3:
                message.plotType = 3;
                break;
            case "LATENCY":
            case 4:
                message.plotType = 4;
                break;
            case "LATITUDE_PLOT":
            case 5:
                message.plotType = 5;
                break;
            case "LONGITUDE_PLOT":
            case 6:
                message.plotType = 6;
                break;
            case "ALTITUDE_PLOT":
            case 7:
                message.plotType = 7;
                break;
            case "ACCURACY_PLOT":
            case 8:
                message.plotType = 8;
                break;
            case "MAGNETOMETER_PLOT":
            case 9:
                message.plotType = 9;
                break;
            case "GYROSCOPE_PLOT":
            case 10:
                message.plotType = 10;
                break;
            case "LIGHT_PLOT":
            case 11:
                message.plotType = 11;
                break;
            case "ACCELEROMETER_PLOT":
            case 12:
                message.plotType = 12;
                break;
            case "MICROPHONE_FFT_LINEAR":
            case 13:
                message.plotType = 13;
                break;
            case "MICROPHONE_FFT_LOG":
            case 14:
                message.plotType = 14;
                break;
            case "AUDIO_PICKER":
            case 15:
                message.plotType = 15;
                break;
            }
            if (object.response != null) {
                if (typeof object.response !== "object")
                    throw TypeError(".protobuf.RealTimePlotResponse.response: object expected");
                message.response = $root.protobuf.Response.fromObject(object.response);
            }
            return message;
        };

        /**
         * Creates a RealTimePlotResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RealTimePlotResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotResponse} RealTimePlotResponse
         */
        RealTimePlotResponse.from = RealTimePlotResponse.fromObject;

        /**
         * Creates a plain object from a RealTimePlotResponse message. Also converts values to other types if specified.
         * @param {protobuf.RealTimePlotResponse} message RealTimePlotResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.deviceId = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.deviceId = options.longs === String ? "0" : 0;
                object.plotType = options.enums === String ? "MICROPHONE_FFT" : 0;
                object.response = null;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.deviceId != null && message.hasOwnProperty("deviceId"))
                if (typeof message.deviceId === "number")
                    object.deviceId = options.longs === String ? String(message.deviceId) : message.deviceId;
                else
                    object.deviceId = options.longs === String ? $util.Long.prototype.toString.call(message.deviceId) : options.longs === Number ? new $util.LongBits(message.deviceId.low >>> 0, message.deviceId.high >>> 0).toNumber(true) : message.deviceId;
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                object.plotType = options.enums === String ? $root.protobuf.PlotType[message.plotType] : message.plotType;
            if (message.response != null && message.hasOwnProperty("response"))
                object.response = $root.protobuf.Response.toObject(message.response, options);
            return object;
        };

        /**
         * Creates a plain object from this RealTimePlotResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RealTimePlotResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RealTimePlotResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RealTimePlotResponse;
    })();

    protobuf.RealTimePlotRequests = (function() {

        /**
         * Properties of a RealTimePlotRequests.
         * @typedef protobuf.RealTimePlotRequests$Properties
         * @type {Object}
         * @property {number|Long} [uuid] RealTimePlotRequests uuid.
         * @property {Array.<protobuf.RealTimePlotRequest$Properties>} [realTimePlotRequests] RealTimePlotRequests realTimePlotRequests.
         */

        /**
         * Constructs a new RealTimePlotRequests.
         * @exports protobuf.RealTimePlotRequests
         * @constructor
         * @param {protobuf.RealTimePlotRequests$Properties=} [properties] Properties to set
         */
        function RealTimePlotRequests(properties) {
            this.realTimePlotRequests = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RealTimePlotRequests uuid.
         * @type {number|Long|undefined}
         */
        RealTimePlotRequests.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RealTimePlotRequests realTimePlotRequests.
         * @type {Array.<protobuf.RealTimePlotRequest$Properties>|undefined}
         */
        RealTimePlotRequests.prototype.realTimePlotRequests = $util.emptyArray;

        /**
         * Creates a new RealTimePlotRequests instance using the specified properties.
         * @param {protobuf.RealTimePlotRequests$Properties=} [properties] Properties to set
         * @returns {protobuf.RealTimePlotRequests} RealTimePlotRequests instance
         */
        RealTimePlotRequests.create = function create(properties) {
            return new RealTimePlotRequests(properties);
        };

        /**
         * Encodes the specified RealTimePlotRequests message. Does not implicitly {@link protobuf.RealTimePlotRequests.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequests$Properties} message RealTimePlotRequests message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequests.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.realTimePlotRequests && message.realTimePlotRequests.length)
                for (var i = 0; i < message.realTimePlotRequests.length; ++i)
                    $root.protobuf.RealTimePlotRequest.encode(message.realTimePlotRequests[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified RealTimePlotRequests message, length delimited. Does not implicitly {@link protobuf.RealTimePlotRequests.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequests$Properties} message RealTimePlotRequests message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequests.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RealTimePlotRequests message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RealTimePlotRequests} RealTimePlotRequests
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequests.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RealTimePlotRequests();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    if (!(message.realTimePlotRequests && message.realTimePlotRequests.length))
                        message.realTimePlotRequests = [];
                    message.realTimePlotRequests.push($root.protobuf.RealTimePlotRequest.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RealTimePlotRequests message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RealTimePlotRequests} RealTimePlotRequests
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequests.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RealTimePlotRequests message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RealTimePlotRequests.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.realTimePlotRequests != null) {
                if (!Array.isArray(message.realTimePlotRequests))
                    return "realTimePlotRequests: array expected";
                for (var i = 0; i < message.realTimePlotRequests.length; ++i) {
                    var error = $root.protobuf.RealTimePlotRequest.verify(message.realTimePlotRequests[i]);
                    if (error)
                        return "realTimePlotRequests." + error;
                }
            }
            return null;
        };

        /**
         * Creates a RealTimePlotRequests message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequests} RealTimePlotRequests
         */
        RealTimePlotRequests.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RealTimePlotRequests)
                return object;
            var message = new $root.protobuf.RealTimePlotRequests();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.realTimePlotRequests) {
                if (!Array.isArray(object.realTimePlotRequests))
                    throw TypeError(".protobuf.RealTimePlotRequests.realTimePlotRequests: array expected");
                message.realTimePlotRequests = [];
                for (var i = 0; i < object.realTimePlotRequests.length; ++i) {
                    if (typeof object.realTimePlotRequests[i] !== "object")
                        throw TypeError(".protobuf.RealTimePlotRequests.realTimePlotRequests: object expected");
                    message.realTimePlotRequests[i] = $root.protobuf.RealTimePlotRequest.fromObject(object.realTimePlotRequests[i]);
                }
            }
            return message;
        };

        /**
         * Creates a RealTimePlotRequests message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RealTimePlotRequests.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequests} RealTimePlotRequests
         */
        RealTimePlotRequests.from = RealTimePlotRequests.fromObject;

        /**
         * Creates a plain object from a RealTimePlotRequests message. Also converts values to other types if specified.
         * @param {protobuf.RealTimePlotRequests} message RealTimePlotRequests
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequests.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.realTimePlotRequests = [];
            if (options.defaults)
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.realTimePlotRequests && message.realTimePlotRequests.length) {
                object.realTimePlotRequests = [];
                for (var j = 0; j < message.realTimePlotRequests.length; ++j)
                    object.realTimePlotRequests[j] = $root.protobuf.RealTimePlotRequest.toObject(message.realTimePlotRequests[j], options);
            }
            return object;
        };

        /**
         * Creates a plain object from this RealTimePlotRequests message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequests.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RealTimePlotRequests to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RealTimePlotRequests.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RealTimePlotRequests;
    })();

    protobuf.RealTimePlotRequestApi900 = (function() {

        /**
         * Properties of a RealTimePlotRequestApi900.
         * @typedef protobuf.RealTimePlotRequestApi900$Properties
         * @type {Object}
         * @property {string} [redvoxId] RealTimePlotRequestApi900 redvoxId.
         * @property {string} [redvoxUuid] RealTimePlotRequestApi900 redvoxUuid.
         * @property {protobuf.PlotType} [plotType] RealTimePlotRequestApi900 plotType.
         * @property {number|Long} [secondsBeforeNow] RealTimePlotRequestApi900 secondsBeforeNow.
         */

        /**
         * Constructs a new RealTimePlotRequestApi900.
         * @exports protobuf.RealTimePlotRequestApi900
         * @constructor
         * @param {protobuf.RealTimePlotRequestApi900$Properties=} [properties] Properties to set
         */
        function RealTimePlotRequestApi900(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RealTimePlotRequestApi900 redvoxId.
         * @type {string|undefined}
         */
        RealTimePlotRequestApi900.prototype.redvoxId = "";

        /**
         * RealTimePlotRequestApi900 redvoxUuid.
         * @type {string|undefined}
         */
        RealTimePlotRequestApi900.prototype.redvoxUuid = "";

        /**
         * RealTimePlotRequestApi900 plotType.
         * @type {protobuf.PlotType|undefined}
         */
        RealTimePlotRequestApi900.prototype.plotType = 0;

        /**
         * RealTimePlotRequestApi900 secondsBeforeNow.
         * @type {number|Long|undefined}
         */
        RealTimePlotRequestApi900.prototype.secondsBeforeNow = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * Creates a new RealTimePlotRequestApi900 instance using the specified properties.
         * @param {protobuf.RealTimePlotRequestApi900$Properties=} [properties] Properties to set
         * @returns {protobuf.RealTimePlotRequestApi900} RealTimePlotRequestApi900 instance
         */
        RealTimePlotRequestApi900.create = function create(properties) {
            return new RealTimePlotRequestApi900(properties);
        };

        /**
         * Encodes the specified RealTimePlotRequestApi900 message. Does not implicitly {@link protobuf.RealTimePlotRequestApi900.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequestApi900$Properties} message RealTimePlotRequestApi900 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequestApi900.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.redvoxId);
            if (message.redvoxUuid != null && message.hasOwnProperty("redvoxUuid"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.redvoxUuid);
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.plotType);
            if (message.secondsBeforeNow != null && message.hasOwnProperty("secondsBeforeNow"))
                writer.uint32(/* id 4, wireType 0 =*/32).uint64(message.secondsBeforeNow);
            return writer;
        };

        /**
         * Encodes the specified RealTimePlotRequestApi900 message, length delimited. Does not implicitly {@link protobuf.RealTimePlotRequestApi900.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequestApi900$Properties} message RealTimePlotRequestApi900 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequestApi900.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RealTimePlotRequestApi900 message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RealTimePlotRequestApi900} RealTimePlotRequestApi900
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequestApi900.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RealTimePlotRequestApi900();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.redvoxId = reader.string();
                    break;
                case 2:
                    message.redvoxUuid = reader.string();
                    break;
                case 3:
                    message.plotType = reader.uint32();
                    break;
                case 4:
                    message.secondsBeforeNow = reader.uint64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RealTimePlotRequestApi900 message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RealTimePlotRequestApi900} RealTimePlotRequestApi900
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequestApi900.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RealTimePlotRequestApi900 message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RealTimePlotRequestApi900.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.redvoxId != null)
                if (!$util.isString(message.redvoxId))
                    return "redvoxId: string expected";
            if (message.redvoxUuid != null)
                if (!$util.isString(message.redvoxUuid))
                    return "redvoxUuid: string expected";
            if (message.plotType != null)
                switch (message.plotType) {
                default:
                    return "plotType: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    break;
                }
            if (message.secondsBeforeNow != null)
                if (!$util.isInteger(message.secondsBeforeNow) && !(message.secondsBeforeNow && $util.isInteger(message.secondsBeforeNow.low) && $util.isInteger(message.secondsBeforeNow.high)))
                    return "secondsBeforeNow: integer|Long expected";
            return null;
        };

        /**
         * Creates a RealTimePlotRequestApi900 message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequestApi900} RealTimePlotRequestApi900
         */
        RealTimePlotRequestApi900.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RealTimePlotRequestApi900)
                return object;
            var message = new $root.protobuf.RealTimePlotRequestApi900();
            if (object.redvoxId != null)
                message.redvoxId = String(object.redvoxId);
            if (object.redvoxUuid != null)
                message.redvoxUuid = String(object.redvoxUuid);
            switch (object.plotType) {
            case "MICROPHONE_FFT":
            case 0:
                message.plotType = 0;
                break;
            case "MICROPHONE_MULTIRES":
            case 1:
                message.plotType = 1;
                break;
            case "BAROMETER_FFT":
            case 2:
                message.plotType = 2;
                break;
            case "BAROMETER_MULTIRES":
            case 3:
                message.plotType = 3;
                break;
            case "LATENCY":
            case 4:
                message.plotType = 4;
                break;
            case "LATITUDE_PLOT":
            case 5:
                message.plotType = 5;
                break;
            case "LONGITUDE_PLOT":
            case 6:
                message.plotType = 6;
                break;
            case "ALTITUDE_PLOT":
            case 7:
                message.plotType = 7;
                break;
            case "ACCURACY_PLOT":
            case 8:
                message.plotType = 8;
                break;
            case "MAGNETOMETER_PLOT":
            case 9:
                message.plotType = 9;
                break;
            case "GYROSCOPE_PLOT":
            case 10:
                message.plotType = 10;
                break;
            case "LIGHT_PLOT":
            case 11:
                message.plotType = 11;
                break;
            case "ACCELEROMETER_PLOT":
            case 12:
                message.plotType = 12;
                break;
            case "MICROPHONE_FFT_LINEAR":
            case 13:
                message.plotType = 13;
                break;
            case "MICROPHONE_FFT_LOG":
            case 14:
                message.plotType = 14;
                break;
            case "AUDIO_PICKER":
            case 15:
                message.plotType = 15;
                break;
            }
            if (object.secondsBeforeNow != null)
                if ($util.Long)
                    (message.secondsBeforeNow = $util.Long.fromValue(object.secondsBeforeNow)).unsigned = true;
                else if (typeof object.secondsBeforeNow === "string")
                    message.secondsBeforeNow = parseInt(object.secondsBeforeNow, 10);
                else if (typeof object.secondsBeforeNow === "number")
                    message.secondsBeforeNow = object.secondsBeforeNow;
                else if (typeof object.secondsBeforeNow === "object")
                    message.secondsBeforeNow = new $util.LongBits(object.secondsBeforeNow.low >>> 0, object.secondsBeforeNow.high >>> 0).toNumber(true);
            return message;
        };

        /**
         * Creates a RealTimePlotRequestApi900 message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RealTimePlotRequestApi900.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequestApi900} RealTimePlotRequestApi900
         */
        RealTimePlotRequestApi900.from = RealTimePlotRequestApi900.fromObject;

        /**
         * Creates a plain object from a RealTimePlotRequestApi900 message. Also converts values to other types if specified.
         * @param {protobuf.RealTimePlotRequestApi900} message RealTimePlotRequestApi900
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequestApi900.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.redvoxId = "";
                object.redvoxUuid = "";
                object.plotType = options.enums === String ? "MICROPHONE_FFT" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.secondsBeforeNow = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.secondsBeforeNow = options.longs === String ? "0" : 0;
            }
            if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
                object.redvoxId = message.redvoxId;
            if (message.redvoxUuid != null && message.hasOwnProperty("redvoxUuid"))
                object.redvoxUuid = message.redvoxUuid;
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                object.plotType = options.enums === String ? $root.protobuf.PlotType[message.plotType] : message.plotType;
            if (message.secondsBeforeNow != null && message.hasOwnProperty("secondsBeforeNow"))
                if (typeof message.secondsBeforeNow === "number")
                    object.secondsBeforeNow = options.longs === String ? String(message.secondsBeforeNow) : message.secondsBeforeNow;
                else
                    object.secondsBeforeNow = options.longs === String ? $util.Long.prototype.toString.call(message.secondsBeforeNow) : options.longs === Number ? new $util.LongBits(message.secondsBeforeNow.low >>> 0, message.secondsBeforeNow.high >>> 0).toNumber(true) : message.secondsBeforeNow;
            return object;
        };

        /**
         * Creates a plain object from this RealTimePlotRequestApi900 message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequestApi900.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RealTimePlotRequestApi900 to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RealTimePlotRequestApi900.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RealTimePlotRequestApi900;
    })();

    protobuf.RealTimePlotResponseApi900 = (function() {

        /**
         * Properties of a RealTimePlotResponseApi900.
         * @typedef protobuf.RealTimePlotResponseApi900$Properties
         * @type {Object}
         * @property {string} [redvoxId] RealTimePlotResponseApi900 redvoxId.
         * @property {string} [redvoxUuid] RealTimePlotResponseApi900 redvoxUuid.
         * @property {protobuf.PlotType} [plotType] RealTimePlotResponseApi900 plotType.
         * @property {protobuf.Response$Properties} [response] RealTimePlotResponseApi900 response.
         * @property {string} [dataKey] RealTimePlotResponseApi900 dataKey.
         */

        /**
         * Constructs a new RealTimePlotResponseApi900.
         * @exports protobuf.RealTimePlotResponseApi900
         * @constructor
         * @param {protobuf.RealTimePlotResponseApi900$Properties=} [properties] Properties to set
         */
        function RealTimePlotResponseApi900(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RealTimePlotResponseApi900 redvoxId.
         * @type {string|undefined}
         */
        RealTimePlotResponseApi900.prototype.redvoxId = "";

        /**
         * RealTimePlotResponseApi900 redvoxUuid.
         * @type {string|undefined}
         */
        RealTimePlotResponseApi900.prototype.redvoxUuid = "";

        /**
         * RealTimePlotResponseApi900 plotType.
         * @type {protobuf.PlotType|undefined}
         */
        RealTimePlotResponseApi900.prototype.plotType = 0;

        /**
         * RealTimePlotResponseApi900 response.
         * @type {protobuf.Response$Properties|undefined}
         */
        RealTimePlotResponseApi900.prototype.response = null;

        /**
         * RealTimePlotResponseApi900 dataKey.
         * @type {string|undefined}
         */
        RealTimePlotResponseApi900.prototype.dataKey = "";

        /**
         * Creates a new RealTimePlotResponseApi900 instance using the specified properties.
         * @param {protobuf.RealTimePlotResponseApi900$Properties=} [properties] Properties to set
         * @returns {protobuf.RealTimePlotResponseApi900} RealTimePlotResponseApi900 instance
         */
        RealTimePlotResponseApi900.create = function create(properties) {
            return new RealTimePlotResponseApi900(properties);
        };

        /**
         * Encodes the specified RealTimePlotResponseApi900 message. Does not implicitly {@link protobuf.RealTimePlotResponseApi900.verify|verify} messages.
         * @param {protobuf.RealTimePlotResponseApi900$Properties} message RealTimePlotResponseApi900 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotResponseApi900.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.redvoxId);
            if (message.redvoxUuid != null && message.hasOwnProperty("redvoxUuid"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.redvoxUuid);
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.plotType);
            if (message.response && message.hasOwnProperty("response"))
                $root.protobuf.Response.encode(message.response, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.dataKey);
            return writer;
        };

        /**
         * Encodes the specified RealTimePlotResponseApi900 message, length delimited. Does not implicitly {@link protobuf.RealTimePlotResponseApi900.verify|verify} messages.
         * @param {protobuf.RealTimePlotResponseApi900$Properties} message RealTimePlotResponseApi900 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotResponseApi900.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RealTimePlotResponseApi900 message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RealTimePlotResponseApi900} RealTimePlotResponseApi900
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotResponseApi900.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RealTimePlotResponseApi900();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.redvoxId = reader.string();
                    break;
                case 2:
                    message.redvoxUuid = reader.string();
                    break;
                case 3:
                    message.plotType = reader.uint32();
                    break;
                case 4:
                    message.response = $root.protobuf.Response.decode(reader, reader.uint32());
                    break;
                case 5:
                    message.dataKey = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RealTimePlotResponseApi900 message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RealTimePlotResponseApi900} RealTimePlotResponseApi900
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotResponseApi900.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RealTimePlotResponseApi900 message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RealTimePlotResponseApi900.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.redvoxId != null)
                if (!$util.isString(message.redvoxId))
                    return "redvoxId: string expected";
            if (message.redvoxUuid != null)
                if (!$util.isString(message.redvoxUuid))
                    return "redvoxUuid: string expected";
            if (message.plotType != null)
                switch (message.plotType) {
                default:
                    return "plotType: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    break;
                }
            if (message.response != null) {
                var error = $root.protobuf.Response.verify(message.response);
                if (error)
                    return "response." + error;
            }
            if (message.dataKey != null)
                if (!$util.isString(message.dataKey))
                    return "dataKey: string expected";
            return null;
        };

        /**
         * Creates a RealTimePlotResponseApi900 message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotResponseApi900} RealTimePlotResponseApi900
         */
        RealTimePlotResponseApi900.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RealTimePlotResponseApi900)
                return object;
            var message = new $root.protobuf.RealTimePlotResponseApi900();
            if (object.redvoxId != null)
                message.redvoxId = String(object.redvoxId);
            if (object.redvoxUuid != null)
                message.redvoxUuid = String(object.redvoxUuid);
            switch (object.plotType) {
            case "MICROPHONE_FFT":
            case 0:
                message.plotType = 0;
                break;
            case "MICROPHONE_MULTIRES":
            case 1:
                message.plotType = 1;
                break;
            case "BAROMETER_FFT":
            case 2:
                message.plotType = 2;
                break;
            case "BAROMETER_MULTIRES":
            case 3:
                message.plotType = 3;
                break;
            case "LATENCY":
            case 4:
                message.plotType = 4;
                break;
            case "LATITUDE_PLOT":
            case 5:
                message.plotType = 5;
                break;
            case "LONGITUDE_PLOT":
            case 6:
                message.plotType = 6;
                break;
            case "ALTITUDE_PLOT":
            case 7:
                message.plotType = 7;
                break;
            case "ACCURACY_PLOT":
            case 8:
                message.plotType = 8;
                break;
            case "MAGNETOMETER_PLOT":
            case 9:
                message.plotType = 9;
                break;
            case "GYROSCOPE_PLOT":
            case 10:
                message.plotType = 10;
                break;
            case "LIGHT_PLOT":
            case 11:
                message.plotType = 11;
                break;
            case "ACCELEROMETER_PLOT":
            case 12:
                message.plotType = 12;
                break;
            case "MICROPHONE_FFT_LINEAR":
            case 13:
                message.plotType = 13;
                break;
            case "MICROPHONE_FFT_LOG":
            case 14:
                message.plotType = 14;
                break;
            case "AUDIO_PICKER":
            case 15:
                message.plotType = 15;
                break;
            }
            if (object.response != null) {
                if (typeof object.response !== "object")
                    throw TypeError(".protobuf.RealTimePlotResponseApi900.response: object expected");
                message.response = $root.protobuf.Response.fromObject(object.response);
            }
            if (object.dataKey != null)
                message.dataKey = String(object.dataKey);
            return message;
        };

        /**
         * Creates a RealTimePlotResponseApi900 message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RealTimePlotResponseApi900.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotResponseApi900} RealTimePlotResponseApi900
         */
        RealTimePlotResponseApi900.from = RealTimePlotResponseApi900.fromObject;

        /**
         * Creates a plain object from a RealTimePlotResponseApi900 message. Also converts values to other types if specified.
         * @param {protobuf.RealTimePlotResponseApi900} message RealTimePlotResponseApi900
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotResponseApi900.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.redvoxId = "";
                object.redvoxUuid = "";
                object.plotType = options.enums === String ? "MICROPHONE_FFT" : 0;
                object.response = null;
                object.dataKey = "";
            }
            if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
                object.redvoxId = message.redvoxId;
            if (message.redvoxUuid != null && message.hasOwnProperty("redvoxUuid"))
                object.redvoxUuid = message.redvoxUuid;
            if (message.plotType != null && message.hasOwnProperty("plotType"))
                object.plotType = options.enums === String ? $root.protobuf.PlotType[message.plotType] : message.plotType;
            if (message.response != null && message.hasOwnProperty("response"))
                object.response = $root.protobuf.Response.toObject(message.response, options);
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                object.dataKey = message.dataKey;
            return object;
        };

        /**
         * Creates a plain object from this RealTimePlotResponseApi900 message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotResponseApi900.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RealTimePlotResponseApi900 to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RealTimePlotResponseApi900.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RealTimePlotResponseApi900;
    })();

    protobuf.RealTimePlotRequestsApi900 = (function() {

        /**
         * Properties of a RealTimePlotRequestsApi900.
         * @typedef protobuf.RealTimePlotRequestsApi900$Properties
         * @type {Object}
         * @property {Array.<protobuf.RealTimePlotRequestApi900$Properties>} [realTimePlotRequestsApi900] RealTimePlotRequestsApi900 realTimePlotRequestsApi900.
         */

        /**
         * Constructs a new RealTimePlotRequestsApi900.
         * @exports protobuf.RealTimePlotRequestsApi900
         * @constructor
         * @param {protobuf.RealTimePlotRequestsApi900$Properties=} [properties] Properties to set
         */
        function RealTimePlotRequestsApi900(properties) {
            this.realTimePlotRequestsApi900 = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RealTimePlotRequestsApi900 realTimePlotRequestsApi900.
         * @type {Array.<protobuf.RealTimePlotRequestApi900$Properties>|undefined}
         */
        RealTimePlotRequestsApi900.prototype.realTimePlotRequestsApi900 = $util.emptyArray;

        /**
         * Creates a new RealTimePlotRequestsApi900 instance using the specified properties.
         * @param {protobuf.RealTimePlotRequestsApi900$Properties=} [properties] Properties to set
         * @returns {protobuf.RealTimePlotRequestsApi900} RealTimePlotRequestsApi900 instance
         */
        RealTimePlotRequestsApi900.create = function create(properties) {
            return new RealTimePlotRequestsApi900(properties);
        };

        /**
         * Encodes the specified RealTimePlotRequestsApi900 message. Does not implicitly {@link protobuf.RealTimePlotRequestsApi900.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequestsApi900$Properties} message RealTimePlotRequestsApi900 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequestsApi900.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.realTimePlotRequestsApi900 && message.realTimePlotRequestsApi900.length)
                for (var i = 0; i < message.realTimePlotRequestsApi900.length; ++i)
                    $root.protobuf.RealTimePlotRequestApi900.encode(message.realTimePlotRequestsApi900[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified RealTimePlotRequestsApi900 message, length delimited. Does not implicitly {@link protobuf.RealTimePlotRequestsApi900.verify|verify} messages.
         * @param {protobuf.RealTimePlotRequestsApi900$Properties} message RealTimePlotRequestsApi900 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RealTimePlotRequestsApi900.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RealTimePlotRequestsApi900 message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RealTimePlotRequestsApi900} RealTimePlotRequestsApi900
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequestsApi900.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RealTimePlotRequestsApi900();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.realTimePlotRequestsApi900 && message.realTimePlotRequestsApi900.length))
                        message.realTimePlotRequestsApi900 = [];
                    message.realTimePlotRequestsApi900.push($root.protobuf.RealTimePlotRequestApi900.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RealTimePlotRequestsApi900 message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RealTimePlotRequestsApi900} RealTimePlotRequestsApi900
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RealTimePlotRequestsApi900.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RealTimePlotRequestsApi900 message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RealTimePlotRequestsApi900.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.realTimePlotRequestsApi900 != null) {
                if (!Array.isArray(message.realTimePlotRequestsApi900))
                    return "realTimePlotRequestsApi900: array expected";
                for (var i = 0; i < message.realTimePlotRequestsApi900.length; ++i) {
                    var error = $root.protobuf.RealTimePlotRequestApi900.verify(message.realTimePlotRequestsApi900[i]);
                    if (error)
                        return "realTimePlotRequestsApi900." + error;
                }
            }
            return null;
        };

        /**
         * Creates a RealTimePlotRequestsApi900 message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequestsApi900} RealTimePlotRequestsApi900
         */
        RealTimePlotRequestsApi900.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RealTimePlotRequestsApi900)
                return object;
            var message = new $root.protobuf.RealTimePlotRequestsApi900();
            if (object.realTimePlotRequestsApi900) {
                if (!Array.isArray(object.realTimePlotRequestsApi900))
                    throw TypeError(".protobuf.RealTimePlotRequestsApi900.realTimePlotRequestsApi900: array expected");
                message.realTimePlotRequestsApi900 = [];
                for (var i = 0; i < object.realTimePlotRequestsApi900.length; ++i) {
                    if (typeof object.realTimePlotRequestsApi900[i] !== "object")
                        throw TypeError(".protobuf.RealTimePlotRequestsApi900.realTimePlotRequestsApi900: object expected");
                    message.realTimePlotRequestsApi900[i] = $root.protobuf.RealTimePlotRequestApi900.fromObject(object.realTimePlotRequestsApi900[i]);
                }
            }
            return message;
        };

        /**
         * Creates a RealTimePlotRequestsApi900 message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RealTimePlotRequestsApi900.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RealTimePlotRequestsApi900} RealTimePlotRequestsApi900
         */
        RealTimePlotRequestsApi900.from = RealTimePlotRequestsApi900.fromObject;

        /**
         * Creates a plain object from a RealTimePlotRequestsApi900 message. Also converts values to other types if specified.
         * @param {protobuf.RealTimePlotRequestsApi900} message RealTimePlotRequestsApi900
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequestsApi900.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.realTimePlotRequestsApi900 = [];
            if (message.realTimePlotRequestsApi900 && message.realTimePlotRequestsApi900.length) {
                object.realTimePlotRequestsApi900 = [];
                for (var j = 0; j < message.realTimePlotRequestsApi900.length; ++j)
                    object.realTimePlotRequestsApi900[j] = $root.protobuf.RealTimePlotRequestApi900.toObject(message.realTimePlotRequestsApi900[j], options);
            }
            return object;
        };

        /**
         * Creates a plain object from this RealTimePlotRequestsApi900 message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RealTimePlotRequestsApi900.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RealTimePlotRequestsApi900 to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RealTimePlotRequestsApi900.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RealTimePlotRequestsApi900;
    })();

    protobuf.FcmRequest = (function() {

        /**
         * Properties of a FcmRequest.
         * @typedef protobuf.FcmRequest$Properties
         * @type {Object}
         * @property {number|Long} [uuid] FcmRequest uuid.
         * @property {string} [title] FcmRequest title.
         * @property {string} [body] FcmRequest body.
         * @property {Array.<string>} [fcmTokens] FcmRequest fcmTokens.
         */

        /**
         * Constructs a new FcmRequest.
         * @exports protobuf.FcmRequest
         * @constructor
         * @param {protobuf.FcmRequest$Properties=} [properties] Properties to set
         */
        function FcmRequest(properties) {
            this.fcmTokens = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * FcmRequest uuid.
         * @type {number|Long|undefined}
         */
        FcmRequest.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * FcmRequest title.
         * @type {string|undefined}
         */
        FcmRequest.prototype.title = "";

        /**
         * FcmRequest body.
         * @type {string|undefined}
         */
        FcmRequest.prototype.body = "";

        /**
         * FcmRequest fcmTokens.
         * @type {Array.<string>|undefined}
         */
        FcmRequest.prototype.fcmTokens = $util.emptyArray;

        /**
         * Creates a new FcmRequest instance using the specified properties.
         * @param {protobuf.FcmRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.FcmRequest} FcmRequest instance
         */
        FcmRequest.create = function create(properties) {
            return new FcmRequest(properties);
        };

        /**
         * Encodes the specified FcmRequest message. Does not implicitly {@link protobuf.FcmRequest.verify|verify} messages.
         * @param {protobuf.FcmRequest$Properties} message FcmRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FcmRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.title != null && message.hasOwnProperty("title"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.title);
            if (message.body != null && message.hasOwnProperty("body"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.body);
            if (message.fcmTokens && message.fcmTokens.length)
                for (var i = 0; i < message.fcmTokens.length; ++i)
                    writer.uint32(/* id 4, wireType 2 =*/34).string(message.fcmTokens[i]);
            return writer;
        };

        /**
         * Encodes the specified FcmRequest message, length delimited. Does not implicitly {@link protobuf.FcmRequest.verify|verify} messages.
         * @param {protobuf.FcmRequest$Properties} message FcmRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FcmRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FcmRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.FcmRequest} FcmRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FcmRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.FcmRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.title = reader.string();
                    break;
                case 3:
                    message.body = reader.string();
                    break;
                case 4:
                    if (!(message.fcmTokens && message.fcmTokens.length))
                        message.fcmTokens = [];
                    message.fcmTokens.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FcmRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.FcmRequest} FcmRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FcmRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FcmRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        FcmRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.title != null)
                if (!$util.isString(message.title))
                    return "title: string expected";
            if (message.body != null)
                if (!$util.isString(message.body))
                    return "body: string expected";
            if (message.fcmTokens != null) {
                if (!Array.isArray(message.fcmTokens))
                    return "fcmTokens: array expected";
                for (var i = 0; i < message.fcmTokens.length; ++i)
                    if (!$util.isString(message.fcmTokens[i]))
                        return "fcmTokens: string[] expected";
            }
            return null;
        };

        /**
         * Creates a FcmRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.FcmRequest} FcmRequest
         */
        FcmRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.FcmRequest)
                return object;
            var message = new $root.protobuf.FcmRequest();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.title != null)
                message.title = String(object.title);
            if (object.body != null)
                message.body = String(object.body);
            if (object.fcmTokens) {
                if (!Array.isArray(object.fcmTokens))
                    throw TypeError(".protobuf.FcmRequest.fcmTokens: array expected");
                message.fcmTokens = [];
                for (var i = 0; i < object.fcmTokens.length; ++i)
                    message.fcmTokens[i] = String(object.fcmTokens[i]);
            }
            return message;
        };

        /**
         * Creates a FcmRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.FcmRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.FcmRequest} FcmRequest
         */
        FcmRequest.from = FcmRequest.fromObject;

        /**
         * Creates a plain object from a FcmRequest message. Also converts values to other types if specified.
         * @param {protobuf.FcmRequest} message FcmRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FcmRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.fcmTokens = [];
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.title = "";
                object.body = "";
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.title != null && message.hasOwnProperty("title"))
                object.title = message.title;
            if (message.body != null && message.hasOwnProperty("body"))
                object.body = message.body;
            if (message.fcmTokens && message.fcmTokens.length) {
                object.fcmTokens = [];
                for (var j = 0; j < message.fcmTokens.length; ++j)
                    object.fcmTokens[j] = message.fcmTokens[j];
            }
            return object;
        };

        /**
         * Creates a plain object from this FcmRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FcmRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this FcmRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        FcmRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FcmRequest;
    })();

    protobuf.FcmResponse = (function() {

        /**
         * Properties of a FcmResponse.
         * @typedef protobuf.FcmResponse$Properties
         * @type {Object}
         * @property {number|Long} [uuid] FcmResponse uuid.
         * @property {Array.<string>} [responses] FcmResponse responses.
         */

        /**
         * Constructs a new FcmResponse.
         * @exports protobuf.FcmResponse
         * @constructor
         * @param {protobuf.FcmResponse$Properties=} [properties] Properties to set
         */
        function FcmResponse(properties) {
            this.responses = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * FcmResponse uuid.
         * @type {number|Long|undefined}
         */
        FcmResponse.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * FcmResponse responses.
         * @type {Array.<string>|undefined}
         */
        FcmResponse.prototype.responses = $util.emptyArray;

        /**
         * Creates a new FcmResponse instance using the specified properties.
         * @param {protobuf.FcmResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.FcmResponse} FcmResponse instance
         */
        FcmResponse.create = function create(properties) {
            return new FcmResponse(properties);
        };

        /**
         * Encodes the specified FcmResponse message. Does not implicitly {@link protobuf.FcmResponse.verify|verify} messages.
         * @param {protobuf.FcmResponse$Properties} message FcmResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FcmResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.responses && message.responses.length)
                for (var i = 0; i < message.responses.length; ++i)
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.responses[i]);
            return writer;
        };

        /**
         * Encodes the specified FcmResponse message, length delimited. Does not implicitly {@link protobuf.FcmResponse.verify|verify} messages.
         * @param {protobuf.FcmResponse$Properties} message FcmResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FcmResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FcmResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.FcmResponse} FcmResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FcmResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.FcmResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    if (!(message.responses && message.responses.length))
                        message.responses = [];
                    message.responses.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FcmResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.FcmResponse} FcmResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FcmResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FcmResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        FcmResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.responses != null) {
                if (!Array.isArray(message.responses))
                    return "responses: array expected";
                for (var i = 0; i < message.responses.length; ++i)
                    if (!$util.isString(message.responses[i]))
                        return "responses: string[] expected";
            }
            return null;
        };

        /**
         * Creates a FcmResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.FcmResponse} FcmResponse
         */
        FcmResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.FcmResponse)
                return object;
            var message = new $root.protobuf.FcmResponse();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.responses) {
                if (!Array.isArray(object.responses))
                    throw TypeError(".protobuf.FcmResponse.responses: array expected");
                message.responses = [];
                for (var i = 0; i < object.responses.length; ++i)
                    message.responses[i] = String(object.responses[i]);
            }
            return message;
        };

        /**
         * Creates a FcmResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.FcmResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.FcmResponse} FcmResponse
         */
        FcmResponse.from = FcmResponse.fromObject;

        /**
         * Creates a plain object from a FcmResponse message. Also converts values to other types if specified.
         * @param {protobuf.FcmResponse} message FcmResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FcmResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.responses = [];
            if (options.defaults)
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.responses && message.responses.length) {
                object.responses = [];
                for (var j = 0; j < message.responses.length; ++j)
                    object.responses[j] = message.responses[j];
            }
            return object;
        };

        /**
         * Creates a plain object from this FcmResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FcmResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this FcmResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        FcmResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FcmResponse;
    })();

    protobuf.AnalysisRequest = (function() {

        /**
         * Properties of an AnalysisRequest.
         * @typedef protobuf.AnalysisRequest$Properties
         * @type {Object}
         * @property {number|Long} [uuid] AnalysisRequest uuid.
         * @property {number} [timeoutSeconds] AnalysisRequest timeoutSeconds.
         * @property {protobuf.WebBasedReportRequest$Properties} [webBasedReportRequest] AnalysisRequest webBasedReportRequest.
         * @property {protobuf.RealTimePlotRequest$Properties} [realTimePlotRequest] AnalysisRequest realTimePlotRequest.
         * @property {protobuf.DataRequest$Properties} [dataRequest] AnalysisRequest dataRequest.
         * @property {protobuf.RealTimePlotRequests$Properties} [realTimePlotRequests] AnalysisRequest realTimePlotRequests.
         * @property {protobuf.RealTimePlotRequestApi900$Properties} [realTimePlotRequestApi900] AnalysisRequest realTimePlotRequestApi900.
         * @property {protobuf.RealTimePlotRequestsApi900$Properties} [realTimePlotRequestsApi900] AnalysisRequest realTimePlotRequestsApi900.
         * @property {protobuf.FcmRequest$Properties} [fcmRequest] AnalysisRequest fcmRequest.
         * @property {string} [authenticatedEmail] AnalysisRequest authenticatedEmail.
         * @property {string} [authenticationToken] AnalysisRequest authenticationToken.
         * @property {number|Long} [timestampEpochSeconds] AnalysisRequest timestampEpochSeconds.
         */

        /**
         * Constructs a new AnalysisRequest.
         * @exports protobuf.AnalysisRequest
         * @constructor
         * @param {protobuf.AnalysisRequest$Properties=} [properties] Properties to set
         */
        function AnalysisRequest(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * AnalysisRequest uuid.
         * @type {number|Long|undefined}
         */
        AnalysisRequest.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * AnalysisRequest timeoutSeconds.
         * @type {number|undefined}
         */
        AnalysisRequest.prototype.timeoutSeconds = 0;

        /**
         * AnalysisRequest webBasedReportRequest.
         * @type {protobuf.WebBasedReportRequest$Properties|undefined}
         */
        AnalysisRequest.prototype.webBasedReportRequest = null;

        /**
         * AnalysisRequest realTimePlotRequest.
         * @type {protobuf.RealTimePlotRequest$Properties|undefined}
         */
        AnalysisRequest.prototype.realTimePlotRequest = null;

        /**
         * AnalysisRequest dataRequest.
         * @type {protobuf.DataRequest$Properties|undefined}
         */
        AnalysisRequest.prototype.dataRequest = null;

        /**
         * AnalysisRequest realTimePlotRequests.
         * @type {protobuf.RealTimePlotRequests$Properties|undefined}
         */
        AnalysisRequest.prototype.realTimePlotRequests = null;

        /**
         * AnalysisRequest realTimePlotRequestApi900.
         * @type {protobuf.RealTimePlotRequestApi900$Properties|undefined}
         */
        AnalysisRequest.prototype.realTimePlotRequestApi900 = null;

        /**
         * AnalysisRequest realTimePlotRequestsApi900.
         * @type {protobuf.RealTimePlotRequestsApi900$Properties|undefined}
         */
        AnalysisRequest.prototype.realTimePlotRequestsApi900 = null;

        /**
         * AnalysisRequest fcmRequest.
         * @type {protobuf.FcmRequest$Properties|undefined}
         */
        AnalysisRequest.prototype.fcmRequest = null;

        /**
         * AnalysisRequest authenticatedEmail.
         * @type {string|undefined}
         */
        AnalysisRequest.prototype.authenticatedEmail = "";

        /**
         * AnalysisRequest authenticationToken.
         * @type {string|undefined}
         */
        AnalysisRequest.prototype.authenticationToken = "";

        /**
         * AnalysisRequest timestampEpochSeconds.
         * @type {number|Long|undefined}
         */
        AnalysisRequest.prototype.timestampEpochSeconds = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * AnalysisRequest request.
         * @name protobuf.AnalysisRequest#request
         * @type {string|undefined}
         */
        Object.defineProperty(AnalysisRequest.prototype, "request", {
            get: $util.oneOfGetter($oneOfFields = ["webBasedReportRequest", "realTimePlotRequest", "dataRequest", "realTimePlotRequests", "realTimePlotRequestApi900", "realTimePlotRequestsApi900", "fcmRequest"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new AnalysisRequest instance using the specified properties.
         * @param {protobuf.AnalysisRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.AnalysisRequest} AnalysisRequest instance
         */
        AnalysisRequest.create = function create(properties) {
            return new AnalysisRequest(properties);
        };

        /**
         * Encodes the specified AnalysisRequest message. Does not implicitly {@link protobuf.AnalysisRequest.verify|verify} messages.
         * @param {protobuf.AnalysisRequest$Properties} message AnalysisRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AnalysisRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.timeoutSeconds != null && message.hasOwnProperty("timeoutSeconds"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.timeoutSeconds);
            if (message.webBasedReportRequest && message.hasOwnProperty("webBasedReportRequest"))
                $root.protobuf.WebBasedReportRequest.encode(message.webBasedReportRequest, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.realTimePlotRequest && message.hasOwnProperty("realTimePlotRequest"))
                $root.protobuf.RealTimePlotRequest.encode(message.realTimePlotRequest, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.dataRequest && message.hasOwnProperty("dataRequest"))
                $root.protobuf.DataRequest.encode(message.dataRequest, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            if (message.realTimePlotRequests && message.hasOwnProperty("realTimePlotRequests"))
                $root.protobuf.RealTimePlotRequests.encode(message.realTimePlotRequests, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            if (message.realTimePlotRequestApi900 && message.hasOwnProperty("realTimePlotRequestApi900"))
                $root.protobuf.RealTimePlotRequestApi900.encode(message.realTimePlotRequestApi900, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            if (message.realTimePlotRequestsApi900 && message.hasOwnProperty("realTimePlotRequestsApi900"))
                $root.protobuf.RealTimePlotRequestsApi900.encode(message.realTimePlotRequestsApi900, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
            if (message.fcmRequest && message.hasOwnProperty("fcmRequest"))
                $root.protobuf.FcmRequest.encode(message.fcmRequest, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                writer.uint32(/* id 20, wireType 2 =*/162).string(message.authenticatedEmail);
            if (message.authenticationToken != null && message.hasOwnProperty("authenticationToken"))
                writer.uint32(/* id 21, wireType 2 =*/170).string(message.authenticationToken);
            if (message.timestampEpochSeconds != null && message.hasOwnProperty("timestampEpochSeconds"))
                writer.uint32(/* id 22, wireType 0 =*/176).uint64(message.timestampEpochSeconds);
            return writer;
        };

        /**
         * Encodes the specified AnalysisRequest message, length delimited. Does not implicitly {@link protobuf.AnalysisRequest.verify|verify} messages.
         * @param {protobuf.AnalysisRequest$Properties} message AnalysisRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AnalysisRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an AnalysisRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.AnalysisRequest} AnalysisRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AnalysisRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.AnalysisRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.timeoutSeconds = reader.uint32();
                    break;
                case 3:
                    message.webBasedReportRequest = $root.protobuf.WebBasedReportRequest.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.realTimePlotRequest = $root.protobuf.RealTimePlotRequest.decode(reader, reader.uint32());
                    break;
                case 5:
                    message.dataRequest = $root.protobuf.DataRequest.decode(reader, reader.uint32());
                    break;
                case 6:
                    message.realTimePlotRequests = $root.protobuf.RealTimePlotRequests.decode(reader, reader.uint32());
                    break;
                case 7:
                    message.realTimePlotRequestApi900 = $root.protobuf.RealTimePlotRequestApi900.decode(reader, reader.uint32());
                    break;
                case 8:
                    message.realTimePlotRequestsApi900 = $root.protobuf.RealTimePlotRequestsApi900.decode(reader, reader.uint32());
                    break;
                case 9:
                    message.fcmRequest = $root.protobuf.FcmRequest.decode(reader, reader.uint32());
                    break;
                case 20:
                    message.authenticatedEmail = reader.string();
                    break;
                case 21:
                    message.authenticationToken = reader.string();
                    break;
                case 22:
                    message.timestampEpochSeconds = reader.uint64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an AnalysisRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.AnalysisRequest} AnalysisRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AnalysisRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an AnalysisRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        AnalysisRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.timeoutSeconds != null)
                if (!$util.isInteger(message.timeoutSeconds))
                    return "timeoutSeconds: integer expected";
            if (message.webBasedReportRequest != null) {
                properties.request = 1;
                var error = $root.protobuf.WebBasedReportRequest.verify(message.webBasedReportRequest);
                if (error)
                    return "webBasedReportRequest." + error;
            }
            if (message.realTimePlotRequest != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.RealTimePlotRequest.verify(message.realTimePlotRequest);
                if (error)
                    return "realTimePlotRequest." + error;
            }
            if (message.dataRequest != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.DataRequest.verify(message.dataRequest);
                if (error)
                    return "dataRequest." + error;
            }
            if (message.realTimePlotRequests != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.RealTimePlotRequests.verify(message.realTimePlotRequests);
                if (error)
                    return "realTimePlotRequests." + error;
            }
            if (message.realTimePlotRequestApi900 != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.RealTimePlotRequestApi900.verify(message.realTimePlotRequestApi900);
                if (error)
                    return "realTimePlotRequestApi900." + error;
            }
            if (message.realTimePlotRequestsApi900 != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.RealTimePlotRequestsApi900.verify(message.realTimePlotRequestsApi900);
                if (error)
                    return "realTimePlotRequestsApi900." + error;
            }
            if (message.fcmRequest != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.FcmRequest.verify(message.fcmRequest);
                if (error)
                    return "fcmRequest." + error;
            }
            if (message.authenticatedEmail != null)
                if (!$util.isString(message.authenticatedEmail))
                    return "authenticatedEmail: string expected";
            if (message.authenticationToken != null)
                if (!$util.isString(message.authenticationToken))
                    return "authenticationToken: string expected";
            if (message.timestampEpochSeconds != null)
                if (!$util.isInteger(message.timestampEpochSeconds) && !(message.timestampEpochSeconds && $util.isInteger(message.timestampEpochSeconds.low) && $util.isInteger(message.timestampEpochSeconds.high)))
                    return "timestampEpochSeconds: integer|Long expected";
            return null;
        };

        /**
         * Creates an AnalysisRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.AnalysisRequest} AnalysisRequest
         */
        AnalysisRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.AnalysisRequest)
                return object;
            var message = new $root.protobuf.AnalysisRequest();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.timeoutSeconds != null)
                message.timeoutSeconds = object.timeoutSeconds >>> 0;
            if (object.webBasedReportRequest != null) {
                if (typeof object.webBasedReportRequest !== "object")
                    throw TypeError(".protobuf.AnalysisRequest.webBasedReportRequest: object expected");
                message.webBasedReportRequest = $root.protobuf.WebBasedReportRequest.fromObject(object.webBasedReportRequest);
            }
            if (object.realTimePlotRequest != null) {
                if (typeof object.realTimePlotRequest !== "object")
                    throw TypeError(".protobuf.AnalysisRequest.realTimePlotRequest: object expected");
                message.realTimePlotRequest = $root.protobuf.RealTimePlotRequest.fromObject(object.realTimePlotRequest);
            }
            if (object.dataRequest != null) {
                if (typeof object.dataRequest !== "object")
                    throw TypeError(".protobuf.AnalysisRequest.dataRequest: object expected");
                message.dataRequest = $root.protobuf.DataRequest.fromObject(object.dataRequest);
            }
            if (object.realTimePlotRequests != null) {
                if (typeof object.realTimePlotRequests !== "object")
                    throw TypeError(".protobuf.AnalysisRequest.realTimePlotRequests: object expected");
                message.realTimePlotRequests = $root.protobuf.RealTimePlotRequests.fromObject(object.realTimePlotRequests);
            }
            if (object.realTimePlotRequestApi900 != null) {
                if (typeof object.realTimePlotRequestApi900 !== "object")
                    throw TypeError(".protobuf.AnalysisRequest.realTimePlotRequestApi900: object expected");
                message.realTimePlotRequestApi900 = $root.protobuf.RealTimePlotRequestApi900.fromObject(object.realTimePlotRequestApi900);
            }
            if (object.realTimePlotRequestsApi900 != null) {
                if (typeof object.realTimePlotRequestsApi900 !== "object")
                    throw TypeError(".protobuf.AnalysisRequest.realTimePlotRequestsApi900: object expected");
                message.realTimePlotRequestsApi900 = $root.protobuf.RealTimePlotRequestsApi900.fromObject(object.realTimePlotRequestsApi900);
            }
            if (object.fcmRequest != null) {
                if (typeof object.fcmRequest !== "object")
                    throw TypeError(".protobuf.AnalysisRequest.fcmRequest: object expected");
                message.fcmRequest = $root.protobuf.FcmRequest.fromObject(object.fcmRequest);
            }
            if (object.authenticatedEmail != null)
                message.authenticatedEmail = String(object.authenticatedEmail);
            if (object.authenticationToken != null)
                message.authenticationToken = String(object.authenticationToken);
            if (object.timestampEpochSeconds != null)
                if ($util.Long)
                    (message.timestampEpochSeconds = $util.Long.fromValue(object.timestampEpochSeconds)).unsigned = true;
                else if (typeof object.timestampEpochSeconds === "string")
                    message.timestampEpochSeconds = parseInt(object.timestampEpochSeconds, 10);
                else if (typeof object.timestampEpochSeconds === "number")
                    message.timestampEpochSeconds = object.timestampEpochSeconds;
                else if (typeof object.timestampEpochSeconds === "object")
                    message.timestampEpochSeconds = new $util.LongBits(object.timestampEpochSeconds.low >>> 0, object.timestampEpochSeconds.high >>> 0).toNumber(true);
            return message;
        };

        /**
         * Creates an AnalysisRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.AnalysisRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.AnalysisRequest} AnalysisRequest
         */
        AnalysisRequest.from = AnalysisRequest.fromObject;

        /**
         * Creates a plain object from an AnalysisRequest message. Also converts values to other types if specified.
         * @param {protobuf.AnalysisRequest} message AnalysisRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        AnalysisRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.timeoutSeconds = 0;
                object.authenticatedEmail = "";
                object.authenticationToken = "";
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.timestampEpochSeconds = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestampEpochSeconds = options.longs === String ? "0" : 0;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.timeoutSeconds != null && message.hasOwnProperty("timeoutSeconds"))
                object.timeoutSeconds = message.timeoutSeconds;
            if (message.webBasedReportRequest != null && message.hasOwnProperty("webBasedReportRequest")) {
                object.webBasedReportRequest = $root.protobuf.WebBasedReportRequest.toObject(message.webBasedReportRequest, options);
                if (options.oneofs)
                    object.request = "webBasedReportRequest";
            }
            if (message.realTimePlotRequest != null && message.hasOwnProperty("realTimePlotRequest")) {
                object.realTimePlotRequest = $root.protobuf.RealTimePlotRequest.toObject(message.realTimePlotRequest, options);
                if (options.oneofs)
                    object.request = "realTimePlotRequest";
            }
            if (message.dataRequest != null && message.hasOwnProperty("dataRequest")) {
                object.dataRequest = $root.protobuf.DataRequest.toObject(message.dataRequest, options);
                if (options.oneofs)
                    object.request = "dataRequest";
            }
            if (message.realTimePlotRequests != null && message.hasOwnProperty("realTimePlotRequests")) {
                object.realTimePlotRequests = $root.protobuf.RealTimePlotRequests.toObject(message.realTimePlotRequests, options);
                if (options.oneofs)
                    object.request = "realTimePlotRequests";
            }
            if (message.realTimePlotRequestApi900 != null && message.hasOwnProperty("realTimePlotRequestApi900")) {
                object.realTimePlotRequestApi900 = $root.protobuf.RealTimePlotRequestApi900.toObject(message.realTimePlotRequestApi900, options);
                if (options.oneofs)
                    object.request = "realTimePlotRequestApi900";
            }
            if (message.realTimePlotRequestsApi900 != null && message.hasOwnProperty("realTimePlotRequestsApi900")) {
                object.realTimePlotRequestsApi900 = $root.protobuf.RealTimePlotRequestsApi900.toObject(message.realTimePlotRequestsApi900, options);
                if (options.oneofs)
                    object.request = "realTimePlotRequestsApi900";
            }
            if (message.fcmRequest != null && message.hasOwnProperty("fcmRequest")) {
                object.fcmRequest = $root.protobuf.FcmRequest.toObject(message.fcmRequest, options);
                if (options.oneofs)
                    object.request = "fcmRequest";
            }
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                object.authenticatedEmail = message.authenticatedEmail;
            if (message.authenticationToken != null && message.hasOwnProperty("authenticationToken"))
                object.authenticationToken = message.authenticationToken;
            if (message.timestampEpochSeconds != null && message.hasOwnProperty("timestampEpochSeconds"))
                if (typeof message.timestampEpochSeconds === "number")
                    object.timestampEpochSeconds = options.longs === String ? String(message.timestampEpochSeconds) : message.timestampEpochSeconds;
                else
                    object.timestampEpochSeconds = options.longs === String ? $util.Long.prototype.toString.call(message.timestampEpochSeconds) : options.longs === Number ? new $util.LongBits(message.timestampEpochSeconds.low >>> 0, message.timestampEpochSeconds.high >>> 0).toNumber(true) : message.timestampEpochSeconds;
            return object;
        };

        /**
         * Creates a plain object from this AnalysisRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        AnalysisRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this AnalysisRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        AnalysisRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return AnalysisRequest;
    })();

    protobuf.AnalysisResponse = (function() {

        /**
         * Properties of an AnalysisResponse.
         * @typedef protobuf.AnalysisResponse$Properties
         * @type {Object}
         * @property {number|Long} [uuid] AnalysisResponse uuid.
         * @property {protobuf.WebBasedReportResponse$Properties} [webBasedReportResponse] AnalysisResponse webBasedReportResponse.
         * @property {protobuf.RealTimePlotResponse$Properties} [realTimePlotResponse] AnalysisResponse realTimePlotResponse.
         * @property {protobuf.ProgressUpdate$Properties} [progressUpdate] AnalysisResponse progressUpdate.
         * @property {protobuf.DataResponse$Properties} [dataResponse] AnalysisResponse dataResponse.
         * @property {protobuf.RealTimePlotResponseApi900$Properties} [realTimePlotResponseApi900] AnalysisResponse realTimePlotResponseApi900.
         * @property {protobuf.FcmResponse$Properties} [fcmResponse] AnalysisResponse fcmResponse.
         * @property {protobuf.AnalysisRequest$Properties} [originalRequest] AnalysisResponse originalRequest.
         * @property {number|Long} [timestampEpochSeconds] AnalysisResponse timestampEpochSeconds.
         */

        /**
         * Constructs a new AnalysisResponse.
         * @exports protobuf.AnalysisResponse
         * @constructor
         * @param {protobuf.AnalysisResponse$Properties=} [properties] Properties to set
         */
        function AnalysisResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * AnalysisResponse uuid.
         * @type {number|Long|undefined}
         */
        AnalysisResponse.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * AnalysisResponse webBasedReportResponse.
         * @type {protobuf.WebBasedReportResponse$Properties|undefined}
         */
        AnalysisResponse.prototype.webBasedReportResponse = null;

        /**
         * AnalysisResponse realTimePlotResponse.
         * @type {protobuf.RealTimePlotResponse$Properties|undefined}
         */
        AnalysisResponse.prototype.realTimePlotResponse = null;

        /**
         * AnalysisResponse progressUpdate.
         * @type {protobuf.ProgressUpdate$Properties|undefined}
         */
        AnalysisResponse.prototype.progressUpdate = null;

        /**
         * AnalysisResponse dataResponse.
         * @type {protobuf.DataResponse$Properties|undefined}
         */
        AnalysisResponse.prototype.dataResponse = null;

        /**
         * AnalysisResponse realTimePlotResponseApi900.
         * @type {protobuf.RealTimePlotResponseApi900$Properties|undefined}
         */
        AnalysisResponse.prototype.realTimePlotResponseApi900 = null;

        /**
         * AnalysisResponse fcmResponse.
         * @type {protobuf.FcmResponse$Properties|undefined}
         */
        AnalysisResponse.prototype.fcmResponse = null;

        /**
         * AnalysisResponse originalRequest.
         * @type {protobuf.AnalysisRequest$Properties|undefined}
         */
        AnalysisResponse.prototype.originalRequest = null;

        /**
         * AnalysisResponse timestampEpochSeconds.
         * @type {number|Long|undefined}
         */
        AnalysisResponse.prototype.timestampEpochSeconds = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * AnalysisResponse response.
         * @name protobuf.AnalysisResponse#response
         * @type {string|undefined}
         */
        Object.defineProperty(AnalysisResponse.prototype, "response", {
            get: $util.oneOfGetter($oneOfFields = ["webBasedReportResponse", "realTimePlotResponse", "progressUpdate", "dataResponse", "realTimePlotResponseApi900", "fcmResponse"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new AnalysisResponse instance using the specified properties.
         * @param {protobuf.AnalysisResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.AnalysisResponse} AnalysisResponse instance
         */
        AnalysisResponse.create = function create(properties) {
            return new AnalysisResponse(properties);
        };

        /**
         * Encodes the specified AnalysisResponse message. Does not implicitly {@link protobuf.AnalysisResponse.verify|verify} messages.
         * @param {protobuf.AnalysisResponse$Properties} message AnalysisResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AnalysisResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.webBasedReportResponse && message.hasOwnProperty("webBasedReportResponse"))
                $root.protobuf.WebBasedReportResponse.encode(message.webBasedReportResponse, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.realTimePlotResponse && message.hasOwnProperty("realTimePlotResponse"))
                $root.protobuf.RealTimePlotResponse.encode(message.realTimePlotResponse, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.progressUpdate && message.hasOwnProperty("progressUpdate"))
                $root.protobuf.ProgressUpdate.encode(message.progressUpdate, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.dataResponse && message.hasOwnProperty("dataResponse"))
                $root.protobuf.DataResponse.encode(message.dataResponse, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            if (message.realTimePlotResponseApi900 && message.hasOwnProperty("realTimePlotResponseApi900"))
                $root.protobuf.RealTimePlotResponseApi900.encode(message.realTimePlotResponseApi900, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            if (message.fcmResponse && message.hasOwnProperty("fcmResponse"))
                $root.protobuf.FcmResponse.encode(message.fcmResponse, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            if (message.originalRequest && message.hasOwnProperty("originalRequest"))
                $root.protobuf.AnalysisRequest.encode(message.originalRequest, writer.uint32(/* id 20, wireType 2 =*/162).fork()).ldelim();
            if (message.timestampEpochSeconds != null && message.hasOwnProperty("timestampEpochSeconds"))
                writer.uint32(/* id 21, wireType 0 =*/168).uint64(message.timestampEpochSeconds);
            return writer;
        };

        /**
         * Encodes the specified AnalysisResponse message, length delimited. Does not implicitly {@link protobuf.AnalysisResponse.verify|verify} messages.
         * @param {protobuf.AnalysisResponse$Properties} message AnalysisResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AnalysisResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an AnalysisResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.AnalysisResponse} AnalysisResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AnalysisResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.AnalysisResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.webBasedReportResponse = $root.protobuf.WebBasedReportResponse.decode(reader, reader.uint32());
                    break;
                case 3:
                    message.realTimePlotResponse = $root.protobuf.RealTimePlotResponse.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.progressUpdate = $root.protobuf.ProgressUpdate.decode(reader, reader.uint32());
                    break;
                case 5:
                    message.dataResponse = $root.protobuf.DataResponse.decode(reader, reader.uint32());
                    break;
                case 6:
                    message.realTimePlotResponseApi900 = $root.protobuf.RealTimePlotResponseApi900.decode(reader, reader.uint32());
                    break;
                case 7:
                    message.fcmResponse = $root.protobuf.FcmResponse.decode(reader, reader.uint32());
                    break;
                case 20:
                    message.originalRequest = $root.protobuf.AnalysisRequest.decode(reader, reader.uint32());
                    break;
                case 21:
                    message.timestampEpochSeconds = reader.uint64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an AnalysisResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.AnalysisResponse} AnalysisResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AnalysisResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an AnalysisResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        AnalysisResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.webBasedReportResponse != null) {
                properties.response = 1;
                var error = $root.protobuf.WebBasedReportResponse.verify(message.webBasedReportResponse);
                if (error)
                    return "webBasedReportResponse." + error;
            }
            if (message.realTimePlotResponse != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.RealTimePlotResponse.verify(message.realTimePlotResponse);
                if (error)
                    return "realTimePlotResponse." + error;
            }
            if (message.progressUpdate != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.ProgressUpdate.verify(message.progressUpdate);
                if (error)
                    return "progressUpdate." + error;
            }
            if (message.dataResponse != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.DataResponse.verify(message.dataResponse);
                if (error)
                    return "dataResponse." + error;
            }
            if (message.realTimePlotResponseApi900 != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.RealTimePlotResponseApi900.verify(message.realTimePlotResponseApi900);
                if (error)
                    return "realTimePlotResponseApi900." + error;
            }
            if (message.fcmResponse != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.FcmResponse.verify(message.fcmResponse);
                if (error)
                    return "fcmResponse." + error;
            }
            if (message.originalRequest != null) {
                var error = $root.protobuf.AnalysisRequest.verify(message.originalRequest);
                if (error)
                    return "originalRequest." + error;
            }
            if (message.timestampEpochSeconds != null)
                if (!$util.isInteger(message.timestampEpochSeconds) && !(message.timestampEpochSeconds && $util.isInteger(message.timestampEpochSeconds.low) && $util.isInteger(message.timestampEpochSeconds.high)))
                    return "timestampEpochSeconds: integer|Long expected";
            return null;
        };

        /**
         * Creates an AnalysisResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.AnalysisResponse} AnalysisResponse
         */
        AnalysisResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.AnalysisResponse)
                return object;
            var message = new $root.protobuf.AnalysisResponse();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.webBasedReportResponse != null) {
                if (typeof object.webBasedReportResponse !== "object")
                    throw TypeError(".protobuf.AnalysisResponse.webBasedReportResponse: object expected");
                message.webBasedReportResponse = $root.protobuf.WebBasedReportResponse.fromObject(object.webBasedReportResponse);
            }
            if (object.realTimePlotResponse != null) {
                if (typeof object.realTimePlotResponse !== "object")
                    throw TypeError(".protobuf.AnalysisResponse.realTimePlotResponse: object expected");
                message.realTimePlotResponse = $root.protobuf.RealTimePlotResponse.fromObject(object.realTimePlotResponse);
            }
            if (object.progressUpdate != null) {
                if (typeof object.progressUpdate !== "object")
                    throw TypeError(".protobuf.AnalysisResponse.progressUpdate: object expected");
                message.progressUpdate = $root.protobuf.ProgressUpdate.fromObject(object.progressUpdate);
            }
            if (object.dataResponse != null) {
                if (typeof object.dataResponse !== "object")
                    throw TypeError(".protobuf.AnalysisResponse.dataResponse: object expected");
                message.dataResponse = $root.protobuf.DataResponse.fromObject(object.dataResponse);
            }
            if (object.realTimePlotResponseApi900 != null) {
                if (typeof object.realTimePlotResponseApi900 !== "object")
                    throw TypeError(".protobuf.AnalysisResponse.realTimePlotResponseApi900: object expected");
                message.realTimePlotResponseApi900 = $root.protobuf.RealTimePlotResponseApi900.fromObject(object.realTimePlotResponseApi900);
            }
            if (object.fcmResponse != null) {
                if (typeof object.fcmResponse !== "object")
                    throw TypeError(".protobuf.AnalysisResponse.fcmResponse: object expected");
                message.fcmResponse = $root.protobuf.FcmResponse.fromObject(object.fcmResponse);
            }
            if (object.originalRequest != null) {
                if (typeof object.originalRequest !== "object")
                    throw TypeError(".protobuf.AnalysisResponse.originalRequest: object expected");
                message.originalRequest = $root.protobuf.AnalysisRequest.fromObject(object.originalRequest);
            }
            if (object.timestampEpochSeconds != null)
                if ($util.Long)
                    (message.timestampEpochSeconds = $util.Long.fromValue(object.timestampEpochSeconds)).unsigned = true;
                else if (typeof object.timestampEpochSeconds === "string")
                    message.timestampEpochSeconds = parseInt(object.timestampEpochSeconds, 10);
                else if (typeof object.timestampEpochSeconds === "number")
                    message.timestampEpochSeconds = object.timestampEpochSeconds;
                else if (typeof object.timestampEpochSeconds === "object")
                    message.timestampEpochSeconds = new $util.LongBits(object.timestampEpochSeconds.low >>> 0, object.timestampEpochSeconds.high >>> 0).toNumber(true);
            return message;
        };

        /**
         * Creates an AnalysisResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.AnalysisResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.AnalysisResponse} AnalysisResponse
         */
        AnalysisResponse.from = AnalysisResponse.fromObject;

        /**
         * Creates a plain object from an AnalysisResponse message. Also converts values to other types if specified.
         * @param {protobuf.AnalysisResponse} message AnalysisResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        AnalysisResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.originalRequest = null;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.timestampEpochSeconds = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestampEpochSeconds = options.longs === String ? "0" : 0;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.webBasedReportResponse != null && message.hasOwnProperty("webBasedReportResponse")) {
                object.webBasedReportResponse = $root.protobuf.WebBasedReportResponse.toObject(message.webBasedReportResponse, options);
                if (options.oneofs)
                    object.response = "webBasedReportResponse";
            }
            if (message.realTimePlotResponse != null && message.hasOwnProperty("realTimePlotResponse")) {
                object.realTimePlotResponse = $root.protobuf.RealTimePlotResponse.toObject(message.realTimePlotResponse, options);
                if (options.oneofs)
                    object.response = "realTimePlotResponse";
            }
            if (message.progressUpdate != null && message.hasOwnProperty("progressUpdate")) {
                object.progressUpdate = $root.protobuf.ProgressUpdate.toObject(message.progressUpdate, options);
                if (options.oneofs)
                    object.response = "progressUpdate";
            }
            if (message.dataResponse != null && message.hasOwnProperty("dataResponse")) {
                object.dataResponse = $root.protobuf.DataResponse.toObject(message.dataResponse, options);
                if (options.oneofs)
                    object.response = "dataResponse";
            }
            if (message.realTimePlotResponseApi900 != null && message.hasOwnProperty("realTimePlotResponseApi900")) {
                object.realTimePlotResponseApi900 = $root.protobuf.RealTimePlotResponseApi900.toObject(message.realTimePlotResponseApi900, options);
                if (options.oneofs)
                    object.response = "realTimePlotResponseApi900";
            }
            if (message.fcmResponse != null && message.hasOwnProperty("fcmResponse")) {
                object.fcmResponse = $root.protobuf.FcmResponse.toObject(message.fcmResponse, options);
                if (options.oneofs)
                    object.response = "fcmResponse";
            }
            if (message.originalRequest != null && message.hasOwnProperty("originalRequest"))
                object.originalRequest = $root.protobuf.AnalysisRequest.toObject(message.originalRequest, options);
            if (message.timestampEpochSeconds != null && message.hasOwnProperty("timestampEpochSeconds"))
                if (typeof message.timestampEpochSeconds === "number")
                    object.timestampEpochSeconds = options.longs === String ? String(message.timestampEpochSeconds) : message.timestampEpochSeconds;
                else
                    object.timestampEpochSeconds = options.longs === String ? $util.Long.prototype.toString.call(message.timestampEpochSeconds) : options.longs === Number ? new $util.LongBits(message.timestampEpochSeconds.low >>> 0, message.timestampEpochSeconds.high >>> 0).toNumber(true) : message.timestampEpochSeconds;
            return object;
        };

        /**
         * Creates a plain object from this AnalysisResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        AnalysisResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this AnalysisResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        AnalysisResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return AnalysisResponse;
    })();

    return protobuf;
})();

var redvoxpb = protobuf.roots["redvoxanalysis"]["protobuf"];
