/*eslint-disable block-scoped-var, no-redeclare, no-control-regex, no-prototype-builtins*/
var $protobuf = protobuf;

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots.redvoxweb || ($protobuf.roots.redvoxweb = {});

$root.protobuf = (function() {

    /**
     * Namespace protobuf.
     * @exports protobuf
     * @namespace
     */
    var protobuf = {};

    protobuf.RedvoxWebRequest = (function() {

        /**
         * Properties of a RedvoxWebRequest.
         * @typedef protobuf.RedvoxWebRequest$Properties
         * @type {Object}
         * @property {number|Long} [uuid] RedvoxWebRequest uuid.
         * @property {number} [timeoutSeconds] RedvoxWebRequest timeoutSeconds.
         * @property {string} [requestee] RedvoxWebRequest requestee.
         * @property {protobuf.MetadataApi800Request$Properties} [metadataApi800Request] RedvoxWebRequest metadataApi800Request.
         * @property {protobuf.DeviceActivityRequest$Properties} [deviceActivityRequest] RedvoxWebRequest deviceActivityRequest.
         * @property {protobuf.MetadataApi900Request$Properties} [metadataApi900Request] RedvoxWebRequest metadataApi900Request.
         * @property {protobuf.DetailsApi900Request$Properties} [detailsApi900Request] RedvoxWebRequest detailsApi900Request.
         * @property {protobuf.ActiveDevicesRequest$Properties} [activeDevicesRequest] RedvoxWebRequest activeDevicesRequest.
         * @property {protobuf.HistoricalDevicesRequest$Properties} [historicalDevicesRequest] RedvoxWebRequest historicalDevicesRequest.
         * @property {protobuf.RdvxzS3Request$Properties} [rdvxzS3Request] RedvoxWebRequest rdvxzS3Request.
         * @property {protobuf.RdvxJsonRequest$Properties} [rdvxJsonRequest] RedvoxWebRequest rdvxJsonRequest.
         * @property {string} [jsonWebToken] RedvoxWebRequest jsonWebToken.
         * @property {string} [authenticatedEmail] RedvoxWebRequest authenticatedEmail.
         */

        /**
         * Constructs a new RedvoxWebRequest.
         * @exports protobuf.RedvoxWebRequest
         * @constructor
         * @param {protobuf.RedvoxWebRequest$Properties=} [properties] Properties to set
         */
        function RedvoxWebRequest(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RedvoxWebRequest uuid.
         * @type {number|Long|undefined}
         */
        RedvoxWebRequest.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RedvoxWebRequest timeoutSeconds.
         * @type {number|undefined}
         */
        RedvoxWebRequest.prototype.timeoutSeconds = 0;

        /**
         * RedvoxWebRequest requestee.
         * @type {string|undefined}
         */
        RedvoxWebRequest.prototype.requestee = "";

        /**
         * RedvoxWebRequest metadataApi800Request.
         * @type {protobuf.MetadataApi800Request$Properties|undefined}
         */
        RedvoxWebRequest.prototype.metadataApi800Request = null;

        /**
         * RedvoxWebRequest deviceActivityRequest.
         * @type {protobuf.DeviceActivityRequest$Properties|undefined}
         */
        RedvoxWebRequest.prototype.deviceActivityRequest = null;

        /**
         * RedvoxWebRequest metadataApi900Request.
         * @type {protobuf.MetadataApi900Request$Properties|undefined}
         */
        RedvoxWebRequest.prototype.metadataApi900Request = null;

        /**
         * RedvoxWebRequest detailsApi900Request.
         * @type {protobuf.DetailsApi900Request$Properties|undefined}
         */
        RedvoxWebRequest.prototype.detailsApi900Request = null;

        /**
         * RedvoxWebRequest activeDevicesRequest.
         * @type {protobuf.ActiveDevicesRequest$Properties|undefined}
         */
        RedvoxWebRequest.prototype.activeDevicesRequest = null;

        /**
         * RedvoxWebRequest historicalDevicesRequest.
         * @type {protobuf.HistoricalDevicesRequest$Properties|undefined}
         */
        RedvoxWebRequest.prototype.historicalDevicesRequest = null;

        /**
         * RedvoxWebRequest rdvxzS3Request.
         * @type {protobuf.RdvxzS3Request$Properties|undefined}
         */
        RedvoxWebRequest.prototype.rdvxzS3Request = null;

        /**
         * RedvoxWebRequest rdvxJsonRequest.
         * @type {protobuf.RdvxJsonRequest$Properties|undefined}
         */
        RedvoxWebRequest.prototype.rdvxJsonRequest = null;

        /**
         * RedvoxWebRequest jsonWebToken.
         * @type {string|undefined}
         */
        RedvoxWebRequest.prototype.jsonWebToken = "";

        /**
         * RedvoxWebRequest authenticatedEmail.
         * @type {string|undefined}
         */
        RedvoxWebRequest.prototype.authenticatedEmail = "";

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * RedvoxWebRequest request.
         * @name protobuf.RedvoxWebRequest#request
         * @type {string|undefined}
         */
        Object.defineProperty(RedvoxWebRequest.prototype, "request", {
            get: $util.oneOfGetter($oneOfFields = ["metadataApi800Request", "deviceActivityRequest", "metadataApi900Request", "detailsApi900Request", "activeDevicesRequest", "historicalDevicesRequest", "rdvxzS3Request", "rdvxJsonRequest"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new RedvoxWebRequest instance using the specified properties.
         * @param {protobuf.RedvoxWebRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.RedvoxWebRequest} RedvoxWebRequest instance
         */
        RedvoxWebRequest.create = function create(properties) {
            return new RedvoxWebRequest(properties);
        };

        /**
         * Encodes the specified RedvoxWebRequest message. Does not implicitly {@link protobuf.RedvoxWebRequest.verify|verify} messages.
         * @param {protobuf.RedvoxWebRequest$Properties} message RedvoxWebRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RedvoxWebRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.timeoutSeconds != null && message.hasOwnProperty("timeoutSeconds"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.timeoutSeconds);
            if (message.requestee != null && message.hasOwnProperty("requestee"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.requestee);
            if (message.metadataApi800Request && message.hasOwnProperty("metadataApi800Request"))
                $root.protobuf.MetadataApi800Request.encode(message.metadataApi800Request, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.deviceActivityRequest && message.hasOwnProperty("deviceActivityRequest"))
                $root.protobuf.DeviceActivityRequest.encode(message.deviceActivityRequest, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            if (message.metadataApi900Request && message.hasOwnProperty("metadataApi900Request"))
                $root.protobuf.MetadataApi900Request.encode(message.metadataApi900Request, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            if (message.detailsApi900Request && message.hasOwnProperty("detailsApi900Request"))
                $root.protobuf.DetailsApi900Request.encode(message.detailsApi900Request, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            if (message.activeDevicesRequest && message.hasOwnProperty("activeDevicesRequest"))
                $root.protobuf.ActiveDevicesRequest.encode(message.activeDevicesRequest, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
            if (message.historicalDevicesRequest && message.hasOwnProperty("historicalDevicesRequest"))
                $root.protobuf.HistoricalDevicesRequest.encode(message.historicalDevicesRequest, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
            if (message.jsonWebToken != null && message.hasOwnProperty("jsonWebToken"))
                writer.uint32(/* id 10, wireType 2 =*/82).string(message.jsonWebToken);
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                writer.uint32(/* id 11, wireType 2 =*/90).string(message.authenticatedEmail);
            if (message.rdvxzS3Request && message.hasOwnProperty("rdvxzS3Request"))
                $root.protobuf.RdvxzS3Request.encode(message.rdvxzS3Request, writer.uint32(/* id 12, wireType 2 =*/98).fork()).ldelim();
            if (message.rdvxJsonRequest && message.hasOwnProperty("rdvxJsonRequest"))
                $root.protobuf.RdvxJsonRequest.encode(message.rdvxJsonRequest, writer.uint32(/* id 13, wireType 2 =*/106).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified RedvoxWebRequest message, length delimited. Does not implicitly {@link protobuf.RedvoxWebRequest.verify|verify} messages.
         * @param {protobuf.RedvoxWebRequest$Properties} message RedvoxWebRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RedvoxWebRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RedvoxWebRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RedvoxWebRequest} RedvoxWebRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RedvoxWebRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RedvoxWebRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.timeoutSeconds = reader.uint32();
                    break;
                case 3:
                    message.requestee = reader.string();
                    break;
                case 4:
                    message.metadataApi800Request = $root.protobuf.MetadataApi800Request.decode(reader, reader.uint32());
                    break;
                case 5:
                    message.deviceActivityRequest = $root.protobuf.DeviceActivityRequest.decode(reader, reader.uint32());
                    break;
                case 6:
                    message.metadataApi900Request = $root.protobuf.MetadataApi900Request.decode(reader, reader.uint32());
                    break;
                case 7:
                    message.detailsApi900Request = $root.protobuf.DetailsApi900Request.decode(reader, reader.uint32());
                    break;
                case 8:
                    message.activeDevicesRequest = $root.protobuf.ActiveDevicesRequest.decode(reader, reader.uint32());
                    break;
                case 9:
                    message.historicalDevicesRequest = $root.protobuf.HistoricalDevicesRequest.decode(reader, reader.uint32());
                    break;
                case 12:
                    message.rdvxzS3Request = $root.protobuf.RdvxzS3Request.decode(reader, reader.uint32());
                    break;
                case 13:
                    message.rdvxJsonRequest = $root.protobuf.RdvxJsonRequest.decode(reader, reader.uint32());
                    break;
                case 10:
                    message.jsonWebToken = reader.string();
                    break;
                case 11:
                    message.authenticatedEmail = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RedvoxWebRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RedvoxWebRequest} RedvoxWebRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RedvoxWebRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RedvoxWebRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RedvoxWebRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.timeoutSeconds != null)
                if (!$util.isInteger(message.timeoutSeconds))
                    return "timeoutSeconds: integer expected";
            if (message.requestee != null)
                if (!$util.isString(message.requestee))
                    return "requestee: string expected";
            if (message.metadataApi800Request != null) {
                properties.request = 1;
                var error = $root.protobuf.MetadataApi800Request.verify(message.metadataApi800Request);
                if (error)
                    return "metadataApi800Request." + error;
            }
            if (message.deviceActivityRequest != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.DeviceActivityRequest.verify(message.deviceActivityRequest);
                if (error)
                    return "deviceActivityRequest." + error;
            }
            if (message.metadataApi900Request != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.MetadataApi900Request.verify(message.metadataApi900Request);
                if (error)
                    return "metadataApi900Request." + error;
            }
            if (message.detailsApi900Request != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.DetailsApi900Request.verify(message.detailsApi900Request);
                if (error)
                    return "detailsApi900Request." + error;
            }
            if (message.activeDevicesRequest != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.ActiveDevicesRequest.verify(message.activeDevicesRequest);
                if (error)
                    return "activeDevicesRequest." + error;
            }
            if (message.historicalDevicesRequest != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.HistoricalDevicesRequest.verify(message.historicalDevicesRequest);
                if (error)
                    return "historicalDevicesRequest." + error;
            }
            if (message.rdvxzS3Request != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.RdvxzS3Request.verify(message.rdvxzS3Request);
                if (error)
                    return "rdvxzS3Request." + error;
            }
            if (message.rdvxJsonRequest != null) {
                if (properties.request === 1)
                    return "request: multiple values";
                properties.request = 1;
                var error = $root.protobuf.RdvxJsonRequest.verify(message.rdvxJsonRequest);
                if (error)
                    return "rdvxJsonRequest." + error;
            }
            if (message.jsonWebToken != null)
                if (!$util.isString(message.jsonWebToken))
                    return "jsonWebToken: string expected";
            if (message.authenticatedEmail != null)
                if (!$util.isString(message.authenticatedEmail))
                    return "authenticatedEmail: string expected";
            return null;
        };

        /**
         * Creates a RedvoxWebRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RedvoxWebRequest} RedvoxWebRequest
         */
        RedvoxWebRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RedvoxWebRequest)
                return object;
            var message = new $root.protobuf.RedvoxWebRequest();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.timeoutSeconds != null)
                message.timeoutSeconds = object.timeoutSeconds >>> 0;
            if (object.requestee != null)
                message.requestee = String(object.requestee);
            if (object.metadataApi800Request != null) {
                if (typeof object.metadataApi800Request !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.metadataApi800Request: object expected");
                message.metadataApi800Request = $root.protobuf.MetadataApi800Request.fromObject(object.metadataApi800Request);
            }
            if (object.deviceActivityRequest != null) {
                if (typeof object.deviceActivityRequest !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.deviceActivityRequest: object expected");
                message.deviceActivityRequest = $root.protobuf.DeviceActivityRequest.fromObject(object.deviceActivityRequest);
            }
            if (object.metadataApi900Request != null) {
                if (typeof object.metadataApi900Request !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.metadataApi900Request: object expected");
                message.metadataApi900Request = $root.protobuf.MetadataApi900Request.fromObject(object.metadataApi900Request);
            }
            if (object.detailsApi900Request != null) {
                if (typeof object.detailsApi900Request !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.detailsApi900Request: object expected");
                message.detailsApi900Request = $root.protobuf.DetailsApi900Request.fromObject(object.detailsApi900Request);
            }
            if (object.activeDevicesRequest != null) {
                if (typeof object.activeDevicesRequest !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.activeDevicesRequest: object expected");
                message.activeDevicesRequest = $root.protobuf.ActiveDevicesRequest.fromObject(object.activeDevicesRequest);
            }
            if (object.historicalDevicesRequest != null) {
                if (typeof object.historicalDevicesRequest !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.historicalDevicesRequest: object expected");
                message.historicalDevicesRequest = $root.protobuf.HistoricalDevicesRequest.fromObject(object.historicalDevicesRequest);
            }
            if (object.rdvxzS3Request != null) {
                if (typeof object.rdvxzS3Request !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.rdvxzS3Request: object expected");
                message.rdvxzS3Request = $root.protobuf.RdvxzS3Request.fromObject(object.rdvxzS3Request);
            }
            if (object.rdvxJsonRequest != null) {
                if (typeof object.rdvxJsonRequest !== "object")
                    throw TypeError(".protobuf.RedvoxWebRequest.rdvxJsonRequest: object expected");
                message.rdvxJsonRequest = $root.protobuf.RdvxJsonRequest.fromObject(object.rdvxJsonRequest);
            }
            if (object.jsonWebToken != null)
                message.jsonWebToken = String(object.jsonWebToken);
            if (object.authenticatedEmail != null)
                message.authenticatedEmail = String(object.authenticatedEmail);
            return message;
        };

        /**
         * Creates a RedvoxWebRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RedvoxWebRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RedvoxWebRequest} RedvoxWebRequest
         */
        RedvoxWebRequest.from = RedvoxWebRequest.fromObject;

        /**
         * Creates a plain object from a RedvoxWebRequest message. Also converts values to other types if specified.
         * @param {protobuf.RedvoxWebRequest} message RedvoxWebRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RedvoxWebRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.timeoutSeconds = 0;
                object.requestee = "";
                object.jsonWebToken = "";
                object.authenticatedEmail = "";
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.timeoutSeconds != null && message.hasOwnProperty("timeoutSeconds"))
                object.timeoutSeconds = message.timeoutSeconds;
            if (message.requestee != null && message.hasOwnProperty("requestee"))
                object.requestee = message.requestee;
            if (message.metadataApi800Request != null && message.hasOwnProperty("metadataApi800Request")) {
                object.metadataApi800Request = $root.protobuf.MetadataApi800Request.toObject(message.metadataApi800Request, options);
                if (options.oneofs)
                    object.request = "metadataApi800Request";
            }
            if (message.deviceActivityRequest != null && message.hasOwnProperty("deviceActivityRequest")) {
                object.deviceActivityRequest = $root.protobuf.DeviceActivityRequest.toObject(message.deviceActivityRequest, options);
                if (options.oneofs)
                    object.request = "deviceActivityRequest";
            }
            if (message.metadataApi900Request != null && message.hasOwnProperty("metadataApi900Request")) {
                object.metadataApi900Request = $root.protobuf.MetadataApi900Request.toObject(message.metadataApi900Request, options);
                if (options.oneofs)
                    object.request = "metadataApi900Request";
            }
            if (message.detailsApi900Request != null && message.hasOwnProperty("detailsApi900Request")) {
                object.detailsApi900Request = $root.protobuf.DetailsApi900Request.toObject(message.detailsApi900Request, options);
                if (options.oneofs)
                    object.request = "detailsApi900Request";
            }
            if (message.activeDevicesRequest != null && message.hasOwnProperty("activeDevicesRequest")) {
                object.activeDevicesRequest = $root.protobuf.ActiveDevicesRequest.toObject(message.activeDevicesRequest, options);
                if (options.oneofs)
                    object.request = "activeDevicesRequest";
            }
            if (message.historicalDevicesRequest != null && message.hasOwnProperty("historicalDevicesRequest")) {
                object.historicalDevicesRequest = $root.protobuf.HistoricalDevicesRequest.toObject(message.historicalDevicesRequest, options);
                if (options.oneofs)
                    object.request = "historicalDevicesRequest";
            }
            if (message.jsonWebToken != null && message.hasOwnProperty("jsonWebToken"))
                object.jsonWebToken = message.jsonWebToken;
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                object.authenticatedEmail = message.authenticatedEmail;
            if (message.rdvxzS3Request != null && message.hasOwnProperty("rdvxzS3Request")) {
                object.rdvxzS3Request = $root.protobuf.RdvxzS3Request.toObject(message.rdvxzS3Request, options);
                if (options.oneofs)
                    object.request = "rdvxzS3Request";
            }
            if (message.rdvxJsonRequest != null && message.hasOwnProperty("rdvxJsonRequest")) {
                object.rdvxJsonRequest = $root.protobuf.RdvxJsonRequest.toObject(message.rdvxJsonRequest, options);
                if (options.oneofs)
                    object.request = "rdvxJsonRequest";
            }
            return object;
        };

        /**
         * Creates a plain object from this RedvoxWebRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RedvoxWebRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RedvoxWebRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RedvoxWebRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RedvoxWebRequest;
    })();

    protobuf.RedvoxWebResponse = (function() {

        /**
         * Properties of a RedvoxWebResponse.
         * @typedef protobuf.RedvoxWebResponse$Properties
         * @type {Object}
         * @property {number|Long} [uuid] RedvoxWebResponse uuid.
         * @property {protobuf.MetadataApi800Response$Properties} [metadataApi800Response] RedvoxWebResponse metadataApi800Response.
         * @property {protobuf.DeviceActivityResponse$Properties} [deviceActivityResponse] RedvoxWebResponse deviceActivityResponse.
         * @property {protobuf.MetadataApi900Response$Properties} [metadataApi900Response] RedvoxWebResponse metadataApi900Response.
         * @property {protobuf.DetailsApi900Response$Properties} [detailsApi900Response] RedvoxWebResponse detailsApi900Response.
         * @property {protobuf.ActiveDevicesResponse$Properties} [activeDevicesResponse] RedvoxWebResponse activeDevicesResponse.
         * @property {protobuf.HistoricalDevicesResponse$Properties} [historicalDevicesResponse] RedvoxWebResponse historicalDevicesResponse.
         * @property {protobuf.RdvxzS3Response$Properties} [rdvxzS3Response] RedvoxWebResponse rdvxzS3Response.
         * @property {protobuf.StringResponse$Properties} [stringResponse] RedvoxWebResponse stringResponse.
         * @property {protobuf.ResponseType} [responseType] RedvoxWebResponse responseType.
         */

        /**
         * Constructs a new RedvoxWebResponse.
         * @exports protobuf.RedvoxWebResponse
         * @constructor
         * @param {protobuf.RedvoxWebResponse$Properties=} [properties] Properties to set
         */
        function RedvoxWebResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RedvoxWebResponse uuid.
         * @type {number|Long|undefined}
         */
        RedvoxWebResponse.prototype.uuid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RedvoxWebResponse metadataApi800Response.
         * @type {protobuf.MetadataApi800Response$Properties|undefined}
         */
        RedvoxWebResponse.prototype.metadataApi800Response = null;

        /**
         * RedvoxWebResponse deviceActivityResponse.
         * @type {protobuf.DeviceActivityResponse$Properties|undefined}
         */
        RedvoxWebResponse.prototype.deviceActivityResponse = null;

        /**
         * RedvoxWebResponse metadataApi900Response.
         * @type {protobuf.MetadataApi900Response$Properties|undefined}
         */
        RedvoxWebResponse.prototype.metadataApi900Response = null;

        /**
         * RedvoxWebResponse detailsApi900Response.
         * @type {protobuf.DetailsApi900Response$Properties|undefined}
         */
        RedvoxWebResponse.prototype.detailsApi900Response = null;

        /**
         * RedvoxWebResponse activeDevicesResponse.
         * @type {protobuf.ActiveDevicesResponse$Properties|undefined}
         */
        RedvoxWebResponse.prototype.activeDevicesResponse = null;

        /**
         * RedvoxWebResponse historicalDevicesResponse.
         * @type {protobuf.HistoricalDevicesResponse$Properties|undefined}
         */
        RedvoxWebResponse.prototype.historicalDevicesResponse = null;

        /**
         * RedvoxWebResponse rdvxzS3Response.
         * @type {protobuf.RdvxzS3Response$Properties|undefined}
         */
        RedvoxWebResponse.prototype.rdvxzS3Response = null;

        /**
         * RedvoxWebResponse stringResponse.
         * @type {protobuf.StringResponse$Properties|undefined}
         */
        RedvoxWebResponse.prototype.stringResponse = null;

        /**
         * RedvoxWebResponse responseType.
         * @type {protobuf.ResponseType|undefined}
         */
        RedvoxWebResponse.prototype.responseType = 0;

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * RedvoxWebResponse response.
         * @name protobuf.RedvoxWebResponse#response
         * @type {string|undefined}
         */
        Object.defineProperty(RedvoxWebResponse.prototype, "response", {
            get: $util.oneOfGetter($oneOfFields = ["metadataApi800Response", "deviceActivityResponse", "metadataApi900Response", "detailsApi900Response", "activeDevicesResponse", "historicalDevicesResponse", "rdvxzS3Response", "stringResponse"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new RedvoxWebResponse instance using the specified properties.
         * @param {protobuf.RedvoxWebResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.RedvoxWebResponse} RedvoxWebResponse instance
         */
        RedvoxWebResponse.create = function create(properties) {
            return new RedvoxWebResponse(properties);
        };

        /**
         * Encodes the specified RedvoxWebResponse message. Does not implicitly {@link protobuf.RedvoxWebResponse.verify|verify} messages.
         * @param {protobuf.RedvoxWebResponse$Properties} message RedvoxWebResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RedvoxWebResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.uuid);
            if (message.metadataApi800Response && message.hasOwnProperty("metadataApi800Response"))
                $root.protobuf.MetadataApi800Response.encode(message.metadataApi800Response, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.deviceActivityResponse && message.hasOwnProperty("deviceActivityResponse"))
                $root.protobuf.DeviceActivityResponse.encode(message.deviceActivityResponse, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.metadataApi900Response && message.hasOwnProperty("metadataApi900Response"))
                $root.protobuf.MetadataApi900Response.encode(message.metadataApi900Response, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.detailsApi900Response && message.hasOwnProperty("detailsApi900Response"))
                $root.protobuf.DetailsApi900Response.encode(message.detailsApi900Response, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            if (message.activeDevicesResponse && message.hasOwnProperty("activeDevicesResponse"))
                $root.protobuf.ActiveDevicesResponse.encode(message.activeDevicesResponse, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            if (message.historicalDevicesResponse && message.hasOwnProperty("historicalDevicesResponse"))
                $root.protobuf.HistoricalDevicesResponse.encode(message.historicalDevicesResponse, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            if (message.rdvxzS3Response && message.hasOwnProperty("rdvxzS3Response"))
                $root.protobuf.RdvxzS3Response.encode(message.rdvxzS3Response, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
            if (message.stringResponse && message.hasOwnProperty("stringResponse"))
                $root.protobuf.StringResponse.encode(message.stringResponse, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
            if (message.responseType != null && message.hasOwnProperty("responseType"))
                writer.uint32(/* id 20, wireType 0 =*/160).uint32(message.responseType);
            return writer;
        };

        /**
         * Encodes the specified RedvoxWebResponse message, length delimited. Does not implicitly {@link protobuf.RedvoxWebResponse.verify|verify} messages.
         * @param {protobuf.RedvoxWebResponse$Properties} message RedvoxWebResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RedvoxWebResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RedvoxWebResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RedvoxWebResponse} RedvoxWebResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RedvoxWebResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RedvoxWebResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uuid = reader.uint64();
                    break;
                case 2:
                    message.metadataApi800Response = $root.protobuf.MetadataApi800Response.decode(reader, reader.uint32());
                    break;
                case 3:
                    message.deviceActivityResponse = $root.protobuf.DeviceActivityResponse.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.metadataApi900Response = $root.protobuf.MetadataApi900Response.decode(reader, reader.uint32());
                    break;
                case 5:
                    message.detailsApi900Response = $root.protobuf.DetailsApi900Response.decode(reader, reader.uint32());
                    break;
                case 6:
                    message.activeDevicesResponse = $root.protobuf.ActiveDevicesResponse.decode(reader, reader.uint32());
                    break;
                case 7:
                    message.historicalDevicesResponse = $root.protobuf.HistoricalDevicesResponse.decode(reader, reader.uint32());
                    break;
                case 8:
                    message.rdvxzS3Response = $root.protobuf.RdvxzS3Response.decode(reader, reader.uint32());
                    break;
                case 9:
                    message.stringResponse = $root.protobuf.StringResponse.decode(reader, reader.uint32());
                    break;
                case 20:
                    message.responseType = reader.uint32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RedvoxWebResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RedvoxWebResponse} RedvoxWebResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RedvoxWebResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RedvoxWebResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RedvoxWebResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.uuid != null)
                if (!$util.isInteger(message.uuid) && !(message.uuid && $util.isInteger(message.uuid.low) && $util.isInteger(message.uuid.high)))
                    return "uuid: integer|Long expected";
            if (message.metadataApi800Response != null) {
                properties.response = 1;
                var error = $root.protobuf.MetadataApi800Response.verify(message.metadataApi800Response);
                if (error)
                    return "metadataApi800Response." + error;
            }
            if (message.deviceActivityResponse != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.DeviceActivityResponse.verify(message.deviceActivityResponse);
                if (error)
                    return "deviceActivityResponse." + error;
            }
            if (message.metadataApi900Response != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.MetadataApi900Response.verify(message.metadataApi900Response);
                if (error)
                    return "metadataApi900Response." + error;
            }
            if (message.detailsApi900Response != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.DetailsApi900Response.verify(message.detailsApi900Response);
                if (error)
                    return "detailsApi900Response." + error;
            }
            if (message.activeDevicesResponse != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.ActiveDevicesResponse.verify(message.activeDevicesResponse);
                if (error)
                    return "activeDevicesResponse." + error;
            }
            if (message.historicalDevicesResponse != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.HistoricalDevicesResponse.verify(message.historicalDevicesResponse);
                if (error)
                    return "historicalDevicesResponse." + error;
            }
            if (message.rdvxzS3Response != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.RdvxzS3Response.verify(message.rdvxzS3Response);
                if (error)
                    return "rdvxzS3Response." + error;
            }
            if (message.stringResponse != null) {
                if (properties.response === 1)
                    return "response: multiple values";
                properties.response = 1;
                var error = $root.protobuf.StringResponse.verify(message.stringResponse);
                if (error)
                    return "stringResponse." + error;
            }
            if (message.responseType != null)
                switch (message.responseType) {
                default:
                    return "responseType: enum value expected";
                case 0:
                case 1:
                case 2:
                case 30:
                    break;
                }
            return null;
        };

        /**
         * Creates a RedvoxWebResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RedvoxWebResponse} RedvoxWebResponse
         */
        RedvoxWebResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RedvoxWebResponse)
                return object;
            var message = new $root.protobuf.RedvoxWebResponse();
            if (object.uuid != null)
                if ($util.Long)
                    (message.uuid = $util.Long.fromValue(object.uuid)).unsigned = true;
                else if (typeof object.uuid === "string")
                    message.uuid = parseInt(object.uuid, 10);
                else if (typeof object.uuid === "number")
                    message.uuid = object.uuid;
                else if (typeof object.uuid === "object")
                    message.uuid = new $util.LongBits(object.uuid.low >>> 0, object.uuid.high >>> 0).toNumber(true);
            if (object.metadataApi800Response != null) {
                if (typeof object.metadataApi800Response !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.metadataApi800Response: object expected");
                message.metadataApi800Response = $root.protobuf.MetadataApi800Response.fromObject(object.metadataApi800Response);
            }
            if (object.deviceActivityResponse != null) {
                if (typeof object.deviceActivityResponse !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.deviceActivityResponse: object expected");
                message.deviceActivityResponse = $root.protobuf.DeviceActivityResponse.fromObject(object.deviceActivityResponse);
            }
            if (object.metadataApi900Response != null) {
                if (typeof object.metadataApi900Response !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.metadataApi900Response: object expected");
                message.metadataApi900Response = $root.protobuf.MetadataApi900Response.fromObject(object.metadataApi900Response);
            }
            if (object.detailsApi900Response != null) {
                if (typeof object.detailsApi900Response !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.detailsApi900Response: object expected");
                message.detailsApi900Response = $root.protobuf.DetailsApi900Response.fromObject(object.detailsApi900Response);
            }
            if (object.activeDevicesResponse != null) {
                if (typeof object.activeDevicesResponse !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.activeDevicesResponse: object expected");
                message.activeDevicesResponse = $root.protobuf.ActiveDevicesResponse.fromObject(object.activeDevicesResponse);
            }
            if (object.historicalDevicesResponse != null) {
                if (typeof object.historicalDevicesResponse !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.historicalDevicesResponse: object expected");
                message.historicalDevicesResponse = $root.protobuf.HistoricalDevicesResponse.fromObject(object.historicalDevicesResponse);
            }
            if (object.rdvxzS3Response != null) {
                if (typeof object.rdvxzS3Response !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.rdvxzS3Response: object expected");
                message.rdvxzS3Response = $root.protobuf.RdvxzS3Response.fromObject(object.rdvxzS3Response);
            }
            if (object.stringResponse != null) {
                if (typeof object.stringResponse !== "object")
                    throw TypeError(".protobuf.RedvoxWebResponse.stringResponse: object expected");
                message.stringResponse = $root.protobuf.StringResponse.fromObject(object.stringResponse);
            }
            switch (object.responseType) {
            case "OK":
            case 0:
                message.responseType = 0;
                break;
            case "ERROR_NOT_AUTHENTICATED":
            case 1:
                message.responseType = 1;
                break;
            case "ERROR_TIMEOUT":
            case 2:
                message.responseType = 2;
                break;
            case "ERROR_OTHER":
            case 30:
                message.responseType = 30;
                break;
            }
            return message;
        };

        /**
         * Creates a RedvoxWebResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RedvoxWebResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RedvoxWebResponse} RedvoxWebResponse
         */
        RedvoxWebResponse.from = RedvoxWebResponse.fromObject;

        /**
         * Creates a plain object from a RedvoxWebResponse message. Also converts values to other types if specified.
         * @param {protobuf.RedvoxWebResponse} message RedvoxWebResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RedvoxWebResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.uuid = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.uuid = options.longs === String ? "0" : 0;
                object.responseType = options.enums === String ? "OK" : 0;
            }
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                if (typeof message.uuid === "number")
                    object.uuid = options.longs === String ? String(message.uuid) : message.uuid;
                else
                    object.uuid = options.longs === String ? $util.Long.prototype.toString.call(message.uuid) : options.longs === Number ? new $util.LongBits(message.uuid.low >>> 0, message.uuid.high >>> 0).toNumber(true) : message.uuid;
            if (message.metadataApi800Response != null && message.hasOwnProperty("metadataApi800Response")) {
                object.metadataApi800Response = $root.protobuf.MetadataApi800Response.toObject(message.metadataApi800Response, options);
                if (options.oneofs)
                    object.response = "metadataApi800Response";
            }
            if (message.deviceActivityResponse != null && message.hasOwnProperty("deviceActivityResponse")) {
                object.deviceActivityResponse = $root.protobuf.DeviceActivityResponse.toObject(message.deviceActivityResponse, options);
                if (options.oneofs)
                    object.response = "deviceActivityResponse";
            }
            if (message.metadataApi900Response != null && message.hasOwnProperty("metadataApi900Response")) {
                object.metadataApi900Response = $root.protobuf.MetadataApi900Response.toObject(message.metadataApi900Response, options);
                if (options.oneofs)
                    object.response = "metadataApi900Response";
            }
            if (message.detailsApi900Response != null && message.hasOwnProperty("detailsApi900Response")) {
                object.detailsApi900Response = $root.protobuf.DetailsApi900Response.toObject(message.detailsApi900Response, options);
                if (options.oneofs)
                    object.response = "detailsApi900Response";
            }
            if (message.activeDevicesResponse != null && message.hasOwnProperty("activeDevicesResponse")) {
                object.activeDevicesResponse = $root.protobuf.ActiveDevicesResponse.toObject(message.activeDevicesResponse, options);
                if (options.oneofs)
                    object.response = "activeDevicesResponse";
            }
            if (message.historicalDevicesResponse != null && message.hasOwnProperty("historicalDevicesResponse")) {
                object.historicalDevicesResponse = $root.protobuf.HistoricalDevicesResponse.toObject(message.historicalDevicesResponse, options);
                if (options.oneofs)
                    object.response = "historicalDevicesResponse";
            }
            if (message.rdvxzS3Response != null && message.hasOwnProperty("rdvxzS3Response")) {
                object.rdvxzS3Response = $root.protobuf.RdvxzS3Response.toObject(message.rdvxzS3Response, options);
                if (options.oneofs)
                    object.response = "rdvxzS3Response";
            }
            if (message.stringResponse != null && message.hasOwnProperty("stringResponse")) {
                object.stringResponse = $root.protobuf.StringResponse.toObject(message.stringResponse, options);
                if (options.oneofs)
                    object.response = "stringResponse";
            }
            if (message.responseType != null && message.hasOwnProperty("responseType"))
                object.responseType = options.enums === String ? $root.protobuf.ResponseType[message.responseType] : message.responseType;
            return object;
        };

        /**
         * Creates a plain object from this RedvoxWebResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RedvoxWebResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RedvoxWebResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RedvoxWebResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RedvoxWebResponse;
    })();

    /**
     * ResponseType enum.
     * @name ResponseType
     * @memberof protobuf
     * @enum {number}
     * @property {number} OK=0 OK value
     * @property {number} ERROR_NOT_AUTHENTICATED=1 ERROR_NOT_AUTHENTICATED value
     * @property {number} ERROR_TIMEOUT=2 ERROR_TIMEOUT value
     * @property {number} ERROR_OTHER=30 ERROR_OTHER value
     */
    protobuf.ResponseType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "OK"] = 0;
        values[valuesById[1] = "ERROR_NOT_AUTHENTICATED"] = 1;
        values[valuesById[2] = "ERROR_TIMEOUT"] = 2;
        values[valuesById[30] = "ERROR_OTHER"] = 30;
        return values;
    })();

    protobuf.MetadataApi800Request = (function() {

        /**
         * Properties of a MetadataApi800Request.
         * @typedef protobuf.MetadataApi800Request$Properties
         * @type {Object}
         * @property {number|Long} [startTimestampSecondsUtc] MetadataApi800Request startTimestampSecondsUtc.
         * @property {number|Long} [endTimestampSecondsUtc] MetadataApi800Request endTimestampSecondsUtc.
         * @property {Array.<number|Long>} [deviceIds] MetadataApi800Request deviceIds.
         * @property {boolean} [objectId] MetadataApi800Request objectId.
         * @property {boolean} [deviceId] MetadataApi800Request deviceId.
         * @property {boolean} [deviceUuid] MetadataApi800Request deviceUuid.
         * @property {boolean} [api] MetadataApi800Request api.
         * @property {boolean} [sizeInBytes] MetadataApi800Request sizeInBytes.
         * @property {boolean} [latitude] MetadataApi800Request latitude.
         * @property {boolean} [longitude] MetadataApi800Request longitude.
         * @property {boolean} [altitude] MetadataApi800Request altitude.
         * @property {boolean} [speed] MetadataApi800Request speed.
         * @property {boolean} [calibrationTrim] MetadataApi800Request calibrationTrim.
         * @property {boolean} [deviceEpochUs] MetadataApi800Request deviceEpochUs.
         * @property {boolean} [deviceMach] MetadataApi800Request deviceMach.
         * @property {boolean} [serverEpochMs] MetadataApi800Request serverEpochMs.
         * @property {boolean} [deviceName] MetadataApi800Request deviceName.
         * @property {boolean} [sensorType] MetadataApi800Request sensorType.
         * @property {boolean} [sensorName] MetadataApi800Request sensorName.
         * @property {boolean} [messageExchanges] MetadataApi800Request messageExchanges.
         * @property {boolean} [averageRoundTripLatency] MetadataApi800Request averageRoundTripLatency.
         * @property {boolean} [syncServer] MetadataApi800Request syncServer.
         * @property {boolean} [dataServer] MetadataApi800Request dataServer.
         * @property {boolean} [sampleRate] MetadataApi800Request sampleRate.
         * @property {boolean} [bitsPerSample] MetadataApi800Request bitsPerSample.
         * @property {boolean} [dataLink] MetadataApi800Request dataLink.
         * @property {boolean} [valid] MetadataApi800Request valid.
         */

        /**
         * Constructs a new MetadataApi800Request.
         * @exports protobuf.MetadataApi800Request
         * @constructor
         * @param {protobuf.MetadataApi800Request$Properties=} [properties] Properties to set
         */
        function MetadataApi800Request(properties) {
            this.deviceIds = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * MetadataApi800Request startTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        MetadataApi800Request.prototype.startTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * MetadataApi800Request endTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        MetadataApi800Request.prototype.endTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * MetadataApi800Request deviceIds.
         * @type {Array.<number|Long>|undefined}
         */
        MetadataApi800Request.prototype.deviceIds = $util.emptyArray;

        /**
         * MetadataApi800Request objectId.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.objectId = false;

        /**
         * MetadataApi800Request deviceId.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.deviceId = false;

        /**
         * MetadataApi800Request deviceUuid.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.deviceUuid = false;

        /**
         * MetadataApi800Request api.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.api = false;

        /**
         * MetadataApi800Request sizeInBytes.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.sizeInBytes = false;

        /**
         * MetadataApi800Request latitude.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.latitude = false;

        /**
         * MetadataApi800Request longitude.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.longitude = false;

        /**
         * MetadataApi800Request altitude.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.altitude = false;

        /**
         * MetadataApi800Request speed.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.speed = false;

        /**
         * MetadataApi800Request calibrationTrim.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.calibrationTrim = false;

        /**
         * MetadataApi800Request deviceEpochUs.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.deviceEpochUs = false;

        /**
         * MetadataApi800Request deviceMach.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.deviceMach = false;

        /**
         * MetadataApi800Request serverEpochMs.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.serverEpochMs = false;

        /**
         * MetadataApi800Request deviceName.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.deviceName = false;

        /**
         * MetadataApi800Request sensorType.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.sensorType = false;

        /**
         * MetadataApi800Request sensorName.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.sensorName = false;

        /**
         * MetadataApi800Request messageExchanges.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.messageExchanges = false;

        /**
         * MetadataApi800Request averageRoundTripLatency.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.averageRoundTripLatency = false;

        /**
         * MetadataApi800Request syncServer.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.syncServer = false;

        /**
         * MetadataApi800Request dataServer.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.dataServer = false;

        /**
         * MetadataApi800Request sampleRate.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.sampleRate = false;

        /**
         * MetadataApi800Request bitsPerSample.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.bitsPerSample = false;

        /**
         * MetadataApi800Request dataLink.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.dataLink = false;

        /**
         * MetadataApi800Request valid.
         * @type {boolean|undefined}
         */
        MetadataApi800Request.prototype.valid = false;

        /**
         * Creates a new MetadataApi800Request instance using the specified properties.
         * @param {protobuf.MetadataApi800Request$Properties=} [properties] Properties to set
         * @returns {protobuf.MetadataApi800Request} MetadataApi800Request instance
         */
        MetadataApi800Request.create = function create(properties) {
            return new MetadataApi800Request(properties);
        };

        /**
         * Encodes the specified MetadataApi800Request message. Does not implicitly {@link protobuf.MetadataApi800Request.verify|verify} messages.
         * @param {protobuf.MetadataApi800Request$Properties} message MetadataApi800Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi800Request.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.startTimestampSecondsUtc);
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.endTimestampSecondsUtc);
            if (message.deviceIds && message.deviceIds.length) {
                writer.uint32(/* id 3, wireType 2 =*/26).fork();
                for (var i = 0; i < message.deviceIds.length; ++i)
                    writer.uint64(message.deviceIds[i]);
                writer.ldelim();
            }
            if (message.objectId != null && message.hasOwnProperty("objectId"))
                writer.uint32(/* id 4, wireType 0 =*/32).bool(message.objectId);
            if (message.deviceId != null && message.hasOwnProperty("deviceId"))
                writer.uint32(/* id 5, wireType 0 =*/40).bool(message.deviceId);
            if (message.deviceUuid != null && message.hasOwnProperty("deviceUuid"))
                writer.uint32(/* id 6, wireType 0 =*/48).bool(message.deviceUuid);
            if (message.api != null && message.hasOwnProperty("api"))
                writer.uint32(/* id 7, wireType 0 =*/56).bool(message.api);
            if (message.sizeInBytes != null && message.hasOwnProperty("sizeInBytes"))
                writer.uint32(/* id 8, wireType 0 =*/64).bool(message.sizeInBytes);
            if (message.latitude != null && message.hasOwnProperty("latitude"))
                writer.uint32(/* id 9, wireType 0 =*/72).bool(message.latitude);
            if (message.longitude != null && message.hasOwnProperty("longitude"))
                writer.uint32(/* id 10, wireType 0 =*/80).bool(message.longitude);
            if (message.altitude != null && message.hasOwnProperty("altitude"))
                writer.uint32(/* id 11, wireType 0 =*/88).bool(message.altitude);
            if (message.speed != null && message.hasOwnProperty("speed"))
                writer.uint32(/* id 12, wireType 0 =*/96).bool(message.speed);
            if (message.calibrationTrim != null && message.hasOwnProperty("calibrationTrim"))
                writer.uint32(/* id 13, wireType 0 =*/104).bool(message.calibrationTrim);
            if (message.deviceEpochUs != null && message.hasOwnProperty("deviceEpochUs"))
                writer.uint32(/* id 14, wireType 0 =*/112).bool(message.deviceEpochUs);
            if (message.deviceMach != null && message.hasOwnProperty("deviceMach"))
                writer.uint32(/* id 15, wireType 0 =*/120).bool(message.deviceMach);
            if (message.serverEpochMs != null && message.hasOwnProperty("serverEpochMs"))
                writer.uint32(/* id 16, wireType 0 =*/128).bool(message.serverEpochMs);
            if (message.deviceName != null && message.hasOwnProperty("deviceName"))
                writer.uint32(/* id 17, wireType 0 =*/136).bool(message.deviceName);
            if (message.sensorType != null && message.hasOwnProperty("sensorType"))
                writer.uint32(/* id 18, wireType 0 =*/144).bool(message.sensorType);
            if (message.sensorName != null && message.hasOwnProperty("sensorName"))
                writer.uint32(/* id 19, wireType 0 =*/152).bool(message.sensorName);
            if (message.messageExchanges != null && message.hasOwnProperty("messageExchanges"))
                writer.uint32(/* id 20, wireType 0 =*/160).bool(message.messageExchanges);
            if (message.averageRoundTripLatency != null && message.hasOwnProperty("averageRoundTripLatency"))
                writer.uint32(/* id 21, wireType 0 =*/168).bool(message.averageRoundTripLatency);
            if (message.syncServer != null && message.hasOwnProperty("syncServer"))
                writer.uint32(/* id 22, wireType 0 =*/176).bool(message.syncServer);
            if (message.dataServer != null && message.hasOwnProperty("dataServer"))
                writer.uint32(/* id 23, wireType 0 =*/184).bool(message.dataServer);
            if (message.sampleRate != null && message.hasOwnProperty("sampleRate"))
                writer.uint32(/* id 24, wireType 0 =*/192).bool(message.sampleRate);
            if (message.bitsPerSample != null && message.hasOwnProperty("bitsPerSample"))
                writer.uint32(/* id 25, wireType 0 =*/200).bool(message.bitsPerSample);
            if (message.dataLink != null && message.hasOwnProperty("dataLink"))
                writer.uint32(/* id 26, wireType 0 =*/208).bool(message.dataLink);
            if (message.valid != null && message.hasOwnProperty("valid"))
                writer.uint32(/* id 27, wireType 0 =*/216).bool(message.valid);
            return writer;
        };

        /**
         * Encodes the specified MetadataApi800Request message, length delimited. Does not implicitly {@link protobuf.MetadataApi800Request.verify|verify} messages.
         * @param {protobuf.MetadataApi800Request$Properties} message MetadataApi800Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi800Request.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a MetadataApi800Request message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.MetadataApi800Request} MetadataApi800Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi800Request.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.MetadataApi800Request();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.startTimestampSecondsUtc = reader.uint64();
                    break;
                case 2:
                    message.endTimestampSecondsUtc = reader.uint64();
                    break;
                case 3:
                    if (!(message.deviceIds && message.deviceIds.length))
                        message.deviceIds = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.deviceIds.push(reader.uint64());
                    } else
                        message.deviceIds.push(reader.uint64());
                    break;
                case 4:
                    message.objectId = reader.bool();
                    break;
                case 5:
                    message.deviceId = reader.bool();
                    break;
                case 6:
                    message.deviceUuid = reader.bool();
                    break;
                case 7:
                    message.api = reader.bool();
                    break;
                case 8:
                    message.sizeInBytes = reader.bool();
                    break;
                case 9:
                    message.latitude = reader.bool();
                    break;
                case 10:
                    message.longitude = reader.bool();
                    break;
                case 11:
                    message.altitude = reader.bool();
                    break;
                case 12:
                    message.speed = reader.bool();
                    break;
                case 13:
                    message.calibrationTrim = reader.bool();
                    break;
                case 14:
                    message.deviceEpochUs = reader.bool();
                    break;
                case 15:
                    message.deviceMach = reader.bool();
                    break;
                case 16:
                    message.serverEpochMs = reader.bool();
                    break;
                case 17:
                    message.deviceName = reader.bool();
                    break;
                case 18:
                    message.sensorType = reader.bool();
                    break;
                case 19:
                    message.sensorName = reader.bool();
                    break;
                case 20:
                    message.messageExchanges = reader.bool();
                    break;
                case 21:
                    message.averageRoundTripLatency = reader.bool();
                    break;
                case 22:
                    message.syncServer = reader.bool();
                    break;
                case 23:
                    message.dataServer = reader.bool();
                    break;
                case 24:
                    message.sampleRate = reader.bool();
                    break;
                case 25:
                    message.bitsPerSample = reader.bool();
                    break;
                case 26:
                    message.dataLink = reader.bool();
                    break;
                case 27:
                    message.valid = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a MetadataApi800Request message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.MetadataApi800Request} MetadataApi800Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi800Request.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a MetadataApi800Request message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        MetadataApi800Request.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.startTimestampSecondsUtc != null)
                if (!$util.isInteger(message.startTimestampSecondsUtc) && !(message.startTimestampSecondsUtc && $util.isInteger(message.startTimestampSecondsUtc.low) && $util.isInteger(message.startTimestampSecondsUtc.high)))
                    return "startTimestampSecondsUtc: integer|Long expected";
            if (message.endTimestampSecondsUtc != null)
                if (!$util.isInteger(message.endTimestampSecondsUtc) && !(message.endTimestampSecondsUtc && $util.isInteger(message.endTimestampSecondsUtc.low) && $util.isInteger(message.endTimestampSecondsUtc.high)))
                    return "endTimestampSecondsUtc: integer|Long expected";
            if (message.deviceIds != null) {
                if (!Array.isArray(message.deviceIds))
                    return "deviceIds: array expected";
                for (var i = 0; i < message.deviceIds.length; ++i)
                    if (!$util.isInteger(message.deviceIds[i]) && !(message.deviceIds[i] && $util.isInteger(message.deviceIds[i].low) && $util.isInteger(message.deviceIds[i].high)))
                        return "deviceIds: integer|Long[] expected";
            }
            if (message.objectId != null)
                if (typeof message.objectId !== "boolean")
                    return "objectId: boolean expected";
            if (message.deviceId != null)
                if (typeof message.deviceId !== "boolean")
                    return "deviceId: boolean expected";
            if (message.deviceUuid != null)
                if (typeof message.deviceUuid !== "boolean")
                    return "deviceUuid: boolean expected";
            if (message.api != null)
                if (typeof message.api !== "boolean")
                    return "api: boolean expected";
            if (message.sizeInBytes != null)
                if (typeof message.sizeInBytes !== "boolean")
                    return "sizeInBytes: boolean expected";
            if (message.latitude != null)
                if (typeof message.latitude !== "boolean")
                    return "latitude: boolean expected";
            if (message.longitude != null)
                if (typeof message.longitude !== "boolean")
                    return "longitude: boolean expected";
            if (message.altitude != null)
                if (typeof message.altitude !== "boolean")
                    return "altitude: boolean expected";
            if (message.speed != null)
                if (typeof message.speed !== "boolean")
                    return "speed: boolean expected";
            if (message.calibrationTrim != null)
                if (typeof message.calibrationTrim !== "boolean")
                    return "calibrationTrim: boolean expected";
            if (message.deviceEpochUs != null)
                if (typeof message.deviceEpochUs !== "boolean")
                    return "deviceEpochUs: boolean expected";
            if (message.deviceMach != null)
                if (typeof message.deviceMach !== "boolean")
                    return "deviceMach: boolean expected";
            if (message.serverEpochMs != null)
                if (typeof message.serverEpochMs !== "boolean")
                    return "serverEpochMs: boolean expected";
            if (message.deviceName != null)
                if (typeof message.deviceName !== "boolean")
                    return "deviceName: boolean expected";
            if (message.sensorType != null)
                if (typeof message.sensorType !== "boolean")
                    return "sensorType: boolean expected";
            if (message.sensorName != null)
                if (typeof message.sensorName !== "boolean")
                    return "sensorName: boolean expected";
            if (message.messageExchanges != null)
                if (typeof message.messageExchanges !== "boolean")
                    return "messageExchanges: boolean expected";
            if (message.averageRoundTripLatency != null)
                if (typeof message.averageRoundTripLatency !== "boolean")
                    return "averageRoundTripLatency: boolean expected";
            if (message.syncServer != null)
                if (typeof message.syncServer !== "boolean")
                    return "syncServer: boolean expected";
            if (message.dataServer != null)
                if (typeof message.dataServer !== "boolean")
                    return "dataServer: boolean expected";
            if (message.sampleRate != null)
                if (typeof message.sampleRate !== "boolean")
                    return "sampleRate: boolean expected";
            if (message.bitsPerSample != null)
                if (typeof message.bitsPerSample !== "boolean")
                    return "bitsPerSample: boolean expected";
            if (message.dataLink != null)
                if (typeof message.dataLink !== "boolean")
                    return "dataLink: boolean expected";
            if (message.valid != null)
                if (typeof message.valid !== "boolean")
                    return "valid: boolean expected";
            return null;
        };

        /**
         * Creates a MetadataApi800Request message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi800Request} MetadataApi800Request
         */
        MetadataApi800Request.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.MetadataApi800Request)
                return object;
            var message = new $root.protobuf.MetadataApi800Request();
            if (object.startTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.startTimestampSecondsUtc = $util.Long.fromValue(object.startTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.startTimestampSecondsUtc === "string")
                    message.startTimestampSecondsUtc = parseInt(object.startTimestampSecondsUtc, 10);
                else if (typeof object.startTimestampSecondsUtc === "number")
                    message.startTimestampSecondsUtc = object.startTimestampSecondsUtc;
                else if (typeof object.startTimestampSecondsUtc === "object")
                    message.startTimestampSecondsUtc = new $util.LongBits(object.startTimestampSecondsUtc.low >>> 0, object.startTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.endTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.endTimestampSecondsUtc = $util.Long.fromValue(object.endTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.endTimestampSecondsUtc === "string")
                    message.endTimestampSecondsUtc = parseInt(object.endTimestampSecondsUtc, 10);
                else if (typeof object.endTimestampSecondsUtc === "number")
                    message.endTimestampSecondsUtc = object.endTimestampSecondsUtc;
                else if (typeof object.endTimestampSecondsUtc === "object")
                    message.endTimestampSecondsUtc = new $util.LongBits(object.endTimestampSecondsUtc.low >>> 0, object.endTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.deviceIds) {
                if (!Array.isArray(object.deviceIds))
                    throw TypeError(".protobuf.MetadataApi800Request.deviceIds: array expected");
                message.deviceIds = [];
                for (var i = 0; i < object.deviceIds.length; ++i)
                    if ($util.Long)
                        (message.deviceIds[i] = $util.Long.fromValue(object.deviceIds[i])).unsigned = true;
                    else if (typeof object.deviceIds[i] === "string")
                        message.deviceIds[i] = parseInt(object.deviceIds[i], 10);
                    else if (typeof object.deviceIds[i] === "number")
                        message.deviceIds[i] = object.deviceIds[i];
                    else if (typeof object.deviceIds[i] === "object")
                        message.deviceIds[i] = new $util.LongBits(object.deviceIds[i].low >>> 0, object.deviceIds[i].high >>> 0).toNumber(true);
            }
            if (object.objectId != null)
                message.objectId = Boolean(object.objectId);
            if (object.deviceId != null)
                message.deviceId = Boolean(object.deviceId);
            if (object.deviceUuid != null)
                message.deviceUuid = Boolean(object.deviceUuid);
            if (object.api != null)
                message.api = Boolean(object.api);
            if (object.sizeInBytes != null)
                message.sizeInBytes = Boolean(object.sizeInBytes);
            if (object.latitude != null)
                message.latitude = Boolean(object.latitude);
            if (object.longitude != null)
                message.longitude = Boolean(object.longitude);
            if (object.altitude != null)
                message.altitude = Boolean(object.altitude);
            if (object.speed != null)
                message.speed = Boolean(object.speed);
            if (object.calibrationTrim != null)
                message.calibrationTrim = Boolean(object.calibrationTrim);
            if (object.deviceEpochUs != null)
                message.deviceEpochUs = Boolean(object.deviceEpochUs);
            if (object.deviceMach != null)
                message.deviceMach = Boolean(object.deviceMach);
            if (object.serverEpochMs != null)
                message.serverEpochMs = Boolean(object.serverEpochMs);
            if (object.deviceName != null)
                message.deviceName = Boolean(object.deviceName);
            if (object.sensorType != null)
                message.sensorType = Boolean(object.sensorType);
            if (object.sensorName != null)
                message.sensorName = Boolean(object.sensorName);
            if (object.messageExchanges != null)
                message.messageExchanges = Boolean(object.messageExchanges);
            if (object.averageRoundTripLatency != null)
                message.averageRoundTripLatency = Boolean(object.averageRoundTripLatency);
            if (object.syncServer != null)
                message.syncServer = Boolean(object.syncServer);
            if (object.dataServer != null)
                message.dataServer = Boolean(object.dataServer);
            if (object.sampleRate != null)
                message.sampleRate = Boolean(object.sampleRate);
            if (object.bitsPerSample != null)
                message.bitsPerSample = Boolean(object.bitsPerSample);
            if (object.dataLink != null)
                message.dataLink = Boolean(object.dataLink);
            if (object.valid != null)
                message.valid = Boolean(object.valid);
            return message;
        };

        /**
         * Creates a MetadataApi800Request message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.MetadataApi800Request.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi800Request} MetadataApi800Request
         */
        MetadataApi800Request.from = MetadataApi800Request.fromObject;

        /**
         * Creates a plain object from a MetadataApi800Request message. Also converts values to other types if specified.
         * @param {protobuf.MetadataApi800Request} message MetadataApi800Request
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi800Request.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.deviceIds = [];
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.startTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.startTimestampSecondsUtc = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.endTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.endTimestampSecondsUtc = options.longs === String ? "0" : 0;
                object.objectId = false;
                object.deviceId = false;
                object.deviceUuid = false;
                object.api = false;
                object.sizeInBytes = false;
                object.latitude = false;
                object.longitude = false;
                object.altitude = false;
                object.speed = false;
                object.calibrationTrim = false;
                object.deviceEpochUs = false;
                object.deviceMach = false;
                object.serverEpochMs = false;
                object.deviceName = false;
                object.sensorType = false;
                object.sensorName = false;
                object.messageExchanges = false;
                object.averageRoundTripLatency = false;
                object.syncServer = false;
                object.dataServer = false;
                object.sampleRate = false;
                object.bitsPerSample = false;
                object.dataLink = false;
                object.valid = false;
            }
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                if (typeof message.startTimestampSecondsUtc === "number")
                    object.startTimestampSecondsUtc = options.longs === String ? String(message.startTimestampSecondsUtc) : message.startTimestampSecondsUtc;
                else
                    object.startTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.startTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.startTimestampSecondsUtc.low >>> 0, message.startTimestampSecondsUtc.high >>> 0).toNumber(true) : message.startTimestampSecondsUtc;
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                if (typeof message.endTimestampSecondsUtc === "number")
                    object.endTimestampSecondsUtc = options.longs === String ? String(message.endTimestampSecondsUtc) : message.endTimestampSecondsUtc;
                else
                    object.endTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.endTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.endTimestampSecondsUtc.low >>> 0, message.endTimestampSecondsUtc.high >>> 0).toNumber(true) : message.endTimestampSecondsUtc;
            if (message.deviceIds && message.deviceIds.length) {
                object.deviceIds = [];
                for (var j = 0; j < message.deviceIds.length; ++j)
                    if (typeof message.deviceIds[j] === "number")
                        object.deviceIds[j] = options.longs === String ? String(message.deviceIds[j]) : message.deviceIds[j];
                    else
                        object.deviceIds[j] = options.longs === String ? $util.Long.prototype.toString.call(message.deviceIds[j]) : options.longs === Number ? new $util.LongBits(message.deviceIds[j].low >>> 0, message.deviceIds[j].high >>> 0).toNumber(true) : message.deviceIds[j];
            }
            if (message.objectId != null && message.hasOwnProperty("objectId"))
                object.objectId = message.objectId;
            if (message.deviceId != null && message.hasOwnProperty("deviceId"))
                object.deviceId = message.deviceId;
            if (message.deviceUuid != null && message.hasOwnProperty("deviceUuid"))
                object.deviceUuid = message.deviceUuid;
            if (message.api != null && message.hasOwnProperty("api"))
                object.api = message.api;
            if (message.sizeInBytes != null && message.hasOwnProperty("sizeInBytes"))
                object.sizeInBytes = message.sizeInBytes;
            if (message.latitude != null && message.hasOwnProperty("latitude"))
                object.latitude = message.latitude;
            if (message.longitude != null && message.hasOwnProperty("longitude"))
                object.longitude = message.longitude;
            if (message.altitude != null && message.hasOwnProperty("altitude"))
                object.altitude = message.altitude;
            if (message.speed != null && message.hasOwnProperty("speed"))
                object.speed = message.speed;
            if (message.calibrationTrim != null && message.hasOwnProperty("calibrationTrim"))
                object.calibrationTrim = message.calibrationTrim;
            if (message.deviceEpochUs != null && message.hasOwnProperty("deviceEpochUs"))
                object.deviceEpochUs = message.deviceEpochUs;
            if (message.deviceMach != null && message.hasOwnProperty("deviceMach"))
                object.deviceMach = message.deviceMach;
            if (message.serverEpochMs != null && message.hasOwnProperty("serverEpochMs"))
                object.serverEpochMs = message.serverEpochMs;
            if (message.deviceName != null && message.hasOwnProperty("deviceName"))
                object.deviceName = message.deviceName;
            if (message.sensorType != null && message.hasOwnProperty("sensorType"))
                object.sensorType = message.sensorType;
            if (message.sensorName != null && message.hasOwnProperty("sensorName"))
                object.sensorName = message.sensorName;
            if (message.messageExchanges != null && message.hasOwnProperty("messageExchanges"))
                object.messageExchanges = message.messageExchanges;
            if (message.averageRoundTripLatency != null && message.hasOwnProperty("averageRoundTripLatency"))
                object.averageRoundTripLatency = message.averageRoundTripLatency;
            if (message.syncServer != null && message.hasOwnProperty("syncServer"))
                object.syncServer = message.syncServer;
            if (message.dataServer != null && message.hasOwnProperty("dataServer"))
                object.dataServer = message.dataServer;
            if (message.sampleRate != null && message.hasOwnProperty("sampleRate"))
                object.sampleRate = message.sampleRate;
            if (message.bitsPerSample != null && message.hasOwnProperty("bitsPerSample"))
                object.bitsPerSample = message.bitsPerSample;
            if (message.dataLink != null && message.hasOwnProperty("dataLink"))
                object.dataLink = message.dataLink;
            if (message.valid != null && message.hasOwnProperty("valid"))
                object.valid = message.valid;
            return object;
        };

        /**
         * Creates a plain object from this MetadataApi800Request message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi800Request.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this MetadataApi800Request to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        MetadataApi800Request.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return MetadataApi800Request;
    })();

    protobuf.MetadataApi900Request = (function() {

        /**
         * Properties of a MetadataApi900Request.
         * @typedef protobuf.MetadataApi900Request$Properties
         * @type {Object}
         * @property {string} [jwtToken] MetadataApi900Request jwtToken.
         * @property {string} [authenticatedUser] MetadataApi900Request authenticatedUser.
         * @property {number|Long} [startTimestampMsEpochUtc] MetadataApi900Request startTimestampMsEpochUtc.
         * @property {number|Long} [endTimestampMsEpochUtc] MetadataApi900Request endTimestampMsEpochUtc.
         * @property {Array.<string>} [ids] MetadataApi900Request ids.
         * @property {boolean} [isOwnedPublic] MetadataApi900Request isOwnedPublic.
         * @property {boolean} [isOwnedPrivate] MetadataApi900Request isOwnedPrivate.
         * @property {boolean} [isOtherPublic] MetadataApi900Request isOtherPublic.
         * @property {protobuf.MetadataApi900Request.Backfilling} [backfilling] MetadataApi900Request backfilling.
         * @property {protobuf.MetadataApi900Request.Scrambling} [scrambling] MetadataApi900Request scrambling.
         * @property {Array.<string>} [makes] MetadataApi900Request makes.
         * @property {Array.<string>} [models] MetadataApi900Request models.
         * @property {protobuf.MetadataApi900Request.Oses} [oses] MetadataApi900Request oses.
         * @property {Array.<string>} [osVersions] MetadataApi900Request osVersions.
         * @property {Array.<string>} [appVersions] MetadataApi900Request appVersions.
         * @property {Array.<string>} [dataUrls] MetadataApi900Request dataUrls.
         * @property {Array.<string>} [synchUrls] MetadataApi900Request synchUrls.
         * @property {Array.<string>} [authUrls] MetadataApi900Request authUrls.
         * @property {Array.<string>} [channels] MetadataApi900Request channels.
         * @property {Array.<number>} [micSampleRates] MetadataApi900Request micSampleRates.
         */

        /**
         * Constructs a new MetadataApi900Request.
         * @exports protobuf.MetadataApi900Request
         * @constructor
         * @param {protobuf.MetadataApi900Request$Properties=} [properties] Properties to set
         */
        function MetadataApi900Request(properties) {
            this.ids = [];
            this.makes = [];
            this.models = [];
            this.osVersions = [];
            this.appVersions = [];
            this.dataUrls = [];
            this.synchUrls = [];
            this.authUrls = [];
            this.channels = [];
            this.micSampleRates = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * MetadataApi900Request jwtToken.
         * @type {string|undefined}
         */
        MetadataApi900Request.prototype.jwtToken = "";

        /**
         * MetadataApi900Request authenticatedUser.
         * @type {string|undefined}
         */
        MetadataApi900Request.prototype.authenticatedUser = "";

        /**
         * MetadataApi900Request startTimestampMsEpochUtc.
         * @type {number|Long|undefined}
         */
        MetadataApi900Request.prototype.startTimestampMsEpochUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * MetadataApi900Request endTimestampMsEpochUtc.
         * @type {number|Long|undefined}
         */
        MetadataApi900Request.prototype.endTimestampMsEpochUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * MetadataApi900Request ids.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.ids = $util.emptyArray;

        /**
         * MetadataApi900Request isOwnedPublic.
         * @type {boolean|undefined}
         */
        MetadataApi900Request.prototype.isOwnedPublic = false;

        /**
         * MetadataApi900Request isOwnedPrivate.
         * @type {boolean|undefined}
         */
        MetadataApi900Request.prototype.isOwnedPrivate = false;

        /**
         * MetadataApi900Request isOtherPublic.
         * @type {boolean|undefined}
         */
        MetadataApi900Request.prototype.isOtherPublic = false;

        /**
         * MetadataApi900Request backfilling.
         * @type {protobuf.MetadataApi900Request.Backfilling|undefined}
         */
        MetadataApi900Request.prototype.backfilling = 0;

        /**
         * MetadataApi900Request scrambling.
         * @type {protobuf.MetadataApi900Request.Scrambling|undefined}
         */
        MetadataApi900Request.prototype.scrambling = 0;

        /**
         * MetadataApi900Request makes.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.makes = $util.emptyArray;

        /**
         * MetadataApi900Request models.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.models = $util.emptyArray;

        /**
         * MetadataApi900Request oses.
         * @type {protobuf.MetadataApi900Request.Oses|undefined}
         */
        MetadataApi900Request.prototype.oses = 0;

        /**
         * MetadataApi900Request osVersions.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.osVersions = $util.emptyArray;

        /**
         * MetadataApi900Request appVersions.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.appVersions = $util.emptyArray;

        /**
         * MetadataApi900Request dataUrls.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.dataUrls = $util.emptyArray;

        /**
         * MetadataApi900Request synchUrls.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.synchUrls = $util.emptyArray;

        /**
         * MetadataApi900Request authUrls.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.authUrls = $util.emptyArray;

        /**
         * MetadataApi900Request channels.
         * @type {Array.<string>|undefined}
         */
        MetadataApi900Request.prototype.channels = $util.emptyArray;

        /**
         * MetadataApi900Request micSampleRates.
         * @type {Array.<number>|undefined}
         */
        MetadataApi900Request.prototype.micSampleRates = $util.emptyArray;

        /**
         * Creates a new MetadataApi900Request instance using the specified properties.
         * @param {protobuf.MetadataApi900Request$Properties=} [properties] Properties to set
         * @returns {protobuf.MetadataApi900Request} MetadataApi900Request instance
         */
        MetadataApi900Request.create = function create(properties) {
            return new MetadataApi900Request(properties);
        };

        /**
         * Encodes the specified MetadataApi900Request message. Does not implicitly {@link protobuf.MetadataApi900Request.verify|verify} messages.
         * @param {protobuf.MetadataApi900Request$Properties} message MetadataApi900Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi900Request.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.jwtToken != null && message.hasOwnProperty("jwtToken"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.jwtToken);
            if (message.authenticatedUser != null && message.hasOwnProperty("authenticatedUser"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.authenticatedUser);
            if (message.startTimestampMsEpochUtc != null && message.hasOwnProperty("startTimestampMsEpochUtc"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint64(message.startTimestampMsEpochUtc);
            if (message.endTimestampMsEpochUtc != null && message.hasOwnProperty("endTimestampMsEpochUtc"))
                writer.uint32(/* id 4, wireType 0 =*/32).uint64(message.endTimestampMsEpochUtc);
            if (message.ids && message.ids.length)
                for (var i = 0; i < message.ids.length; ++i)
                    writer.uint32(/* id 5, wireType 2 =*/42).string(message.ids[i]);
            if (message.isOwnedPublic != null && message.hasOwnProperty("isOwnedPublic"))
                writer.uint32(/* id 6, wireType 0 =*/48).bool(message.isOwnedPublic);
            if (message.isOwnedPrivate != null && message.hasOwnProperty("isOwnedPrivate"))
                writer.uint32(/* id 7, wireType 0 =*/56).bool(message.isOwnedPrivate);
            if (message.isOtherPublic != null && message.hasOwnProperty("isOtherPublic"))
                writer.uint32(/* id 8, wireType 0 =*/64).bool(message.isOtherPublic);
            if (message.backfilling != null && message.hasOwnProperty("backfilling"))
                writer.uint32(/* id 9, wireType 0 =*/72).uint32(message.backfilling);
            if (message.scrambling != null && message.hasOwnProperty("scrambling"))
                writer.uint32(/* id 10, wireType 0 =*/80).uint32(message.scrambling);
            if (message.makes && message.makes.length)
                for (var i = 0; i < message.makes.length; ++i)
                    writer.uint32(/* id 11, wireType 2 =*/90).string(message.makes[i]);
            if (message.models && message.models.length)
                for (var i = 0; i < message.models.length; ++i)
                    writer.uint32(/* id 12, wireType 2 =*/98).string(message.models[i]);
            if (message.oses != null && message.hasOwnProperty("oses"))
                writer.uint32(/* id 13, wireType 0 =*/104).uint32(message.oses);
            if (message.osVersions && message.osVersions.length)
                for (var i = 0; i < message.osVersions.length; ++i)
                    writer.uint32(/* id 14, wireType 2 =*/114).string(message.osVersions[i]);
            if (message.appVersions && message.appVersions.length)
                for (var i = 0; i < message.appVersions.length; ++i)
                    writer.uint32(/* id 15, wireType 2 =*/122).string(message.appVersions[i]);
            if (message.dataUrls && message.dataUrls.length)
                for (var i = 0; i < message.dataUrls.length; ++i)
                    writer.uint32(/* id 16, wireType 2 =*/130).string(message.dataUrls[i]);
            if (message.synchUrls && message.synchUrls.length)
                for (var i = 0; i < message.synchUrls.length; ++i)
                    writer.uint32(/* id 17, wireType 2 =*/138).string(message.synchUrls[i]);
            if (message.authUrls && message.authUrls.length)
                for (var i = 0; i < message.authUrls.length; ++i)
                    writer.uint32(/* id 18, wireType 2 =*/146).string(message.authUrls[i]);
            if (message.channels && message.channels.length)
                for (var i = 0; i < message.channels.length; ++i)
                    writer.uint32(/* id 19, wireType 2 =*/154).string(message.channels[i]);
            if (message.micSampleRates && message.micSampleRates.length) {
                writer.uint32(/* id 20, wireType 2 =*/162).fork();
                for (var i = 0; i < message.micSampleRates.length; ++i)
                    writer.float(message.micSampleRates[i]);
                writer.ldelim();
            }
            return writer;
        };

        /**
         * Encodes the specified MetadataApi900Request message, length delimited. Does not implicitly {@link protobuf.MetadataApi900Request.verify|verify} messages.
         * @param {protobuf.MetadataApi900Request$Properties} message MetadataApi900Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi900Request.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a MetadataApi900Request message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.MetadataApi900Request} MetadataApi900Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi900Request.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.MetadataApi900Request();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.jwtToken = reader.string();
                    break;
                case 2:
                    message.authenticatedUser = reader.string();
                    break;
                case 3:
                    message.startTimestampMsEpochUtc = reader.uint64();
                    break;
                case 4:
                    message.endTimestampMsEpochUtc = reader.uint64();
                    break;
                case 5:
                    if (!(message.ids && message.ids.length))
                        message.ids = [];
                    message.ids.push(reader.string());
                    break;
                case 6:
                    message.isOwnedPublic = reader.bool();
                    break;
                case 7:
                    message.isOwnedPrivate = reader.bool();
                    break;
                case 8:
                    message.isOtherPublic = reader.bool();
                    break;
                case 9:
                    message.backfilling = reader.uint32();
                    break;
                case 10:
                    message.scrambling = reader.uint32();
                    break;
                case 11:
                    if (!(message.makes && message.makes.length))
                        message.makes = [];
                    message.makes.push(reader.string());
                    break;
                case 12:
                    if (!(message.models && message.models.length))
                        message.models = [];
                    message.models.push(reader.string());
                    break;
                case 13:
                    message.oses = reader.uint32();
                    break;
                case 14:
                    if (!(message.osVersions && message.osVersions.length))
                        message.osVersions = [];
                    message.osVersions.push(reader.string());
                    break;
                case 15:
                    if (!(message.appVersions && message.appVersions.length))
                        message.appVersions = [];
                    message.appVersions.push(reader.string());
                    break;
                case 16:
                    if (!(message.dataUrls && message.dataUrls.length))
                        message.dataUrls = [];
                    message.dataUrls.push(reader.string());
                    break;
                case 17:
                    if (!(message.synchUrls && message.synchUrls.length))
                        message.synchUrls = [];
                    message.synchUrls.push(reader.string());
                    break;
                case 18:
                    if (!(message.authUrls && message.authUrls.length))
                        message.authUrls = [];
                    message.authUrls.push(reader.string());
                    break;
                case 19:
                    if (!(message.channels && message.channels.length))
                        message.channels = [];
                    message.channels.push(reader.string());
                    break;
                case 20:
                    if (!(message.micSampleRates && message.micSampleRates.length))
                        message.micSampleRates = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.micSampleRates.push(reader.float());
                    } else
                        message.micSampleRates.push(reader.float());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a MetadataApi900Request message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.MetadataApi900Request} MetadataApi900Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi900Request.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a MetadataApi900Request message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        MetadataApi900Request.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.jwtToken != null)
                if (!$util.isString(message.jwtToken))
                    return "jwtToken: string expected";
            if (message.authenticatedUser != null)
                if (!$util.isString(message.authenticatedUser))
                    return "authenticatedUser: string expected";
            if (message.startTimestampMsEpochUtc != null)
                if (!$util.isInteger(message.startTimestampMsEpochUtc) && !(message.startTimestampMsEpochUtc && $util.isInteger(message.startTimestampMsEpochUtc.low) && $util.isInteger(message.startTimestampMsEpochUtc.high)))
                    return "startTimestampMsEpochUtc: integer|Long expected";
            if (message.endTimestampMsEpochUtc != null)
                if (!$util.isInteger(message.endTimestampMsEpochUtc) && !(message.endTimestampMsEpochUtc && $util.isInteger(message.endTimestampMsEpochUtc.low) && $util.isInteger(message.endTimestampMsEpochUtc.high)))
                    return "endTimestampMsEpochUtc: integer|Long expected";
            if (message.ids != null) {
                if (!Array.isArray(message.ids))
                    return "ids: array expected";
                for (var i = 0; i < message.ids.length; ++i)
                    if (!$util.isString(message.ids[i]))
                        return "ids: string[] expected";
            }
            if (message.isOwnedPublic != null)
                if (typeof message.isOwnedPublic !== "boolean")
                    return "isOwnedPublic: boolean expected";
            if (message.isOwnedPrivate != null)
                if (typeof message.isOwnedPrivate !== "boolean")
                    return "isOwnedPrivate: boolean expected";
            if (message.isOtherPublic != null)
                if (typeof message.isOtherPublic !== "boolean")
                    return "isOtherPublic: boolean expected";
            if (message.backfilling != null)
                switch (message.backfilling) {
                default:
                    return "backfilling: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.scrambling != null)
                switch (message.scrambling) {
                default:
                    return "scrambling: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.makes != null) {
                if (!Array.isArray(message.makes))
                    return "makes: array expected";
                for (var i = 0; i < message.makes.length; ++i)
                    if (!$util.isString(message.makes[i]))
                        return "makes: string[] expected";
            }
            if (message.models != null) {
                if (!Array.isArray(message.models))
                    return "models: array expected";
                for (var i = 0; i < message.models.length; ++i)
                    if (!$util.isString(message.models[i]))
                        return "models: string[] expected";
            }
            if (message.oses != null)
                switch (message.oses) {
                default:
                    return "oses: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.osVersions != null) {
                if (!Array.isArray(message.osVersions))
                    return "osVersions: array expected";
                for (var i = 0; i < message.osVersions.length; ++i)
                    if (!$util.isString(message.osVersions[i]))
                        return "osVersions: string[] expected";
            }
            if (message.appVersions != null) {
                if (!Array.isArray(message.appVersions))
                    return "appVersions: array expected";
                for (var i = 0; i < message.appVersions.length; ++i)
                    if (!$util.isString(message.appVersions[i]))
                        return "appVersions: string[] expected";
            }
            if (message.dataUrls != null) {
                if (!Array.isArray(message.dataUrls))
                    return "dataUrls: array expected";
                for (var i = 0; i < message.dataUrls.length; ++i)
                    if (!$util.isString(message.dataUrls[i]))
                        return "dataUrls: string[] expected";
            }
            if (message.synchUrls != null) {
                if (!Array.isArray(message.synchUrls))
                    return "synchUrls: array expected";
                for (var i = 0; i < message.synchUrls.length; ++i)
                    if (!$util.isString(message.synchUrls[i]))
                        return "synchUrls: string[] expected";
            }
            if (message.authUrls != null) {
                if (!Array.isArray(message.authUrls))
                    return "authUrls: array expected";
                for (var i = 0; i < message.authUrls.length; ++i)
                    if (!$util.isString(message.authUrls[i]))
                        return "authUrls: string[] expected";
            }
            if (message.channels != null) {
                if (!Array.isArray(message.channels))
                    return "channels: array expected";
                for (var i = 0; i < message.channels.length; ++i)
                    if (!$util.isString(message.channels[i]))
                        return "channels: string[] expected";
            }
            if (message.micSampleRates != null) {
                if (!Array.isArray(message.micSampleRates))
                    return "micSampleRates: array expected";
                for (var i = 0; i < message.micSampleRates.length; ++i)
                    if (typeof message.micSampleRates[i] !== "number")
                        return "micSampleRates: number[] expected";
            }
            return null;
        };

        /**
         * Creates a MetadataApi900Request message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi900Request} MetadataApi900Request
         */
        MetadataApi900Request.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.MetadataApi900Request)
                return object;
            var message = new $root.protobuf.MetadataApi900Request();
            if (object.jwtToken != null)
                message.jwtToken = String(object.jwtToken);
            if (object.authenticatedUser != null)
                message.authenticatedUser = String(object.authenticatedUser);
            if (object.startTimestampMsEpochUtc != null)
                if ($util.Long)
                    (message.startTimestampMsEpochUtc = $util.Long.fromValue(object.startTimestampMsEpochUtc)).unsigned = true;
                else if (typeof object.startTimestampMsEpochUtc === "string")
                    message.startTimestampMsEpochUtc = parseInt(object.startTimestampMsEpochUtc, 10);
                else if (typeof object.startTimestampMsEpochUtc === "number")
                    message.startTimestampMsEpochUtc = object.startTimestampMsEpochUtc;
                else if (typeof object.startTimestampMsEpochUtc === "object")
                    message.startTimestampMsEpochUtc = new $util.LongBits(object.startTimestampMsEpochUtc.low >>> 0, object.startTimestampMsEpochUtc.high >>> 0).toNumber(true);
            if (object.endTimestampMsEpochUtc != null)
                if ($util.Long)
                    (message.endTimestampMsEpochUtc = $util.Long.fromValue(object.endTimestampMsEpochUtc)).unsigned = true;
                else if (typeof object.endTimestampMsEpochUtc === "string")
                    message.endTimestampMsEpochUtc = parseInt(object.endTimestampMsEpochUtc, 10);
                else if (typeof object.endTimestampMsEpochUtc === "number")
                    message.endTimestampMsEpochUtc = object.endTimestampMsEpochUtc;
                else if (typeof object.endTimestampMsEpochUtc === "object")
                    message.endTimestampMsEpochUtc = new $util.LongBits(object.endTimestampMsEpochUtc.low >>> 0, object.endTimestampMsEpochUtc.high >>> 0).toNumber(true);
            if (object.ids) {
                if (!Array.isArray(object.ids))
                    throw TypeError(".protobuf.MetadataApi900Request.ids: array expected");
                message.ids = [];
                for (var i = 0; i < object.ids.length; ++i)
                    message.ids[i] = String(object.ids[i]);
            }
            if (object.isOwnedPublic != null)
                message.isOwnedPublic = Boolean(object.isOwnedPublic);
            if (object.isOwnedPrivate != null)
                message.isOwnedPrivate = Boolean(object.isOwnedPrivate);
            if (object.isOtherPublic != null)
                message.isOtherPublic = Boolean(object.isOtherPublic);
            switch (object.backfilling) {
            case "NO_BACKFILLING_PREFERENCE":
            case 0:
                message.backfilling = 0;
                break;
            case "BACKFILLED":
            case 1:
                message.backfilling = 1;
                break;
            case "NOT_BACKFILLED":
            case 2:
                message.backfilling = 2;
                break;
            }
            switch (object.scrambling) {
            case "NO_SCRAMBLING_PREFERENCE":
            case 0:
                message.scrambling = 0;
                break;
            case "SCRAMBLED":
            case 1:
                message.scrambling = 1;
                break;
            case "NOT_SCRAMBLED":
            case 2:
                message.scrambling = 2;
                break;
            }
            if (object.makes) {
                if (!Array.isArray(object.makes))
                    throw TypeError(".protobuf.MetadataApi900Request.makes: array expected");
                message.makes = [];
                for (var i = 0; i < object.makes.length; ++i)
                    message.makes[i] = String(object.makes[i]);
            }
            if (object.models) {
                if (!Array.isArray(object.models))
                    throw TypeError(".protobuf.MetadataApi900Request.models: array expected");
                message.models = [];
                for (var i = 0; i < object.models.length; ++i)
                    message.models[i] = String(object.models[i]);
            }
            switch (object.oses) {
            case "NO_OS_PREFERENCE":
            case 0:
                message.oses = 0;
                break;
            case "ANDROID":
            case 1:
                message.oses = 1;
                break;
            case "IOS":
            case 2:
                message.oses = 2;
                break;
            }
            if (object.osVersions) {
                if (!Array.isArray(object.osVersions))
                    throw TypeError(".protobuf.MetadataApi900Request.osVersions: array expected");
                message.osVersions = [];
                for (var i = 0; i < object.osVersions.length; ++i)
                    message.osVersions[i] = String(object.osVersions[i]);
            }
            if (object.appVersions) {
                if (!Array.isArray(object.appVersions))
                    throw TypeError(".protobuf.MetadataApi900Request.appVersions: array expected");
                message.appVersions = [];
                for (var i = 0; i < object.appVersions.length; ++i)
                    message.appVersions[i] = String(object.appVersions[i]);
            }
            if (object.dataUrls) {
                if (!Array.isArray(object.dataUrls))
                    throw TypeError(".protobuf.MetadataApi900Request.dataUrls: array expected");
                message.dataUrls = [];
                for (var i = 0; i < object.dataUrls.length; ++i)
                    message.dataUrls[i] = String(object.dataUrls[i]);
            }
            if (object.synchUrls) {
                if (!Array.isArray(object.synchUrls))
                    throw TypeError(".protobuf.MetadataApi900Request.synchUrls: array expected");
                message.synchUrls = [];
                for (var i = 0; i < object.synchUrls.length; ++i)
                    message.synchUrls[i] = String(object.synchUrls[i]);
            }
            if (object.authUrls) {
                if (!Array.isArray(object.authUrls))
                    throw TypeError(".protobuf.MetadataApi900Request.authUrls: array expected");
                message.authUrls = [];
                for (var i = 0; i < object.authUrls.length; ++i)
                    message.authUrls[i] = String(object.authUrls[i]);
            }
            if (object.channels) {
                if (!Array.isArray(object.channels))
                    throw TypeError(".protobuf.MetadataApi900Request.channels: array expected");
                message.channels = [];
                for (var i = 0; i < object.channels.length; ++i)
                    message.channels[i] = String(object.channels[i]);
            }
            if (object.micSampleRates) {
                if (!Array.isArray(object.micSampleRates))
                    throw TypeError(".protobuf.MetadataApi900Request.micSampleRates: array expected");
                message.micSampleRates = [];
                for (var i = 0; i < object.micSampleRates.length; ++i)
                    message.micSampleRates[i] = Number(object.micSampleRates[i]);
            }
            return message;
        };

        /**
         * Creates a MetadataApi900Request message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.MetadataApi900Request.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi900Request} MetadataApi900Request
         */
        MetadataApi900Request.from = MetadataApi900Request.fromObject;

        /**
         * Creates a plain object from a MetadataApi900Request message. Also converts values to other types if specified.
         * @param {protobuf.MetadataApi900Request} message MetadataApi900Request
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi900Request.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.ids = [];
                object.makes = [];
                object.models = [];
                object.osVersions = [];
                object.appVersions = [];
                object.dataUrls = [];
                object.synchUrls = [];
                object.authUrls = [];
                object.channels = [];
                object.micSampleRates = [];
            }
            if (options.defaults) {
                object.jwtToken = "";
                object.authenticatedUser = "";
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.startTimestampMsEpochUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.startTimestampMsEpochUtc = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.endTimestampMsEpochUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.endTimestampMsEpochUtc = options.longs === String ? "0" : 0;
                object.isOwnedPublic = false;
                object.isOwnedPrivate = false;
                object.isOtherPublic = false;
                object.backfilling = options.enums === String ? "NO_BACKFILLING_PREFERENCE" : 0;
                object.scrambling = options.enums === String ? "NO_SCRAMBLING_PREFERENCE" : 0;
                object.oses = options.enums === String ? "NO_OS_PREFERENCE" : 0;
            }
            if (message.jwtToken != null && message.hasOwnProperty("jwtToken"))
                object.jwtToken = message.jwtToken;
            if (message.authenticatedUser != null && message.hasOwnProperty("authenticatedUser"))
                object.authenticatedUser = message.authenticatedUser;
            if (message.startTimestampMsEpochUtc != null && message.hasOwnProperty("startTimestampMsEpochUtc"))
                if (typeof message.startTimestampMsEpochUtc === "number")
                    object.startTimestampMsEpochUtc = options.longs === String ? String(message.startTimestampMsEpochUtc) : message.startTimestampMsEpochUtc;
                else
                    object.startTimestampMsEpochUtc = options.longs === String ? $util.Long.prototype.toString.call(message.startTimestampMsEpochUtc) : options.longs === Number ? new $util.LongBits(message.startTimestampMsEpochUtc.low >>> 0, message.startTimestampMsEpochUtc.high >>> 0).toNumber(true) : message.startTimestampMsEpochUtc;
            if (message.endTimestampMsEpochUtc != null && message.hasOwnProperty("endTimestampMsEpochUtc"))
                if (typeof message.endTimestampMsEpochUtc === "number")
                    object.endTimestampMsEpochUtc = options.longs === String ? String(message.endTimestampMsEpochUtc) : message.endTimestampMsEpochUtc;
                else
                    object.endTimestampMsEpochUtc = options.longs === String ? $util.Long.prototype.toString.call(message.endTimestampMsEpochUtc) : options.longs === Number ? new $util.LongBits(message.endTimestampMsEpochUtc.low >>> 0, message.endTimestampMsEpochUtc.high >>> 0).toNumber(true) : message.endTimestampMsEpochUtc;
            if (message.ids && message.ids.length) {
                object.ids = [];
                for (var j = 0; j < message.ids.length; ++j)
                    object.ids[j] = message.ids[j];
            }
            if (message.isOwnedPublic != null && message.hasOwnProperty("isOwnedPublic"))
                object.isOwnedPublic = message.isOwnedPublic;
            if (message.isOwnedPrivate != null && message.hasOwnProperty("isOwnedPrivate"))
                object.isOwnedPrivate = message.isOwnedPrivate;
            if (message.isOtherPublic != null && message.hasOwnProperty("isOtherPublic"))
                object.isOtherPublic = message.isOtherPublic;
            if (message.backfilling != null && message.hasOwnProperty("backfilling"))
                object.backfilling = options.enums === String ? $root.protobuf.MetadataApi900Request.Backfilling[message.backfilling] : message.backfilling;
            if (message.scrambling != null && message.hasOwnProperty("scrambling"))
                object.scrambling = options.enums === String ? $root.protobuf.MetadataApi900Request.Scrambling[message.scrambling] : message.scrambling;
            if (message.makes && message.makes.length) {
                object.makes = [];
                for (var j = 0; j < message.makes.length; ++j)
                    object.makes[j] = message.makes[j];
            }
            if (message.models && message.models.length) {
                object.models = [];
                for (var j = 0; j < message.models.length; ++j)
                    object.models[j] = message.models[j];
            }
            if (message.oses != null && message.hasOwnProperty("oses"))
                object.oses = options.enums === String ? $root.protobuf.MetadataApi900Request.Oses[message.oses] : message.oses;
            if (message.osVersions && message.osVersions.length) {
                object.osVersions = [];
                for (var j = 0; j < message.osVersions.length; ++j)
                    object.osVersions[j] = message.osVersions[j];
            }
            if (message.appVersions && message.appVersions.length) {
                object.appVersions = [];
                for (var j = 0; j < message.appVersions.length; ++j)
                    object.appVersions[j] = message.appVersions[j];
            }
            if (message.dataUrls && message.dataUrls.length) {
                object.dataUrls = [];
                for (var j = 0; j < message.dataUrls.length; ++j)
                    object.dataUrls[j] = message.dataUrls[j];
            }
            if (message.synchUrls && message.synchUrls.length) {
                object.synchUrls = [];
                for (var j = 0; j < message.synchUrls.length; ++j)
                    object.synchUrls[j] = message.synchUrls[j];
            }
            if (message.authUrls && message.authUrls.length) {
                object.authUrls = [];
                for (var j = 0; j < message.authUrls.length; ++j)
                    object.authUrls[j] = message.authUrls[j];
            }
            if (message.channels && message.channels.length) {
                object.channels = [];
                for (var j = 0; j < message.channels.length; ++j)
                    object.channels[j] = message.channels[j];
            }
            if (message.micSampleRates && message.micSampleRates.length) {
                object.micSampleRates = [];
                for (var j = 0; j < message.micSampleRates.length; ++j)
                    object.micSampleRates[j] = message.micSampleRates[j];
            }
            return object;
        };

        /**
         * Creates a plain object from this MetadataApi900Request message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi900Request.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this MetadataApi900Request to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        MetadataApi900Request.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Backfilling enum.
         * @name Backfilling
         * @memberof protobuf.MetadataApi900Request
         * @enum {number}
         * @property {number} NO_BACKFILLING_PREFERENCE=0 NO_BACKFILLING_PREFERENCE value
         * @property {number} BACKFILLED=1 BACKFILLED value
         * @property {number} NOT_BACKFILLED=2 NOT_BACKFILLED value
         */
        MetadataApi900Request.Backfilling = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "NO_BACKFILLING_PREFERENCE"] = 0;
            values[valuesById[1] = "BACKFILLED"] = 1;
            values[valuesById[2] = "NOT_BACKFILLED"] = 2;
            return values;
        })();

        /**
         * Scrambling enum.
         * @name Scrambling
         * @memberof protobuf.MetadataApi900Request
         * @enum {number}
         * @property {number} NO_SCRAMBLING_PREFERENCE=0 NO_SCRAMBLING_PREFERENCE value
         * @property {number} SCRAMBLED=1 SCRAMBLED value
         * @property {number} NOT_SCRAMBLED=2 NOT_SCRAMBLED value
         */
        MetadataApi900Request.Scrambling = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "NO_SCRAMBLING_PREFERENCE"] = 0;
            values[valuesById[1] = "SCRAMBLED"] = 1;
            values[valuesById[2] = "NOT_SCRAMBLED"] = 2;
            return values;
        })();

        /**
         * Oses enum.
         * @name Oses
         * @memberof protobuf.MetadataApi900Request
         * @enum {number}
         * @property {number} NO_OS_PREFERENCE=0 NO_OS_PREFERENCE value
         * @property {number} ANDROID=1 ANDROID value
         * @property {number} IOS=2 IOS value
         */
        MetadataApi900Request.Oses = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "NO_OS_PREFERENCE"] = 0;
            values[valuesById[1] = "ANDROID"] = 1;
            values[valuesById[2] = "IOS"] = 2;
            return values;
        })();

        return MetadataApi900Request;
    })();

    protobuf.DetailsApi900Request = (function() {

        /**
         * Properties of a DetailsApi900Request.
         * @typedef protobuf.DetailsApi900Request$Properties
         * @type {Object}
         * @property {string} [jwtToken] DetailsApi900Request jwtToken.
         * @property {string} [authenticatedUser] DetailsApi900Request authenticatedUser.
         * @property {string} [dataKey] DetailsApi900Request dataKey.
         */

        /**
         * Constructs a new DetailsApi900Request.
         * @exports protobuf.DetailsApi900Request
         * @constructor
         * @param {protobuf.DetailsApi900Request$Properties=} [properties] Properties to set
         */
        function DetailsApi900Request(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * DetailsApi900Request jwtToken.
         * @type {string|undefined}
         */
        DetailsApi900Request.prototype.jwtToken = "";

        /**
         * DetailsApi900Request authenticatedUser.
         * @type {string|undefined}
         */
        DetailsApi900Request.prototype.authenticatedUser = "";

        /**
         * DetailsApi900Request dataKey.
         * @type {string|undefined}
         */
        DetailsApi900Request.prototype.dataKey = "";

        /**
         * Creates a new DetailsApi900Request instance using the specified properties.
         * @param {protobuf.DetailsApi900Request$Properties=} [properties] Properties to set
         * @returns {protobuf.DetailsApi900Request} DetailsApi900Request instance
         */
        DetailsApi900Request.create = function create(properties) {
            return new DetailsApi900Request(properties);
        };

        /**
         * Encodes the specified DetailsApi900Request message. Does not implicitly {@link protobuf.DetailsApi900Request.verify|verify} messages.
         * @param {protobuf.DetailsApi900Request$Properties} message DetailsApi900Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DetailsApi900Request.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.jwtToken != null && message.hasOwnProperty("jwtToken"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.jwtToken);
            if (message.authenticatedUser != null && message.hasOwnProperty("authenticatedUser"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.authenticatedUser);
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.dataKey);
            return writer;
        };

        /**
         * Encodes the specified DetailsApi900Request message, length delimited. Does not implicitly {@link protobuf.DetailsApi900Request.verify|verify} messages.
         * @param {protobuf.DetailsApi900Request$Properties} message DetailsApi900Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DetailsApi900Request.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DetailsApi900Request message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.DetailsApi900Request} DetailsApi900Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DetailsApi900Request.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.DetailsApi900Request();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.jwtToken = reader.string();
                    break;
                case 2:
                    message.authenticatedUser = reader.string();
                    break;
                case 3:
                    message.dataKey = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DetailsApi900Request message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.DetailsApi900Request} DetailsApi900Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DetailsApi900Request.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DetailsApi900Request message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        DetailsApi900Request.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.jwtToken != null)
                if (!$util.isString(message.jwtToken))
                    return "jwtToken: string expected";
            if (message.authenticatedUser != null)
                if (!$util.isString(message.authenticatedUser))
                    return "authenticatedUser: string expected";
            if (message.dataKey != null)
                if (!$util.isString(message.dataKey))
                    return "dataKey: string expected";
            return null;
        };

        /**
         * Creates a DetailsApi900Request message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DetailsApi900Request} DetailsApi900Request
         */
        DetailsApi900Request.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.DetailsApi900Request)
                return object;
            var message = new $root.protobuf.DetailsApi900Request();
            if (object.jwtToken != null)
                message.jwtToken = String(object.jwtToken);
            if (object.authenticatedUser != null)
                message.authenticatedUser = String(object.authenticatedUser);
            if (object.dataKey != null)
                message.dataKey = String(object.dataKey);
            return message;
        };

        /**
         * Creates a DetailsApi900Request message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.DetailsApi900Request.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DetailsApi900Request} DetailsApi900Request
         */
        DetailsApi900Request.from = DetailsApi900Request.fromObject;

        /**
         * Creates a plain object from a DetailsApi900Request message. Also converts values to other types if specified.
         * @param {protobuf.DetailsApi900Request} message DetailsApi900Request
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DetailsApi900Request.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.jwtToken = "";
                object.authenticatedUser = "";
                object.dataKey = "";
            }
            if (message.jwtToken != null && message.hasOwnProperty("jwtToken"))
                object.jwtToken = message.jwtToken;
            if (message.authenticatedUser != null && message.hasOwnProperty("authenticatedUser"))
                object.authenticatedUser = message.authenticatedUser;
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                object.dataKey = message.dataKey;
            return object;
        };

        /**
         * Creates a plain object from this DetailsApi900Request message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DetailsApi900Request.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this DetailsApi900Request to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        DetailsApi900Request.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DetailsApi900Request;
    })();

    protobuf.RdvxzS3Request = (function() {

        /**
         * Properties of a RdvxzS3Request.
         * @typedef protobuf.RdvxzS3Request$Properties
         * @type {Object}
         * @property {string} [dataKey] RdvxzS3Request dataKey.
         */

        /**
         * Constructs a new RdvxzS3Request.
         * @exports protobuf.RdvxzS3Request
         * @constructor
         * @param {protobuf.RdvxzS3Request$Properties=} [properties] Properties to set
         */
        function RdvxzS3Request(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RdvxzS3Request dataKey.
         * @type {string|undefined}
         */
        RdvxzS3Request.prototype.dataKey = "";

        /**
         * Creates a new RdvxzS3Request instance using the specified properties.
         * @param {protobuf.RdvxzS3Request$Properties=} [properties] Properties to set
         * @returns {protobuf.RdvxzS3Request} RdvxzS3Request instance
         */
        RdvxzS3Request.create = function create(properties) {
            return new RdvxzS3Request(properties);
        };

        /**
         * Encodes the specified RdvxzS3Request message. Does not implicitly {@link protobuf.RdvxzS3Request.verify|verify} messages.
         * @param {protobuf.RdvxzS3Request$Properties} message RdvxzS3Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RdvxzS3Request.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.dataKey);
            return writer;
        };

        /**
         * Encodes the specified RdvxzS3Request message, length delimited. Does not implicitly {@link protobuf.RdvxzS3Request.verify|verify} messages.
         * @param {protobuf.RdvxzS3Request$Properties} message RdvxzS3Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RdvxzS3Request.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RdvxzS3Request message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RdvxzS3Request} RdvxzS3Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RdvxzS3Request.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RdvxzS3Request();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.dataKey = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RdvxzS3Request message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RdvxzS3Request} RdvxzS3Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RdvxzS3Request.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RdvxzS3Request message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RdvxzS3Request.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.dataKey != null)
                if (!$util.isString(message.dataKey))
                    return "dataKey: string expected";
            return null;
        };

        /**
         * Creates a RdvxzS3Request message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RdvxzS3Request} RdvxzS3Request
         */
        RdvxzS3Request.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RdvxzS3Request)
                return object;
            var message = new $root.protobuf.RdvxzS3Request();
            if (object.dataKey != null)
                message.dataKey = String(object.dataKey);
            return message;
        };

        /**
         * Creates a RdvxzS3Request message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RdvxzS3Request.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RdvxzS3Request} RdvxzS3Request
         */
        RdvxzS3Request.from = RdvxzS3Request.fromObject;

        /**
         * Creates a plain object from a RdvxzS3Request message. Also converts values to other types if specified.
         * @param {protobuf.RdvxzS3Request} message RdvxzS3Request
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RdvxzS3Request.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.dataKey = "";
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                object.dataKey = message.dataKey;
            return object;
        };

        /**
         * Creates a plain object from this RdvxzS3Request message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RdvxzS3Request.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RdvxzS3Request to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RdvxzS3Request.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RdvxzS3Request;
    })();

    protobuf.RdvxJsonRequest = (function() {

        /**
         * Properties of a RdvxJsonRequest.
         * @typedef protobuf.RdvxJsonRequest$Properties
         * @type {Object}
         * @property {string} [dataKey] RdvxJsonRequest dataKey.
         */

        /**
         * Constructs a new RdvxJsonRequest.
         * @exports protobuf.RdvxJsonRequest
         * @constructor
         * @param {protobuf.RdvxJsonRequest$Properties=} [properties] Properties to set
         */
        function RdvxJsonRequest(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RdvxJsonRequest dataKey.
         * @type {string|undefined}
         */
        RdvxJsonRequest.prototype.dataKey = "";

        /**
         * Creates a new RdvxJsonRequest instance using the specified properties.
         * @param {protobuf.RdvxJsonRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.RdvxJsonRequest} RdvxJsonRequest instance
         */
        RdvxJsonRequest.create = function create(properties) {
            return new RdvxJsonRequest(properties);
        };

        /**
         * Encodes the specified RdvxJsonRequest message. Does not implicitly {@link protobuf.RdvxJsonRequest.verify|verify} messages.
         * @param {protobuf.RdvxJsonRequest$Properties} message RdvxJsonRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RdvxJsonRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.dataKey);
            return writer;
        };

        /**
         * Encodes the specified RdvxJsonRequest message, length delimited. Does not implicitly {@link protobuf.RdvxJsonRequest.verify|verify} messages.
         * @param {protobuf.RdvxJsonRequest$Properties} message RdvxJsonRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RdvxJsonRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RdvxJsonRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RdvxJsonRequest} RdvxJsonRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RdvxJsonRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RdvxJsonRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.dataKey = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RdvxJsonRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RdvxJsonRequest} RdvxJsonRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RdvxJsonRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RdvxJsonRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RdvxJsonRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.dataKey != null)
                if (!$util.isString(message.dataKey))
                    return "dataKey: string expected";
            return null;
        };

        /**
         * Creates a RdvxJsonRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RdvxJsonRequest} RdvxJsonRequest
         */
        RdvxJsonRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RdvxJsonRequest)
                return object;
            var message = new $root.protobuf.RdvxJsonRequest();
            if (object.dataKey != null)
                message.dataKey = String(object.dataKey);
            return message;
        };

        /**
         * Creates a RdvxJsonRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RdvxJsonRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RdvxJsonRequest} RdvxJsonRequest
         */
        RdvxJsonRequest.from = RdvxJsonRequest.fromObject;

        /**
         * Creates a plain object from a RdvxJsonRequest message. Also converts values to other types if specified.
         * @param {protobuf.RdvxJsonRequest} message RdvxJsonRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RdvxJsonRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.dataKey = "";
            if (message.dataKey != null && message.hasOwnProperty("dataKey"))
                object.dataKey = message.dataKey;
            return object;
        };

        /**
         * Creates a plain object from this RdvxJsonRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RdvxJsonRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RdvxJsonRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RdvxJsonRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RdvxJsonRequest;
    })();

    protobuf.StringResponse = (function() {

        /**
         * Properties of a StringResponse.
         * @typedef protobuf.StringResponse$Properties
         * @type {Object}
         * @property {string} [payload] StringResponse payload.
         */

        /**
         * Constructs a new StringResponse.
         * @exports protobuf.StringResponse
         * @constructor
         * @param {protobuf.StringResponse$Properties=} [properties] Properties to set
         */
        function StringResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * StringResponse payload.
         * @type {string|undefined}
         */
        StringResponse.prototype.payload = "";

        /**
         * Creates a new StringResponse instance using the specified properties.
         * @param {protobuf.StringResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.StringResponse} StringResponse instance
         */
        StringResponse.create = function create(properties) {
            return new StringResponse(properties);
        };

        /**
         * Encodes the specified StringResponse message. Does not implicitly {@link protobuf.StringResponse.verify|verify} messages.
         * @param {protobuf.StringResponse$Properties} message StringResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StringResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.payload != null && message.hasOwnProperty("payload"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.payload);
            return writer;
        };

        /**
         * Encodes the specified StringResponse message, length delimited. Does not implicitly {@link protobuf.StringResponse.verify|verify} messages.
         * @param {protobuf.StringResponse$Properties} message StringResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StringResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a StringResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.StringResponse} StringResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StringResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.StringResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.payload = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a StringResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.StringResponse} StringResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StringResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a StringResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        StringResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.payload != null)
                if (!$util.isString(message.payload))
                    return "payload: string expected";
            return null;
        };

        /**
         * Creates a StringResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.StringResponse} StringResponse
         */
        StringResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.StringResponse)
                return object;
            var message = new $root.protobuf.StringResponse();
            if (object.payload != null)
                message.payload = String(object.payload);
            return message;
        };

        /**
         * Creates a StringResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.StringResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.StringResponse} StringResponse
         */
        StringResponse.from = StringResponse.fromObject;

        /**
         * Creates a plain object from a StringResponse message. Also converts values to other types if specified.
         * @param {protobuf.StringResponse} message StringResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        StringResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.payload = "";
            if (message.payload != null && message.hasOwnProperty("payload"))
                object.payload = message.payload;
            return object;
        };

        /**
         * Creates a plain object from this StringResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        StringResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this StringResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        StringResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return StringResponse;
    })();

    protobuf.DeviceActivityRequest = (function() {

        /**
         * Properties of a DeviceActivityRequest.
         * @typedef protobuf.DeviceActivityRequest$Properties
         * @type {Object}
         * @property {number|Long} [startTimestampSecondsUtc] DeviceActivityRequest startTimestampSecondsUtc.
         * @property {number|Long} [endTimestampSecondsUtc] DeviceActivityRequest endTimestampSecondsUtc.
         * @property {number|Long} [stepSizeSeconds] DeviceActivityRequest stepSizeSeconds.
         * @property {Array.<number|Long>} [deviceIds] DeviceActivityRequest deviceIds.
         * @property {Array.<string>} [deviceIdsApi900] DeviceActivityRequest deviceIdsApi900.
         */

        /**
         * Constructs a new DeviceActivityRequest.
         * @exports protobuf.DeviceActivityRequest
         * @constructor
         * @param {protobuf.DeviceActivityRequest$Properties=} [properties] Properties to set
         */
        function DeviceActivityRequest(properties) {
            this.deviceIds = [];
            this.deviceIdsApi900 = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * DeviceActivityRequest startTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        DeviceActivityRequest.prototype.startTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * DeviceActivityRequest endTimestampSecondsUtc.
         * @type {number|Long|undefined}
         */
        DeviceActivityRequest.prototype.endTimestampSecondsUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * DeviceActivityRequest stepSizeSeconds.
         * @type {number|Long|undefined}
         */
        DeviceActivityRequest.prototype.stepSizeSeconds = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * DeviceActivityRequest deviceIds.
         * @type {Array.<number|Long>|undefined}
         */
        DeviceActivityRequest.prototype.deviceIds = $util.emptyArray;

        /**
         * DeviceActivityRequest deviceIdsApi900.
         * @type {Array.<string>|undefined}
         */
        DeviceActivityRequest.prototype.deviceIdsApi900 = $util.emptyArray;

        /**
         * Creates a new DeviceActivityRequest instance using the specified properties.
         * @param {protobuf.DeviceActivityRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.DeviceActivityRequest} DeviceActivityRequest instance
         */
        DeviceActivityRequest.create = function create(properties) {
            return new DeviceActivityRequest(properties);
        };

        /**
         * Encodes the specified DeviceActivityRequest message. Does not implicitly {@link protobuf.DeviceActivityRequest.verify|verify} messages.
         * @param {protobuf.DeviceActivityRequest$Properties} message DeviceActivityRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceActivityRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.startTimestampSecondsUtc);
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.endTimestampSecondsUtc);
            if (message.stepSizeSeconds != null && message.hasOwnProperty("stepSizeSeconds"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint64(message.stepSizeSeconds);
            if (message.deviceIds && message.deviceIds.length) {
                writer.uint32(/* id 4, wireType 2 =*/34).fork();
                for (var i = 0; i < message.deviceIds.length; ++i)
                    writer.uint64(message.deviceIds[i]);
                writer.ldelim();
            }
            if (message.deviceIdsApi900 && message.deviceIdsApi900.length)
                for (var i = 0; i < message.deviceIdsApi900.length; ++i)
                    writer.uint32(/* id 5, wireType 2 =*/42).string(message.deviceIdsApi900[i]);
            return writer;
        };

        /**
         * Encodes the specified DeviceActivityRequest message, length delimited. Does not implicitly {@link protobuf.DeviceActivityRequest.verify|verify} messages.
         * @param {protobuf.DeviceActivityRequest$Properties} message DeviceActivityRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceActivityRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DeviceActivityRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.DeviceActivityRequest} DeviceActivityRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceActivityRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.DeviceActivityRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.startTimestampSecondsUtc = reader.uint64();
                    break;
                case 2:
                    message.endTimestampSecondsUtc = reader.uint64();
                    break;
                case 3:
                    message.stepSizeSeconds = reader.uint64();
                    break;
                case 4:
                    if (!(message.deviceIds && message.deviceIds.length))
                        message.deviceIds = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.deviceIds.push(reader.uint64());
                    } else
                        message.deviceIds.push(reader.uint64());
                    break;
                case 5:
                    if (!(message.deviceIdsApi900 && message.deviceIdsApi900.length))
                        message.deviceIdsApi900 = [];
                    message.deviceIdsApi900.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DeviceActivityRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.DeviceActivityRequest} DeviceActivityRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceActivityRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DeviceActivityRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        DeviceActivityRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.startTimestampSecondsUtc != null)
                if (!$util.isInteger(message.startTimestampSecondsUtc) && !(message.startTimestampSecondsUtc && $util.isInteger(message.startTimestampSecondsUtc.low) && $util.isInteger(message.startTimestampSecondsUtc.high)))
                    return "startTimestampSecondsUtc: integer|Long expected";
            if (message.endTimestampSecondsUtc != null)
                if (!$util.isInteger(message.endTimestampSecondsUtc) && !(message.endTimestampSecondsUtc && $util.isInteger(message.endTimestampSecondsUtc.low) && $util.isInteger(message.endTimestampSecondsUtc.high)))
                    return "endTimestampSecondsUtc: integer|Long expected";
            if (message.stepSizeSeconds != null)
                if (!$util.isInteger(message.stepSizeSeconds) && !(message.stepSizeSeconds && $util.isInteger(message.stepSizeSeconds.low) && $util.isInteger(message.stepSizeSeconds.high)))
                    return "stepSizeSeconds: integer|Long expected";
            if (message.deviceIds != null) {
                if (!Array.isArray(message.deviceIds))
                    return "deviceIds: array expected";
                for (var i = 0; i < message.deviceIds.length; ++i)
                    if (!$util.isInteger(message.deviceIds[i]) && !(message.deviceIds[i] && $util.isInteger(message.deviceIds[i].low) && $util.isInteger(message.deviceIds[i].high)))
                        return "deviceIds: integer|Long[] expected";
            }
            if (message.deviceIdsApi900 != null) {
                if (!Array.isArray(message.deviceIdsApi900))
                    return "deviceIdsApi900: array expected";
                for (var i = 0; i < message.deviceIdsApi900.length; ++i)
                    if (!$util.isString(message.deviceIdsApi900[i]))
                        return "deviceIdsApi900: string[] expected";
            }
            return null;
        };

        /**
         * Creates a DeviceActivityRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DeviceActivityRequest} DeviceActivityRequest
         */
        DeviceActivityRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.DeviceActivityRequest)
                return object;
            var message = new $root.protobuf.DeviceActivityRequest();
            if (object.startTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.startTimestampSecondsUtc = $util.Long.fromValue(object.startTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.startTimestampSecondsUtc === "string")
                    message.startTimestampSecondsUtc = parseInt(object.startTimestampSecondsUtc, 10);
                else if (typeof object.startTimestampSecondsUtc === "number")
                    message.startTimestampSecondsUtc = object.startTimestampSecondsUtc;
                else if (typeof object.startTimestampSecondsUtc === "object")
                    message.startTimestampSecondsUtc = new $util.LongBits(object.startTimestampSecondsUtc.low >>> 0, object.startTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.endTimestampSecondsUtc != null)
                if ($util.Long)
                    (message.endTimestampSecondsUtc = $util.Long.fromValue(object.endTimestampSecondsUtc)).unsigned = true;
                else if (typeof object.endTimestampSecondsUtc === "string")
                    message.endTimestampSecondsUtc = parseInt(object.endTimestampSecondsUtc, 10);
                else if (typeof object.endTimestampSecondsUtc === "number")
                    message.endTimestampSecondsUtc = object.endTimestampSecondsUtc;
                else if (typeof object.endTimestampSecondsUtc === "object")
                    message.endTimestampSecondsUtc = new $util.LongBits(object.endTimestampSecondsUtc.low >>> 0, object.endTimestampSecondsUtc.high >>> 0).toNumber(true);
            if (object.stepSizeSeconds != null)
                if ($util.Long)
                    (message.stepSizeSeconds = $util.Long.fromValue(object.stepSizeSeconds)).unsigned = true;
                else if (typeof object.stepSizeSeconds === "string")
                    message.stepSizeSeconds = parseInt(object.stepSizeSeconds, 10);
                else if (typeof object.stepSizeSeconds === "number")
                    message.stepSizeSeconds = object.stepSizeSeconds;
                else if (typeof object.stepSizeSeconds === "object")
                    message.stepSizeSeconds = new $util.LongBits(object.stepSizeSeconds.low >>> 0, object.stepSizeSeconds.high >>> 0).toNumber(true);
            if (object.deviceIds) {
                if (!Array.isArray(object.deviceIds))
                    throw TypeError(".protobuf.DeviceActivityRequest.deviceIds: array expected");
                message.deviceIds = [];
                for (var i = 0; i < object.deviceIds.length; ++i)
                    if ($util.Long)
                        (message.deviceIds[i] = $util.Long.fromValue(object.deviceIds[i])).unsigned = true;
                    else if (typeof object.deviceIds[i] === "string")
                        message.deviceIds[i] = parseInt(object.deviceIds[i], 10);
                    else if (typeof object.deviceIds[i] === "number")
                        message.deviceIds[i] = object.deviceIds[i];
                    else if (typeof object.deviceIds[i] === "object")
                        message.deviceIds[i] = new $util.LongBits(object.deviceIds[i].low >>> 0, object.deviceIds[i].high >>> 0).toNumber(true);
            }
            if (object.deviceIdsApi900) {
                if (!Array.isArray(object.deviceIdsApi900))
                    throw TypeError(".protobuf.DeviceActivityRequest.deviceIdsApi900: array expected");
                message.deviceIdsApi900 = [];
                for (var i = 0; i < object.deviceIdsApi900.length; ++i)
                    message.deviceIdsApi900[i] = String(object.deviceIdsApi900[i]);
            }
            return message;
        };

        /**
         * Creates a DeviceActivityRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.DeviceActivityRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DeviceActivityRequest} DeviceActivityRequest
         */
        DeviceActivityRequest.from = DeviceActivityRequest.fromObject;

        /**
         * Creates a plain object from a DeviceActivityRequest message. Also converts values to other types if specified.
         * @param {protobuf.DeviceActivityRequest} message DeviceActivityRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DeviceActivityRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.deviceIds = [];
                object.deviceIdsApi900 = [];
            }
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.startTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.startTimestampSecondsUtc = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.endTimestampSecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.endTimestampSecondsUtc = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.stepSizeSeconds = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.stepSizeSeconds = options.longs === String ? "0" : 0;
            }
            if (message.startTimestampSecondsUtc != null && message.hasOwnProperty("startTimestampSecondsUtc"))
                if (typeof message.startTimestampSecondsUtc === "number")
                    object.startTimestampSecondsUtc = options.longs === String ? String(message.startTimestampSecondsUtc) : message.startTimestampSecondsUtc;
                else
                    object.startTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.startTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.startTimestampSecondsUtc.low >>> 0, message.startTimestampSecondsUtc.high >>> 0).toNumber(true) : message.startTimestampSecondsUtc;
            if (message.endTimestampSecondsUtc != null && message.hasOwnProperty("endTimestampSecondsUtc"))
                if (typeof message.endTimestampSecondsUtc === "number")
                    object.endTimestampSecondsUtc = options.longs === String ? String(message.endTimestampSecondsUtc) : message.endTimestampSecondsUtc;
                else
                    object.endTimestampSecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.endTimestampSecondsUtc) : options.longs === Number ? new $util.LongBits(message.endTimestampSecondsUtc.low >>> 0, message.endTimestampSecondsUtc.high >>> 0).toNumber(true) : message.endTimestampSecondsUtc;
            if (message.stepSizeSeconds != null && message.hasOwnProperty("stepSizeSeconds"))
                if (typeof message.stepSizeSeconds === "number")
                    object.stepSizeSeconds = options.longs === String ? String(message.stepSizeSeconds) : message.stepSizeSeconds;
                else
                    object.stepSizeSeconds = options.longs === String ? $util.Long.prototype.toString.call(message.stepSizeSeconds) : options.longs === Number ? new $util.LongBits(message.stepSizeSeconds.low >>> 0, message.stepSizeSeconds.high >>> 0).toNumber(true) : message.stepSizeSeconds;
            if (message.deviceIds && message.deviceIds.length) {
                object.deviceIds = [];
                for (var j = 0; j < message.deviceIds.length; ++j)
                    if (typeof message.deviceIds[j] === "number")
                        object.deviceIds[j] = options.longs === String ? String(message.deviceIds[j]) : message.deviceIds[j];
                    else
                        object.deviceIds[j] = options.longs === String ? $util.Long.prototype.toString.call(message.deviceIds[j]) : options.longs === Number ? new $util.LongBits(message.deviceIds[j].low >>> 0, message.deviceIds[j].high >>> 0).toNumber(true) : message.deviceIds[j];
            }
            if (message.deviceIdsApi900 && message.deviceIdsApi900.length) {
                object.deviceIdsApi900 = [];
                for (var j = 0; j < message.deviceIdsApi900.length; ++j)
                    object.deviceIdsApi900[j] = message.deviceIdsApi900[j];
            }
            return object;
        };

        /**
         * Creates a plain object from this DeviceActivityRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DeviceActivityRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this DeviceActivityRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        DeviceActivityRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DeviceActivityRequest;
    })();

    protobuf.ActiveDevicesRequest = (function() {

        /**
         * Properties of an ActiveDevicesRequest.
         * @typedef protobuf.ActiveDevicesRequest$Properties
         * @type {Object}
         * @property {number|Long} [startTimestampMsEpochUtc] ActiveDevicesRequest startTimestampMsEpochUtc.
         * @property {number|Long} [endTimestampMsEpochUtc] ActiveDevicesRequest endTimestampMsEpochUtc.
         * @property {Array.<string>} [deviceIds] ActiveDevicesRequest deviceIds.
         * @property {string} [authenticatedEmail] ActiveDevicesRequest authenticatedEmail.
         */

        /**
         * Constructs a new ActiveDevicesRequest.
         * @exports protobuf.ActiveDevicesRequest
         * @constructor
         * @param {protobuf.ActiveDevicesRequest$Properties=} [properties] Properties to set
         */
        function ActiveDevicesRequest(properties) {
            this.deviceIds = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * ActiveDevicesRequest startTimestampMsEpochUtc.
         * @type {number|Long|undefined}
         */
        ActiveDevicesRequest.prototype.startTimestampMsEpochUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * ActiveDevicesRequest endTimestampMsEpochUtc.
         * @type {number|Long|undefined}
         */
        ActiveDevicesRequest.prototype.endTimestampMsEpochUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * ActiveDevicesRequest deviceIds.
         * @type {Array.<string>|undefined}
         */
        ActiveDevicesRequest.prototype.deviceIds = $util.emptyArray;

        /**
         * ActiveDevicesRequest authenticatedEmail.
         * @type {string|undefined}
         */
        ActiveDevicesRequest.prototype.authenticatedEmail = "";

        /**
         * Creates a new ActiveDevicesRequest instance using the specified properties.
         * @param {protobuf.ActiveDevicesRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.ActiveDevicesRequest} ActiveDevicesRequest instance
         */
        ActiveDevicesRequest.create = function create(properties) {
            return new ActiveDevicesRequest(properties);
        };

        /**
         * Encodes the specified ActiveDevicesRequest message. Does not implicitly {@link protobuf.ActiveDevicesRequest.verify|verify} messages.
         * @param {protobuf.ActiveDevicesRequest$Properties} message ActiveDevicesRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ActiveDevicesRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.startTimestampMsEpochUtc != null && message.hasOwnProperty("startTimestampMsEpochUtc"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.startTimestampMsEpochUtc);
            if (message.endTimestampMsEpochUtc != null && message.hasOwnProperty("endTimestampMsEpochUtc"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.endTimestampMsEpochUtc);
            if (message.deviceIds && message.deviceIds.length)
                for (var i = 0; i < message.deviceIds.length; ++i)
                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.deviceIds[i]);
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.authenticatedEmail);
            return writer;
        };

        /**
         * Encodes the specified ActiveDevicesRequest message, length delimited. Does not implicitly {@link protobuf.ActiveDevicesRequest.verify|verify} messages.
         * @param {protobuf.ActiveDevicesRequest$Properties} message ActiveDevicesRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ActiveDevicesRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an ActiveDevicesRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.ActiveDevicesRequest} ActiveDevicesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ActiveDevicesRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.ActiveDevicesRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.startTimestampMsEpochUtc = reader.uint64();
                    break;
                case 2:
                    message.endTimestampMsEpochUtc = reader.uint64();
                    break;
                case 3:
                    if (!(message.deviceIds && message.deviceIds.length))
                        message.deviceIds = [];
                    message.deviceIds.push(reader.string());
                    break;
                case 4:
                    message.authenticatedEmail = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an ActiveDevicesRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.ActiveDevicesRequest} ActiveDevicesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ActiveDevicesRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an ActiveDevicesRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        ActiveDevicesRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.startTimestampMsEpochUtc != null)
                if (!$util.isInteger(message.startTimestampMsEpochUtc) && !(message.startTimestampMsEpochUtc && $util.isInteger(message.startTimestampMsEpochUtc.low) && $util.isInteger(message.startTimestampMsEpochUtc.high)))
                    return "startTimestampMsEpochUtc: integer|Long expected";
            if (message.endTimestampMsEpochUtc != null)
                if (!$util.isInteger(message.endTimestampMsEpochUtc) && !(message.endTimestampMsEpochUtc && $util.isInteger(message.endTimestampMsEpochUtc.low) && $util.isInteger(message.endTimestampMsEpochUtc.high)))
                    return "endTimestampMsEpochUtc: integer|Long expected";
            if (message.deviceIds != null) {
                if (!Array.isArray(message.deviceIds))
                    return "deviceIds: array expected";
                for (var i = 0; i < message.deviceIds.length; ++i)
                    if (!$util.isString(message.deviceIds[i]))
                        return "deviceIds: string[] expected";
            }
            if (message.authenticatedEmail != null)
                if (!$util.isString(message.authenticatedEmail))
                    return "authenticatedEmail: string expected";
            return null;
        };

        /**
         * Creates an ActiveDevicesRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.ActiveDevicesRequest} ActiveDevicesRequest
         */
        ActiveDevicesRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.ActiveDevicesRequest)
                return object;
            var message = new $root.protobuf.ActiveDevicesRequest();
            if (object.startTimestampMsEpochUtc != null)
                if ($util.Long)
                    (message.startTimestampMsEpochUtc = $util.Long.fromValue(object.startTimestampMsEpochUtc)).unsigned = true;
                else if (typeof object.startTimestampMsEpochUtc === "string")
                    message.startTimestampMsEpochUtc = parseInt(object.startTimestampMsEpochUtc, 10);
                else if (typeof object.startTimestampMsEpochUtc === "number")
                    message.startTimestampMsEpochUtc = object.startTimestampMsEpochUtc;
                else if (typeof object.startTimestampMsEpochUtc === "object")
                    message.startTimestampMsEpochUtc = new $util.LongBits(object.startTimestampMsEpochUtc.low >>> 0, object.startTimestampMsEpochUtc.high >>> 0).toNumber(true);
            if (object.endTimestampMsEpochUtc != null)
                if ($util.Long)
                    (message.endTimestampMsEpochUtc = $util.Long.fromValue(object.endTimestampMsEpochUtc)).unsigned = true;
                else if (typeof object.endTimestampMsEpochUtc === "string")
                    message.endTimestampMsEpochUtc = parseInt(object.endTimestampMsEpochUtc, 10);
                else if (typeof object.endTimestampMsEpochUtc === "number")
                    message.endTimestampMsEpochUtc = object.endTimestampMsEpochUtc;
                else if (typeof object.endTimestampMsEpochUtc === "object")
                    message.endTimestampMsEpochUtc = new $util.LongBits(object.endTimestampMsEpochUtc.low >>> 0, object.endTimestampMsEpochUtc.high >>> 0).toNumber(true);
            if (object.deviceIds) {
                if (!Array.isArray(object.deviceIds))
                    throw TypeError(".protobuf.ActiveDevicesRequest.deviceIds: array expected");
                message.deviceIds = [];
                for (var i = 0; i < object.deviceIds.length; ++i)
                    message.deviceIds[i] = String(object.deviceIds[i]);
            }
            if (object.authenticatedEmail != null)
                message.authenticatedEmail = String(object.authenticatedEmail);
            return message;
        };

        /**
         * Creates an ActiveDevicesRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.ActiveDevicesRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.ActiveDevicesRequest} ActiveDevicesRequest
         */
        ActiveDevicesRequest.from = ActiveDevicesRequest.fromObject;

        /**
         * Creates a plain object from an ActiveDevicesRequest message. Also converts values to other types if specified.
         * @param {protobuf.ActiveDevicesRequest} message ActiveDevicesRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ActiveDevicesRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.deviceIds = [];
            if (options.defaults) {
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.startTimestampMsEpochUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.startTimestampMsEpochUtc = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.endTimestampMsEpochUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.endTimestampMsEpochUtc = options.longs === String ? "0" : 0;
                object.authenticatedEmail = "";
            }
            if (message.startTimestampMsEpochUtc != null && message.hasOwnProperty("startTimestampMsEpochUtc"))
                if (typeof message.startTimestampMsEpochUtc === "number")
                    object.startTimestampMsEpochUtc = options.longs === String ? String(message.startTimestampMsEpochUtc) : message.startTimestampMsEpochUtc;
                else
                    object.startTimestampMsEpochUtc = options.longs === String ? $util.Long.prototype.toString.call(message.startTimestampMsEpochUtc) : options.longs === Number ? new $util.LongBits(message.startTimestampMsEpochUtc.low >>> 0, message.startTimestampMsEpochUtc.high >>> 0).toNumber(true) : message.startTimestampMsEpochUtc;
            if (message.endTimestampMsEpochUtc != null && message.hasOwnProperty("endTimestampMsEpochUtc"))
                if (typeof message.endTimestampMsEpochUtc === "number")
                    object.endTimestampMsEpochUtc = options.longs === String ? String(message.endTimestampMsEpochUtc) : message.endTimestampMsEpochUtc;
                else
                    object.endTimestampMsEpochUtc = options.longs === String ? $util.Long.prototype.toString.call(message.endTimestampMsEpochUtc) : options.longs === Number ? new $util.LongBits(message.endTimestampMsEpochUtc.low >>> 0, message.endTimestampMsEpochUtc.high >>> 0).toNumber(true) : message.endTimestampMsEpochUtc;
            if (message.deviceIds && message.deviceIds.length) {
                object.deviceIds = [];
                for (var j = 0; j < message.deviceIds.length; ++j)
                    object.deviceIds[j] = message.deviceIds[j];
            }
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                object.authenticatedEmail = message.authenticatedEmail;
            return object;
        };

        /**
         * Creates a plain object from this ActiveDevicesRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ActiveDevicesRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this ActiveDevicesRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        ActiveDevicesRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return ActiveDevicesRequest;
    })();

    protobuf.ActiveDevicesResponse = (function() {

        /**
         * Properties of an ActiveDevicesResponse.
         * @typedef protobuf.ActiveDevicesResponse$Properties
         * @type {Object}
         * @property {Array.<protobuf.RedvoxDevice$Properties>} [redvoxDevices] ActiveDevicesResponse redvoxDevices.
         */

        /**
         * Constructs a new ActiveDevicesResponse.
         * @exports protobuf.ActiveDevicesResponse
         * @constructor
         * @param {protobuf.ActiveDevicesResponse$Properties=} [properties] Properties to set
         */
        function ActiveDevicesResponse(properties) {
            this.redvoxDevices = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * ActiveDevicesResponse redvoxDevices.
         * @type {Array.<protobuf.RedvoxDevice$Properties>|undefined}
         */
        ActiveDevicesResponse.prototype.redvoxDevices = $util.emptyArray;

        /**
         * Creates a new ActiveDevicesResponse instance using the specified properties.
         * @param {protobuf.ActiveDevicesResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.ActiveDevicesResponse} ActiveDevicesResponse instance
         */
        ActiveDevicesResponse.create = function create(properties) {
            return new ActiveDevicesResponse(properties);
        };

        /**
         * Encodes the specified ActiveDevicesResponse message. Does not implicitly {@link protobuf.ActiveDevicesResponse.verify|verify} messages.
         * @param {protobuf.ActiveDevicesResponse$Properties} message ActiveDevicesResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ActiveDevicesResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.redvoxDevices && message.redvoxDevices.length)
                for (var i = 0; i < message.redvoxDevices.length; ++i)
                    $root.protobuf.RedvoxDevice.encode(message.redvoxDevices[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified ActiveDevicesResponse message, length delimited. Does not implicitly {@link protobuf.ActiveDevicesResponse.verify|verify} messages.
         * @param {protobuf.ActiveDevicesResponse$Properties} message ActiveDevicesResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ActiveDevicesResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an ActiveDevicesResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.ActiveDevicesResponse} ActiveDevicesResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ActiveDevicesResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.ActiveDevicesResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.redvoxDevices && message.redvoxDevices.length))
                        message.redvoxDevices = [];
                    message.redvoxDevices.push($root.protobuf.RedvoxDevice.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an ActiveDevicesResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.ActiveDevicesResponse} ActiveDevicesResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ActiveDevicesResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an ActiveDevicesResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        ActiveDevicesResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.redvoxDevices != null) {
                if (!Array.isArray(message.redvoxDevices))
                    return "redvoxDevices: array expected";
                for (var i = 0; i < message.redvoxDevices.length; ++i) {
                    var error = $root.protobuf.RedvoxDevice.verify(message.redvoxDevices[i]);
                    if (error)
                        return "redvoxDevices." + error;
                }
            }
            return null;
        };

        /**
         * Creates an ActiveDevicesResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.ActiveDevicesResponse} ActiveDevicesResponse
         */
        ActiveDevicesResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.ActiveDevicesResponse)
                return object;
            var message = new $root.protobuf.ActiveDevicesResponse();
            if (object.redvoxDevices) {
                if (!Array.isArray(object.redvoxDevices))
                    throw TypeError(".protobuf.ActiveDevicesResponse.redvoxDevices: array expected");
                message.redvoxDevices = [];
                for (var i = 0; i < object.redvoxDevices.length; ++i) {
                    if (typeof object.redvoxDevices[i] !== "object")
                        throw TypeError(".protobuf.ActiveDevicesResponse.redvoxDevices: object expected");
                    message.redvoxDevices[i] = $root.protobuf.RedvoxDevice.fromObject(object.redvoxDevices[i]);
                }
            }
            return message;
        };

        /**
         * Creates an ActiveDevicesResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.ActiveDevicesResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.ActiveDevicesResponse} ActiveDevicesResponse
         */
        ActiveDevicesResponse.from = ActiveDevicesResponse.fromObject;

        /**
         * Creates a plain object from an ActiveDevicesResponse message. Also converts values to other types if specified.
         * @param {protobuf.ActiveDevicesResponse} message ActiveDevicesResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ActiveDevicesResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.redvoxDevices = [];
            if (message.redvoxDevices && message.redvoxDevices.length) {
                object.redvoxDevices = [];
                for (var j = 0; j < message.redvoxDevices.length; ++j)
                    object.redvoxDevices[j] = $root.protobuf.RedvoxDevice.toObject(message.redvoxDevices[j], options);
            }
            return object;
        };

        /**
         * Creates a plain object from this ActiveDevicesResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ActiveDevicesResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this ActiveDevicesResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        ActiveDevicesResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return ActiveDevicesResponse;
    })();

    protobuf.HistoricalDevicesRequest = (function() {

        /**
         * Properties of a HistoricalDevicesRequest.
         * @typedef protobuf.HistoricalDevicesRequest$Properties
         * @type {Object}
         * @property {boolean} [includeOwned] HistoricalDevicesRequest includeOwned.
         */

        /**
         * Constructs a new HistoricalDevicesRequest.
         * @exports protobuf.HistoricalDevicesRequest
         * @constructor
         * @param {protobuf.HistoricalDevicesRequest$Properties=} [properties] Properties to set
         */
        function HistoricalDevicesRequest(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * HistoricalDevicesRequest includeOwned.
         * @type {boolean|undefined}
         */
        HistoricalDevicesRequest.prototype.includeOwned = false;

        /**
         * Creates a new HistoricalDevicesRequest instance using the specified properties.
         * @param {protobuf.HistoricalDevicesRequest$Properties=} [properties] Properties to set
         * @returns {protobuf.HistoricalDevicesRequest} HistoricalDevicesRequest instance
         */
        HistoricalDevicesRequest.create = function create(properties) {
            return new HistoricalDevicesRequest(properties);
        };

        /**
         * Encodes the specified HistoricalDevicesRequest message. Does not implicitly {@link protobuf.HistoricalDevicesRequest.verify|verify} messages.
         * @param {protobuf.HistoricalDevicesRequest$Properties} message HistoricalDevicesRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        HistoricalDevicesRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.includeOwned != null && message.hasOwnProperty("includeOwned"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.includeOwned);
            return writer;
        };

        /**
         * Encodes the specified HistoricalDevicesRequest message, length delimited. Does not implicitly {@link protobuf.HistoricalDevicesRequest.verify|verify} messages.
         * @param {protobuf.HistoricalDevicesRequest$Properties} message HistoricalDevicesRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        HistoricalDevicesRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a HistoricalDevicesRequest message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.HistoricalDevicesRequest} HistoricalDevicesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        HistoricalDevicesRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.HistoricalDevicesRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.includeOwned = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a HistoricalDevicesRequest message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.HistoricalDevicesRequest} HistoricalDevicesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        HistoricalDevicesRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a HistoricalDevicesRequest message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        HistoricalDevicesRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.includeOwned != null)
                if (typeof message.includeOwned !== "boolean")
                    return "includeOwned: boolean expected";
            return null;
        };

        /**
         * Creates a HistoricalDevicesRequest message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.HistoricalDevicesRequest} HistoricalDevicesRequest
         */
        HistoricalDevicesRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.HistoricalDevicesRequest)
                return object;
            var message = new $root.protobuf.HistoricalDevicesRequest();
            if (object.includeOwned != null)
                message.includeOwned = Boolean(object.includeOwned);
            return message;
        };

        /**
         * Creates a HistoricalDevicesRequest message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.HistoricalDevicesRequest.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.HistoricalDevicesRequest} HistoricalDevicesRequest
         */
        HistoricalDevicesRequest.from = HistoricalDevicesRequest.fromObject;

        /**
         * Creates a plain object from a HistoricalDevicesRequest message. Also converts values to other types if specified.
         * @param {protobuf.HistoricalDevicesRequest} message HistoricalDevicesRequest
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        HistoricalDevicesRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.includeOwned = false;
            if (message.includeOwned != null && message.hasOwnProperty("includeOwned"))
                object.includeOwned = message.includeOwned;
            return object;
        };

        /**
         * Creates a plain object from this HistoricalDevicesRequest message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        HistoricalDevicesRequest.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this HistoricalDevicesRequest to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        HistoricalDevicesRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return HistoricalDevicesRequest;
    })();

    protobuf.HistoricalDevicesResponse = (function() {

        /**
         * Properties of a HistoricalDevicesResponse.
         * @typedef protobuf.HistoricalDevicesResponse$Properties
         * @type {Object}
         * @property {Array.<protobuf.RedvoxDevice$Properties>} [redvoxDevices] HistoricalDevicesResponse redvoxDevices.
         */

        /**
         * Constructs a new HistoricalDevicesResponse.
         * @exports protobuf.HistoricalDevicesResponse
         * @constructor
         * @param {protobuf.HistoricalDevicesResponse$Properties=} [properties] Properties to set
         */
        function HistoricalDevicesResponse(properties) {
            this.redvoxDevices = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * HistoricalDevicesResponse redvoxDevices.
         * @type {Array.<protobuf.RedvoxDevice$Properties>|undefined}
         */
        HistoricalDevicesResponse.prototype.redvoxDevices = $util.emptyArray;

        /**
         * Creates a new HistoricalDevicesResponse instance using the specified properties.
         * @param {protobuf.HistoricalDevicesResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.HistoricalDevicesResponse} HistoricalDevicesResponse instance
         */
        HistoricalDevicesResponse.create = function create(properties) {
            return new HistoricalDevicesResponse(properties);
        };

        /**
         * Encodes the specified HistoricalDevicesResponse message. Does not implicitly {@link protobuf.HistoricalDevicesResponse.verify|verify} messages.
         * @param {protobuf.HistoricalDevicesResponse$Properties} message HistoricalDevicesResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        HistoricalDevicesResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.redvoxDevices && message.redvoxDevices.length)
                for (var i = 0; i < message.redvoxDevices.length; ++i)
                    $root.protobuf.RedvoxDevice.encode(message.redvoxDevices[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified HistoricalDevicesResponse message, length delimited. Does not implicitly {@link protobuf.HistoricalDevicesResponse.verify|verify} messages.
         * @param {protobuf.HistoricalDevicesResponse$Properties} message HistoricalDevicesResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        HistoricalDevicesResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a HistoricalDevicesResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.HistoricalDevicesResponse} HistoricalDevicesResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        HistoricalDevicesResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.HistoricalDevicesResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.redvoxDevices && message.redvoxDevices.length))
                        message.redvoxDevices = [];
                    message.redvoxDevices.push($root.protobuf.RedvoxDevice.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a HistoricalDevicesResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.HistoricalDevicesResponse} HistoricalDevicesResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        HistoricalDevicesResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a HistoricalDevicesResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        HistoricalDevicesResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.redvoxDevices != null) {
                if (!Array.isArray(message.redvoxDevices))
                    return "redvoxDevices: array expected";
                for (var i = 0; i < message.redvoxDevices.length; ++i) {
                    var error = $root.protobuf.RedvoxDevice.verify(message.redvoxDevices[i]);
                    if (error)
                        return "redvoxDevices." + error;
                }
            }
            return null;
        };

        /**
         * Creates a HistoricalDevicesResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.HistoricalDevicesResponse} HistoricalDevicesResponse
         */
        HistoricalDevicesResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.HistoricalDevicesResponse)
                return object;
            var message = new $root.protobuf.HistoricalDevicesResponse();
            if (object.redvoxDevices) {
                if (!Array.isArray(object.redvoxDevices))
                    throw TypeError(".protobuf.HistoricalDevicesResponse.redvoxDevices: array expected");
                message.redvoxDevices = [];
                for (var i = 0; i < object.redvoxDevices.length; ++i) {
                    if (typeof object.redvoxDevices[i] !== "object")
                        throw TypeError(".protobuf.HistoricalDevicesResponse.redvoxDevices: object expected");
                    message.redvoxDevices[i] = $root.protobuf.RedvoxDevice.fromObject(object.redvoxDevices[i]);
                }
            }
            return message;
        };

        /**
         * Creates a HistoricalDevicesResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.HistoricalDevicesResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.HistoricalDevicesResponse} HistoricalDevicesResponse
         */
        HistoricalDevicesResponse.from = HistoricalDevicesResponse.fromObject;

        /**
         * Creates a plain object from a HistoricalDevicesResponse message. Also converts values to other types if specified.
         * @param {protobuf.HistoricalDevicesResponse} message HistoricalDevicesResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        HistoricalDevicesResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.redvoxDevices = [];
            if (message.redvoxDevices && message.redvoxDevices.length) {
                object.redvoxDevices = [];
                for (var j = 0; j < message.redvoxDevices.length; ++j)
                    object.redvoxDevices[j] = $root.protobuf.RedvoxDevice.toObject(message.redvoxDevices[j], options);
            }
            return object;
        };

        /**
         * Creates a plain object from this HistoricalDevicesResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        HistoricalDevicesResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this HistoricalDevicesResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        HistoricalDevicesResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return HistoricalDevicesResponse;
    })();

    protobuf.RedvoxDevice = (function() {

        /**
         * Properties of a RedvoxDevice.
         * @typedef protobuf.RedvoxDevice$Properties
         * @type {Object}
         * @property {string} [redvoxId] RedvoxDevice redvoxId.
         * @property {string} [uuid] RedvoxDevice uuid.
         * @property {number|Long} [lastActiveTimestampMsEpochUtc] RedvoxDevice lastActiveTimestampMsEpochUtc.
         * @property {number} [lastLatitude] RedvoxDevice lastLatitude.
         * @property {number} [lastLongitude] RedvoxDevice lastLongitude.
         * @property {string} [os] RedvoxDevice os.
         * @property {string} [authenticatedEmail] RedvoxDevice authenticatedEmail.
         * @property {protobuf.PrivacyPolicy} [privacyPolicy] RedvoxDevice privacyPolicy.
         * @property {string} [firebaseToken] RedvoxDevice firebaseToken.
         */

        /**
         * Constructs a new RedvoxDevice.
         * @exports protobuf.RedvoxDevice
         * @constructor
         * @param {protobuf.RedvoxDevice$Properties=} [properties] Properties to set
         */
        function RedvoxDevice(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RedvoxDevice redvoxId.
         * @type {string|undefined}
         */
        RedvoxDevice.prototype.redvoxId = "";

        /**
         * RedvoxDevice uuid.
         * @type {string|undefined}
         */
        RedvoxDevice.prototype.uuid = "";

        /**
         * RedvoxDevice lastActiveTimestampMsEpochUtc.
         * @type {number|Long|undefined}
         */
        RedvoxDevice.prototype.lastActiveTimestampMsEpochUtc = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * RedvoxDevice lastLatitude.
         * @type {number|undefined}
         */
        RedvoxDevice.prototype.lastLatitude = 0;

        /**
         * RedvoxDevice lastLongitude.
         * @type {number|undefined}
         */
        RedvoxDevice.prototype.lastLongitude = 0;

        /**
         * RedvoxDevice os.
         * @type {string|undefined}
         */
        RedvoxDevice.prototype.os = "";

        /**
         * RedvoxDevice authenticatedEmail.
         * @type {string|undefined}
         */
        RedvoxDevice.prototype.authenticatedEmail = "";

        /**
         * RedvoxDevice privacyPolicy.
         * @type {protobuf.PrivacyPolicy|undefined}
         */
        RedvoxDevice.prototype.privacyPolicy = 0;

        /**
         * RedvoxDevice firebaseToken.
         * @type {string|undefined}
         */
        RedvoxDevice.prototype.firebaseToken = "";

        /**
         * Creates a new RedvoxDevice instance using the specified properties.
         * @param {protobuf.RedvoxDevice$Properties=} [properties] Properties to set
         * @returns {protobuf.RedvoxDevice} RedvoxDevice instance
         */
        RedvoxDevice.create = function create(properties) {
            return new RedvoxDevice(properties);
        };

        /**
         * Encodes the specified RedvoxDevice message. Does not implicitly {@link protobuf.RedvoxDevice.verify|verify} messages.
         * @param {protobuf.RedvoxDevice$Properties} message RedvoxDevice message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RedvoxDevice.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.redvoxId);
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.uuid);
            if (message.lastActiveTimestampMsEpochUtc != null && message.hasOwnProperty("lastActiveTimestampMsEpochUtc"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint64(message.lastActiveTimestampMsEpochUtc);
            if (message.lastLatitude != null && message.hasOwnProperty("lastLatitude"))
                writer.uint32(/* id 4, wireType 1 =*/33).double(message.lastLatitude);
            if (message.lastLongitude != null && message.hasOwnProperty("lastLongitude"))
                writer.uint32(/* id 5, wireType 1 =*/41).double(message.lastLongitude);
            if (message.os != null && message.hasOwnProperty("os"))
                writer.uint32(/* id 6, wireType 2 =*/50).string(message.os);
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                writer.uint32(/* id 7, wireType 2 =*/58).string(message.authenticatedEmail);
            if (message.privacyPolicy != null && message.hasOwnProperty("privacyPolicy"))
                writer.uint32(/* id 8, wireType 0 =*/64).uint32(message.privacyPolicy);
            if (message.firebaseToken != null && message.hasOwnProperty("firebaseToken"))
                writer.uint32(/* id 9, wireType 2 =*/74).string(message.firebaseToken);
            return writer;
        };

        /**
         * Encodes the specified RedvoxDevice message, length delimited. Does not implicitly {@link protobuf.RedvoxDevice.verify|verify} messages.
         * @param {protobuf.RedvoxDevice$Properties} message RedvoxDevice message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RedvoxDevice.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RedvoxDevice message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RedvoxDevice} RedvoxDevice
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RedvoxDevice.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RedvoxDevice();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.redvoxId = reader.string();
                    break;
                case 2:
                    message.uuid = reader.string();
                    break;
                case 3:
                    message.lastActiveTimestampMsEpochUtc = reader.uint64();
                    break;
                case 4:
                    message.lastLatitude = reader.double();
                    break;
                case 5:
                    message.lastLongitude = reader.double();
                    break;
                case 6:
                    message.os = reader.string();
                    break;
                case 7:
                    message.authenticatedEmail = reader.string();
                    break;
                case 8:
                    message.privacyPolicy = reader.uint32();
                    break;
                case 9:
                    message.firebaseToken = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RedvoxDevice message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RedvoxDevice} RedvoxDevice
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RedvoxDevice.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RedvoxDevice message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RedvoxDevice.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.redvoxId != null)
                if (!$util.isString(message.redvoxId))
                    return "redvoxId: string expected";
            if (message.uuid != null)
                if (!$util.isString(message.uuid))
                    return "uuid: string expected";
            if (message.lastActiveTimestampMsEpochUtc != null)
                if (!$util.isInteger(message.lastActiveTimestampMsEpochUtc) && !(message.lastActiveTimestampMsEpochUtc && $util.isInteger(message.lastActiveTimestampMsEpochUtc.low) && $util.isInteger(message.lastActiveTimestampMsEpochUtc.high)))
                    return "lastActiveTimestampMsEpochUtc: integer|Long expected";
            if (message.lastLatitude != null)
                if (typeof message.lastLatitude !== "number")
                    return "lastLatitude: number expected";
            if (message.lastLongitude != null)
                if (typeof message.lastLongitude !== "number")
                    return "lastLongitude: number expected";
            if (message.os != null)
                if (!$util.isString(message.os))
                    return "os: string expected";
            if (message.authenticatedEmail != null)
                if (!$util.isString(message.authenticatedEmail))
                    return "authenticatedEmail: string expected";
            if (message.privacyPolicy != null)
                switch (message.privacyPolicy) {
                default:
                    return "privacyPolicy: enum value expected";
                case 0:
                case 1:
                    break;
                }
            if (message.firebaseToken != null)
                if (!$util.isString(message.firebaseToken))
                    return "firebaseToken: string expected";
            return null;
        };

        /**
         * Creates a RedvoxDevice message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RedvoxDevice} RedvoxDevice
         */
        RedvoxDevice.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RedvoxDevice)
                return object;
            var message = new $root.protobuf.RedvoxDevice();
            if (object.redvoxId != null)
                message.redvoxId = String(object.redvoxId);
            if (object.uuid != null)
                message.uuid = String(object.uuid);
            if (object.lastActiveTimestampMsEpochUtc != null)
                if ($util.Long)
                    (message.lastActiveTimestampMsEpochUtc = $util.Long.fromValue(object.lastActiveTimestampMsEpochUtc)).unsigned = true;
                else if (typeof object.lastActiveTimestampMsEpochUtc === "string")
                    message.lastActiveTimestampMsEpochUtc = parseInt(object.lastActiveTimestampMsEpochUtc, 10);
                else if (typeof object.lastActiveTimestampMsEpochUtc === "number")
                    message.lastActiveTimestampMsEpochUtc = object.lastActiveTimestampMsEpochUtc;
                else if (typeof object.lastActiveTimestampMsEpochUtc === "object")
                    message.lastActiveTimestampMsEpochUtc = new $util.LongBits(object.lastActiveTimestampMsEpochUtc.low >>> 0, object.lastActiveTimestampMsEpochUtc.high >>> 0).toNumber(true);
            if (object.lastLatitude != null)
                message.lastLatitude = Number(object.lastLatitude);
            if (object.lastLongitude != null)
                message.lastLongitude = Number(object.lastLongitude);
            if (object.os != null)
                message.os = String(object.os);
            if (object.authenticatedEmail != null)
                message.authenticatedEmail = String(object.authenticatedEmail);
            switch (object.privacyPolicy) {
            case "PUBLIC":
            case 0:
                message.privacyPolicy = 0;
                break;
            case "PRIVATE":
            case 1:
                message.privacyPolicy = 1;
                break;
            }
            if (object.firebaseToken != null)
                message.firebaseToken = String(object.firebaseToken);
            return message;
        };

        /**
         * Creates a RedvoxDevice message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RedvoxDevice.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RedvoxDevice} RedvoxDevice
         */
        RedvoxDevice.from = RedvoxDevice.fromObject;

        /**
         * Creates a plain object from a RedvoxDevice message. Also converts values to other types if specified.
         * @param {protobuf.RedvoxDevice} message RedvoxDevice
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RedvoxDevice.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.redvoxId = "";
                object.uuid = "";
                if ($util.Long) {
                    var long = new $util.Long(0, 0, true);
                    object.lastActiveTimestampMsEpochUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.lastActiveTimestampMsEpochUtc = options.longs === String ? "0" : 0;
                object.lastLatitude = 0;
                object.lastLongitude = 0;
                object.os = "";
                object.authenticatedEmail = "";
                object.privacyPolicy = options.enums === String ? "PUBLIC" : 0;
                object.firebaseToken = "";
            }
            if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
                object.redvoxId = message.redvoxId;
            if (message.uuid != null && message.hasOwnProperty("uuid"))
                object.uuid = message.uuid;
            if (message.lastActiveTimestampMsEpochUtc != null && message.hasOwnProperty("lastActiveTimestampMsEpochUtc"))
                if (typeof message.lastActiveTimestampMsEpochUtc === "number")
                    object.lastActiveTimestampMsEpochUtc = options.longs === String ? String(message.lastActiveTimestampMsEpochUtc) : message.lastActiveTimestampMsEpochUtc;
                else
                    object.lastActiveTimestampMsEpochUtc = options.longs === String ? $util.Long.prototype.toString.call(message.lastActiveTimestampMsEpochUtc) : options.longs === Number ? new $util.LongBits(message.lastActiveTimestampMsEpochUtc.low >>> 0, message.lastActiveTimestampMsEpochUtc.high >>> 0).toNumber(true) : message.lastActiveTimestampMsEpochUtc;
            if (message.lastLatitude != null && message.hasOwnProperty("lastLatitude"))
                object.lastLatitude = message.lastLatitude;
            if (message.lastLongitude != null && message.hasOwnProperty("lastLongitude"))
                object.lastLongitude = message.lastLongitude;
            if (message.os != null && message.hasOwnProperty("os"))
                object.os = message.os;
            if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
                object.authenticatedEmail = message.authenticatedEmail;
            if (message.privacyPolicy != null && message.hasOwnProperty("privacyPolicy"))
                object.privacyPolicy = options.enums === String ? $root.protobuf.PrivacyPolicy[message.privacyPolicy] : message.privacyPolicy;
            if (message.firebaseToken != null && message.hasOwnProperty("firebaseToken"))
                object.firebaseToken = message.firebaseToken;
            return object;
        };

        /**
         * Creates a plain object from this RedvoxDevice message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RedvoxDevice.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RedvoxDevice to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RedvoxDevice.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RedvoxDevice;
    })();

    /**
     * PrivacyPolicy enum.
     * @name PrivacyPolicy
     * @memberof protobuf
     * @enum {number}
     * @property {number} PUBLIC=0 PUBLIC value
     * @property {number} PRIVATE=1 PRIVATE value
     */
    protobuf.PrivacyPolicy = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "PUBLIC"] = 0;
        values[valuesById[1] = "PRIVATE"] = 1;
        return values;
    })();

    protobuf.DeviceActivityResponse = (function() {

        /**
         * Properties of a DeviceActivityResponse.
         * @typedef protobuf.DeviceActivityResponse$Properties
         * @type {Object}
         * @property {string} [payload] DeviceActivityResponse payload.
         */

        /**
         * Constructs a new DeviceActivityResponse.
         * @exports protobuf.DeviceActivityResponse
         * @constructor
         * @param {protobuf.DeviceActivityResponse$Properties=} [properties] Properties to set
         */
        function DeviceActivityResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * DeviceActivityResponse payload.
         * @type {string|undefined}
         */
        DeviceActivityResponse.prototype.payload = "";

        /**
         * Creates a new DeviceActivityResponse instance using the specified properties.
         * @param {protobuf.DeviceActivityResponse$Properties=} [properties] Properties to set
         * @returns {protobuf.DeviceActivityResponse} DeviceActivityResponse instance
         */
        DeviceActivityResponse.create = function create(properties) {
            return new DeviceActivityResponse(properties);
        };

        /**
         * Encodes the specified DeviceActivityResponse message. Does not implicitly {@link protobuf.DeviceActivityResponse.verify|verify} messages.
         * @param {protobuf.DeviceActivityResponse$Properties} message DeviceActivityResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceActivityResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.payload != null && message.hasOwnProperty("payload"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.payload);
            return writer;
        };

        /**
         * Encodes the specified DeviceActivityResponse message, length delimited. Does not implicitly {@link protobuf.DeviceActivityResponse.verify|verify} messages.
         * @param {protobuf.DeviceActivityResponse$Properties} message DeviceActivityResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceActivityResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DeviceActivityResponse message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.DeviceActivityResponse} DeviceActivityResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceActivityResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.DeviceActivityResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.payload = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DeviceActivityResponse message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.DeviceActivityResponse} DeviceActivityResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceActivityResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DeviceActivityResponse message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        DeviceActivityResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.payload != null)
                if (!$util.isString(message.payload))
                    return "payload: string expected";
            return null;
        };

        /**
         * Creates a DeviceActivityResponse message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DeviceActivityResponse} DeviceActivityResponse
         */
        DeviceActivityResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.DeviceActivityResponse)
                return object;
            var message = new $root.protobuf.DeviceActivityResponse();
            if (object.payload != null)
                message.payload = String(object.payload);
            return message;
        };

        /**
         * Creates a DeviceActivityResponse message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.DeviceActivityResponse.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DeviceActivityResponse} DeviceActivityResponse
         */
        DeviceActivityResponse.from = DeviceActivityResponse.fromObject;

        /**
         * Creates a plain object from a DeviceActivityResponse message. Also converts values to other types if specified.
         * @param {protobuf.DeviceActivityResponse} message DeviceActivityResponse
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DeviceActivityResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.payload = "";
            if (message.payload != null && message.hasOwnProperty("payload"))
                object.payload = message.payload;
            return object;
        };

        /**
         * Creates a plain object from this DeviceActivityResponse message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DeviceActivityResponse.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this DeviceActivityResponse to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        DeviceActivityResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DeviceActivityResponse;
    })();

    protobuf.MetadataApi800Response = (function() {

        /**
         * Properties of a MetadataApi800Response.
         * @typedef protobuf.MetadataApi800Response$Properties
         * @type {Object}
         * @property {string} [url] MetadataApi800Response url.
         */

        /**
         * Constructs a new MetadataApi800Response.
         * @exports protobuf.MetadataApi800Response
         * @constructor
         * @param {protobuf.MetadataApi800Response$Properties=} [properties] Properties to set
         */
        function MetadataApi800Response(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * MetadataApi800Response url.
         * @type {string|undefined}
         */
        MetadataApi800Response.prototype.url = "";

        /**
         * Creates a new MetadataApi800Response instance using the specified properties.
         * @param {protobuf.MetadataApi800Response$Properties=} [properties] Properties to set
         * @returns {protobuf.MetadataApi800Response} MetadataApi800Response instance
         */
        MetadataApi800Response.create = function create(properties) {
            return new MetadataApi800Response(properties);
        };

        /**
         * Encodes the specified MetadataApi800Response message. Does not implicitly {@link protobuf.MetadataApi800Response.verify|verify} messages.
         * @param {protobuf.MetadataApi800Response$Properties} message MetadataApi800Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi800Response.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.url != null && message.hasOwnProperty("url"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.url);
            return writer;
        };

        /**
         * Encodes the specified MetadataApi800Response message, length delimited. Does not implicitly {@link protobuf.MetadataApi800Response.verify|verify} messages.
         * @param {protobuf.MetadataApi800Response$Properties} message MetadataApi800Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi800Response.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a MetadataApi800Response message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.MetadataApi800Response} MetadataApi800Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi800Response.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.MetadataApi800Response();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 2:
                    message.url = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a MetadataApi800Response message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.MetadataApi800Response} MetadataApi800Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi800Response.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a MetadataApi800Response message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        MetadataApi800Response.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.url != null)
                if (!$util.isString(message.url))
                    return "url: string expected";
            return null;
        };

        /**
         * Creates a MetadataApi800Response message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi800Response} MetadataApi800Response
         */
        MetadataApi800Response.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.MetadataApi800Response)
                return object;
            var message = new $root.protobuf.MetadataApi800Response();
            if (object.url != null)
                message.url = String(object.url);
            return message;
        };

        /**
         * Creates a MetadataApi800Response message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.MetadataApi800Response.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi800Response} MetadataApi800Response
         */
        MetadataApi800Response.from = MetadataApi800Response.fromObject;

        /**
         * Creates a plain object from a MetadataApi800Response message. Also converts values to other types if specified.
         * @param {protobuf.MetadataApi800Response} message MetadataApi800Response
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi800Response.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.url = "";
            if (message.url != null && message.hasOwnProperty("url"))
                object.url = message.url;
            return object;
        };

        /**
         * Creates a plain object from this MetadataApi800Response message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi800Response.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this MetadataApi800Response to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        MetadataApi800Response.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return MetadataApi800Response;
    })();

    protobuf.MetadataApi900Response = (function() {

        /**
         * Properties of a MetadataApi900Response.
         * @typedef protobuf.MetadataApi900Response$Properties
         * @type {Object}
         * @property {Array.<RedvoxPacket$Properties>} [redvoxPackets] MetadataApi900Response redvoxPackets.
         */

        /**
         * Constructs a new MetadataApi900Response.
         * @exports protobuf.MetadataApi900Response
         * @constructor
         * @param {protobuf.MetadataApi900Response$Properties=} [properties] Properties to set
         */
        function MetadataApi900Response(properties) {
            this.redvoxPackets = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * MetadataApi900Response redvoxPackets.
         * @type {Array.<RedvoxPacket$Properties>|undefined}
         */
        MetadataApi900Response.prototype.redvoxPackets = $util.emptyArray;

        /**
         * Creates a new MetadataApi900Response instance using the specified properties.
         * @param {protobuf.MetadataApi900Response$Properties=} [properties] Properties to set
         * @returns {protobuf.MetadataApi900Response} MetadataApi900Response instance
         */
        MetadataApi900Response.create = function create(properties) {
            return new MetadataApi900Response(properties);
        };

        /**
         * Encodes the specified MetadataApi900Response message. Does not implicitly {@link protobuf.MetadataApi900Response.verify|verify} messages.
         * @param {protobuf.MetadataApi900Response$Properties} message MetadataApi900Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi900Response.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.redvoxPackets && message.redvoxPackets.length)
                for (var i = 0; i < message.redvoxPackets.length; ++i)
                    $root.RedvoxPacket.encode(message.redvoxPackets[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified MetadataApi900Response message, length delimited. Does not implicitly {@link protobuf.MetadataApi900Response.verify|verify} messages.
         * @param {protobuf.MetadataApi900Response$Properties} message MetadataApi900Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        MetadataApi900Response.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a MetadataApi900Response message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.MetadataApi900Response} MetadataApi900Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi900Response.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.MetadataApi900Response();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.redvoxPackets && message.redvoxPackets.length))
                        message.redvoxPackets = [];
                    message.redvoxPackets.push($root.RedvoxPacket.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a MetadataApi900Response message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.MetadataApi900Response} MetadataApi900Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        MetadataApi900Response.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a MetadataApi900Response message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        MetadataApi900Response.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.redvoxPackets != null) {
                if (!Array.isArray(message.redvoxPackets))
                    return "redvoxPackets: array expected";
                for (var i = 0; i < message.redvoxPackets.length; ++i) {
                    var error = $root.RedvoxPacket.verify(message.redvoxPackets[i]);
                    if (error)
                        return "redvoxPackets." + error;
                }
            }
            return null;
        };

        /**
         * Creates a MetadataApi900Response message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi900Response} MetadataApi900Response
         */
        MetadataApi900Response.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.MetadataApi900Response)
                return object;
            var message = new $root.protobuf.MetadataApi900Response();
            if (object.redvoxPackets) {
                if (!Array.isArray(object.redvoxPackets))
                    throw TypeError(".protobuf.MetadataApi900Response.redvoxPackets: array expected");
                message.redvoxPackets = [];
                for (var i = 0; i < object.redvoxPackets.length; ++i) {
                    if (typeof object.redvoxPackets[i] !== "object")
                        throw TypeError(".protobuf.MetadataApi900Response.redvoxPackets: object expected");
                    message.redvoxPackets[i] = $root.RedvoxPacket.fromObject(object.redvoxPackets[i]);
                }
            }
            return message;
        };

        /**
         * Creates a MetadataApi900Response message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.MetadataApi900Response.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.MetadataApi900Response} MetadataApi900Response
         */
        MetadataApi900Response.from = MetadataApi900Response.fromObject;

        /**
         * Creates a plain object from a MetadataApi900Response message. Also converts values to other types if specified.
         * @param {protobuf.MetadataApi900Response} message MetadataApi900Response
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi900Response.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.redvoxPackets = [];
            if (message.redvoxPackets && message.redvoxPackets.length) {
                object.redvoxPackets = [];
                for (var j = 0; j < message.redvoxPackets.length; ++j)
                    object.redvoxPackets[j] = $root.RedvoxPacket.toObject(message.redvoxPackets[j], options);
            }
            return object;
        };

        /**
         * Creates a plain object from this MetadataApi900Response message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        MetadataApi900Response.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this MetadataApi900Response to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        MetadataApi900Response.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return MetadataApi900Response;
    })();

    protobuf.DetailsApi900Response = (function() {

        /**
         * Properties of a DetailsApi900Response.
         * @typedef protobuf.DetailsApi900Response$Properties
         * @type {Object}
         * @property {RedvoxPacket$Properties} [redvoxPacket] DetailsApi900Response redvoxPacket.
         */

        /**
         * Constructs a new DetailsApi900Response.
         * @exports protobuf.DetailsApi900Response
         * @constructor
         * @param {protobuf.DetailsApi900Response$Properties=} [properties] Properties to set
         */
        function DetailsApi900Response(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * DetailsApi900Response redvoxPacket.
         * @type {RedvoxPacket$Properties|undefined}
         */
        DetailsApi900Response.prototype.redvoxPacket = null;

        /**
         * Creates a new DetailsApi900Response instance using the specified properties.
         * @param {protobuf.DetailsApi900Response$Properties=} [properties] Properties to set
         * @returns {protobuf.DetailsApi900Response} DetailsApi900Response instance
         */
        DetailsApi900Response.create = function create(properties) {
            return new DetailsApi900Response(properties);
        };

        /**
         * Encodes the specified DetailsApi900Response message. Does not implicitly {@link protobuf.DetailsApi900Response.verify|verify} messages.
         * @param {protobuf.DetailsApi900Response$Properties} message DetailsApi900Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DetailsApi900Response.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.redvoxPacket && message.hasOwnProperty("redvoxPacket"))
                $root.RedvoxPacket.encode(message.redvoxPacket, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified DetailsApi900Response message, length delimited. Does not implicitly {@link protobuf.DetailsApi900Response.verify|verify} messages.
         * @param {protobuf.DetailsApi900Response$Properties} message DetailsApi900Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DetailsApi900Response.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DetailsApi900Response message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.DetailsApi900Response} DetailsApi900Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DetailsApi900Response.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.DetailsApi900Response();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.redvoxPacket = $root.RedvoxPacket.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DetailsApi900Response message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.DetailsApi900Response} DetailsApi900Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DetailsApi900Response.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DetailsApi900Response message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        DetailsApi900Response.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.redvoxPacket != null) {
                var error = $root.RedvoxPacket.verify(message.redvoxPacket);
                if (error)
                    return "redvoxPacket." + error;
            }
            return null;
        };

        /**
         * Creates a DetailsApi900Response message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DetailsApi900Response} DetailsApi900Response
         */
        DetailsApi900Response.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.DetailsApi900Response)
                return object;
            var message = new $root.protobuf.DetailsApi900Response();
            if (object.redvoxPacket != null) {
                if (typeof object.redvoxPacket !== "object")
                    throw TypeError(".protobuf.DetailsApi900Response.redvoxPacket: object expected");
                message.redvoxPacket = $root.RedvoxPacket.fromObject(object.redvoxPacket);
            }
            return message;
        };

        /**
         * Creates a DetailsApi900Response message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.DetailsApi900Response.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.DetailsApi900Response} DetailsApi900Response
         */
        DetailsApi900Response.from = DetailsApi900Response.fromObject;

        /**
         * Creates a plain object from a DetailsApi900Response message. Also converts values to other types if specified.
         * @param {protobuf.DetailsApi900Response} message DetailsApi900Response
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DetailsApi900Response.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.redvoxPacket = null;
            if (message.redvoxPacket != null && message.hasOwnProperty("redvoxPacket"))
                object.redvoxPacket = $root.RedvoxPacket.toObject(message.redvoxPacket, options);
            return object;
        };

        /**
         * Creates a plain object from this DetailsApi900Response message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DetailsApi900Response.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this DetailsApi900Response to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        DetailsApi900Response.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DetailsApi900Response;
    })();

    protobuf.RdvxzS3Response = (function() {

        /**
         * Properties of a RdvxzS3Response.
         * @typedef protobuf.RdvxzS3Response$Properties
         * @type {Object}
         * @property {Uint8Array} [rdvxz] RdvxzS3Response rdvxz.
         */

        /**
         * Constructs a new RdvxzS3Response.
         * @exports protobuf.RdvxzS3Response
         * @constructor
         * @param {protobuf.RdvxzS3Response$Properties=} [properties] Properties to set
         */
        function RdvxzS3Response(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    this[keys[i]] = properties[keys[i]];
        }

        /**
         * RdvxzS3Response rdvxz.
         * @type {Uint8Array|undefined}
         */
        RdvxzS3Response.prototype.rdvxz = $util.newBuffer([]);

        /**
         * Creates a new RdvxzS3Response instance using the specified properties.
         * @param {protobuf.RdvxzS3Response$Properties=} [properties] Properties to set
         * @returns {protobuf.RdvxzS3Response} RdvxzS3Response instance
         */
        RdvxzS3Response.create = function create(properties) {
            return new RdvxzS3Response(properties);
        };

        /**
         * Encodes the specified RdvxzS3Response message. Does not implicitly {@link protobuf.RdvxzS3Response.verify|verify} messages.
         * @param {protobuf.RdvxzS3Response$Properties} message RdvxzS3Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RdvxzS3Response.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.rdvxz && message.hasOwnProperty("rdvxz"))
                writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.rdvxz);
            return writer;
        };

        /**
         * Encodes the specified RdvxzS3Response message, length delimited. Does not implicitly {@link protobuf.RdvxzS3Response.verify|verify} messages.
         * @param {protobuf.RdvxzS3Response$Properties} message RdvxzS3Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RdvxzS3Response.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RdvxzS3Response message from the specified reader or buffer.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {protobuf.RdvxzS3Response} RdvxzS3Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RdvxzS3Response.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.protobuf.RdvxzS3Response();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.rdvxz = reader.bytes();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RdvxzS3Response message from the specified reader or buffer, length delimited.
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {protobuf.RdvxzS3Response} RdvxzS3Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RdvxzS3Response.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RdvxzS3Response message.
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {?string} `null` if valid, otherwise the reason why it is not
         */
        RdvxzS3Response.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.rdvxz != null)
                if (!(message.rdvxz && typeof message.rdvxz.length === "number" || $util.isString(message.rdvxz)))
                    return "rdvxz: buffer expected";
            return null;
        };

        /**
         * Creates a RdvxzS3Response message from a plain object. Also converts values to their respective internal types.
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RdvxzS3Response} RdvxzS3Response
         */
        RdvxzS3Response.fromObject = function fromObject(object) {
            if (object instanceof $root.protobuf.RdvxzS3Response)
                return object;
            var message = new $root.protobuf.RdvxzS3Response();
            if (object.rdvxz != null)
                if (typeof object.rdvxz === "string")
                    $util.base64.decode(object.rdvxz, message.rdvxz = $util.newBuffer($util.base64.length(object.rdvxz)), 0);
                else if (object.rdvxz.length)
                    message.rdvxz = object.rdvxz;
            return message;
        };

        /**
         * Creates a RdvxzS3Response message from a plain object. Also converts values to their respective internal types.
         * This is an alias of {@link protobuf.RdvxzS3Response.fromObject}.
         * @function
         * @param {Object.<string,*>} object Plain object
         * @returns {protobuf.RdvxzS3Response} RdvxzS3Response
         */
        RdvxzS3Response.from = RdvxzS3Response.fromObject;

        /**
         * Creates a plain object from a RdvxzS3Response message. Also converts values to other types if specified.
         * @param {protobuf.RdvxzS3Response} message RdvxzS3Response
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RdvxzS3Response.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.rdvxz = options.bytes === String ? "" : [];
            if (message.rdvxz != null && message.hasOwnProperty("rdvxz"))
                object.rdvxz = options.bytes === String ? $util.base64.encode(message.rdvxz, 0, message.rdvxz.length) : options.bytes === Array ? Array.prototype.slice.call(message.rdvxz) : message.rdvxz;
            return object;
        };

        /**
         * Creates a plain object from this RdvxzS3Response message. Also converts values to other types if specified.
         * @param {$protobuf.ConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RdvxzS3Response.prototype.toObject = function toObject(options) {
            return this.constructor.toObject(this, options);
        };

        /**
         * Converts this RdvxzS3Response to JSON.
         * @returns {Object.<string,*>} JSON object
         */
        RdvxzS3Response.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RdvxzS3Response;
    })();

    return protobuf;
})();

$root.RedvoxPacket = (function() {

    /**
     * Properties of a RedvoxPacket.
     * @typedef RedvoxPacket$Properties
     * @type {Object}
     * @property {number} [api] RedvoxPacket api.
     * @property {string} [uuid] RedvoxPacket uuid.
     * @property {string} [redvoxId] RedvoxPacket redvoxId.
     * @property {string} [authenticatedEmail] RedvoxPacket authenticatedEmail.
     * @property {string} [authenticationToken] RedvoxPacket authenticationToken.
     * @property {string} [firebaseToken] RedvoxPacket firebaseToken.
     * @property {boolean} [isBackfilled] RedvoxPacket isBackfilled.
     * @property {boolean} [isPrivate] RedvoxPacket isPrivate.
     * @property {boolean} [isScrambled] RedvoxPacket isScrambled.
     * @property {string} [deviceMake] RedvoxPacket deviceMake.
     * @property {string} [deviceModel] RedvoxPacket deviceModel.
     * @property {string} [deviceOs] RedvoxPacket deviceOs.
     * @property {string} [deviceOsVersion] RedvoxPacket deviceOsVersion.
     * @property {string} [appVersion] RedvoxPacket appVersion.
     * @property {number} [batteryLevelPercent] RedvoxPacket batteryLevelPercent.
     * @property {number} [deviceTemperatureC] RedvoxPacket deviceTemperatureC.
     * @property {string} [acquisitionServer] RedvoxPacket acquisitionServer.
     * @property {string} [timeSynchronizationServer] RedvoxPacket timeSynchronizationServer.
     * @property {string} [authenticationServer] RedvoxPacket authenticationServer.
     * @property {number|Long} [appFileStartTimestampEpochMicrosecondsUtc] RedvoxPacket appFileStartTimestampEpochMicrosecondsUtc.
     * @property {number|Long} [appFileStartTimestampMachine] RedvoxPacket appFileStartTimestampMachine.
     * @property {number|Long} [serverTimestampEpochMicrosecondsUtc] RedvoxPacket serverTimestampEpochMicrosecondsUtc.
     * @property {Array.<EvenlySampledChannel$Properties>} [evenlySampledChannels] RedvoxPacket evenlySampledChannels.
     * @property {Array.<UnevenlySampledChannel$Properties>} [unevenlySampledChannels] RedvoxPacket unevenlySampledChannels.
     * @property {Array.<string>} [metadata] RedvoxPacket metadata.
     */

    /**
     * Constructs a new RedvoxPacket.
     * @exports RedvoxPacket
     * @constructor
     * @param {RedvoxPacket$Properties=} [properties] Properties to set
     */
    function RedvoxPacket(properties) {
        this.evenlySampledChannels = [];
        this.unevenlySampledChannels = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * RedvoxPacket api.
     * @type {number|undefined}
     */
    RedvoxPacket.prototype.api = 0;

    /**
     * RedvoxPacket uuid.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.uuid = "";

    /**
     * RedvoxPacket redvoxId.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.redvoxId = "";

    /**
     * RedvoxPacket authenticatedEmail.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.authenticatedEmail = "";

    /**
     * RedvoxPacket authenticationToken.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.authenticationToken = "";

    /**
     * RedvoxPacket firebaseToken.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.firebaseToken = "";

    /**
     * RedvoxPacket isBackfilled.
     * @type {boolean|undefined}
     */
    RedvoxPacket.prototype.isBackfilled = false;

    /**
     * RedvoxPacket isPrivate.
     * @type {boolean|undefined}
     */
    RedvoxPacket.prototype.isPrivate = false;

    /**
     * RedvoxPacket isScrambled.
     * @type {boolean|undefined}
     */
    RedvoxPacket.prototype.isScrambled = false;

    /**
     * RedvoxPacket deviceMake.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceMake = "";

    /**
     * RedvoxPacket deviceModel.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceModel = "";

    /**
     * RedvoxPacket deviceOs.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceOs = "";

    /**
     * RedvoxPacket deviceOsVersion.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceOsVersion = "";

    /**
     * RedvoxPacket appVersion.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.appVersion = "";

    /**
     * RedvoxPacket batteryLevelPercent.
     * @type {number|undefined}
     */
    RedvoxPacket.prototype.batteryLevelPercent = 0;

    /**
     * RedvoxPacket deviceTemperatureC.
     * @type {number|undefined}
     */
    RedvoxPacket.prototype.deviceTemperatureC = 0;

    /**
     * RedvoxPacket acquisitionServer.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.acquisitionServer = "";

    /**
     * RedvoxPacket timeSynchronizationServer.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.timeSynchronizationServer = "";

    /**
     * RedvoxPacket authenticationServer.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.authenticationServer = "";

    /**
     * RedvoxPacket appFileStartTimestampEpochMicrosecondsUtc.
     * @type {number|Long|undefined}
     */
    RedvoxPacket.prototype.appFileStartTimestampEpochMicrosecondsUtc = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacket appFileStartTimestampMachine.
     * @type {number|Long|undefined}
     */
    RedvoxPacket.prototype.appFileStartTimestampMachine = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacket serverTimestampEpochMicrosecondsUtc.
     * @type {number|Long|undefined}
     */
    RedvoxPacket.prototype.serverTimestampEpochMicrosecondsUtc = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacket evenlySampledChannels.
     * @type {Array.<EvenlySampledChannel$Properties>|undefined}
     */
    RedvoxPacket.prototype.evenlySampledChannels = $util.emptyArray;

    /**
     * RedvoxPacket unevenlySampledChannels.
     * @type {Array.<UnevenlySampledChannel$Properties>|undefined}
     */
    RedvoxPacket.prototype.unevenlySampledChannels = $util.emptyArray;

    /**
     * RedvoxPacket metadata.
     * @type {Array.<string>|undefined}
     */
    RedvoxPacket.prototype.metadata = $util.emptyArray;

    /**
     * Creates a new RedvoxPacket instance using the specified properties.
     * @param {RedvoxPacket$Properties=} [properties] Properties to set
     * @returns {RedvoxPacket} RedvoxPacket instance
     */
    RedvoxPacket.create = function create(properties) {
        return new RedvoxPacket(properties);
    };

    /**
     * Encodes the specified RedvoxPacket message. Does not implicitly {@link RedvoxPacket.verify|verify} messages.
     * @param {RedvoxPacket$Properties} message RedvoxPacket message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacket.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.api != null && message.hasOwnProperty("api"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.api);
        if (message.uuid != null && message.hasOwnProperty("uuid"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.uuid);
        if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.redvoxId);
        if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
            writer.uint32(/* id 4, wireType 2 =*/34).string(message.authenticatedEmail);
        if (message.authenticationToken != null && message.hasOwnProperty("authenticationToken"))
            writer.uint32(/* id 5, wireType 2 =*/42).string(message.authenticationToken);
        if (message.isBackfilled != null && message.hasOwnProperty("isBackfilled"))
            writer.uint32(/* id 6, wireType 0 =*/48).bool(message.isBackfilled);
        if (message.isPrivate != null && message.hasOwnProperty("isPrivate"))
            writer.uint32(/* id 7, wireType 0 =*/56).bool(message.isPrivate);
        if (message.isScrambled != null && message.hasOwnProperty("isScrambled"))
            writer.uint32(/* id 8, wireType 0 =*/64).bool(message.isScrambled);
        if (message.deviceMake != null && message.hasOwnProperty("deviceMake"))
            writer.uint32(/* id 9, wireType 2 =*/74).string(message.deviceMake);
        if (message.deviceModel != null && message.hasOwnProperty("deviceModel"))
            writer.uint32(/* id 10, wireType 2 =*/82).string(message.deviceModel);
        if (message.deviceOs != null && message.hasOwnProperty("deviceOs"))
            writer.uint32(/* id 11, wireType 2 =*/90).string(message.deviceOs);
        if (message.deviceOsVersion != null && message.hasOwnProperty("deviceOsVersion"))
            writer.uint32(/* id 12, wireType 2 =*/98).string(message.deviceOsVersion);
        if (message.appVersion != null && message.hasOwnProperty("appVersion"))
            writer.uint32(/* id 13, wireType 2 =*/106).string(message.appVersion);
        if (message.acquisitionServer != null && message.hasOwnProperty("acquisitionServer"))
            writer.uint32(/* id 14, wireType 2 =*/114).string(message.acquisitionServer);
        if (message.timeSynchronizationServer != null && message.hasOwnProperty("timeSynchronizationServer"))
            writer.uint32(/* id 15, wireType 2 =*/122).string(message.timeSynchronizationServer);
        if (message.authenticationServer != null && message.hasOwnProperty("authenticationServer"))
            writer.uint32(/* id 16, wireType 2 =*/130).string(message.authenticationServer);
        if (message.appFileStartTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("appFileStartTimestampEpochMicrosecondsUtc"))
            writer.uint32(/* id 17, wireType 0 =*/136).int64(message.appFileStartTimestampEpochMicrosecondsUtc);
        if (message.appFileStartTimestampMachine != null && message.hasOwnProperty("appFileStartTimestampMachine"))
            writer.uint32(/* id 18, wireType 0 =*/144).int64(message.appFileStartTimestampMachine);
        if (message.serverTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("serverTimestampEpochMicrosecondsUtc"))
            writer.uint32(/* id 19, wireType 0 =*/152).int64(message.serverTimestampEpochMicrosecondsUtc);
        if (message.evenlySampledChannels && message.evenlySampledChannels.length)
            for (var i = 0; i < message.evenlySampledChannels.length; ++i)
                $root.EvenlySampledChannel.encode(message.evenlySampledChannels[i], writer.uint32(/* id 20, wireType 2 =*/162).fork()).ldelim();
        if (message.unevenlySampledChannels && message.unevenlySampledChannels.length)
            for (var i = 0; i < message.unevenlySampledChannels.length; ++i)
                $root.UnevenlySampledChannel.encode(message.unevenlySampledChannels[i], writer.uint32(/* id 21, wireType 2 =*/170).fork()).ldelim();
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 22, wireType 2 =*/178).string(message.metadata[i]);
        if (message.firebaseToken != null && message.hasOwnProperty("firebaseToken"))
            writer.uint32(/* id 23, wireType 2 =*/186).string(message.firebaseToken);
        if (message.batteryLevelPercent != null && message.hasOwnProperty("batteryLevelPercent"))
            writer.uint32(/* id 24, wireType 5 =*/197).float(message.batteryLevelPercent);
        if (message.deviceTemperatureC != null && message.hasOwnProperty("deviceTemperatureC"))
            writer.uint32(/* id 25, wireType 5 =*/205).float(message.deviceTemperatureC);
        return writer;
    };

    /**
     * Encodes the specified RedvoxPacket message, length delimited. Does not implicitly {@link RedvoxPacket.verify|verify} messages.
     * @param {RedvoxPacket$Properties} message RedvoxPacket message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacket.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a RedvoxPacket message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {RedvoxPacket} RedvoxPacket
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacket.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.RedvoxPacket();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.api = reader.uint32();
                break;
            case 2:
                message.uuid = reader.string();
                break;
            case 3:
                message.redvoxId = reader.string();
                break;
            case 4:
                message.authenticatedEmail = reader.string();
                break;
            case 5:
                message.authenticationToken = reader.string();
                break;
            case 23:
                message.firebaseToken = reader.string();
                break;
            case 6:
                message.isBackfilled = reader.bool();
                break;
            case 7:
                message.isPrivate = reader.bool();
                break;
            case 8:
                message.isScrambled = reader.bool();
                break;
            case 9:
                message.deviceMake = reader.string();
                break;
            case 10:
                message.deviceModel = reader.string();
                break;
            case 11:
                message.deviceOs = reader.string();
                break;
            case 12:
                message.deviceOsVersion = reader.string();
                break;
            case 13:
                message.appVersion = reader.string();
                break;
            case 24:
                message.batteryLevelPercent = reader.float();
                break;
            case 25:
                message.deviceTemperatureC = reader.float();
                break;
            case 14:
                message.acquisitionServer = reader.string();
                break;
            case 15:
                message.timeSynchronizationServer = reader.string();
                break;
            case 16:
                message.authenticationServer = reader.string();
                break;
            case 17:
                message.appFileStartTimestampEpochMicrosecondsUtc = reader.int64();
                break;
            case 18:
                message.appFileStartTimestampMachine = reader.int64();
                break;
            case 19:
                message.serverTimestampEpochMicrosecondsUtc = reader.int64();
                break;
            case 20:
                if (!(message.evenlySampledChannels && message.evenlySampledChannels.length))
                    message.evenlySampledChannels = [];
                message.evenlySampledChannels.push($root.EvenlySampledChannel.decode(reader, reader.uint32()));
                break;
            case 21:
                if (!(message.unevenlySampledChannels && message.unevenlySampledChannels.length))
                    message.unevenlySampledChannels = [];
                message.unevenlySampledChannels.push($root.UnevenlySampledChannel.decode(reader, reader.uint32()));
                break;
            case 22:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a RedvoxPacket message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {RedvoxPacket} RedvoxPacket
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacket.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a RedvoxPacket message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    RedvoxPacket.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.api != null)
            if (!$util.isInteger(message.api))
                return "api: integer expected";
        if (message.uuid != null)
            if (!$util.isString(message.uuid))
                return "uuid: string expected";
        if (message.redvoxId != null)
            if (!$util.isString(message.redvoxId))
                return "redvoxId: string expected";
        if (message.authenticatedEmail != null)
            if (!$util.isString(message.authenticatedEmail))
                return "authenticatedEmail: string expected";
        if (message.authenticationToken != null)
            if (!$util.isString(message.authenticationToken))
                return "authenticationToken: string expected";
        if (message.firebaseToken != null)
            if (!$util.isString(message.firebaseToken))
                return "firebaseToken: string expected";
        if (message.isBackfilled != null)
            if (typeof message.isBackfilled !== "boolean")
                return "isBackfilled: boolean expected";
        if (message.isPrivate != null)
            if (typeof message.isPrivate !== "boolean")
                return "isPrivate: boolean expected";
        if (message.isScrambled != null)
            if (typeof message.isScrambled !== "boolean")
                return "isScrambled: boolean expected";
        if (message.deviceMake != null)
            if (!$util.isString(message.deviceMake))
                return "deviceMake: string expected";
        if (message.deviceModel != null)
            if (!$util.isString(message.deviceModel))
                return "deviceModel: string expected";
        if (message.deviceOs != null)
            if (!$util.isString(message.deviceOs))
                return "deviceOs: string expected";
        if (message.deviceOsVersion != null)
            if (!$util.isString(message.deviceOsVersion))
                return "deviceOsVersion: string expected";
        if (message.appVersion != null)
            if (!$util.isString(message.appVersion))
                return "appVersion: string expected";
        if (message.batteryLevelPercent != null)
            if (typeof message.batteryLevelPercent !== "number")
                return "batteryLevelPercent: number expected";
        if (message.deviceTemperatureC != null)
            if (typeof message.deviceTemperatureC !== "number")
                return "deviceTemperatureC: number expected";
        if (message.acquisitionServer != null)
            if (!$util.isString(message.acquisitionServer))
                return "acquisitionServer: string expected";
        if (message.timeSynchronizationServer != null)
            if (!$util.isString(message.timeSynchronizationServer))
                return "timeSynchronizationServer: string expected";
        if (message.authenticationServer != null)
            if (!$util.isString(message.authenticationServer))
                return "authenticationServer: string expected";
        if (message.appFileStartTimestampEpochMicrosecondsUtc != null)
            if (!$util.isInteger(message.appFileStartTimestampEpochMicrosecondsUtc) && !(message.appFileStartTimestampEpochMicrosecondsUtc && $util.isInteger(message.appFileStartTimestampEpochMicrosecondsUtc.low) && $util.isInteger(message.appFileStartTimestampEpochMicrosecondsUtc.high)))
                return "appFileStartTimestampEpochMicrosecondsUtc: integer|Long expected";
        if (message.appFileStartTimestampMachine != null)
            if (!$util.isInteger(message.appFileStartTimestampMachine) && !(message.appFileStartTimestampMachine && $util.isInteger(message.appFileStartTimestampMachine.low) && $util.isInteger(message.appFileStartTimestampMachine.high)))
                return "appFileStartTimestampMachine: integer|Long expected";
        if (message.serverTimestampEpochMicrosecondsUtc != null)
            if (!$util.isInteger(message.serverTimestampEpochMicrosecondsUtc) && !(message.serverTimestampEpochMicrosecondsUtc && $util.isInteger(message.serverTimestampEpochMicrosecondsUtc.low) && $util.isInteger(message.serverTimestampEpochMicrosecondsUtc.high)))
                return "serverTimestampEpochMicrosecondsUtc: integer|Long expected";
        if (message.evenlySampledChannels != null) {
            if (!Array.isArray(message.evenlySampledChannels))
                return "evenlySampledChannels: array expected";
            for (var i = 0; i < message.evenlySampledChannels.length; ++i) {
                var error = $root.EvenlySampledChannel.verify(message.evenlySampledChannels[i]);
                if (error)
                    return "evenlySampledChannels." + error;
            }
        }
        if (message.unevenlySampledChannels != null) {
            if (!Array.isArray(message.unevenlySampledChannels))
                return "unevenlySampledChannels: array expected";
            for (var i = 0; i < message.unevenlySampledChannels.length; ++i) {
                var error = $root.UnevenlySampledChannel.verify(message.unevenlySampledChannels[i]);
                if (error)
                    return "unevenlySampledChannels." + error;
            }
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates a RedvoxPacket message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacket} RedvoxPacket
     */
    RedvoxPacket.fromObject = function fromObject(object) {
        if (object instanceof $root.RedvoxPacket)
            return object;
        var message = new $root.RedvoxPacket();
        if (object.api != null)
            message.api = object.api >>> 0;
        if (object.uuid != null)
            message.uuid = String(object.uuid);
        if (object.redvoxId != null)
            message.redvoxId = String(object.redvoxId);
        if (object.authenticatedEmail != null)
            message.authenticatedEmail = String(object.authenticatedEmail);
        if (object.authenticationToken != null)
            message.authenticationToken = String(object.authenticationToken);
        if (object.firebaseToken != null)
            message.firebaseToken = String(object.firebaseToken);
        if (object.isBackfilled != null)
            message.isBackfilled = Boolean(object.isBackfilled);
        if (object.isPrivate != null)
            message.isPrivate = Boolean(object.isPrivate);
        if (object.isScrambled != null)
            message.isScrambled = Boolean(object.isScrambled);
        if (object.deviceMake != null)
            message.deviceMake = String(object.deviceMake);
        if (object.deviceModel != null)
            message.deviceModel = String(object.deviceModel);
        if (object.deviceOs != null)
            message.deviceOs = String(object.deviceOs);
        if (object.deviceOsVersion != null)
            message.deviceOsVersion = String(object.deviceOsVersion);
        if (object.appVersion != null)
            message.appVersion = String(object.appVersion);
        if (object.batteryLevelPercent != null)
            message.batteryLevelPercent = Number(object.batteryLevelPercent);
        if (object.deviceTemperatureC != null)
            message.deviceTemperatureC = Number(object.deviceTemperatureC);
        if (object.acquisitionServer != null)
            message.acquisitionServer = String(object.acquisitionServer);
        if (object.timeSynchronizationServer != null)
            message.timeSynchronizationServer = String(object.timeSynchronizationServer);
        if (object.authenticationServer != null)
            message.authenticationServer = String(object.authenticationServer);
        if (object.appFileStartTimestampEpochMicrosecondsUtc != null)
            if ($util.Long)
                (message.appFileStartTimestampEpochMicrosecondsUtc = $util.Long.fromValue(object.appFileStartTimestampEpochMicrosecondsUtc)).unsigned = false;
            else if (typeof object.appFileStartTimestampEpochMicrosecondsUtc === "string")
                message.appFileStartTimestampEpochMicrosecondsUtc = parseInt(object.appFileStartTimestampEpochMicrosecondsUtc, 10);
            else if (typeof object.appFileStartTimestampEpochMicrosecondsUtc === "number")
                message.appFileStartTimestampEpochMicrosecondsUtc = object.appFileStartTimestampEpochMicrosecondsUtc;
            else if (typeof object.appFileStartTimestampEpochMicrosecondsUtc === "object")
                message.appFileStartTimestampEpochMicrosecondsUtc = new $util.LongBits(object.appFileStartTimestampEpochMicrosecondsUtc.low >>> 0, object.appFileStartTimestampEpochMicrosecondsUtc.high >>> 0).toNumber();
        if (object.appFileStartTimestampMachine != null)
            if ($util.Long)
                (message.appFileStartTimestampMachine = $util.Long.fromValue(object.appFileStartTimestampMachine)).unsigned = false;
            else if (typeof object.appFileStartTimestampMachine === "string")
                message.appFileStartTimestampMachine = parseInt(object.appFileStartTimestampMachine, 10);
            else if (typeof object.appFileStartTimestampMachine === "number")
                message.appFileStartTimestampMachine = object.appFileStartTimestampMachine;
            else if (typeof object.appFileStartTimestampMachine === "object")
                message.appFileStartTimestampMachine = new $util.LongBits(object.appFileStartTimestampMachine.low >>> 0, object.appFileStartTimestampMachine.high >>> 0).toNumber();
        if (object.serverTimestampEpochMicrosecondsUtc != null)
            if ($util.Long)
                (message.serverTimestampEpochMicrosecondsUtc = $util.Long.fromValue(object.serverTimestampEpochMicrosecondsUtc)).unsigned = false;
            else if (typeof object.serverTimestampEpochMicrosecondsUtc === "string")
                message.serverTimestampEpochMicrosecondsUtc = parseInt(object.serverTimestampEpochMicrosecondsUtc, 10);
            else if (typeof object.serverTimestampEpochMicrosecondsUtc === "number")
                message.serverTimestampEpochMicrosecondsUtc = object.serverTimestampEpochMicrosecondsUtc;
            else if (typeof object.serverTimestampEpochMicrosecondsUtc === "object")
                message.serverTimestampEpochMicrosecondsUtc = new $util.LongBits(object.serverTimestampEpochMicrosecondsUtc.low >>> 0, object.serverTimestampEpochMicrosecondsUtc.high >>> 0).toNumber();
        if (object.evenlySampledChannels) {
            if (!Array.isArray(object.evenlySampledChannels))
                throw TypeError(".RedvoxPacket.evenlySampledChannels: array expected");
            message.evenlySampledChannels = [];
            for (var i = 0; i < object.evenlySampledChannels.length; ++i) {
                if (typeof object.evenlySampledChannels[i] !== "object")
                    throw TypeError(".RedvoxPacket.evenlySampledChannels: object expected");
                message.evenlySampledChannels[i] = $root.EvenlySampledChannel.fromObject(object.evenlySampledChannels[i]);
            }
        }
        if (object.unevenlySampledChannels) {
            if (!Array.isArray(object.unevenlySampledChannels))
                throw TypeError(".RedvoxPacket.unevenlySampledChannels: array expected");
            message.unevenlySampledChannels = [];
            for (var i = 0; i < object.unevenlySampledChannels.length; ++i) {
                if (typeof object.unevenlySampledChannels[i] !== "object")
                    throw TypeError(".RedvoxPacket.unevenlySampledChannels: object expected");
                message.unevenlySampledChannels[i] = $root.UnevenlySampledChannel.fromObject(object.unevenlySampledChannels[i]);
            }
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".RedvoxPacket.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates a RedvoxPacket message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link RedvoxPacket.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacket} RedvoxPacket
     */
    RedvoxPacket.from = RedvoxPacket.fromObject;

    /**
     * Creates a plain object from a RedvoxPacket message. Also converts values to other types if specified.
     * @param {RedvoxPacket} message RedvoxPacket
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacket.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.evenlySampledChannels = [];
            object.unevenlySampledChannels = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.api = 0;
            object.uuid = "";
            object.redvoxId = "";
            object.authenticatedEmail = "";
            object.authenticationToken = "";
            object.isBackfilled = false;
            object.isPrivate = false;
            object.isScrambled = false;
            object.deviceMake = "";
            object.deviceModel = "";
            object.deviceOs = "";
            object.deviceOsVersion = "";
            object.appVersion = "";
            object.acquisitionServer = "";
            object.timeSynchronizationServer = "";
            object.authenticationServer = "";
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? "0" : 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.appFileStartTimestampMachine = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.appFileStartTimestampMachine = options.longs === String ? "0" : 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? "0" : 0;
            object.firebaseToken = "";
            object.batteryLevelPercent = 0;
            object.deviceTemperatureC = 0;
        }
        if (message.api != null && message.hasOwnProperty("api"))
            object.api = message.api;
        if (message.uuid != null && message.hasOwnProperty("uuid"))
            object.uuid = message.uuid;
        if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
            object.redvoxId = message.redvoxId;
        if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
            object.authenticatedEmail = message.authenticatedEmail;
        if (message.authenticationToken != null && message.hasOwnProperty("authenticationToken"))
            object.authenticationToken = message.authenticationToken;
        if (message.isBackfilled != null && message.hasOwnProperty("isBackfilled"))
            object.isBackfilled = message.isBackfilled;
        if (message.isPrivate != null && message.hasOwnProperty("isPrivate"))
            object.isPrivate = message.isPrivate;
        if (message.isScrambled != null && message.hasOwnProperty("isScrambled"))
            object.isScrambled = message.isScrambled;
        if (message.deviceMake != null && message.hasOwnProperty("deviceMake"))
            object.deviceMake = message.deviceMake;
        if (message.deviceModel != null && message.hasOwnProperty("deviceModel"))
            object.deviceModel = message.deviceModel;
        if (message.deviceOs != null && message.hasOwnProperty("deviceOs"))
            object.deviceOs = message.deviceOs;
        if (message.deviceOsVersion != null && message.hasOwnProperty("deviceOsVersion"))
            object.deviceOsVersion = message.deviceOsVersion;
        if (message.appVersion != null && message.hasOwnProperty("appVersion"))
            object.appVersion = message.appVersion;
        if (message.acquisitionServer != null && message.hasOwnProperty("acquisitionServer"))
            object.acquisitionServer = message.acquisitionServer;
        if (message.timeSynchronizationServer != null && message.hasOwnProperty("timeSynchronizationServer"))
            object.timeSynchronizationServer = message.timeSynchronizationServer;
        if (message.authenticationServer != null && message.hasOwnProperty("authenticationServer"))
            object.authenticationServer = message.authenticationServer;
        if (message.appFileStartTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("appFileStartTimestampEpochMicrosecondsUtc"))
            if (typeof message.appFileStartTimestampEpochMicrosecondsUtc === "number")
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? String(message.appFileStartTimestampEpochMicrosecondsUtc) : message.appFileStartTimestampEpochMicrosecondsUtc;
            else
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.appFileStartTimestampEpochMicrosecondsUtc) : options.longs === Number ? new $util.LongBits(message.appFileStartTimestampEpochMicrosecondsUtc.low >>> 0, message.appFileStartTimestampEpochMicrosecondsUtc.high >>> 0).toNumber() : message.appFileStartTimestampEpochMicrosecondsUtc;
        if (message.appFileStartTimestampMachine != null && message.hasOwnProperty("appFileStartTimestampMachine"))
            if (typeof message.appFileStartTimestampMachine === "number")
                object.appFileStartTimestampMachine = options.longs === String ? String(message.appFileStartTimestampMachine) : message.appFileStartTimestampMachine;
            else
                object.appFileStartTimestampMachine = options.longs === String ? $util.Long.prototype.toString.call(message.appFileStartTimestampMachine) : options.longs === Number ? new $util.LongBits(message.appFileStartTimestampMachine.low >>> 0, message.appFileStartTimestampMachine.high >>> 0).toNumber() : message.appFileStartTimestampMachine;
        if (message.serverTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("serverTimestampEpochMicrosecondsUtc"))
            if (typeof message.serverTimestampEpochMicrosecondsUtc === "number")
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? String(message.serverTimestampEpochMicrosecondsUtc) : message.serverTimestampEpochMicrosecondsUtc;
            else
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.serverTimestampEpochMicrosecondsUtc) : options.longs === Number ? new $util.LongBits(message.serverTimestampEpochMicrosecondsUtc.low >>> 0, message.serverTimestampEpochMicrosecondsUtc.high >>> 0).toNumber() : message.serverTimestampEpochMicrosecondsUtc;
        if (message.evenlySampledChannels && message.evenlySampledChannels.length) {
            object.evenlySampledChannels = [];
            for (var j = 0; j < message.evenlySampledChannels.length; ++j)
                object.evenlySampledChannels[j] = $root.EvenlySampledChannel.toObject(message.evenlySampledChannels[j], options);
        }
        if (message.unevenlySampledChannels && message.unevenlySampledChannels.length) {
            object.unevenlySampledChannels = [];
            for (var j = 0; j < message.unevenlySampledChannels.length; ++j)
                object.unevenlySampledChannels[j] = $root.UnevenlySampledChannel.toObject(message.unevenlySampledChannels[j], options);
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        if (message.firebaseToken != null && message.hasOwnProperty("firebaseToken"))
            object.firebaseToken = message.firebaseToken;
        if (message.batteryLevelPercent != null && message.hasOwnProperty("batteryLevelPercent"))
            object.batteryLevelPercent = message.batteryLevelPercent;
        if (message.deviceTemperatureC != null && message.hasOwnProperty("deviceTemperatureC"))
            object.deviceTemperatureC = message.deviceTemperatureC;
        return object;
    };

    /**
     * Creates a plain object from this RedvoxPacket message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacket.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this RedvoxPacket to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    RedvoxPacket.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return RedvoxPacket;
})();

$root.Int32Payload = (function() {

    /**
     * Properties of an Int32Payload.
     * @typedef Int32Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] Int32Payload payload.
     */

    /**
     * Constructs a new Int32Payload.
     * @exports Int32Payload
     * @constructor
     * @param {Int32Payload$Properties=} [properties] Properties to set
     */
    function Int32Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Int32Payload payload.
     * @type {Array.<number>|undefined}
     */
    Int32Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Int32Payload instance using the specified properties.
     * @param {Int32Payload$Properties=} [properties] Properties to set
     * @returns {Int32Payload} Int32Payload instance
     */
    Int32Payload.create = function create(properties) {
        return new Int32Payload(properties);
    };

    /**
     * Encodes the specified Int32Payload message. Does not implicitly {@link Int32Payload.verify|verify} messages.
     * @param {Int32Payload$Properties} message Int32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int32Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.int32(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Int32Payload message, length delimited. Does not implicitly {@link Int32Payload.verify|verify} messages.
     * @param {Int32Payload$Properties} message Int32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int32Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an Int32Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Int32Payload} Int32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int32Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Int32Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.int32());
                } else
                    message.payload.push(reader.int32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an Int32Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Int32Payload} Int32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int32Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an Int32Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Int32Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]))
                    return "payload: integer[] expected";
        }
        return null;
    };

    /**
     * Creates an Int32Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Int32Payload} Int32Payload
     */
    Int32Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Int32Payload)
            return object;
        var message = new $root.Int32Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Int32Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = object.payload[i] | 0;
        }
        return message;
    };

    /**
     * Creates an Int32Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Int32Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Int32Payload} Int32Payload
     */
    Int32Payload.from = Int32Payload.fromObject;

    /**
     * Creates a plain object from an Int32Payload message. Also converts values to other types if specified.
     * @param {Int32Payload} message Int32Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int32Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Int32Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int32Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Int32Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Int32Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Int32Payload;
})();

$root.UInt32Payload = (function() {

    /**
     * Properties of a UInt32Payload.
     * @typedef UInt32Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] UInt32Payload payload.
     */

    /**
     * Constructs a new UInt32Payload.
     * @exports UInt32Payload
     * @constructor
     * @param {UInt32Payload$Properties=} [properties] Properties to set
     */
    function UInt32Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * UInt32Payload payload.
     * @type {Array.<number>|undefined}
     */
    UInt32Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new UInt32Payload instance using the specified properties.
     * @param {UInt32Payload$Properties=} [properties] Properties to set
     * @returns {UInt32Payload} UInt32Payload instance
     */
    UInt32Payload.create = function create(properties) {
        return new UInt32Payload(properties);
    };

    /**
     * Encodes the specified UInt32Payload message. Does not implicitly {@link UInt32Payload.verify|verify} messages.
     * @param {UInt32Payload$Properties} message UInt32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt32Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.uint32(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified UInt32Payload message, length delimited. Does not implicitly {@link UInt32Payload.verify|verify} messages.
     * @param {UInt32Payload$Properties} message UInt32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt32Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a UInt32Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {UInt32Payload} UInt32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt32Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.UInt32Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.uint32());
                } else
                    message.payload.push(reader.uint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a UInt32Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {UInt32Payload} UInt32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt32Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a UInt32Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    UInt32Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]))
                    return "payload: integer[] expected";
        }
        return null;
    };

    /**
     * Creates a UInt32Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt32Payload} UInt32Payload
     */
    UInt32Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.UInt32Payload)
            return object;
        var message = new $root.UInt32Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".UInt32Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = object.payload[i] >>> 0;
        }
        return message;
    };

    /**
     * Creates a UInt32Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link UInt32Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt32Payload} UInt32Payload
     */
    UInt32Payload.from = UInt32Payload.fromObject;

    /**
     * Creates a plain object from a UInt32Payload message. Also converts values to other types if specified.
     * @param {UInt32Payload} message UInt32Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt32Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this UInt32Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt32Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this UInt32Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    UInt32Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return UInt32Payload;
})();

$root.Int64Payload = (function() {

    /**
     * Properties of an Int64Payload.
     * @typedef Int64Payload$Properties
     * @type {Object}
     * @property {Array.<number|Long>} [payload] Int64Payload payload.
     */

    /**
     * Constructs a new Int64Payload.
     * @exports Int64Payload
     * @constructor
     * @param {Int64Payload$Properties=} [properties] Properties to set
     */
    function Int64Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Int64Payload payload.
     * @type {Array.<number|Long>|undefined}
     */
    Int64Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Int64Payload instance using the specified properties.
     * @param {Int64Payload$Properties=} [properties] Properties to set
     * @returns {Int64Payload} Int64Payload instance
     */
    Int64Payload.create = function create(properties) {
        return new Int64Payload(properties);
    };

    /**
     * Encodes the specified Int64Payload message. Does not implicitly {@link Int64Payload.verify|verify} messages.
     * @param {Int64Payload$Properties} message Int64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int64Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.int64(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Int64Payload message, length delimited. Does not implicitly {@link Int64Payload.verify|verify} messages.
     * @param {Int64Payload$Properties} message Int64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int64Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an Int64Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Int64Payload} Int64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int64Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Int64Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.int64());
                } else
                    message.payload.push(reader.int64());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an Int64Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Int64Payload} Int64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int64Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an Int64Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Int64Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]) && !(message.payload[i] && $util.isInteger(message.payload[i].low) && $util.isInteger(message.payload[i].high)))
                    return "payload: integer|Long[] expected";
        }
        return null;
    };

    /**
     * Creates an Int64Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Int64Payload} Int64Payload
     */
    Int64Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Int64Payload)
            return object;
        var message = new $root.Int64Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Int64Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                if ($util.Long)
                    (message.payload[i] = $util.Long.fromValue(object.payload[i])).unsigned = false;
                else if (typeof object.payload[i] === "string")
                    message.payload[i] = parseInt(object.payload[i], 10);
                else if (typeof object.payload[i] === "number")
                    message.payload[i] = object.payload[i];
                else if (typeof object.payload[i] === "object")
                    message.payload[i] = new $util.LongBits(object.payload[i].low >>> 0, object.payload[i].high >>> 0).toNumber();
        }
        return message;
    };

    /**
     * Creates an Int64Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Int64Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Int64Payload} Int64Payload
     */
    Int64Payload.from = Int64Payload.fromObject;

    /**
     * Creates a plain object from an Int64Payload message. Also converts values to other types if specified.
     * @param {Int64Payload} message Int64Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int64Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                if (typeof message.payload[j] === "number")
                    object.payload[j] = options.longs === String ? String(message.payload[j]) : message.payload[j];
                else
                    object.payload[j] = options.longs === String ? $util.Long.prototype.toString.call(message.payload[j]) : options.longs === Number ? new $util.LongBits(message.payload[j].low >>> 0, message.payload[j].high >>> 0).toNumber() : message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Int64Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int64Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Int64Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Int64Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Int64Payload;
})();

$root.UInt64Payload = (function() {

    /**
     * Properties of a UInt64Payload.
     * @typedef UInt64Payload$Properties
     * @type {Object}
     * @property {Array.<number|Long>} [payload] UInt64Payload payload.
     */

    /**
     * Constructs a new UInt64Payload.
     * @exports UInt64Payload
     * @constructor
     * @param {UInt64Payload$Properties=} [properties] Properties to set
     */
    function UInt64Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * UInt64Payload payload.
     * @type {Array.<number|Long>|undefined}
     */
    UInt64Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new UInt64Payload instance using the specified properties.
     * @param {UInt64Payload$Properties=} [properties] Properties to set
     * @returns {UInt64Payload} UInt64Payload instance
     */
    UInt64Payload.create = function create(properties) {
        return new UInt64Payload(properties);
    };

    /**
     * Encodes the specified UInt64Payload message. Does not implicitly {@link UInt64Payload.verify|verify} messages.
     * @param {UInt64Payload$Properties} message UInt64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt64Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.uint64(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified UInt64Payload message, length delimited. Does not implicitly {@link UInt64Payload.verify|verify} messages.
     * @param {UInt64Payload$Properties} message UInt64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt64Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a UInt64Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {UInt64Payload} UInt64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt64Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.UInt64Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.uint64());
                } else
                    message.payload.push(reader.uint64());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a UInt64Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {UInt64Payload} UInt64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt64Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a UInt64Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    UInt64Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]) && !(message.payload[i] && $util.isInteger(message.payload[i].low) && $util.isInteger(message.payload[i].high)))
                    return "payload: integer|Long[] expected";
        }
        return null;
    };

    /**
     * Creates a UInt64Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt64Payload} UInt64Payload
     */
    UInt64Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.UInt64Payload)
            return object;
        var message = new $root.UInt64Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".UInt64Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                if ($util.Long)
                    (message.payload[i] = $util.Long.fromValue(object.payload[i])).unsigned = true;
                else if (typeof object.payload[i] === "string")
                    message.payload[i] = parseInt(object.payload[i], 10);
                else if (typeof object.payload[i] === "number")
                    message.payload[i] = object.payload[i];
                else if (typeof object.payload[i] === "object")
                    message.payload[i] = new $util.LongBits(object.payload[i].low >>> 0, object.payload[i].high >>> 0).toNumber(true);
        }
        return message;
    };

    /**
     * Creates a UInt64Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link UInt64Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt64Payload} UInt64Payload
     */
    UInt64Payload.from = UInt64Payload.fromObject;

    /**
     * Creates a plain object from a UInt64Payload message. Also converts values to other types if specified.
     * @param {UInt64Payload} message UInt64Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt64Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                if (typeof message.payload[j] === "number")
                    object.payload[j] = options.longs === String ? String(message.payload[j]) : message.payload[j];
                else
                    object.payload[j] = options.longs === String ? $util.Long.prototype.toString.call(message.payload[j]) : options.longs === Number ? new $util.LongBits(message.payload[j].low >>> 0, message.payload[j].high >>> 0).toNumber(true) : message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this UInt64Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt64Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this UInt64Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    UInt64Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return UInt64Payload;
})();

$root.Float32Payload = (function() {

    /**
     * Properties of a Float32Payload.
     * @typedef Float32Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] Float32Payload payload.
     */

    /**
     * Constructs a new Float32Payload.
     * @exports Float32Payload
     * @constructor
     * @param {Float32Payload$Properties=} [properties] Properties to set
     */
    function Float32Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Float32Payload payload.
     * @type {Array.<number>|undefined}
     */
    Float32Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Float32Payload instance using the specified properties.
     * @param {Float32Payload$Properties=} [properties] Properties to set
     * @returns {Float32Payload} Float32Payload instance
     */
    Float32Payload.create = function create(properties) {
        return new Float32Payload(properties);
    };

    /**
     * Encodes the specified Float32Payload message. Does not implicitly {@link Float32Payload.verify|verify} messages.
     * @param {Float32Payload$Properties} message Float32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float32Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.float(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Float32Payload message, length delimited. Does not implicitly {@link Float32Payload.verify|verify} messages.
     * @param {Float32Payload$Properties} message Float32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float32Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Float32Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Float32Payload} Float32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float32Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Float32Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.float());
                } else
                    message.payload.push(reader.float());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Float32Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Float32Payload} Float32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float32Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Float32Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Float32Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (typeof message.payload[i] !== "number")
                    return "payload: number[] expected";
        }
        return null;
    };

    /**
     * Creates a Float32Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Float32Payload} Float32Payload
     */
    Float32Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Float32Payload)
            return object;
        var message = new $root.Float32Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Float32Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = Number(object.payload[i]);
        }
        return message;
    };

    /**
     * Creates a Float32Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Float32Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Float32Payload} Float32Payload
     */
    Float32Payload.from = Float32Payload.fromObject;

    /**
     * Creates a plain object from a Float32Payload message. Also converts values to other types if specified.
     * @param {Float32Payload} message Float32Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float32Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Float32Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float32Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Float32Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Float32Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Float32Payload;
})();

$root.Float64Payload = (function() {

    /**
     * Properties of a Float64Payload.
     * @typedef Float64Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] Float64Payload payload.
     */

    /**
     * Constructs a new Float64Payload.
     * @exports Float64Payload
     * @constructor
     * @param {Float64Payload$Properties=} [properties] Properties to set
     */
    function Float64Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Float64Payload payload.
     * @type {Array.<number>|undefined}
     */
    Float64Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Float64Payload instance using the specified properties.
     * @param {Float64Payload$Properties=} [properties] Properties to set
     * @returns {Float64Payload} Float64Payload instance
     */
    Float64Payload.create = function create(properties) {
        return new Float64Payload(properties);
    };

    /**
     * Encodes the specified Float64Payload message. Does not implicitly {@link Float64Payload.verify|verify} messages.
     * @param {Float64Payload$Properties} message Float64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float64Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.double(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Float64Payload message, length delimited. Does not implicitly {@link Float64Payload.verify|verify} messages.
     * @param {Float64Payload$Properties} message Float64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float64Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Float64Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Float64Payload} Float64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float64Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Float64Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.double());
                } else
                    message.payload.push(reader.double());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Float64Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Float64Payload} Float64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float64Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Float64Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Float64Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (typeof message.payload[i] !== "number")
                    return "payload: number[] expected";
        }
        return null;
    };

    /**
     * Creates a Float64Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Float64Payload} Float64Payload
     */
    Float64Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Float64Payload)
            return object;
        var message = new $root.Float64Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Float64Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = Number(object.payload[i]);
        }
        return message;
    };

    /**
     * Creates a Float64Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Float64Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Float64Payload} Float64Payload
     */
    Float64Payload.from = Float64Payload.fromObject;

    /**
     * Creates a plain object from a Float64Payload message. Also converts values to other types if specified.
     * @param {Float64Payload} message Float64Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float64Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Float64Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float64Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Float64Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Float64Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Float64Payload;
})();

$root.BytePayload = (function() {

    /**
     * Properties of a BytePayload.
     * @typedef BytePayload$Properties
     * @type {Object}
     * @property {BytePayload.BytePayloadType} [bytePayloadType] BytePayload bytePayloadType.
     * @property {Uint8Array} [payload] BytePayload payload.
     */

    /**
     * Constructs a new BytePayload.
     * @exports BytePayload
     * @constructor
     * @param {BytePayload$Properties=} [properties] Properties to set
     */
    function BytePayload(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * BytePayload bytePayloadType.
     * @type {BytePayload.BytePayloadType|undefined}
     */
    BytePayload.prototype.bytePayloadType = 0;

    /**
     * BytePayload payload.
     * @type {Uint8Array|undefined}
     */
    BytePayload.prototype.payload = $util.newBuffer([]);

    /**
     * Creates a new BytePayload instance using the specified properties.
     * @param {BytePayload$Properties=} [properties] Properties to set
     * @returns {BytePayload} BytePayload instance
     */
    BytePayload.create = function create(properties) {
        return new BytePayload(properties);
    };

    /**
     * Encodes the specified BytePayload message. Does not implicitly {@link BytePayload.verify|verify} messages.
     * @param {BytePayload$Properties} message BytePayload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    BytePayload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.bytePayloadType != null && message.hasOwnProperty("bytePayloadType"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.bytePayloadType);
        if (message.payload && message.hasOwnProperty("payload"))
            writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.payload);
        return writer;
    };

    /**
     * Encodes the specified BytePayload message, length delimited. Does not implicitly {@link BytePayload.verify|verify} messages.
     * @param {BytePayload$Properties} message BytePayload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    BytePayload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a BytePayload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {BytePayload} BytePayload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    BytePayload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.BytePayload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.bytePayloadType = reader.uint32();
                break;
            case 2:
                message.payload = reader.bytes();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a BytePayload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {BytePayload} BytePayload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    BytePayload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a BytePayload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    BytePayload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.bytePayloadType != null)
            switch (message.bytePayloadType) {
            default:
                return "bytePayloadType: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            }
        if (message.payload != null)
            if (!(message.payload && typeof message.payload.length === "number" || $util.isString(message.payload)))
                return "payload: buffer expected";
        return null;
    };

    /**
     * Creates a BytePayload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {BytePayload} BytePayload
     */
    BytePayload.fromObject = function fromObject(object) {
        if (object instanceof $root.BytePayload)
            return object;
        var message = new $root.BytePayload();
        switch (object.bytePayloadType) {
        case "BYTES":
        case 0:
            message.bytePayloadType = 0;
            break;
        case "UINT8":
        case 1:
            message.bytePayloadType = 1;
            break;
        case "UNINT16":
        case 2:
            message.bytePayloadType = 2;
            break;
        case "UNINT24":
        case 3:
            message.bytePayloadType = 3;
            break;
        case "UINT32":
        case 4:
            message.bytePayloadType = 4;
            break;
        case "UINT64":
        case 5:
            message.bytePayloadType = 5;
            break;
        case "INT8":
        case 6:
            message.bytePayloadType = 6;
            break;
        case "INT16":
        case 7:
            message.bytePayloadType = 7;
            break;
        case "INT24":
        case 8:
            message.bytePayloadType = 8;
            break;
        case "INT32":
        case 9:
            message.bytePayloadType = 9;
            break;
        case "INT64":
        case 10:
            message.bytePayloadType = 10;
            break;
        case "FLOAT32":
        case 11:
            message.bytePayloadType = 11;
            break;
        case "FLOAT64":
        case 12:
            message.bytePayloadType = 12;
            break;
        case "OTHER":
        case 13:
            message.bytePayloadType = 13;
            break;
        }
        if (object.payload != null)
            if (typeof object.payload === "string")
                $util.base64.decode(object.payload, message.payload = $util.newBuffer($util.base64.length(object.payload)), 0);
            else if (object.payload.length)
                message.payload = object.payload;
        return message;
    };

    /**
     * Creates a BytePayload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link BytePayload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {BytePayload} BytePayload
     */
    BytePayload.from = BytePayload.fromObject;

    /**
     * Creates a plain object from a BytePayload message. Also converts values to other types if specified.
     * @param {BytePayload} message BytePayload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    BytePayload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.bytePayloadType = options.enums === String ? "BYTES" : 0;
            object.payload = options.bytes === String ? "" : [];
        }
        if (message.bytePayloadType != null && message.hasOwnProperty("bytePayloadType"))
            object.bytePayloadType = options.enums === String ? $root.BytePayload.BytePayloadType[message.bytePayloadType] : message.bytePayloadType;
        if (message.payload != null && message.hasOwnProperty("payload"))
            object.payload = options.bytes === String ? $util.base64.encode(message.payload, 0, message.payload.length) : options.bytes === Array ? Array.prototype.slice.call(message.payload) : message.payload;
        return object;
    };

    /**
     * Creates a plain object from this BytePayload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    BytePayload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this BytePayload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    BytePayload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    /**
     * BytePayloadType enum.
     * @name BytePayloadType
     * @memberof BytePayload
     * @enum {number}
     * @property {number} BYTES=0 BYTES value
     * @property {number} UINT8=1 UINT8 value
     * @property {number} UNINT16=2 UNINT16 value
     * @property {number} UNINT24=3 UNINT24 value
     * @property {number} UINT32=4 UINT32 value
     * @property {number} UINT64=5 UINT64 value
     * @property {number} INT8=6 INT8 value
     * @property {number} INT16=7 INT16 value
     * @property {number} INT24=8 INT24 value
     * @property {number} INT32=9 INT32 value
     * @property {number} INT64=10 INT64 value
     * @property {number} FLOAT32=11 FLOAT32 value
     * @property {number} FLOAT64=12 FLOAT64 value
     * @property {number} OTHER=13 OTHER value
     */
    BytePayload.BytePayloadType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "BYTES"] = 0;
        values[valuesById[1] = "UINT8"] = 1;
        values[valuesById[2] = "UNINT16"] = 2;
        values[valuesById[3] = "UNINT24"] = 3;
        values[valuesById[4] = "UINT32"] = 4;
        values[valuesById[5] = "UINT64"] = 5;
        values[valuesById[6] = "INT8"] = 6;
        values[valuesById[7] = "INT16"] = 7;
        values[valuesById[8] = "INT24"] = 8;
        values[valuesById[9] = "INT32"] = 9;
        values[valuesById[10] = "INT64"] = 10;
        values[valuesById[11] = "FLOAT32"] = 11;
        values[valuesById[12] = "FLOAT64"] = 12;
        values[valuesById[13] = "OTHER"] = 13;
        return values;
    })();

    return BytePayload;
})();

/**
 * ChannelType enum.
 * @exports ChannelType
 * @enum {number}
 * @property {number} MICROPHONE=0 MICROPHONE value
 * @property {number} BAROMETER=1 BAROMETER value
 * @property {number} LATITUDE=2 LATITUDE value
 * @property {number} LONGITUDE=3 LONGITUDE value
 * @property {number} SPEED=4 SPEED value
 * @property {number} ALTITUDE=5 ALTITUDE value
 * @property {number} RESERVED_0=6 RESERVED_0 value
 * @property {number} RESERVED_1=7 RESERVED_1 value
 * @property {number} RESERVED_2=8 RESERVED_2 value
 * @property {number} TIME_SYNCHRONIZATION=9 TIME_SYNCHRONIZATION value
 * @property {number} ACCURACY=10 ACCURACY value
 * @property {number} ACCELEROMETER_X=11 ACCELEROMETER_X value
 * @property {number} ACCELEROMETER_Y=12 ACCELEROMETER_Y value
 * @property {number} ACCELEROMETER_Z=13 ACCELEROMETER_Z value
 * @property {number} MAGNETOMETER_X=14 MAGNETOMETER_X value
 * @property {number} MAGNETOMETER_Y=15 MAGNETOMETER_Y value
 * @property {number} MAGNETOMETER_Z=16 MAGNETOMETER_Z value
 * @property {number} GYROSCOPE_X=17 GYROSCOPE_X value
 * @property {number} GYROSCOPE_Y=18 GYROSCOPE_Y value
 * @property {number} GYROSCOPE_Z=19 GYROSCOPE_Z value
 * @property {number} OTHER=20 OTHER value
 * @property {number} LIGHT=21 LIGHT value
 * @property {number} IMAGE=22 IMAGE value
 * @property {number} INFRARED=23 INFRARED value
 */
$root.ChannelType = (function() {
    var valuesById = {}, values = Object.create(valuesById);
    values[valuesById[0] = "MICROPHONE"] = 0;
    values[valuesById[1] = "BAROMETER"] = 1;
    values[valuesById[2] = "LATITUDE"] = 2;
    values[valuesById[3] = "LONGITUDE"] = 3;
    values[valuesById[4] = "SPEED"] = 4;
    values[valuesById[5] = "ALTITUDE"] = 5;
    values[valuesById[6] = "RESERVED_0"] = 6;
    values[valuesById[7] = "RESERVED_1"] = 7;
    values[valuesById[8] = "RESERVED_2"] = 8;
    values[valuesById[9] = "TIME_SYNCHRONIZATION"] = 9;
    values[valuesById[10] = "ACCURACY"] = 10;
    values[valuesById[11] = "ACCELEROMETER_X"] = 11;
    values[valuesById[12] = "ACCELEROMETER_Y"] = 12;
    values[valuesById[13] = "ACCELEROMETER_Z"] = 13;
    values[valuesById[14] = "MAGNETOMETER_X"] = 14;
    values[valuesById[15] = "MAGNETOMETER_Y"] = 15;
    values[valuesById[16] = "MAGNETOMETER_Z"] = 16;
    values[valuesById[17] = "GYROSCOPE_X"] = 17;
    values[valuesById[18] = "GYROSCOPE_Y"] = 18;
    values[valuesById[19] = "GYROSCOPE_Z"] = 19;
    values[valuesById[20] = "OTHER"] = 20;
    values[valuesById[21] = "LIGHT"] = 21;
    values[valuesById[22] = "IMAGE"] = 22;
    values[valuesById[23] = "INFRARED"] = 23;
    return values;
})();

$root.EvenlySampledChannel = (function() {

    /**
     * Properties of an EvenlySampledChannel.
     * @typedef EvenlySampledChannel$Properties
     * @type {Object}
     * @property {Array.<ChannelType>} [channelTypes] EvenlySampledChannel channelTypes.
     * @property {string} [sensorName] EvenlySampledChannel sensorName.
     * @property {number} [sampleRateHz] EvenlySampledChannel sampleRateHz.
     * @property {number|Long} [firstSampleTimestampEpochMicrosecondsUtc] EvenlySampledChannel firstSampleTimestampEpochMicrosecondsUtc.
     * @property {BytePayload$Properties} [bytePayload] EvenlySampledChannel bytePayload.
     * @property {UInt32Payload$Properties} [uint32Payload] EvenlySampledChannel uint32Payload.
     * @property {UInt64Payload$Properties} [uint64Payload] EvenlySampledChannel uint64Payload.
     * @property {Int32Payload$Properties} [int32Payload] EvenlySampledChannel int32Payload.
     * @property {Int64Payload$Properties} [int64Payload] EvenlySampledChannel int64Payload.
     * @property {Float32Payload$Properties} [float32Payload] EvenlySampledChannel float32Payload.
     * @property {Float64Payload$Properties} [float64Payload] EvenlySampledChannel float64Payload.
     * @property {Array.<number>} [valueMeans] EvenlySampledChannel valueMeans.
     * @property {Array.<number>} [valueStds] EvenlySampledChannel valueStds.
     * @property {Array.<number>} [valueMedians] EvenlySampledChannel valueMedians.
     * @property {Array.<string>} [metadata] EvenlySampledChannel metadata.
     */

    /**
     * Constructs a new EvenlySampledChannel.
     * @exports EvenlySampledChannel
     * @constructor
     * @param {EvenlySampledChannel$Properties=} [properties] Properties to set
     */
    function EvenlySampledChannel(properties) {
        this.channelTypes = [];
        this.valueMeans = [];
        this.valueStds = [];
        this.valueMedians = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * EvenlySampledChannel channelTypes.
     * @type {Array.<ChannelType>|undefined}
     */
    EvenlySampledChannel.prototype.channelTypes = $util.emptyArray;

    /**
     * EvenlySampledChannel sensorName.
     * @type {string|undefined}
     */
    EvenlySampledChannel.prototype.sensorName = "";

    /**
     * EvenlySampledChannel sampleRateHz.
     * @type {number|undefined}
     */
    EvenlySampledChannel.prototype.sampleRateHz = 0;

    /**
     * EvenlySampledChannel firstSampleTimestampEpochMicrosecondsUtc.
     * @type {number|Long|undefined}
     */
    EvenlySampledChannel.prototype.firstSampleTimestampEpochMicrosecondsUtc = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * EvenlySampledChannel bytePayload.
     * @type {BytePayload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.bytePayload = null;

    /**
     * EvenlySampledChannel uint32Payload.
     * @type {UInt32Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.uint32Payload = null;

    /**
     * EvenlySampledChannel uint64Payload.
     * @type {UInt64Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.uint64Payload = null;

    /**
     * EvenlySampledChannel int32Payload.
     * @type {Int32Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.int32Payload = null;

    /**
     * EvenlySampledChannel int64Payload.
     * @type {Int64Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.int64Payload = null;

    /**
     * EvenlySampledChannel float32Payload.
     * @type {Float32Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.float32Payload = null;

    /**
     * EvenlySampledChannel float64Payload.
     * @type {Float64Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.float64Payload = null;

    /**
     * EvenlySampledChannel valueMeans.
     * @type {Array.<number>|undefined}
     */
    EvenlySampledChannel.prototype.valueMeans = $util.emptyArray;

    /**
     * EvenlySampledChannel valueStds.
     * @type {Array.<number>|undefined}
     */
    EvenlySampledChannel.prototype.valueStds = $util.emptyArray;

    /**
     * EvenlySampledChannel valueMedians.
     * @type {Array.<number>|undefined}
     */
    EvenlySampledChannel.prototype.valueMedians = $util.emptyArray;

    /**
     * EvenlySampledChannel metadata.
     * @type {Array.<string>|undefined}
     */
    EvenlySampledChannel.prototype.metadata = $util.emptyArray;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * EvenlySampledChannel payload.
     * @name EvenlySampledChannel#payload
     * @type {string|undefined}
     */
    Object.defineProperty(EvenlySampledChannel.prototype, "payload", {
        get: $util.oneOfGetter($oneOfFields = ["bytePayload", "uint32Payload", "uint64Payload", "int32Payload", "int64Payload", "float32Payload", "float64Payload"]),
        set: $util.oneOfSetter($oneOfFields)
    });

    /**
     * Creates a new EvenlySampledChannel instance using the specified properties.
     * @param {EvenlySampledChannel$Properties=} [properties] Properties to set
     * @returns {EvenlySampledChannel} EvenlySampledChannel instance
     */
    EvenlySampledChannel.create = function create(properties) {
        return new EvenlySampledChannel(properties);
    };

    /**
     * Encodes the specified EvenlySampledChannel message. Does not implicitly {@link EvenlySampledChannel.verify|verify} messages.
     * @param {EvenlySampledChannel$Properties} message EvenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    EvenlySampledChannel.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.channelTypes && message.channelTypes.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.channelTypes.length; ++i)
                writer.uint32(message.channelTypes[i]);
            writer.ldelim();
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.sensorName);
        if (message.sampleRateHz != null && message.hasOwnProperty("sampleRateHz"))
            writer.uint32(/* id 3, wireType 1 =*/25).double(message.sampleRateHz);
        if (message.firstSampleTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("firstSampleTimestampEpochMicrosecondsUtc"))
            writer.uint32(/* id 4, wireType 0 =*/32).int64(message.firstSampleTimestampEpochMicrosecondsUtc);
        if (message.bytePayload && message.hasOwnProperty("bytePayload"))
            $root.BytePayload.encode(message.bytePayload, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
        if (message.uint32Payload && message.hasOwnProperty("uint32Payload"))
            $root.UInt32Payload.encode(message.uint32Payload, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
        if (message.uint64Payload && message.hasOwnProperty("uint64Payload"))
            $root.UInt64Payload.encode(message.uint64Payload, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
        if (message.int32Payload && message.hasOwnProperty("int32Payload"))
            $root.Int32Payload.encode(message.int32Payload, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
        if (message.int64Payload && message.hasOwnProperty("int64Payload"))
            $root.Int64Payload.encode(message.int64Payload, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
        if (message.float32Payload && message.hasOwnProperty("float32Payload"))
            $root.Float32Payload.encode(message.float32Payload, writer.uint32(/* id 10, wireType 2 =*/82).fork()).ldelim();
        if (message.float64Payload && message.hasOwnProperty("float64Payload"))
            $root.Float64Payload.encode(message.float64Payload, writer.uint32(/* id 11, wireType 2 =*/90).fork()).ldelim();
        if (message.valueMeans && message.valueMeans.length) {
            writer.uint32(/* id 12, wireType 2 =*/98).fork();
            for (var i = 0; i < message.valueMeans.length; ++i)
                writer.double(message.valueMeans[i]);
            writer.ldelim();
        }
        if (message.valueStds && message.valueStds.length) {
            writer.uint32(/* id 13, wireType 2 =*/106).fork();
            for (var i = 0; i < message.valueStds.length; ++i)
                writer.double(message.valueStds[i]);
            writer.ldelim();
        }
        if (message.valueMedians && message.valueMedians.length) {
            writer.uint32(/* id 14, wireType 2 =*/114).fork();
            for (var i = 0; i < message.valueMedians.length; ++i)
                writer.double(message.valueMedians[i]);
            writer.ldelim();
        }
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 15, wireType 2 =*/122).string(message.metadata[i]);
        return writer;
    };

    /**
     * Encodes the specified EvenlySampledChannel message, length delimited. Does not implicitly {@link EvenlySampledChannel.verify|verify} messages.
     * @param {EvenlySampledChannel$Properties} message EvenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    EvenlySampledChannel.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an EvenlySampledChannel message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    EvenlySampledChannel.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.EvenlySampledChannel();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.channelTypes && message.channelTypes.length))
                    message.channelTypes = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.channelTypes.push(reader.uint32());
                } else
                    message.channelTypes.push(reader.uint32());
                break;
            case 2:
                message.sensorName = reader.string();
                break;
            case 3:
                message.sampleRateHz = reader.double();
                break;
            case 4:
                message.firstSampleTimestampEpochMicrosecondsUtc = reader.int64();
                break;
            case 5:
                message.bytePayload = $root.BytePayload.decode(reader, reader.uint32());
                break;
            case 6:
                message.uint32Payload = $root.UInt32Payload.decode(reader, reader.uint32());
                break;
            case 7:
                message.uint64Payload = $root.UInt64Payload.decode(reader, reader.uint32());
                break;
            case 8:
                message.int32Payload = $root.Int32Payload.decode(reader, reader.uint32());
                break;
            case 9:
                message.int64Payload = $root.Int64Payload.decode(reader, reader.uint32());
                break;
            case 10:
                message.float32Payload = $root.Float32Payload.decode(reader, reader.uint32());
                break;
            case 11:
                message.float64Payload = $root.Float64Payload.decode(reader, reader.uint32());
                break;
            case 12:
                if (!(message.valueMeans && message.valueMeans.length))
                    message.valueMeans = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMeans.push(reader.double());
                } else
                    message.valueMeans.push(reader.double());
                break;
            case 13:
                if (!(message.valueStds && message.valueStds.length))
                    message.valueStds = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueStds.push(reader.double());
                } else
                    message.valueStds.push(reader.double());
                break;
            case 14:
                if (!(message.valueMedians && message.valueMedians.length))
                    message.valueMedians = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMedians.push(reader.double());
                } else
                    message.valueMedians.push(reader.double());
                break;
            case 15:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an EvenlySampledChannel message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    EvenlySampledChannel.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an EvenlySampledChannel message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    EvenlySampledChannel.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        var properties = {};
        if (message.channelTypes != null) {
            if (!Array.isArray(message.channelTypes))
                return "channelTypes: array expected";
            for (var i = 0; i < message.channelTypes.length; ++i)
                switch (message.channelTypes[i]) {
                default:
                    return "channelTypes: enum value[] expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                    break;
                }
        }
        if (message.sensorName != null)
            if (!$util.isString(message.sensorName))
                return "sensorName: string expected";
        if (message.sampleRateHz != null)
            if (typeof message.sampleRateHz !== "number")
                return "sampleRateHz: number expected";
        if (message.firstSampleTimestampEpochMicrosecondsUtc != null)
            if (!$util.isInteger(message.firstSampleTimestampEpochMicrosecondsUtc) && !(message.firstSampleTimestampEpochMicrosecondsUtc && $util.isInteger(message.firstSampleTimestampEpochMicrosecondsUtc.low) && $util.isInteger(message.firstSampleTimestampEpochMicrosecondsUtc.high)))
                return "firstSampleTimestampEpochMicrosecondsUtc: integer|Long expected";
        if (message.bytePayload != null) {
            properties.payload = 1;
            var error = $root.BytePayload.verify(message.bytePayload);
            if (error)
                return "bytePayload." + error;
        }
        if (message.uint32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt32Payload.verify(message.uint32Payload);
            if (error)
                return "uint32Payload." + error;
        }
        if (message.uint64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt64Payload.verify(message.uint64Payload);
            if (error)
                return "uint64Payload." + error;
        }
        if (message.int32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int32Payload.verify(message.int32Payload);
            if (error)
                return "int32Payload." + error;
        }
        if (message.int64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int64Payload.verify(message.int64Payload);
            if (error)
                return "int64Payload." + error;
        }
        if (message.float32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float32Payload.verify(message.float32Payload);
            if (error)
                return "float32Payload." + error;
        }
        if (message.float64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float64Payload.verify(message.float64Payload);
            if (error)
                return "float64Payload." + error;
        }
        if (message.valueMeans != null) {
            if (!Array.isArray(message.valueMeans))
                return "valueMeans: array expected";
            for (var i = 0; i < message.valueMeans.length; ++i)
                if (typeof message.valueMeans[i] !== "number")
                    return "valueMeans: number[] expected";
        }
        if (message.valueStds != null) {
            if (!Array.isArray(message.valueStds))
                return "valueStds: array expected";
            for (var i = 0; i < message.valueStds.length; ++i)
                if (typeof message.valueStds[i] !== "number")
                    return "valueStds: number[] expected";
        }
        if (message.valueMedians != null) {
            if (!Array.isArray(message.valueMedians))
                return "valueMedians: array expected";
            for (var i = 0; i < message.valueMedians.length; ++i)
                if (typeof message.valueMedians[i] !== "number")
                    return "valueMedians: number[] expected";
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates an EvenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     */
    EvenlySampledChannel.fromObject = function fromObject(object) {
        if (object instanceof $root.EvenlySampledChannel)
            return object;
        var message = new $root.EvenlySampledChannel();
        if (object.channelTypes) {
            if (!Array.isArray(object.channelTypes))
                throw TypeError(".EvenlySampledChannel.channelTypes: array expected");
            message.channelTypes = [];
            for (var i = 0; i < object.channelTypes.length; ++i)
                switch (object.channelTypes[i]) {
                default:
                case "MICROPHONE":
                case 0:
                    message.channelTypes[i] = 0;
                    break;
                case "BAROMETER":
                case 1:
                    message.channelTypes[i] = 1;
                    break;
                case "LATITUDE":
                case 2:
                    message.channelTypes[i] = 2;
                    break;
                case "LONGITUDE":
                case 3:
                    message.channelTypes[i] = 3;
                    break;
                case "SPEED":
                case 4:
                    message.channelTypes[i] = 4;
                    break;
                case "ALTITUDE":
                case 5:
                    message.channelTypes[i] = 5;
                    break;
                case "RESERVED_0":
                case 6:
                    message.channelTypes[i] = 6;
                    break;
                case "RESERVED_1":
                case 7:
                    message.channelTypes[i] = 7;
                    break;
                case "RESERVED_2":
                case 8:
                    message.channelTypes[i] = 8;
                    break;
                case "TIME_SYNCHRONIZATION":
                case 9:
                    message.channelTypes[i] = 9;
                    break;
                case "ACCURACY":
                case 10:
                    message.channelTypes[i] = 10;
                    break;
                case "ACCELEROMETER_X":
                case 11:
                    message.channelTypes[i] = 11;
                    break;
                case "ACCELEROMETER_Y":
                case 12:
                    message.channelTypes[i] = 12;
                    break;
                case "ACCELEROMETER_Z":
                case 13:
                    message.channelTypes[i] = 13;
                    break;
                case "MAGNETOMETER_X":
                case 14:
                    message.channelTypes[i] = 14;
                    break;
                case "MAGNETOMETER_Y":
                case 15:
                    message.channelTypes[i] = 15;
                    break;
                case "MAGNETOMETER_Z":
                case 16:
                    message.channelTypes[i] = 16;
                    break;
                case "GYROSCOPE_X":
                case 17:
                    message.channelTypes[i] = 17;
                    break;
                case "GYROSCOPE_Y":
                case 18:
                    message.channelTypes[i] = 18;
                    break;
                case "GYROSCOPE_Z":
                case 19:
                    message.channelTypes[i] = 19;
                    break;
                case "OTHER":
                case 20:
                    message.channelTypes[i] = 20;
                    break;
                case "LIGHT":
                case 21:
                    message.channelTypes[i] = 21;
                    break;
                case "IMAGE":
                case 22:
                    message.channelTypes[i] = 22;
                    break;
                case "INFRARED":
                case 23:
                    message.channelTypes[i] = 23;
                    break;
                }
        }
        if (object.sensorName != null)
            message.sensorName = String(object.sensorName);
        if (object.sampleRateHz != null)
            message.sampleRateHz = Number(object.sampleRateHz);
        if (object.firstSampleTimestampEpochMicrosecondsUtc != null)
            if ($util.Long)
                (message.firstSampleTimestampEpochMicrosecondsUtc = $util.Long.fromValue(object.firstSampleTimestampEpochMicrosecondsUtc)).unsigned = false;
            else if (typeof object.firstSampleTimestampEpochMicrosecondsUtc === "string")
                message.firstSampleTimestampEpochMicrosecondsUtc = parseInt(object.firstSampleTimestampEpochMicrosecondsUtc, 10);
            else if (typeof object.firstSampleTimestampEpochMicrosecondsUtc === "number")
                message.firstSampleTimestampEpochMicrosecondsUtc = object.firstSampleTimestampEpochMicrosecondsUtc;
            else if (typeof object.firstSampleTimestampEpochMicrosecondsUtc === "object")
                message.firstSampleTimestampEpochMicrosecondsUtc = new $util.LongBits(object.firstSampleTimestampEpochMicrosecondsUtc.low >>> 0, object.firstSampleTimestampEpochMicrosecondsUtc.high >>> 0).toNumber();
        if (object.bytePayload != null) {
            if (typeof object.bytePayload !== "object")
                throw TypeError(".EvenlySampledChannel.bytePayload: object expected");
            message.bytePayload = $root.BytePayload.fromObject(object.bytePayload);
        }
        if (object.uint32Payload != null) {
            if (typeof object.uint32Payload !== "object")
                throw TypeError(".EvenlySampledChannel.uint32Payload: object expected");
            message.uint32Payload = $root.UInt32Payload.fromObject(object.uint32Payload);
        }
        if (object.uint64Payload != null) {
            if (typeof object.uint64Payload !== "object")
                throw TypeError(".EvenlySampledChannel.uint64Payload: object expected");
            message.uint64Payload = $root.UInt64Payload.fromObject(object.uint64Payload);
        }
        if (object.int32Payload != null) {
            if (typeof object.int32Payload !== "object")
                throw TypeError(".EvenlySampledChannel.int32Payload: object expected");
            message.int32Payload = $root.Int32Payload.fromObject(object.int32Payload);
        }
        if (object.int64Payload != null) {
            if (typeof object.int64Payload !== "object")
                throw TypeError(".EvenlySampledChannel.int64Payload: object expected");
            message.int64Payload = $root.Int64Payload.fromObject(object.int64Payload);
        }
        if (object.float32Payload != null) {
            if (typeof object.float32Payload !== "object")
                throw TypeError(".EvenlySampledChannel.float32Payload: object expected");
            message.float32Payload = $root.Float32Payload.fromObject(object.float32Payload);
        }
        if (object.float64Payload != null) {
            if (typeof object.float64Payload !== "object")
                throw TypeError(".EvenlySampledChannel.float64Payload: object expected");
            message.float64Payload = $root.Float64Payload.fromObject(object.float64Payload);
        }
        if (object.valueMeans) {
            if (!Array.isArray(object.valueMeans))
                throw TypeError(".EvenlySampledChannel.valueMeans: array expected");
            message.valueMeans = [];
            for (var i = 0; i < object.valueMeans.length; ++i)
                message.valueMeans[i] = Number(object.valueMeans[i]);
        }
        if (object.valueStds) {
            if (!Array.isArray(object.valueStds))
                throw TypeError(".EvenlySampledChannel.valueStds: array expected");
            message.valueStds = [];
            for (var i = 0; i < object.valueStds.length; ++i)
                message.valueStds[i] = Number(object.valueStds[i]);
        }
        if (object.valueMedians) {
            if (!Array.isArray(object.valueMedians))
                throw TypeError(".EvenlySampledChannel.valueMedians: array expected");
            message.valueMedians = [];
            for (var i = 0; i < object.valueMedians.length; ++i)
                message.valueMedians[i] = Number(object.valueMedians[i]);
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".EvenlySampledChannel.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates an EvenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link EvenlySampledChannel.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     */
    EvenlySampledChannel.from = EvenlySampledChannel.fromObject;

    /**
     * Creates a plain object from an EvenlySampledChannel message. Also converts values to other types if specified.
     * @param {EvenlySampledChannel} message EvenlySampledChannel
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    EvenlySampledChannel.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.channelTypes = [];
            object.valueMeans = [];
            object.valueStds = [];
            object.valueMedians = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.sensorName = "";
            object.sampleRateHz = 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? "0" : 0;
        }
        if (message.channelTypes && message.channelTypes.length) {
            object.channelTypes = [];
            for (var j = 0; j < message.channelTypes.length; ++j)
                object.channelTypes[j] = options.enums === String ? $root.ChannelType[message.channelTypes[j]] : message.channelTypes[j];
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            object.sensorName = message.sensorName;
        if (message.sampleRateHz != null && message.hasOwnProperty("sampleRateHz"))
            object.sampleRateHz = message.sampleRateHz;
        if (message.firstSampleTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("firstSampleTimestampEpochMicrosecondsUtc"))
            if (typeof message.firstSampleTimestampEpochMicrosecondsUtc === "number")
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? String(message.firstSampleTimestampEpochMicrosecondsUtc) : message.firstSampleTimestampEpochMicrosecondsUtc;
            else
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.firstSampleTimestampEpochMicrosecondsUtc) : options.longs === Number ? new $util.LongBits(message.firstSampleTimestampEpochMicrosecondsUtc.low >>> 0, message.firstSampleTimestampEpochMicrosecondsUtc.high >>> 0).toNumber() : message.firstSampleTimestampEpochMicrosecondsUtc;
        if (message.bytePayload != null && message.hasOwnProperty("bytePayload")) {
            object.bytePayload = $root.BytePayload.toObject(message.bytePayload, options);
            if (options.oneofs)
                object.payload = "bytePayload";
        }
        if (message.uint32Payload != null && message.hasOwnProperty("uint32Payload")) {
            object.uint32Payload = $root.UInt32Payload.toObject(message.uint32Payload, options);
            if (options.oneofs)
                object.payload = "uint32Payload";
        }
        if (message.uint64Payload != null && message.hasOwnProperty("uint64Payload")) {
            object.uint64Payload = $root.UInt64Payload.toObject(message.uint64Payload, options);
            if (options.oneofs)
                object.payload = "uint64Payload";
        }
        if (message.int32Payload != null && message.hasOwnProperty("int32Payload")) {
            object.int32Payload = $root.Int32Payload.toObject(message.int32Payload, options);
            if (options.oneofs)
                object.payload = "int32Payload";
        }
        if (message.int64Payload != null && message.hasOwnProperty("int64Payload")) {
            object.int64Payload = $root.Int64Payload.toObject(message.int64Payload, options);
            if (options.oneofs)
                object.payload = "int64Payload";
        }
        if (message.float32Payload != null && message.hasOwnProperty("float32Payload")) {
            object.float32Payload = $root.Float32Payload.toObject(message.float32Payload, options);
            if (options.oneofs)
                object.payload = "float32Payload";
        }
        if (message.float64Payload != null && message.hasOwnProperty("float64Payload")) {
            object.float64Payload = $root.Float64Payload.toObject(message.float64Payload, options);
            if (options.oneofs)
                object.payload = "float64Payload";
        }
        if (message.valueMeans && message.valueMeans.length) {
            object.valueMeans = [];
            for (var j = 0; j < message.valueMeans.length; ++j)
                object.valueMeans[j] = message.valueMeans[j];
        }
        if (message.valueStds && message.valueStds.length) {
            object.valueStds = [];
            for (var j = 0; j < message.valueStds.length; ++j)
                object.valueStds[j] = message.valueStds[j];
        }
        if (message.valueMedians && message.valueMedians.length) {
            object.valueMedians = [];
            for (var j = 0; j < message.valueMedians.length; ++j)
                object.valueMedians[j] = message.valueMedians[j];
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this EvenlySampledChannel message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    EvenlySampledChannel.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this EvenlySampledChannel to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    EvenlySampledChannel.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return EvenlySampledChannel;
})();

$root.UnevenlySampledChannel = (function() {

    /**
     * Properties of an UnevenlySampledChannel.
     * @typedef UnevenlySampledChannel$Properties
     * @type {Object}
     * @property {Array.<ChannelType>} [channelTypes] UnevenlySampledChannel channelTypes.
     * @property {string} [sensorName] UnevenlySampledChannel sensorName.
     * @property {Array.<number|Long>} [timestampsMicrosecondsUtc] UnevenlySampledChannel timestampsMicrosecondsUtc.
     * @property {BytePayload$Properties} [bytePayload] UnevenlySampledChannel bytePayload.
     * @property {UInt32Payload$Properties} [uint32Payload] UnevenlySampledChannel uint32Payload.
     * @property {UInt64Payload$Properties} [uint64Payload] UnevenlySampledChannel uint64Payload.
     * @property {Int32Payload$Properties} [int32Payload] UnevenlySampledChannel int32Payload.
     * @property {Int64Payload$Properties} [int64Payload] UnevenlySampledChannel int64Payload.
     * @property {Float32Payload$Properties} [float32Payload] UnevenlySampledChannel float32Payload.
     * @property {Float64Payload$Properties} [float64Payload] UnevenlySampledChannel float64Payload.
     * @property {number} [sampleIntervalMean] UnevenlySampledChannel sampleIntervalMean.
     * @property {number} [sampleIntervalStd] UnevenlySampledChannel sampleIntervalStd.
     * @property {number} [sampleIntervalMedian] UnevenlySampledChannel sampleIntervalMedian.
     * @property {Array.<number>} [valueMeans] UnevenlySampledChannel valueMeans.
     * @property {Array.<number>} [valueStds] UnevenlySampledChannel valueStds.
     * @property {Array.<number>} [valueMedians] UnevenlySampledChannel valueMedians.
     * @property {Array.<string>} [metadata] UnevenlySampledChannel metadata.
     */

    /**
     * Constructs a new UnevenlySampledChannel.
     * @exports UnevenlySampledChannel
     * @constructor
     * @param {UnevenlySampledChannel$Properties=} [properties] Properties to set
     */
    function UnevenlySampledChannel(properties) {
        this.channelTypes = [];
        this.timestampsMicrosecondsUtc = [];
        this.valueMeans = [];
        this.valueStds = [];
        this.valueMedians = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * UnevenlySampledChannel channelTypes.
     * @type {Array.<ChannelType>|undefined}
     */
    UnevenlySampledChannel.prototype.channelTypes = $util.emptyArray;

    /**
     * UnevenlySampledChannel sensorName.
     * @type {string|undefined}
     */
    UnevenlySampledChannel.prototype.sensorName = "";

    /**
     * UnevenlySampledChannel timestampsMicrosecondsUtc.
     * @type {Array.<number|Long>|undefined}
     */
    UnevenlySampledChannel.prototype.timestampsMicrosecondsUtc = $util.emptyArray;

    /**
     * UnevenlySampledChannel bytePayload.
     * @type {BytePayload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.bytePayload = null;

    /**
     * UnevenlySampledChannel uint32Payload.
     * @type {UInt32Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.uint32Payload = null;

    /**
     * UnevenlySampledChannel uint64Payload.
     * @type {UInt64Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.uint64Payload = null;

    /**
     * UnevenlySampledChannel int32Payload.
     * @type {Int32Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.int32Payload = null;

    /**
     * UnevenlySampledChannel int64Payload.
     * @type {Int64Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.int64Payload = null;

    /**
     * UnevenlySampledChannel float32Payload.
     * @type {Float32Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.float32Payload = null;

    /**
     * UnevenlySampledChannel float64Payload.
     * @type {Float64Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.float64Payload = null;

    /**
     * UnevenlySampledChannel sampleIntervalMean.
     * @type {number|undefined}
     */
    UnevenlySampledChannel.prototype.sampleIntervalMean = 0;

    /**
     * UnevenlySampledChannel sampleIntervalStd.
     * @type {number|undefined}
     */
    UnevenlySampledChannel.prototype.sampleIntervalStd = 0;

    /**
     * UnevenlySampledChannel sampleIntervalMedian.
     * @type {number|undefined}
     */
    UnevenlySampledChannel.prototype.sampleIntervalMedian = 0;

    /**
     * UnevenlySampledChannel valueMeans.
     * @type {Array.<number>|undefined}
     */
    UnevenlySampledChannel.prototype.valueMeans = $util.emptyArray;

    /**
     * UnevenlySampledChannel valueStds.
     * @type {Array.<number>|undefined}
     */
    UnevenlySampledChannel.prototype.valueStds = $util.emptyArray;

    /**
     * UnevenlySampledChannel valueMedians.
     * @type {Array.<number>|undefined}
     */
    UnevenlySampledChannel.prototype.valueMedians = $util.emptyArray;

    /**
     * UnevenlySampledChannel metadata.
     * @type {Array.<string>|undefined}
     */
    UnevenlySampledChannel.prototype.metadata = $util.emptyArray;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * UnevenlySampledChannel payload.
     * @name UnevenlySampledChannel#payload
     * @type {string|undefined}
     */
    Object.defineProperty(UnevenlySampledChannel.prototype, "payload", {
        get: $util.oneOfGetter($oneOfFields = ["bytePayload", "uint32Payload", "uint64Payload", "int32Payload", "int64Payload", "float32Payload", "float64Payload"]),
        set: $util.oneOfSetter($oneOfFields)
    });

    /**
     * Creates a new UnevenlySampledChannel instance using the specified properties.
     * @param {UnevenlySampledChannel$Properties=} [properties] Properties to set
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel instance
     */
    UnevenlySampledChannel.create = function create(properties) {
        return new UnevenlySampledChannel(properties);
    };

    /**
     * Encodes the specified UnevenlySampledChannel message. Does not implicitly {@link UnevenlySampledChannel.verify|verify} messages.
     * @param {UnevenlySampledChannel$Properties} message UnevenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UnevenlySampledChannel.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.channelTypes && message.channelTypes.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.channelTypes.length; ++i)
                writer.uint32(message.channelTypes[i]);
            writer.ldelim();
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.sensorName);
        if (message.timestampsMicrosecondsUtc && message.timestampsMicrosecondsUtc.length) {
            writer.uint32(/* id 3, wireType 2 =*/26).fork();
            for (var i = 0; i < message.timestampsMicrosecondsUtc.length; ++i)
                writer.int64(message.timestampsMicrosecondsUtc[i]);
            writer.ldelim();
        }
        if (message.bytePayload && message.hasOwnProperty("bytePayload"))
            $root.BytePayload.encode(message.bytePayload, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
        if (message.uint32Payload && message.hasOwnProperty("uint32Payload"))
            $root.UInt32Payload.encode(message.uint32Payload, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
        if (message.uint64Payload && message.hasOwnProperty("uint64Payload"))
            $root.UInt64Payload.encode(message.uint64Payload, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
        if (message.int32Payload && message.hasOwnProperty("int32Payload"))
            $root.Int32Payload.encode(message.int32Payload, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
        if (message.int64Payload && message.hasOwnProperty("int64Payload"))
            $root.Int64Payload.encode(message.int64Payload, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
        if (message.float32Payload && message.hasOwnProperty("float32Payload"))
            $root.Float32Payload.encode(message.float32Payload, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
        if (message.float64Payload && message.hasOwnProperty("float64Payload"))
            $root.Float64Payload.encode(message.float64Payload, writer.uint32(/* id 10, wireType 2 =*/82).fork()).ldelim();
        if (message.sampleIntervalMean != null && message.hasOwnProperty("sampleIntervalMean"))
            writer.uint32(/* id 11, wireType 1 =*/89).double(message.sampleIntervalMean);
        if (message.sampleIntervalStd != null && message.hasOwnProperty("sampleIntervalStd"))
            writer.uint32(/* id 12, wireType 1 =*/97).double(message.sampleIntervalStd);
        if (message.sampleIntervalMedian != null && message.hasOwnProperty("sampleIntervalMedian"))
            writer.uint32(/* id 13, wireType 1 =*/105).double(message.sampleIntervalMedian);
        if (message.valueMeans && message.valueMeans.length) {
            writer.uint32(/* id 14, wireType 2 =*/114).fork();
            for (var i = 0; i < message.valueMeans.length; ++i)
                writer.double(message.valueMeans[i]);
            writer.ldelim();
        }
        if (message.valueStds && message.valueStds.length) {
            writer.uint32(/* id 15, wireType 2 =*/122).fork();
            for (var i = 0; i < message.valueStds.length; ++i)
                writer.double(message.valueStds[i]);
            writer.ldelim();
        }
        if (message.valueMedians && message.valueMedians.length) {
            writer.uint32(/* id 16, wireType 2 =*/130).fork();
            for (var i = 0; i < message.valueMedians.length; ++i)
                writer.double(message.valueMedians[i]);
            writer.ldelim();
        }
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 17, wireType 2 =*/138).string(message.metadata[i]);
        return writer;
    };

    /**
     * Encodes the specified UnevenlySampledChannel message, length delimited. Does not implicitly {@link UnevenlySampledChannel.verify|verify} messages.
     * @param {UnevenlySampledChannel$Properties} message UnevenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UnevenlySampledChannel.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an UnevenlySampledChannel message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UnevenlySampledChannel.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.UnevenlySampledChannel();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.channelTypes && message.channelTypes.length))
                    message.channelTypes = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.channelTypes.push(reader.uint32());
                } else
                    message.channelTypes.push(reader.uint32());
                break;
            case 2:
                message.sensorName = reader.string();
                break;
            case 3:
                if (!(message.timestampsMicrosecondsUtc && message.timestampsMicrosecondsUtc.length))
                    message.timestampsMicrosecondsUtc = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.timestampsMicrosecondsUtc.push(reader.int64());
                } else
                    message.timestampsMicrosecondsUtc.push(reader.int64());
                break;
            case 4:
                message.bytePayload = $root.BytePayload.decode(reader, reader.uint32());
                break;
            case 5:
                message.uint32Payload = $root.UInt32Payload.decode(reader, reader.uint32());
                break;
            case 6:
                message.uint64Payload = $root.UInt64Payload.decode(reader, reader.uint32());
                break;
            case 7:
                message.int32Payload = $root.Int32Payload.decode(reader, reader.uint32());
                break;
            case 8:
                message.int64Payload = $root.Int64Payload.decode(reader, reader.uint32());
                break;
            case 9:
                message.float32Payload = $root.Float32Payload.decode(reader, reader.uint32());
                break;
            case 10:
                message.float64Payload = $root.Float64Payload.decode(reader, reader.uint32());
                break;
            case 11:
                message.sampleIntervalMean = reader.double();
                break;
            case 12:
                message.sampleIntervalStd = reader.double();
                break;
            case 13:
                message.sampleIntervalMedian = reader.double();
                break;
            case 14:
                if (!(message.valueMeans && message.valueMeans.length))
                    message.valueMeans = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMeans.push(reader.double());
                } else
                    message.valueMeans.push(reader.double());
                break;
            case 15:
                if (!(message.valueStds && message.valueStds.length))
                    message.valueStds = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueStds.push(reader.double());
                } else
                    message.valueStds.push(reader.double());
                break;
            case 16:
                if (!(message.valueMedians && message.valueMedians.length))
                    message.valueMedians = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMedians.push(reader.double());
                } else
                    message.valueMedians.push(reader.double());
                break;
            case 17:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an UnevenlySampledChannel message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UnevenlySampledChannel.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an UnevenlySampledChannel message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    UnevenlySampledChannel.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        var properties = {};
        if (message.channelTypes != null) {
            if (!Array.isArray(message.channelTypes))
                return "channelTypes: array expected";
            for (var i = 0; i < message.channelTypes.length; ++i)
                switch (message.channelTypes[i]) {
                default:
                    return "channelTypes: enum value[] expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                    break;
                }
        }
        if (message.sensorName != null)
            if (!$util.isString(message.sensorName))
                return "sensorName: string expected";
        if (message.timestampsMicrosecondsUtc != null) {
            if (!Array.isArray(message.timestampsMicrosecondsUtc))
                return "timestampsMicrosecondsUtc: array expected";
            for (var i = 0; i < message.timestampsMicrosecondsUtc.length; ++i)
                if (!$util.isInteger(message.timestampsMicrosecondsUtc[i]) && !(message.timestampsMicrosecondsUtc[i] && $util.isInteger(message.timestampsMicrosecondsUtc[i].low) && $util.isInteger(message.timestampsMicrosecondsUtc[i].high)))
                    return "timestampsMicrosecondsUtc: integer|Long[] expected";
        }
        if (message.bytePayload != null) {
            properties.payload = 1;
            var error = $root.BytePayload.verify(message.bytePayload);
            if (error)
                return "bytePayload." + error;
        }
        if (message.uint32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt32Payload.verify(message.uint32Payload);
            if (error)
                return "uint32Payload." + error;
        }
        if (message.uint64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt64Payload.verify(message.uint64Payload);
            if (error)
                return "uint64Payload." + error;
        }
        if (message.int32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int32Payload.verify(message.int32Payload);
            if (error)
                return "int32Payload." + error;
        }
        if (message.int64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int64Payload.verify(message.int64Payload);
            if (error)
                return "int64Payload." + error;
        }
        if (message.float32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float32Payload.verify(message.float32Payload);
            if (error)
                return "float32Payload." + error;
        }
        if (message.float64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float64Payload.verify(message.float64Payload);
            if (error)
                return "float64Payload." + error;
        }
        if (message.sampleIntervalMean != null)
            if (typeof message.sampleIntervalMean !== "number")
                return "sampleIntervalMean: number expected";
        if (message.sampleIntervalStd != null)
            if (typeof message.sampleIntervalStd !== "number")
                return "sampleIntervalStd: number expected";
        if (message.sampleIntervalMedian != null)
            if (typeof message.sampleIntervalMedian !== "number")
                return "sampleIntervalMedian: number expected";
        if (message.valueMeans != null) {
            if (!Array.isArray(message.valueMeans))
                return "valueMeans: array expected";
            for (var i = 0; i < message.valueMeans.length; ++i)
                if (typeof message.valueMeans[i] !== "number")
                    return "valueMeans: number[] expected";
        }
        if (message.valueStds != null) {
            if (!Array.isArray(message.valueStds))
                return "valueStds: array expected";
            for (var i = 0; i < message.valueStds.length; ++i)
                if (typeof message.valueStds[i] !== "number")
                    return "valueStds: number[] expected";
        }
        if (message.valueMedians != null) {
            if (!Array.isArray(message.valueMedians))
                return "valueMedians: array expected";
            for (var i = 0; i < message.valueMedians.length; ++i)
                if (typeof message.valueMedians[i] !== "number")
                    return "valueMedians: number[] expected";
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates an UnevenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     */
    UnevenlySampledChannel.fromObject = function fromObject(object) {
        if (object instanceof $root.UnevenlySampledChannel)
            return object;
        var message = new $root.UnevenlySampledChannel();
        if (object.channelTypes) {
            if (!Array.isArray(object.channelTypes))
                throw TypeError(".UnevenlySampledChannel.channelTypes: array expected");
            message.channelTypes = [];
            for (var i = 0; i < object.channelTypes.length; ++i)
                switch (object.channelTypes[i]) {
                default:
                case "MICROPHONE":
                case 0:
                    message.channelTypes[i] = 0;
                    break;
                case "BAROMETER":
                case 1:
                    message.channelTypes[i] = 1;
                    break;
                case "LATITUDE":
                case 2:
                    message.channelTypes[i] = 2;
                    break;
                case "LONGITUDE":
                case 3:
                    message.channelTypes[i] = 3;
                    break;
                case "SPEED":
                case 4:
                    message.channelTypes[i] = 4;
                    break;
                case "ALTITUDE":
                case 5:
                    message.channelTypes[i] = 5;
                    break;
                case "RESERVED_0":
                case 6:
                    message.channelTypes[i] = 6;
                    break;
                case "RESERVED_1":
                case 7:
                    message.channelTypes[i] = 7;
                    break;
                case "RESERVED_2":
                case 8:
                    message.channelTypes[i] = 8;
                    break;
                case "TIME_SYNCHRONIZATION":
                case 9:
                    message.channelTypes[i] = 9;
                    break;
                case "ACCURACY":
                case 10:
                    message.channelTypes[i] = 10;
                    break;
                case "ACCELEROMETER_X":
                case 11:
                    message.channelTypes[i] = 11;
                    break;
                case "ACCELEROMETER_Y":
                case 12:
                    message.channelTypes[i] = 12;
                    break;
                case "ACCELEROMETER_Z":
                case 13:
                    message.channelTypes[i] = 13;
                    break;
                case "MAGNETOMETER_X":
                case 14:
                    message.channelTypes[i] = 14;
                    break;
                case "MAGNETOMETER_Y":
                case 15:
                    message.channelTypes[i] = 15;
                    break;
                case "MAGNETOMETER_Z":
                case 16:
                    message.channelTypes[i] = 16;
                    break;
                case "GYROSCOPE_X":
                case 17:
                    message.channelTypes[i] = 17;
                    break;
                case "GYROSCOPE_Y":
                case 18:
                    message.channelTypes[i] = 18;
                    break;
                case "GYROSCOPE_Z":
                case 19:
                    message.channelTypes[i] = 19;
                    break;
                case "OTHER":
                case 20:
                    message.channelTypes[i] = 20;
                    break;
                case "LIGHT":
                case 21:
                    message.channelTypes[i] = 21;
                    break;
                case "IMAGE":
                case 22:
                    message.channelTypes[i] = 22;
                    break;
                case "INFRARED":
                case 23:
                    message.channelTypes[i] = 23;
                    break;
                }
        }
        if (object.sensorName != null)
            message.sensorName = String(object.sensorName);
        if (object.timestampsMicrosecondsUtc) {
            if (!Array.isArray(object.timestampsMicrosecondsUtc))
                throw TypeError(".UnevenlySampledChannel.timestampsMicrosecondsUtc: array expected");
            message.timestampsMicrosecondsUtc = [];
            for (var i = 0; i < object.timestampsMicrosecondsUtc.length; ++i)
                if ($util.Long)
                    (message.timestampsMicrosecondsUtc[i] = $util.Long.fromValue(object.timestampsMicrosecondsUtc[i])).unsigned = false;
                else if (typeof object.timestampsMicrosecondsUtc[i] === "string")
                    message.timestampsMicrosecondsUtc[i] = parseInt(object.timestampsMicrosecondsUtc[i], 10);
                else if (typeof object.timestampsMicrosecondsUtc[i] === "number")
                    message.timestampsMicrosecondsUtc[i] = object.timestampsMicrosecondsUtc[i];
                else if (typeof object.timestampsMicrosecondsUtc[i] === "object")
                    message.timestampsMicrosecondsUtc[i] = new $util.LongBits(object.timestampsMicrosecondsUtc[i].low >>> 0, object.timestampsMicrosecondsUtc[i].high >>> 0).toNumber();
        }
        if (object.bytePayload != null) {
            if (typeof object.bytePayload !== "object")
                throw TypeError(".UnevenlySampledChannel.bytePayload: object expected");
            message.bytePayload = $root.BytePayload.fromObject(object.bytePayload);
        }
        if (object.uint32Payload != null) {
            if (typeof object.uint32Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.uint32Payload: object expected");
            message.uint32Payload = $root.UInt32Payload.fromObject(object.uint32Payload);
        }
        if (object.uint64Payload != null) {
            if (typeof object.uint64Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.uint64Payload: object expected");
            message.uint64Payload = $root.UInt64Payload.fromObject(object.uint64Payload);
        }
        if (object.int32Payload != null) {
            if (typeof object.int32Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.int32Payload: object expected");
            message.int32Payload = $root.Int32Payload.fromObject(object.int32Payload);
        }
        if (object.int64Payload != null) {
            if (typeof object.int64Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.int64Payload: object expected");
            message.int64Payload = $root.Int64Payload.fromObject(object.int64Payload);
        }
        if (object.float32Payload != null) {
            if (typeof object.float32Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.float32Payload: object expected");
            message.float32Payload = $root.Float32Payload.fromObject(object.float32Payload);
        }
        if (object.float64Payload != null) {
            if (typeof object.float64Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.float64Payload: object expected");
            message.float64Payload = $root.Float64Payload.fromObject(object.float64Payload);
        }
        if (object.sampleIntervalMean != null)
            message.sampleIntervalMean = Number(object.sampleIntervalMean);
        if (object.sampleIntervalStd != null)
            message.sampleIntervalStd = Number(object.sampleIntervalStd);
        if (object.sampleIntervalMedian != null)
            message.sampleIntervalMedian = Number(object.sampleIntervalMedian);
        if (object.valueMeans) {
            if (!Array.isArray(object.valueMeans))
                throw TypeError(".UnevenlySampledChannel.valueMeans: array expected");
            message.valueMeans = [];
            for (var i = 0; i < object.valueMeans.length; ++i)
                message.valueMeans[i] = Number(object.valueMeans[i]);
        }
        if (object.valueStds) {
            if (!Array.isArray(object.valueStds))
                throw TypeError(".UnevenlySampledChannel.valueStds: array expected");
            message.valueStds = [];
            for (var i = 0; i < object.valueStds.length; ++i)
                message.valueStds[i] = Number(object.valueStds[i]);
        }
        if (object.valueMedians) {
            if (!Array.isArray(object.valueMedians))
                throw TypeError(".UnevenlySampledChannel.valueMedians: array expected");
            message.valueMedians = [];
            for (var i = 0; i < object.valueMedians.length; ++i)
                message.valueMedians[i] = Number(object.valueMedians[i]);
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".UnevenlySampledChannel.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates an UnevenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link UnevenlySampledChannel.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     */
    UnevenlySampledChannel.from = UnevenlySampledChannel.fromObject;

    /**
     * Creates a plain object from an UnevenlySampledChannel message. Also converts values to other types if specified.
     * @param {UnevenlySampledChannel} message UnevenlySampledChannel
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UnevenlySampledChannel.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.channelTypes = [];
            object.timestampsMicrosecondsUtc = [];
            object.valueMeans = [];
            object.valueStds = [];
            object.valueMedians = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.sensorName = "";
            object.sampleIntervalMean = 0;
            object.sampleIntervalStd = 0;
            object.sampleIntervalMedian = 0;
        }
        if (message.channelTypes && message.channelTypes.length) {
            object.channelTypes = [];
            for (var j = 0; j < message.channelTypes.length; ++j)
                object.channelTypes[j] = options.enums === String ? $root.ChannelType[message.channelTypes[j]] : message.channelTypes[j];
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            object.sensorName = message.sensorName;
        if (message.timestampsMicrosecondsUtc && message.timestampsMicrosecondsUtc.length) {
            object.timestampsMicrosecondsUtc = [];
            for (var j = 0; j < message.timestampsMicrosecondsUtc.length; ++j)
                if (typeof message.timestampsMicrosecondsUtc[j] === "number")
                    object.timestampsMicrosecondsUtc[j] = options.longs === String ? String(message.timestampsMicrosecondsUtc[j]) : message.timestampsMicrosecondsUtc[j];
                else
                    object.timestampsMicrosecondsUtc[j] = options.longs === String ? $util.Long.prototype.toString.call(message.timestampsMicrosecondsUtc[j]) : options.longs === Number ? new $util.LongBits(message.timestampsMicrosecondsUtc[j].low >>> 0, message.timestampsMicrosecondsUtc[j].high >>> 0).toNumber() : message.timestampsMicrosecondsUtc[j];
        }
        if (message.bytePayload != null && message.hasOwnProperty("bytePayload")) {
            object.bytePayload = $root.BytePayload.toObject(message.bytePayload, options);
            if (options.oneofs)
                object.payload = "bytePayload";
        }
        if (message.uint32Payload != null && message.hasOwnProperty("uint32Payload")) {
            object.uint32Payload = $root.UInt32Payload.toObject(message.uint32Payload, options);
            if (options.oneofs)
                object.payload = "uint32Payload";
        }
        if (message.uint64Payload != null && message.hasOwnProperty("uint64Payload")) {
            object.uint64Payload = $root.UInt64Payload.toObject(message.uint64Payload, options);
            if (options.oneofs)
                object.payload = "uint64Payload";
        }
        if (message.int32Payload != null && message.hasOwnProperty("int32Payload")) {
            object.int32Payload = $root.Int32Payload.toObject(message.int32Payload, options);
            if (options.oneofs)
                object.payload = "int32Payload";
        }
        if (message.int64Payload != null && message.hasOwnProperty("int64Payload")) {
            object.int64Payload = $root.Int64Payload.toObject(message.int64Payload, options);
            if (options.oneofs)
                object.payload = "int64Payload";
        }
        if (message.float32Payload != null && message.hasOwnProperty("float32Payload")) {
            object.float32Payload = $root.Float32Payload.toObject(message.float32Payload, options);
            if (options.oneofs)
                object.payload = "float32Payload";
        }
        if (message.float64Payload != null && message.hasOwnProperty("float64Payload")) {
            object.float64Payload = $root.Float64Payload.toObject(message.float64Payload, options);
            if (options.oneofs)
                object.payload = "float64Payload";
        }
        if (message.sampleIntervalMean != null && message.hasOwnProperty("sampleIntervalMean"))
            object.sampleIntervalMean = message.sampleIntervalMean;
        if (message.sampleIntervalStd != null && message.hasOwnProperty("sampleIntervalStd"))
            object.sampleIntervalStd = message.sampleIntervalStd;
        if (message.sampleIntervalMedian != null && message.hasOwnProperty("sampleIntervalMedian"))
            object.sampleIntervalMedian = message.sampleIntervalMedian;
        if (message.valueMeans && message.valueMeans.length) {
            object.valueMeans = [];
            for (var j = 0; j < message.valueMeans.length; ++j)
                object.valueMeans[j] = message.valueMeans[j];
        }
        if (message.valueStds && message.valueStds.length) {
            object.valueStds = [];
            for (var j = 0; j < message.valueStds.length; ++j)
                object.valueStds[j] = message.valueStds[j];
        }
        if (message.valueMedians && message.valueMedians.length) {
            object.valueMedians = [];
            for (var j = 0; j < message.valueMedians.length; ++j)
                object.valueMedians[j] = message.valueMedians[j];
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this UnevenlySampledChannel message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UnevenlySampledChannel.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this UnevenlySampledChannel to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    UnevenlySampledChannel.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return UnevenlySampledChannel;
})();

$root.RedvoxPacketResponse = (function() {

    /**
     * Properties of a RedvoxPacketResponse.
     * @typedef RedvoxPacketResponse$Properties
     * @type {Object}
     * @property {RedvoxPacketResponse.Type} [type] RedvoxPacketResponse type.
     * @property {number|Long} [checksum] RedvoxPacketResponse checksum.
     * @property {Array.<RedvoxPacketResponse.Error>} [errors] RedvoxPacketResponse errors.
     * @property {Array.<string>} [metadata] RedvoxPacketResponse metadata.
     */

    /**
     * Constructs a new RedvoxPacketResponse.
     * @exports RedvoxPacketResponse
     * @constructor
     * @param {RedvoxPacketResponse$Properties=} [properties] Properties to set
     */
    function RedvoxPacketResponse(properties) {
        this.errors = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * RedvoxPacketResponse type.
     * @type {RedvoxPacketResponse.Type|undefined}
     */
    RedvoxPacketResponse.prototype.type = 0;

    /**
     * RedvoxPacketResponse checksum.
     * @type {number|Long|undefined}
     */
    RedvoxPacketResponse.prototype.checksum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacketResponse errors.
     * @type {Array.<RedvoxPacketResponse.Error>|undefined}
     */
    RedvoxPacketResponse.prototype.errors = $util.emptyArray;

    /**
     * RedvoxPacketResponse metadata.
     * @type {Array.<string>|undefined}
     */
    RedvoxPacketResponse.prototype.metadata = $util.emptyArray;

    /**
     * Creates a new RedvoxPacketResponse instance using the specified properties.
     * @param {RedvoxPacketResponse$Properties=} [properties] Properties to set
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse instance
     */
    RedvoxPacketResponse.create = function create(properties) {
        return new RedvoxPacketResponse(properties);
    };

    /**
     * Encodes the specified RedvoxPacketResponse message. Does not implicitly {@link RedvoxPacketResponse.verify|verify} messages.
     * @param {RedvoxPacketResponse$Properties} message RedvoxPacketResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacketResponse.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.type != null && message.hasOwnProperty("type"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.type);
        if (message.checksum != null && message.hasOwnProperty("checksum"))
            writer.uint32(/* id 2, wireType 0 =*/16).int64(message.checksum);
        if (message.errors && message.errors.length) {
            writer.uint32(/* id 3, wireType 2 =*/26).fork();
            for (var i = 0; i < message.errors.length; ++i)
                writer.uint32(message.errors[i]);
            writer.ldelim();
        }
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.metadata[i]);
        return writer;
    };

    /**
     * Encodes the specified RedvoxPacketResponse message, length delimited. Does not implicitly {@link RedvoxPacketResponse.verify|verify} messages.
     * @param {RedvoxPacketResponse$Properties} message RedvoxPacketResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacketResponse.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a RedvoxPacketResponse message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacketResponse.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.RedvoxPacketResponse();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.type = reader.uint32();
                break;
            case 2:
                message.checksum = reader.int64();
                break;
            case 3:
                if (!(message.errors && message.errors.length))
                    message.errors = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.errors.push(reader.uint32());
                } else
                    message.errors.push(reader.uint32());
                break;
            case 4:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a RedvoxPacketResponse message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacketResponse.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a RedvoxPacketResponse message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    RedvoxPacketResponse.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.type != null)
            switch (message.type) {
            default:
                return "type: enum value expected";
            case 0:
            case 1:
                break;
            }
        if (message.checksum != null)
            if (!$util.isInteger(message.checksum) && !(message.checksum && $util.isInteger(message.checksum.low) && $util.isInteger(message.checksum.high)))
                return "checksum: integer|Long expected";
        if (message.errors != null) {
            if (!Array.isArray(message.errors))
                return "errors: array expected";
            for (var i = 0; i < message.errors.length; ++i)
                switch (message.errors[i]) {
                default:
                    return "errors: enum value[] expected";
                case 0:
                case 1:
                    break;
                }
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates a RedvoxPacketResponse message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     */
    RedvoxPacketResponse.fromObject = function fromObject(object) {
        if (object instanceof $root.RedvoxPacketResponse)
            return object;
        var message = new $root.RedvoxPacketResponse();
        switch (object.type) {
        case "OK":
        case 0:
            message.type = 0;
            break;
        case "ERROR":
        case 1:
            message.type = 1;
            break;
        }
        if (object.checksum != null)
            if ($util.Long)
                (message.checksum = $util.Long.fromValue(object.checksum)).unsigned = false;
            else if (typeof object.checksum === "string")
                message.checksum = parseInt(object.checksum, 10);
            else if (typeof object.checksum === "number")
                message.checksum = object.checksum;
            else if (typeof object.checksum === "object")
                message.checksum = new $util.LongBits(object.checksum.low >>> 0, object.checksum.high >>> 0).toNumber();
        if (object.errors) {
            if (!Array.isArray(object.errors))
                throw TypeError(".RedvoxPacketResponse.errors: array expected");
            message.errors = [];
            for (var i = 0; i < object.errors.length; ++i)
                switch (object.errors[i]) {
                default:
                case "NOT_AUTHENTICATED":
                case 0:
                    message.errors[i] = 0;
                    break;
                case "OTHER":
                case 1:
                    message.errors[i] = 1;
                    break;
                }
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".RedvoxPacketResponse.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates a RedvoxPacketResponse message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link RedvoxPacketResponse.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     */
    RedvoxPacketResponse.from = RedvoxPacketResponse.fromObject;

    /**
     * Creates a plain object from a RedvoxPacketResponse message. Also converts values to other types if specified.
     * @param {RedvoxPacketResponse} message RedvoxPacketResponse
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacketResponse.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.errors = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.type = options.enums === String ? "OK" : 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.checksum = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.checksum = options.longs === String ? "0" : 0;
        }
        if (message.type != null && message.hasOwnProperty("type"))
            object.type = options.enums === String ? $root.RedvoxPacketResponse.Type[message.type] : message.type;
        if (message.checksum != null && message.hasOwnProperty("checksum"))
            if (typeof message.checksum === "number")
                object.checksum = options.longs === String ? String(message.checksum) : message.checksum;
            else
                object.checksum = options.longs === String ? $util.Long.prototype.toString.call(message.checksum) : options.longs === Number ? new $util.LongBits(message.checksum.low >>> 0, message.checksum.high >>> 0).toNumber() : message.checksum;
        if (message.errors && message.errors.length) {
            object.errors = [];
            for (var j = 0; j < message.errors.length; ++j)
                object.errors[j] = options.enums === String ? $root.RedvoxPacketResponse.Error[message.errors[j]] : message.errors[j];
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this RedvoxPacketResponse message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacketResponse.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this RedvoxPacketResponse to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    RedvoxPacketResponse.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    /**
     * Type enum.
     * @name Type
     * @memberof RedvoxPacketResponse
     * @enum {number}
     * @property {number} OK=0 OK value
     * @property {number} ERROR=1 ERROR value
     */
    RedvoxPacketResponse.Type = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "OK"] = 0;
        values[valuesById[1] = "ERROR"] = 1;
        return values;
    })();

    /**
     * Error enum.
     * @name Error
     * @memberof RedvoxPacketResponse
     * @enum {number}
     * @property {number} NOT_AUTHENTICATED=0 NOT_AUTHENTICATED value
     * @property {number} OTHER=1 OTHER value
     */
    RedvoxPacketResponse.Error = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "NOT_AUTHENTICATED"] = 0;
        values[valuesById[1] = "OTHER"] = 1;
        return values;
    })();

    return RedvoxPacketResponse;
})();

var redvoxwebpb = protobuf.roots["redvoxweb"]["protobuf"];
