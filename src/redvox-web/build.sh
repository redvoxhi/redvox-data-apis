#!/bin/bash

#cd ../api00
#cwd="$(pwd)"
#api900path=${cwd}"/api900.proto"
#echo $api900path
#cd -


set -o xtrace

NAME="redvoxweb"

echo "Compiling ${NAME}.proto"
rm -rf java
rm -rf python
rm -rf javascript
mkdir -p java
mkdir -p python
mkdir -p javascript
protoc --java_out=./java --python_out=./python ${NAME}.proto
pbjs --target static-module --wrap ./jswrapper.js --root redvoxweb --out ./javascript/${NAME}.pb.js ${NAME}.proto # Requires node, npm, protobufjs to be installed globally

set +o xtrace
