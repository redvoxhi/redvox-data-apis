#!/bin/bash

set -o xtrace

echo "Starting build..."

# Check that all dependencies are met
hash protoc 2>/dev/null || { echo >&2 "protoc not found on path. Obtain latest version from https://github.com/google/protobuf/releases"; exit 1; }
#hash protoc-gen-javalite  2>/dev/null || { echo >&2 "protoc-gen-javalite not found on path. Obtain latest version from https://repo1.maven.org/maven2/com/google/protobuf/protoc-gen-javalite/"; exit 1; }
hash pbjs 2>/dev/null || { echo >&2 "pbjs not found on path. Obtain latest version by installing package globally via npm/node. More at https://www.npmjs.com/package/pbjs"; exit 1; }

# Define constants
SRC=api900.proto

GEN=generated_code
GEN_CPP=${GEN}/cpp
GEN_CPPLITE=${GEN}/cpplite
GEN_CSHARP=${GEN}/csharp
GEN_GO=${GEN}/go
GEN_JAVA=${GEN}/java
GEN_JAVASCRIPT=${GEN}/javascript
GEN_OBJC=${GEN}/obj-c
GEN_PHP=${GEN}/php
GEN_PYTHON=${GEN}/python
GEN_RUBY=${GEN}/ruby

JS_OUT=api900.pb.js

# Remove old generated code
rm -rf ${GEN}

# Create directories for newly generated code
mkdir -p ${GEN_CPP}
mkdir -p ${GEN_CPPLITE}
mkdir -p ${GEN_CSHARP}
mkdir -p ${GEN_JAVA}
mkdir -p ${GEN_JAVASCRIPT}
mkdir -p ${GEN_OBJC}
mkdir -p ${GEN_PHP}
mkdir -p ${GEN_PYTHON}
mkdir -p ${GEN_RUBY}

echo "Compiling ${SRC} with protoc"
protoc --cpp_out=./${GEN_CPP} \
       --cpp_out=lite:./${GEN_CPPLITE} \
       --csharp_out=./${GEN_CSHARP} \
       --java_out=./${GEN_JAVA} \
       --objc_out=./${GEN_OBJC} \
       --php_out=./${GEN_PHP} \
       --python_out=./${GEN_PYTHON} \
       --ruby_out=./${GEN_RUBY} \
       ${SRC}

# Generate javascript seperately using 3rd party pbjs
# Requires node, npm, protobufjs to be installed globally
echo "Compiling ${SRC} with pbjs"
pbjs --target static --root redvox --out ./${GEN_JAVASCRIPT}/${JS_OUT} ${SRC}

echo "Done."

set +o xtrace
