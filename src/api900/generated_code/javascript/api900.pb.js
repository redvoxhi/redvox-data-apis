// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots.redvox || ($protobuf.roots.redvox = {});

$root.RedvoxPacket = (function() {

    /**
     * Properties of a RedvoxPacket.
     * @typedef RedvoxPacket$Properties
     * @type {Object}
     * @property {number} [api] RedvoxPacket api.
     * @property {string} [uuid] RedvoxPacket uuid.
     * @property {string} [redvoxId] RedvoxPacket redvoxId.
     * @property {string} [authenticatedEmail] RedvoxPacket authenticatedEmail.
     * @property {string} [authenticationToken] RedvoxPacket authenticationToken.
     * @property {string} [firebaseToken] RedvoxPacket firebaseToken.
     * @property {boolean} [isBackfilled] RedvoxPacket isBackfilled.
     * @property {boolean} [isPrivate] RedvoxPacket isPrivate.
     * @property {boolean} [isScrambled] RedvoxPacket isScrambled.
     * @property {string} [deviceMake] RedvoxPacket deviceMake.
     * @property {string} [deviceModel] RedvoxPacket deviceModel.
     * @property {string} [deviceOs] RedvoxPacket deviceOs.
     * @property {string} [deviceOsVersion] RedvoxPacket deviceOsVersion.
     * @property {string} [appVersion] RedvoxPacket appVersion.
     * @property {number} [batteryLevelPercent] RedvoxPacket batteryLevelPercent.
     * @property {number} [deviceTemperatureC] RedvoxPacket deviceTemperatureC.
     * @property {string} [acquisitionServer] RedvoxPacket acquisitionServer.
     * @property {string} [timeSynchronizationServer] RedvoxPacket timeSynchronizationServer.
     * @property {string} [authenticationServer] RedvoxPacket authenticationServer.
     * @property {number|Long} [appFileStartTimestampEpochMicrosecondsUtc] RedvoxPacket appFileStartTimestampEpochMicrosecondsUtc.
     * @property {number|Long} [appFileStartTimestampMachine] RedvoxPacket appFileStartTimestampMachine.
     * @property {number|Long} [serverTimestampEpochMicrosecondsUtc] RedvoxPacket serverTimestampEpochMicrosecondsUtc.
     * @property {Array.<EvenlySampledChannel$Properties>} [evenlySampledChannels] RedvoxPacket evenlySampledChannels.
     * @property {Array.<UnevenlySampledChannel$Properties>} [unevenlySampledChannels] RedvoxPacket unevenlySampledChannels.
     * @property {Array.<string>} [metadata] RedvoxPacket metadata.
     */

    /**
     * Constructs a new RedvoxPacket.
     * @exports RedvoxPacket
     * @constructor
     * @param {RedvoxPacket$Properties=} [properties] Properties to set
     */
    function RedvoxPacket(properties) {
        this.evenlySampledChannels = [];
        this.unevenlySampledChannels = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * RedvoxPacket api.
     * @type {number|undefined}
     */
    RedvoxPacket.prototype.api = 0;

    /**
     * RedvoxPacket uuid.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.uuid = "";

    /**
     * RedvoxPacket redvoxId.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.redvoxId = "";

    /**
     * RedvoxPacket authenticatedEmail.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.authenticatedEmail = "";

    /**
     * RedvoxPacket authenticationToken.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.authenticationToken = "";

    /**
     * RedvoxPacket firebaseToken.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.firebaseToken = "";

    /**
     * RedvoxPacket isBackfilled.
     * @type {boolean|undefined}
     */
    RedvoxPacket.prototype.isBackfilled = false;

    /**
     * RedvoxPacket isPrivate.
     * @type {boolean|undefined}
     */
    RedvoxPacket.prototype.isPrivate = false;

    /**
     * RedvoxPacket isScrambled.
     * @type {boolean|undefined}
     */
    RedvoxPacket.prototype.isScrambled = false;

    /**
     * RedvoxPacket deviceMake.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceMake = "";

    /**
     * RedvoxPacket deviceModel.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceModel = "";

    /**
     * RedvoxPacket deviceOs.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceOs = "";

    /**
     * RedvoxPacket deviceOsVersion.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.deviceOsVersion = "";

    /**
     * RedvoxPacket appVersion.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.appVersion = "";

    /**
     * RedvoxPacket batteryLevelPercent.
     * @type {number|undefined}
     */
    RedvoxPacket.prototype.batteryLevelPercent = 0;

    /**
     * RedvoxPacket deviceTemperatureC.
     * @type {number|undefined}
     */
    RedvoxPacket.prototype.deviceTemperatureC = 0;

    /**
     * RedvoxPacket acquisitionServer.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.acquisitionServer = "";

    /**
     * RedvoxPacket timeSynchronizationServer.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.timeSynchronizationServer = "";

    /**
     * RedvoxPacket authenticationServer.
     * @type {string|undefined}
     */
    RedvoxPacket.prototype.authenticationServer = "";

    /**
     * RedvoxPacket appFileStartTimestampEpochMicrosecondsUtc.
     * @type {number|Long|undefined}
     */
    RedvoxPacket.prototype.appFileStartTimestampEpochMicrosecondsUtc = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacket appFileStartTimestampMachine.
     * @type {number|Long|undefined}
     */
    RedvoxPacket.prototype.appFileStartTimestampMachine = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacket serverTimestampEpochMicrosecondsUtc.
     * @type {number|Long|undefined}
     */
    RedvoxPacket.prototype.serverTimestampEpochMicrosecondsUtc = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacket evenlySampledChannels.
     * @type {Array.<EvenlySampledChannel$Properties>|undefined}
     */
    RedvoxPacket.prototype.evenlySampledChannels = $util.emptyArray;

    /**
     * RedvoxPacket unevenlySampledChannels.
     * @type {Array.<UnevenlySampledChannel$Properties>|undefined}
     */
    RedvoxPacket.prototype.unevenlySampledChannels = $util.emptyArray;

    /**
     * RedvoxPacket metadata.
     * @type {Array.<string>|undefined}
     */
    RedvoxPacket.prototype.metadata = $util.emptyArray;

    /**
     * Creates a new RedvoxPacket instance using the specified properties.
     * @param {RedvoxPacket$Properties=} [properties] Properties to set
     * @returns {RedvoxPacket} RedvoxPacket instance
     */
    RedvoxPacket.create = function create(properties) {
        return new RedvoxPacket(properties);
    };

    /**
     * Encodes the specified RedvoxPacket message. Does not implicitly {@link RedvoxPacket.verify|verify} messages.
     * @param {RedvoxPacket$Properties} message RedvoxPacket message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacket.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.api != null && message.hasOwnProperty("api"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.api);
        if (message.uuid != null && message.hasOwnProperty("uuid"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.uuid);
        if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.redvoxId);
        if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
            writer.uint32(/* id 4, wireType 2 =*/34).string(message.authenticatedEmail);
        if (message.authenticationToken != null && message.hasOwnProperty("authenticationToken"))
            writer.uint32(/* id 5, wireType 2 =*/42).string(message.authenticationToken);
        if (message.isBackfilled != null && message.hasOwnProperty("isBackfilled"))
            writer.uint32(/* id 6, wireType 0 =*/48).bool(message.isBackfilled);
        if (message.isPrivate != null && message.hasOwnProperty("isPrivate"))
            writer.uint32(/* id 7, wireType 0 =*/56).bool(message.isPrivate);
        if (message.isScrambled != null && message.hasOwnProperty("isScrambled"))
            writer.uint32(/* id 8, wireType 0 =*/64).bool(message.isScrambled);
        if (message.deviceMake != null && message.hasOwnProperty("deviceMake"))
            writer.uint32(/* id 9, wireType 2 =*/74).string(message.deviceMake);
        if (message.deviceModel != null && message.hasOwnProperty("deviceModel"))
            writer.uint32(/* id 10, wireType 2 =*/82).string(message.deviceModel);
        if (message.deviceOs != null && message.hasOwnProperty("deviceOs"))
            writer.uint32(/* id 11, wireType 2 =*/90).string(message.deviceOs);
        if (message.deviceOsVersion != null && message.hasOwnProperty("deviceOsVersion"))
            writer.uint32(/* id 12, wireType 2 =*/98).string(message.deviceOsVersion);
        if (message.appVersion != null && message.hasOwnProperty("appVersion"))
            writer.uint32(/* id 13, wireType 2 =*/106).string(message.appVersion);
        if (message.acquisitionServer != null && message.hasOwnProperty("acquisitionServer"))
            writer.uint32(/* id 14, wireType 2 =*/114).string(message.acquisitionServer);
        if (message.timeSynchronizationServer != null && message.hasOwnProperty("timeSynchronizationServer"))
            writer.uint32(/* id 15, wireType 2 =*/122).string(message.timeSynchronizationServer);
        if (message.authenticationServer != null && message.hasOwnProperty("authenticationServer"))
            writer.uint32(/* id 16, wireType 2 =*/130).string(message.authenticationServer);
        if (message.appFileStartTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("appFileStartTimestampEpochMicrosecondsUtc"))
            writer.uint32(/* id 17, wireType 0 =*/136).int64(message.appFileStartTimestampEpochMicrosecondsUtc);
        if (message.appFileStartTimestampMachine != null && message.hasOwnProperty("appFileStartTimestampMachine"))
            writer.uint32(/* id 18, wireType 0 =*/144).int64(message.appFileStartTimestampMachine);
        if (message.serverTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("serverTimestampEpochMicrosecondsUtc"))
            writer.uint32(/* id 19, wireType 0 =*/152).int64(message.serverTimestampEpochMicrosecondsUtc);
        if (message.evenlySampledChannels && message.evenlySampledChannels.length)
            for (var i = 0; i < message.evenlySampledChannels.length; ++i)
                $root.EvenlySampledChannel.encode(message.evenlySampledChannels[i], writer.uint32(/* id 20, wireType 2 =*/162).fork()).ldelim();
        if (message.unevenlySampledChannels && message.unevenlySampledChannels.length)
            for (var i = 0; i < message.unevenlySampledChannels.length; ++i)
                $root.UnevenlySampledChannel.encode(message.unevenlySampledChannels[i], writer.uint32(/* id 21, wireType 2 =*/170).fork()).ldelim();
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 22, wireType 2 =*/178).string(message.metadata[i]);
        if (message.firebaseToken != null && message.hasOwnProperty("firebaseToken"))
            writer.uint32(/* id 23, wireType 2 =*/186).string(message.firebaseToken);
        if (message.batteryLevelPercent != null && message.hasOwnProperty("batteryLevelPercent"))
            writer.uint32(/* id 24, wireType 5 =*/197).float(message.batteryLevelPercent);
        if (message.deviceTemperatureC != null && message.hasOwnProperty("deviceTemperatureC"))
            writer.uint32(/* id 25, wireType 5 =*/205).float(message.deviceTemperatureC);
        return writer;
    };

    /**
     * Encodes the specified RedvoxPacket message, length delimited. Does not implicitly {@link RedvoxPacket.verify|verify} messages.
     * @param {RedvoxPacket$Properties} message RedvoxPacket message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacket.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a RedvoxPacket message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {RedvoxPacket} RedvoxPacket
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacket.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.RedvoxPacket();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.api = reader.uint32();
                break;
            case 2:
                message.uuid = reader.string();
                break;
            case 3:
                message.redvoxId = reader.string();
                break;
            case 4:
                message.authenticatedEmail = reader.string();
                break;
            case 5:
                message.authenticationToken = reader.string();
                break;
            case 23:
                message.firebaseToken = reader.string();
                break;
            case 6:
                message.isBackfilled = reader.bool();
                break;
            case 7:
                message.isPrivate = reader.bool();
                break;
            case 8:
                message.isScrambled = reader.bool();
                break;
            case 9:
                message.deviceMake = reader.string();
                break;
            case 10:
                message.deviceModel = reader.string();
                break;
            case 11:
                message.deviceOs = reader.string();
                break;
            case 12:
                message.deviceOsVersion = reader.string();
                break;
            case 13:
                message.appVersion = reader.string();
                break;
            case 24:
                message.batteryLevelPercent = reader.float();
                break;
            case 25:
                message.deviceTemperatureC = reader.float();
                break;
            case 14:
                message.acquisitionServer = reader.string();
                break;
            case 15:
                message.timeSynchronizationServer = reader.string();
                break;
            case 16:
                message.authenticationServer = reader.string();
                break;
            case 17:
                message.appFileStartTimestampEpochMicrosecondsUtc = reader.int64();
                break;
            case 18:
                message.appFileStartTimestampMachine = reader.int64();
                break;
            case 19:
                message.serverTimestampEpochMicrosecondsUtc = reader.int64();
                break;
            case 20:
                if (!(message.evenlySampledChannels && message.evenlySampledChannels.length))
                    message.evenlySampledChannels = [];
                message.evenlySampledChannels.push($root.EvenlySampledChannel.decode(reader, reader.uint32()));
                break;
            case 21:
                if (!(message.unevenlySampledChannels && message.unevenlySampledChannels.length))
                    message.unevenlySampledChannels = [];
                message.unevenlySampledChannels.push($root.UnevenlySampledChannel.decode(reader, reader.uint32()));
                break;
            case 22:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a RedvoxPacket message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {RedvoxPacket} RedvoxPacket
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacket.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a RedvoxPacket message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    RedvoxPacket.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.api != null)
            if (!$util.isInteger(message.api))
                return "api: integer expected";
        if (message.uuid != null)
            if (!$util.isString(message.uuid))
                return "uuid: string expected";
        if (message.redvoxId != null)
            if (!$util.isString(message.redvoxId))
                return "redvoxId: string expected";
        if (message.authenticatedEmail != null)
            if (!$util.isString(message.authenticatedEmail))
                return "authenticatedEmail: string expected";
        if (message.authenticationToken != null)
            if (!$util.isString(message.authenticationToken))
                return "authenticationToken: string expected";
        if (message.firebaseToken != null)
            if (!$util.isString(message.firebaseToken))
                return "firebaseToken: string expected";
        if (message.isBackfilled != null)
            if (typeof message.isBackfilled !== "boolean")
                return "isBackfilled: boolean expected";
        if (message.isPrivate != null)
            if (typeof message.isPrivate !== "boolean")
                return "isPrivate: boolean expected";
        if (message.isScrambled != null)
            if (typeof message.isScrambled !== "boolean")
                return "isScrambled: boolean expected";
        if (message.deviceMake != null)
            if (!$util.isString(message.deviceMake))
                return "deviceMake: string expected";
        if (message.deviceModel != null)
            if (!$util.isString(message.deviceModel))
                return "deviceModel: string expected";
        if (message.deviceOs != null)
            if (!$util.isString(message.deviceOs))
                return "deviceOs: string expected";
        if (message.deviceOsVersion != null)
            if (!$util.isString(message.deviceOsVersion))
                return "deviceOsVersion: string expected";
        if (message.appVersion != null)
            if (!$util.isString(message.appVersion))
                return "appVersion: string expected";
        if (message.batteryLevelPercent != null)
            if (typeof message.batteryLevelPercent !== "number")
                return "batteryLevelPercent: number expected";
        if (message.deviceTemperatureC != null)
            if (typeof message.deviceTemperatureC !== "number")
                return "deviceTemperatureC: number expected";
        if (message.acquisitionServer != null)
            if (!$util.isString(message.acquisitionServer))
                return "acquisitionServer: string expected";
        if (message.timeSynchronizationServer != null)
            if (!$util.isString(message.timeSynchronizationServer))
                return "timeSynchronizationServer: string expected";
        if (message.authenticationServer != null)
            if (!$util.isString(message.authenticationServer))
                return "authenticationServer: string expected";
        if (message.appFileStartTimestampEpochMicrosecondsUtc != null)
            if (!$util.isInteger(message.appFileStartTimestampEpochMicrosecondsUtc) && !(message.appFileStartTimestampEpochMicrosecondsUtc && $util.isInteger(message.appFileStartTimestampEpochMicrosecondsUtc.low) && $util.isInteger(message.appFileStartTimestampEpochMicrosecondsUtc.high)))
                return "appFileStartTimestampEpochMicrosecondsUtc: integer|Long expected";
        if (message.appFileStartTimestampMachine != null)
            if (!$util.isInteger(message.appFileStartTimestampMachine) && !(message.appFileStartTimestampMachine && $util.isInteger(message.appFileStartTimestampMachine.low) && $util.isInteger(message.appFileStartTimestampMachine.high)))
                return "appFileStartTimestampMachine: integer|Long expected";
        if (message.serverTimestampEpochMicrosecondsUtc != null)
            if (!$util.isInteger(message.serverTimestampEpochMicrosecondsUtc) && !(message.serverTimestampEpochMicrosecondsUtc && $util.isInteger(message.serverTimestampEpochMicrosecondsUtc.low) && $util.isInteger(message.serverTimestampEpochMicrosecondsUtc.high)))
                return "serverTimestampEpochMicrosecondsUtc: integer|Long expected";
        if (message.evenlySampledChannels != null) {
            if (!Array.isArray(message.evenlySampledChannels))
                return "evenlySampledChannels: array expected";
            for (var i = 0; i < message.evenlySampledChannels.length; ++i) {
                var error = $root.EvenlySampledChannel.verify(message.evenlySampledChannels[i]);
                if (error)
                    return "evenlySampledChannels." + error;
            }
        }
        if (message.unevenlySampledChannels != null) {
            if (!Array.isArray(message.unevenlySampledChannels))
                return "unevenlySampledChannels: array expected";
            for (var i = 0; i < message.unevenlySampledChannels.length; ++i) {
                var error = $root.UnevenlySampledChannel.verify(message.unevenlySampledChannels[i]);
                if (error)
                    return "unevenlySampledChannels." + error;
            }
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates a RedvoxPacket message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacket} RedvoxPacket
     */
    RedvoxPacket.fromObject = function fromObject(object) {
        if (object instanceof $root.RedvoxPacket)
            return object;
        var message = new $root.RedvoxPacket();
        if (object.api != null)
            message.api = object.api >>> 0;
        if (object.uuid != null)
            message.uuid = String(object.uuid);
        if (object.redvoxId != null)
            message.redvoxId = String(object.redvoxId);
        if (object.authenticatedEmail != null)
            message.authenticatedEmail = String(object.authenticatedEmail);
        if (object.authenticationToken != null)
            message.authenticationToken = String(object.authenticationToken);
        if (object.firebaseToken != null)
            message.firebaseToken = String(object.firebaseToken);
        if (object.isBackfilled != null)
            message.isBackfilled = Boolean(object.isBackfilled);
        if (object.isPrivate != null)
            message.isPrivate = Boolean(object.isPrivate);
        if (object.isScrambled != null)
            message.isScrambled = Boolean(object.isScrambled);
        if (object.deviceMake != null)
            message.deviceMake = String(object.deviceMake);
        if (object.deviceModel != null)
            message.deviceModel = String(object.deviceModel);
        if (object.deviceOs != null)
            message.deviceOs = String(object.deviceOs);
        if (object.deviceOsVersion != null)
            message.deviceOsVersion = String(object.deviceOsVersion);
        if (object.appVersion != null)
            message.appVersion = String(object.appVersion);
        if (object.batteryLevelPercent != null)
            message.batteryLevelPercent = Number(object.batteryLevelPercent);
        if (object.deviceTemperatureC != null)
            message.deviceTemperatureC = Number(object.deviceTemperatureC);
        if (object.acquisitionServer != null)
            message.acquisitionServer = String(object.acquisitionServer);
        if (object.timeSynchronizationServer != null)
            message.timeSynchronizationServer = String(object.timeSynchronizationServer);
        if (object.authenticationServer != null)
            message.authenticationServer = String(object.authenticationServer);
        if (object.appFileStartTimestampEpochMicrosecondsUtc != null)
            if ($util.Long)
                (message.appFileStartTimestampEpochMicrosecondsUtc = $util.Long.fromValue(object.appFileStartTimestampEpochMicrosecondsUtc)).unsigned = false;
            else if (typeof object.appFileStartTimestampEpochMicrosecondsUtc === "string")
                message.appFileStartTimestampEpochMicrosecondsUtc = parseInt(object.appFileStartTimestampEpochMicrosecondsUtc, 10);
            else if (typeof object.appFileStartTimestampEpochMicrosecondsUtc === "number")
                message.appFileStartTimestampEpochMicrosecondsUtc = object.appFileStartTimestampEpochMicrosecondsUtc;
            else if (typeof object.appFileStartTimestampEpochMicrosecondsUtc === "object")
                message.appFileStartTimestampEpochMicrosecondsUtc = new $util.LongBits(object.appFileStartTimestampEpochMicrosecondsUtc.low >>> 0, object.appFileStartTimestampEpochMicrosecondsUtc.high >>> 0).toNumber();
        if (object.appFileStartTimestampMachine != null)
            if ($util.Long)
                (message.appFileStartTimestampMachine = $util.Long.fromValue(object.appFileStartTimestampMachine)).unsigned = false;
            else if (typeof object.appFileStartTimestampMachine === "string")
                message.appFileStartTimestampMachine = parseInt(object.appFileStartTimestampMachine, 10);
            else if (typeof object.appFileStartTimestampMachine === "number")
                message.appFileStartTimestampMachine = object.appFileStartTimestampMachine;
            else if (typeof object.appFileStartTimestampMachine === "object")
                message.appFileStartTimestampMachine = new $util.LongBits(object.appFileStartTimestampMachine.low >>> 0, object.appFileStartTimestampMachine.high >>> 0).toNumber();
        if (object.serverTimestampEpochMicrosecondsUtc != null)
            if ($util.Long)
                (message.serverTimestampEpochMicrosecondsUtc = $util.Long.fromValue(object.serverTimestampEpochMicrosecondsUtc)).unsigned = false;
            else if (typeof object.serverTimestampEpochMicrosecondsUtc === "string")
                message.serverTimestampEpochMicrosecondsUtc = parseInt(object.serverTimestampEpochMicrosecondsUtc, 10);
            else if (typeof object.serverTimestampEpochMicrosecondsUtc === "number")
                message.serverTimestampEpochMicrosecondsUtc = object.serverTimestampEpochMicrosecondsUtc;
            else if (typeof object.serverTimestampEpochMicrosecondsUtc === "object")
                message.serverTimestampEpochMicrosecondsUtc = new $util.LongBits(object.serverTimestampEpochMicrosecondsUtc.low >>> 0, object.serverTimestampEpochMicrosecondsUtc.high >>> 0).toNumber();
        if (object.evenlySampledChannels) {
            if (!Array.isArray(object.evenlySampledChannels))
                throw TypeError(".RedvoxPacket.evenlySampledChannels: array expected");
            message.evenlySampledChannels = [];
            for (var i = 0; i < object.evenlySampledChannels.length; ++i) {
                if (typeof object.evenlySampledChannels[i] !== "object")
                    throw TypeError(".RedvoxPacket.evenlySampledChannels: object expected");
                message.evenlySampledChannels[i] = $root.EvenlySampledChannel.fromObject(object.evenlySampledChannels[i]);
            }
        }
        if (object.unevenlySampledChannels) {
            if (!Array.isArray(object.unevenlySampledChannels))
                throw TypeError(".RedvoxPacket.unevenlySampledChannels: array expected");
            message.unevenlySampledChannels = [];
            for (var i = 0; i < object.unevenlySampledChannels.length; ++i) {
                if (typeof object.unevenlySampledChannels[i] !== "object")
                    throw TypeError(".RedvoxPacket.unevenlySampledChannels: object expected");
                message.unevenlySampledChannels[i] = $root.UnevenlySampledChannel.fromObject(object.unevenlySampledChannels[i]);
            }
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".RedvoxPacket.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates a RedvoxPacket message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link RedvoxPacket.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacket} RedvoxPacket
     */
    RedvoxPacket.from = RedvoxPacket.fromObject;

    /**
     * Creates a plain object from a RedvoxPacket message. Also converts values to other types if specified.
     * @param {RedvoxPacket} message RedvoxPacket
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacket.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.evenlySampledChannels = [];
            object.unevenlySampledChannels = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.api = 0;
            object.uuid = "";
            object.redvoxId = "";
            object.authenticatedEmail = "";
            object.authenticationToken = "";
            object.isBackfilled = false;
            object.isPrivate = false;
            object.isScrambled = false;
            object.deviceMake = "";
            object.deviceModel = "";
            object.deviceOs = "";
            object.deviceOsVersion = "";
            object.appVersion = "";
            object.acquisitionServer = "";
            object.timeSynchronizationServer = "";
            object.authenticationServer = "";
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? "0" : 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.appFileStartTimestampMachine = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.appFileStartTimestampMachine = options.longs === String ? "0" : 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? "0" : 0;
            object.firebaseToken = "";
            object.batteryLevelPercent = 0;
            object.deviceTemperatureC = 0;
        }
        if (message.api != null && message.hasOwnProperty("api"))
            object.api = message.api;
        if (message.uuid != null && message.hasOwnProperty("uuid"))
            object.uuid = message.uuid;
        if (message.redvoxId != null && message.hasOwnProperty("redvoxId"))
            object.redvoxId = message.redvoxId;
        if (message.authenticatedEmail != null && message.hasOwnProperty("authenticatedEmail"))
            object.authenticatedEmail = message.authenticatedEmail;
        if (message.authenticationToken != null && message.hasOwnProperty("authenticationToken"))
            object.authenticationToken = message.authenticationToken;
        if (message.isBackfilled != null && message.hasOwnProperty("isBackfilled"))
            object.isBackfilled = message.isBackfilled;
        if (message.isPrivate != null && message.hasOwnProperty("isPrivate"))
            object.isPrivate = message.isPrivate;
        if (message.isScrambled != null && message.hasOwnProperty("isScrambled"))
            object.isScrambled = message.isScrambled;
        if (message.deviceMake != null && message.hasOwnProperty("deviceMake"))
            object.deviceMake = message.deviceMake;
        if (message.deviceModel != null && message.hasOwnProperty("deviceModel"))
            object.deviceModel = message.deviceModel;
        if (message.deviceOs != null && message.hasOwnProperty("deviceOs"))
            object.deviceOs = message.deviceOs;
        if (message.deviceOsVersion != null && message.hasOwnProperty("deviceOsVersion"))
            object.deviceOsVersion = message.deviceOsVersion;
        if (message.appVersion != null && message.hasOwnProperty("appVersion"))
            object.appVersion = message.appVersion;
        if (message.acquisitionServer != null && message.hasOwnProperty("acquisitionServer"))
            object.acquisitionServer = message.acquisitionServer;
        if (message.timeSynchronizationServer != null && message.hasOwnProperty("timeSynchronizationServer"))
            object.timeSynchronizationServer = message.timeSynchronizationServer;
        if (message.authenticationServer != null && message.hasOwnProperty("authenticationServer"))
            object.authenticationServer = message.authenticationServer;
        if (message.appFileStartTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("appFileStartTimestampEpochMicrosecondsUtc"))
            if (typeof message.appFileStartTimestampEpochMicrosecondsUtc === "number")
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? String(message.appFileStartTimestampEpochMicrosecondsUtc) : message.appFileStartTimestampEpochMicrosecondsUtc;
            else
                object.appFileStartTimestampEpochMicrosecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.appFileStartTimestampEpochMicrosecondsUtc) : options.longs === Number ? new $util.LongBits(message.appFileStartTimestampEpochMicrosecondsUtc.low >>> 0, message.appFileStartTimestampEpochMicrosecondsUtc.high >>> 0).toNumber() : message.appFileStartTimestampEpochMicrosecondsUtc;
        if (message.appFileStartTimestampMachine != null && message.hasOwnProperty("appFileStartTimestampMachine"))
            if (typeof message.appFileStartTimestampMachine === "number")
                object.appFileStartTimestampMachine = options.longs === String ? String(message.appFileStartTimestampMachine) : message.appFileStartTimestampMachine;
            else
                object.appFileStartTimestampMachine = options.longs === String ? $util.Long.prototype.toString.call(message.appFileStartTimestampMachine) : options.longs === Number ? new $util.LongBits(message.appFileStartTimestampMachine.low >>> 0, message.appFileStartTimestampMachine.high >>> 0).toNumber() : message.appFileStartTimestampMachine;
        if (message.serverTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("serverTimestampEpochMicrosecondsUtc"))
            if (typeof message.serverTimestampEpochMicrosecondsUtc === "number")
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? String(message.serverTimestampEpochMicrosecondsUtc) : message.serverTimestampEpochMicrosecondsUtc;
            else
                object.serverTimestampEpochMicrosecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.serverTimestampEpochMicrosecondsUtc) : options.longs === Number ? new $util.LongBits(message.serverTimestampEpochMicrosecondsUtc.low >>> 0, message.serverTimestampEpochMicrosecondsUtc.high >>> 0).toNumber() : message.serverTimestampEpochMicrosecondsUtc;
        if (message.evenlySampledChannels && message.evenlySampledChannels.length) {
            object.evenlySampledChannels = [];
            for (var j = 0; j < message.evenlySampledChannels.length; ++j)
                object.evenlySampledChannels[j] = $root.EvenlySampledChannel.toObject(message.evenlySampledChannels[j], options);
        }
        if (message.unevenlySampledChannels && message.unevenlySampledChannels.length) {
            object.unevenlySampledChannels = [];
            for (var j = 0; j < message.unevenlySampledChannels.length; ++j)
                object.unevenlySampledChannels[j] = $root.UnevenlySampledChannel.toObject(message.unevenlySampledChannels[j], options);
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        if (message.firebaseToken != null && message.hasOwnProperty("firebaseToken"))
            object.firebaseToken = message.firebaseToken;
        if (message.batteryLevelPercent != null && message.hasOwnProperty("batteryLevelPercent"))
            object.batteryLevelPercent = message.batteryLevelPercent;
        if (message.deviceTemperatureC != null && message.hasOwnProperty("deviceTemperatureC"))
            object.deviceTemperatureC = message.deviceTemperatureC;
        return object;
    };

    /**
     * Creates a plain object from this RedvoxPacket message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacket.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this RedvoxPacket to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    RedvoxPacket.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return RedvoxPacket;
})();

$root.Int32Payload = (function() {

    /**
     * Properties of an Int32Payload.
     * @typedef Int32Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] Int32Payload payload.
     */

    /**
     * Constructs a new Int32Payload.
     * @exports Int32Payload
     * @constructor
     * @param {Int32Payload$Properties=} [properties] Properties to set
     */
    function Int32Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Int32Payload payload.
     * @type {Array.<number>|undefined}
     */
    Int32Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Int32Payload instance using the specified properties.
     * @param {Int32Payload$Properties=} [properties] Properties to set
     * @returns {Int32Payload} Int32Payload instance
     */
    Int32Payload.create = function create(properties) {
        return new Int32Payload(properties);
    };

    /**
     * Encodes the specified Int32Payload message. Does not implicitly {@link Int32Payload.verify|verify} messages.
     * @param {Int32Payload$Properties} message Int32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int32Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.int32(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Int32Payload message, length delimited. Does not implicitly {@link Int32Payload.verify|verify} messages.
     * @param {Int32Payload$Properties} message Int32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int32Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an Int32Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Int32Payload} Int32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int32Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Int32Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.int32());
                } else
                    message.payload.push(reader.int32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an Int32Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Int32Payload} Int32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int32Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an Int32Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Int32Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]))
                    return "payload: integer[] expected";
        }
        return null;
    };

    /**
     * Creates an Int32Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Int32Payload} Int32Payload
     */
    Int32Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Int32Payload)
            return object;
        var message = new $root.Int32Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Int32Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = object.payload[i] | 0;
        }
        return message;
    };

    /**
     * Creates an Int32Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Int32Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Int32Payload} Int32Payload
     */
    Int32Payload.from = Int32Payload.fromObject;

    /**
     * Creates a plain object from an Int32Payload message. Also converts values to other types if specified.
     * @param {Int32Payload} message Int32Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int32Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Int32Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int32Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Int32Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Int32Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Int32Payload;
})();

$root.UInt32Payload = (function() {

    /**
     * Properties of a UInt32Payload.
     * @typedef UInt32Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] UInt32Payload payload.
     */

    /**
     * Constructs a new UInt32Payload.
     * @exports UInt32Payload
     * @constructor
     * @param {UInt32Payload$Properties=} [properties] Properties to set
     */
    function UInt32Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * UInt32Payload payload.
     * @type {Array.<number>|undefined}
     */
    UInt32Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new UInt32Payload instance using the specified properties.
     * @param {UInt32Payload$Properties=} [properties] Properties to set
     * @returns {UInt32Payload} UInt32Payload instance
     */
    UInt32Payload.create = function create(properties) {
        return new UInt32Payload(properties);
    };

    /**
     * Encodes the specified UInt32Payload message. Does not implicitly {@link UInt32Payload.verify|verify} messages.
     * @param {UInt32Payload$Properties} message UInt32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt32Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.uint32(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified UInt32Payload message, length delimited. Does not implicitly {@link UInt32Payload.verify|verify} messages.
     * @param {UInt32Payload$Properties} message UInt32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt32Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a UInt32Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {UInt32Payload} UInt32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt32Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.UInt32Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.uint32());
                } else
                    message.payload.push(reader.uint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a UInt32Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {UInt32Payload} UInt32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt32Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a UInt32Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    UInt32Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]))
                    return "payload: integer[] expected";
        }
        return null;
    };

    /**
     * Creates a UInt32Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt32Payload} UInt32Payload
     */
    UInt32Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.UInt32Payload)
            return object;
        var message = new $root.UInt32Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".UInt32Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = object.payload[i] >>> 0;
        }
        return message;
    };

    /**
     * Creates a UInt32Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link UInt32Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt32Payload} UInt32Payload
     */
    UInt32Payload.from = UInt32Payload.fromObject;

    /**
     * Creates a plain object from a UInt32Payload message. Also converts values to other types if specified.
     * @param {UInt32Payload} message UInt32Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt32Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this UInt32Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt32Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this UInt32Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    UInt32Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return UInt32Payload;
})();

$root.Int64Payload = (function() {

    /**
     * Properties of an Int64Payload.
     * @typedef Int64Payload$Properties
     * @type {Object}
     * @property {Array.<number|Long>} [payload] Int64Payload payload.
     */

    /**
     * Constructs a new Int64Payload.
     * @exports Int64Payload
     * @constructor
     * @param {Int64Payload$Properties=} [properties] Properties to set
     */
    function Int64Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Int64Payload payload.
     * @type {Array.<number|Long>|undefined}
     */
    Int64Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Int64Payload instance using the specified properties.
     * @param {Int64Payload$Properties=} [properties] Properties to set
     * @returns {Int64Payload} Int64Payload instance
     */
    Int64Payload.create = function create(properties) {
        return new Int64Payload(properties);
    };

    /**
     * Encodes the specified Int64Payload message. Does not implicitly {@link Int64Payload.verify|verify} messages.
     * @param {Int64Payload$Properties} message Int64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int64Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.int64(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Int64Payload message, length delimited. Does not implicitly {@link Int64Payload.verify|verify} messages.
     * @param {Int64Payload$Properties} message Int64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Int64Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an Int64Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Int64Payload} Int64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int64Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Int64Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.int64());
                } else
                    message.payload.push(reader.int64());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an Int64Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Int64Payload} Int64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Int64Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an Int64Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Int64Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]) && !(message.payload[i] && $util.isInteger(message.payload[i].low) && $util.isInteger(message.payload[i].high)))
                    return "payload: integer|Long[] expected";
        }
        return null;
    };

    /**
     * Creates an Int64Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Int64Payload} Int64Payload
     */
    Int64Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Int64Payload)
            return object;
        var message = new $root.Int64Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Int64Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                if ($util.Long)
                    (message.payload[i] = $util.Long.fromValue(object.payload[i])).unsigned = false;
                else if (typeof object.payload[i] === "string")
                    message.payload[i] = parseInt(object.payload[i], 10);
                else if (typeof object.payload[i] === "number")
                    message.payload[i] = object.payload[i];
                else if (typeof object.payload[i] === "object")
                    message.payload[i] = new $util.LongBits(object.payload[i].low >>> 0, object.payload[i].high >>> 0).toNumber();
        }
        return message;
    };

    /**
     * Creates an Int64Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Int64Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Int64Payload} Int64Payload
     */
    Int64Payload.from = Int64Payload.fromObject;

    /**
     * Creates a plain object from an Int64Payload message. Also converts values to other types if specified.
     * @param {Int64Payload} message Int64Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int64Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                if (typeof message.payload[j] === "number")
                    object.payload[j] = options.longs === String ? String(message.payload[j]) : message.payload[j];
                else
                    object.payload[j] = options.longs === String ? $util.Long.prototype.toString.call(message.payload[j]) : options.longs === Number ? new $util.LongBits(message.payload[j].low >>> 0, message.payload[j].high >>> 0).toNumber() : message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Int64Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Int64Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Int64Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Int64Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Int64Payload;
})();

$root.UInt64Payload = (function() {

    /**
     * Properties of a UInt64Payload.
     * @typedef UInt64Payload$Properties
     * @type {Object}
     * @property {Array.<number|Long>} [payload] UInt64Payload payload.
     */

    /**
     * Constructs a new UInt64Payload.
     * @exports UInt64Payload
     * @constructor
     * @param {UInt64Payload$Properties=} [properties] Properties to set
     */
    function UInt64Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * UInt64Payload payload.
     * @type {Array.<number|Long>|undefined}
     */
    UInt64Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new UInt64Payload instance using the specified properties.
     * @param {UInt64Payload$Properties=} [properties] Properties to set
     * @returns {UInt64Payload} UInt64Payload instance
     */
    UInt64Payload.create = function create(properties) {
        return new UInt64Payload(properties);
    };

    /**
     * Encodes the specified UInt64Payload message. Does not implicitly {@link UInt64Payload.verify|verify} messages.
     * @param {UInt64Payload$Properties} message UInt64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt64Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.uint64(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified UInt64Payload message, length delimited. Does not implicitly {@link UInt64Payload.verify|verify} messages.
     * @param {UInt64Payload$Properties} message UInt64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UInt64Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a UInt64Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {UInt64Payload} UInt64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt64Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.UInt64Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.uint64());
                } else
                    message.payload.push(reader.uint64());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a UInt64Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {UInt64Payload} UInt64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UInt64Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a UInt64Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    UInt64Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (!$util.isInteger(message.payload[i]) && !(message.payload[i] && $util.isInteger(message.payload[i].low) && $util.isInteger(message.payload[i].high)))
                    return "payload: integer|Long[] expected";
        }
        return null;
    };

    /**
     * Creates a UInt64Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt64Payload} UInt64Payload
     */
    UInt64Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.UInt64Payload)
            return object;
        var message = new $root.UInt64Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".UInt64Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                if ($util.Long)
                    (message.payload[i] = $util.Long.fromValue(object.payload[i])).unsigned = true;
                else if (typeof object.payload[i] === "string")
                    message.payload[i] = parseInt(object.payload[i], 10);
                else if (typeof object.payload[i] === "number")
                    message.payload[i] = object.payload[i];
                else if (typeof object.payload[i] === "object")
                    message.payload[i] = new $util.LongBits(object.payload[i].low >>> 0, object.payload[i].high >>> 0).toNumber(true);
        }
        return message;
    };

    /**
     * Creates a UInt64Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link UInt64Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {UInt64Payload} UInt64Payload
     */
    UInt64Payload.from = UInt64Payload.fromObject;

    /**
     * Creates a plain object from a UInt64Payload message. Also converts values to other types if specified.
     * @param {UInt64Payload} message UInt64Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt64Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                if (typeof message.payload[j] === "number")
                    object.payload[j] = options.longs === String ? String(message.payload[j]) : message.payload[j];
                else
                    object.payload[j] = options.longs === String ? $util.Long.prototype.toString.call(message.payload[j]) : options.longs === Number ? new $util.LongBits(message.payload[j].low >>> 0, message.payload[j].high >>> 0).toNumber(true) : message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this UInt64Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UInt64Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this UInt64Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    UInt64Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return UInt64Payload;
})();

$root.Float32Payload = (function() {

    /**
     * Properties of a Float32Payload.
     * @typedef Float32Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] Float32Payload payload.
     */

    /**
     * Constructs a new Float32Payload.
     * @exports Float32Payload
     * @constructor
     * @param {Float32Payload$Properties=} [properties] Properties to set
     */
    function Float32Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Float32Payload payload.
     * @type {Array.<number>|undefined}
     */
    Float32Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Float32Payload instance using the specified properties.
     * @param {Float32Payload$Properties=} [properties] Properties to set
     * @returns {Float32Payload} Float32Payload instance
     */
    Float32Payload.create = function create(properties) {
        return new Float32Payload(properties);
    };

    /**
     * Encodes the specified Float32Payload message. Does not implicitly {@link Float32Payload.verify|verify} messages.
     * @param {Float32Payload$Properties} message Float32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float32Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.float(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Float32Payload message, length delimited. Does not implicitly {@link Float32Payload.verify|verify} messages.
     * @param {Float32Payload$Properties} message Float32Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float32Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Float32Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Float32Payload} Float32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float32Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Float32Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.float());
                } else
                    message.payload.push(reader.float());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Float32Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Float32Payload} Float32Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float32Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Float32Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Float32Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (typeof message.payload[i] !== "number")
                    return "payload: number[] expected";
        }
        return null;
    };

    /**
     * Creates a Float32Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Float32Payload} Float32Payload
     */
    Float32Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Float32Payload)
            return object;
        var message = new $root.Float32Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Float32Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = Number(object.payload[i]);
        }
        return message;
    };

    /**
     * Creates a Float32Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Float32Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Float32Payload} Float32Payload
     */
    Float32Payload.from = Float32Payload.fromObject;

    /**
     * Creates a plain object from a Float32Payload message. Also converts values to other types if specified.
     * @param {Float32Payload} message Float32Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float32Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Float32Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float32Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Float32Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Float32Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Float32Payload;
})();

$root.Float64Payload = (function() {

    /**
     * Properties of a Float64Payload.
     * @typedef Float64Payload$Properties
     * @type {Object}
     * @property {Array.<number>} [payload] Float64Payload payload.
     */

    /**
     * Constructs a new Float64Payload.
     * @exports Float64Payload
     * @constructor
     * @param {Float64Payload$Properties=} [properties] Properties to set
     */
    function Float64Payload(properties) {
        this.payload = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * Float64Payload payload.
     * @type {Array.<number>|undefined}
     */
    Float64Payload.prototype.payload = $util.emptyArray;

    /**
     * Creates a new Float64Payload instance using the specified properties.
     * @param {Float64Payload$Properties=} [properties] Properties to set
     * @returns {Float64Payload} Float64Payload instance
     */
    Float64Payload.create = function create(properties) {
        return new Float64Payload(properties);
    };

    /**
     * Encodes the specified Float64Payload message. Does not implicitly {@link Float64Payload.verify|verify} messages.
     * @param {Float64Payload$Properties} message Float64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float64Payload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.payload && message.payload.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.payload.length; ++i)
                writer.double(message.payload[i]);
            writer.ldelim();
        }
        return writer;
    };

    /**
     * Encodes the specified Float64Payload message, length delimited. Does not implicitly {@link Float64Payload.verify|verify} messages.
     * @param {Float64Payload$Properties} message Float64Payload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Float64Payload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Float64Payload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Float64Payload} Float64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float64Payload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Float64Payload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.payload && message.payload.length))
                    message.payload = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.payload.push(reader.double());
                } else
                    message.payload.push(reader.double());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Float64Payload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Float64Payload} Float64Payload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Float64Payload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Float64Payload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    Float64Payload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.payload != null) {
            if (!Array.isArray(message.payload))
                return "payload: array expected";
            for (var i = 0; i < message.payload.length; ++i)
                if (typeof message.payload[i] !== "number")
                    return "payload: number[] expected";
        }
        return null;
    };

    /**
     * Creates a Float64Payload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {Float64Payload} Float64Payload
     */
    Float64Payload.fromObject = function fromObject(object) {
        if (object instanceof $root.Float64Payload)
            return object;
        var message = new $root.Float64Payload();
        if (object.payload) {
            if (!Array.isArray(object.payload))
                throw TypeError(".Float64Payload.payload: array expected");
            message.payload = [];
            for (var i = 0; i < object.payload.length; ++i)
                message.payload[i] = Number(object.payload[i]);
        }
        return message;
    };

    /**
     * Creates a Float64Payload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link Float64Payload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {Float64Payload} Float64Payload
     */
    Float64Payload.from = Float64Payload.fromObject;

    /**
     * Creates a plain object from a Float64Payload message. Also converts values to other types if specified.
     * @param {Float64Payload} message Float64Payload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float64Payload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.payload = [];
        if (message.payload && message.payload.length) {
            object.payload = [];
            for (var j = 0; j < message.payload.length; ++j)
                object.payload[j] = message.payload[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this Float64Payload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Float64Payload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this Float64Payload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    Float64Payload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Float64Payload;
})();

$root.BytePayload = (function() {

    /**
     * Properties of a BytePayload.
     * @typedef BytePayload$Properties
     * @type {Object}
     * @property {BytePayload.BytePayloadType} [bytePayloadType] BytePayload bytePayloadType.
     * @property {Uint8Array} [payload] BytePayload payload.
     */

    /**
     * Constructs a new BytePayload.
     * @exports BytePayload
     * @constructor
     * @param {BytePayload$Properties=} [properties] Properties to set
     */
    function BytePayload(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * BytePayload bytePayloadType.
     * @type {BytePayload.BytePayloadType|undefined}
     */
    BytePayload.prototype.bytePayloadType = 0;

    /**
     * BytePayload payload.
     * @type {Uint8Array|undefined}
     */
    BytePayload.prototype.payload = $util.newBuffer([]);

    /**
     * Creates a new BytePayload instance using the specified properties.
     * @param {BytePayload$Properties=} [properties] Properties to set
     * @returns {BytePayload} BytePayload instance
     */
    BytePayload.create = function create(properties) {
        return new BytePayload(properties);
    };

    /**
     * Encodes the specified BytePayload message. Does not implicitly {@link BytePayload.verify|verify} messages.
     * @param {BytePayload$Properties} message BytePayload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    BytePayload.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.bytePayloadType != null && message.hasOwnProperty("bytePayloadType"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.bytePayloadType);
        if (message.payload && message.hasOwnProperty("payload"))
            writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.payload);
        return writer;
    };

    /**
     * Encodes the specified BytePayload message, length delimited. Does not implicitly {@link BytePayload.verify|verify} messages.
     * @param {BytePayload$Properties} message BytePayload message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    BytePayload.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a BytePayload message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {BytePayload} BytePayload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    BytePayload.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.BytePayload();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.bytePayloadType = reader.uint32();
                break;
            case 2:
                message.payload = reader.bytes();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a BytePayload message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {BytePayload} BytePayload
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    BytePayload.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a BytePayload message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    BytePayload.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.bytePayloadType != null)
            switch (message.bytePayloadType) {
            default:
                return "bytePayloadType: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            }
        if (message.payload != null)
            if (!(message.payload && typeof message.payload.length === "number" || $util.isString(message.payload)))
                return "payload: buffer expected";
        return null;
    };

    /**
     * Creates a BytePayload message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {BytePayload} BytePayload
     */
    BytePayload.fromObject = function fromObject(object) {
        if (object instanceof $root.BytePayload)
            return object;
        var message = new $root.BytePayload();
        switch (object.bytePayloadType) {
        case "BYTES":
        case 0:
            message.bytePayloadType = 0;
            break;
        case "UINT8":
        case 1:
            message.bytePayloadType = 1;
            break;
        case "UNINT16":
        case 2:
            message.bytePayloadType = 2;
            break;
        case "UNINT24":
        case 3:
            message.bytePayloadType = 3;
            break;
        case "UINT32":
        case 4:
            message.bytePayloadType = 4;
            break;
        case "UINT64":
        case 5:
            message.bytePayloadType = 5;
            break;
        case "INT8":
        case 6:
            message.bytePayloadType = 6;
            break;
        case "INT16":
        case 7:
            message.bytePayloadType = 7;
            break;
        case "INT24":
        case 8:
            message.bytePayloadType = 8;
            break;
        case "INT32":
        case 9:
            message.bytePayloadType = 9;
            break;
        case "INT64":
        case 10:
            message.bytePayloadType = 10;
            break;
        case "FLOAT32":
        case 11:
            message.bytePayloadType = 11;
            break;
        case "FLOAT64":
        case 12:
            message.bytePayloadType = 12;
            break;
        case "OTHER":
        case 13:
            message.bytePayloadType = 13;
            break;
        }
        if (object.payload != null)
            if (typeof object.payload === "string")
                $util.base64.decode(object.payload, message.payload = $util.newBuffer($util.base64.length(object.payload)), 0);
            else if (object.payload.length)
                message.payload = object.payload;
        return message;
    };

    /**
     * Creates a BytePayload message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link BytePayload.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {BytePayload} BytePayload
     */
    BytePayload.from = BytePayload.fromObject;

    /**
     * Creates a plain object from a BytePayload message. Also converts values to other types if specified.
     * @param {BytePayload} message BytePayload
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    BytePayload.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.bytePayloadType = options.enums === String ? "BYTES" : 0;
            object.payload = options.bytes === String ? "" : [];
        }
        if (message.bytePayloadType != null && message.hasOwnProperty("bytePayloadType"))
            object.bytePayloadType = options.enums === String ? $root.BytePayload.BytePayloadType[message.bytePayloadType] : message.bytePayloadType;
        if (message.payload != null && message.hasOwnProperty("payload"))
            object.payload = options.bytes === String ? $util.base64.encode(message.payload, 0, message.payload.length) : options.bytes === Array ? Array.prototype.slice.call(message.payload) : message.payload;
        return object;
    };

    /**
     * Creates a plain object from this BytePayload message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    BytePayload.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this BytePayload to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    BytePayload.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    /**
     * BytePayloadType enum.
     * @name BytePayloadType
     * @memberof BytePayload
     * @enum {number}
     * @property {number} BYTES=0 BYTES value
     * @property {number} UINT8=1 UINT8 value
     * @property {number} UNINT16=2 UNINT16 value
     * @property {number} UNINT24=3 UNINT24 value
     * @property {number} UINT32=4 UINT32 value
     * @property {number} UINT64=5 UINT64 value
     * @property {number} INT8=6 INT8 value
     * @property {number} INT16=7 INT16 value
     * @property {number} INT24=8 INT24 value
     * @property {number} INT32=9 INT32 value
     * @property {number} INT64=10 INT64 value
     * @property {number} FLOAT32=11 FLOAT32 value
     * @property {number} FLOAT64=12 FLOAT64 value
     * @property {number} OTHER=13 OTHER value
     */
    BytePayload.BytePayloadType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "BYTES"] = 0;
        values[valuesById[1] = "UINT8"] = 1;
        values[valuesById[2] = "UNINT16"] = 2;
        values[valuesById[3] = "UNINT24"] = 3;
        values[valuesById[4] = "UINT32"] = 4;
        values[valuesById[5] = "UINT64"] = 5;
        values[valuesById[6] = "INT8"] = 6;
        values[valuesById[7] = "INT16"] = 7;
        values[valuesById[8] = "INT24"] = 8;
        values[valuesById[9] = "INT32"] = 9;
        values[valuesById[10] = "INT64"] = 10;
        values[valuesById[11] = "FLOAT32"] = 11;
        values[valuesById[12] = "FLOAT64"] = 12;
        values[valuesById[13] = "OTHER"] = 13;
        return values;
    })();

    return BytePayload;
})();

/**
 * ChannelType enum.
 * @exports ChannelType
 * @enum {number}
 * @property {number} MICROPHONE=0 MICROPHONE value
 * @property {number} BAROMETER=1 BAROMETER value
 * @property {number} LATITUDE=2 LATITUDE value
 * @property {number} LONGITUDE=3 LONGITUDE value
 * @property {number} SPEED=4 SPEED value
 * @property {number} ALTITUDE=5 ALTITUDE value
 * @property {number} RESERVED_0=6 RESERVED_0 value
 * @property {number} RESERVED_1=7 RESERVED_1 value
 * @property {number} RESERVED_2=8 RESERVED_2 value
 * @property {number} TIME_SYNCHRONIZATION=9 TIME_SYNCHRONIZATION value
 * @property {number} ACCURACY=10 ACCURACY value
 * @property {number} ACCELEROMETER_X=11 ACCELEROMETER_X value
 * @property {number} ACCELEROMETER_Y=12 ACCELEROMETER_Y value
 * @property {number} ACCELEROMETER_Z=13 ACCELEROMETER_Z value
 * @property {number} MAGNETOMETER_X=14 MAGNETOMETER_X value
 * @property {number} MAGNETOMETER_Y=15 MAGNETOMETER_Y value
 * @property {number} MAGNETOMETER_Z=16 MAGNETOMETER_Z value
 * @property {number} GYROSCOPE_X=17 GYROSCOPE_X value
 * @property {number} GYROSCOPE_Y=18 GYROSCOPE_Y value
 * @property {number} GYROSCOPE_Z=19 GYROSCOPE_Z value
 * @property {number} OTHER=20 OTHER value
 * @property {number} LIGHT=21 LIGHT value
 * @property {number} IMAGE=22 IMAGE value
 * @property {number} INFRARED=23 INFRARED value
 */
$root.ChannelType = (function() {
    var valuesById = {}, values = Object.create(valuesById);
    values[valuesById[0] = "MICROPHONE"] = 0;
    values[valuesById[1] = "BAROMETER"] = 1;
    values[valuesById[2] = "LATITUDE"] = 2;
    values[valuesById[3] = "LONGITUDE"] = 3;
    values[valuesById[4] = "SPEED"] = 4;
    values[valuesById[5] = "ALTITUDE"] = 5;
    values[valuesById[6] = "RESERVED_0"] = 6;
    values[valuesById[7] = "RESERVED_1"] = 7;
    values[valuesById[8] = "RESERVED_2"] = 8;
    values[valuesById[9] = "TIME_SYNCHRONIZATION"] = 9;
    values[valuesById[10] = "ACCURACY"] = 10;
    values[valuesById[11] = "ACCELEROMETER_X"] = 11;
    values[valuesById[12] = "ACCELEROMETER_Y"] = 12;
    values[valuesById[13] = "ACCELEROMETER_Z"] = 13;
    values[valuesById[14] = "MAGNETOMETER_X"] = 14;
    values[valuesById[15] = "MAGNETOMETER_Y"] = 15;
    values[valuesById[16] = "MAGNETOMETER_Z"] = 16;
    values[valuesById[17] = "GYROSCOPE_X"] = 17;
    values[valuesById[18] = "GYROSCOPE_Y"] = 18;
    values[valuesById[19] = "GYROSCOPE_Z"] = 19;
    values[valuesById[20] = "OTHER"] = 20;
    values[valuesById[21] = "LIGHT"] = 21;
    values[valuesById[22] = "IMAGE"] = 22;
    values[valuesById[23] = "INFRARED"] = 23;
    return values;
})();

$root.EvenlySampledChannel = (function() {

    /**
     * Properties of an EvenlySampledChannel.
     * @typedef EvenlySampledChannel$Properties
     * @type {Object}
     * @property {Array.<ChannelType>} [channelTypes] EvenlySampledChannel channelTypes.
     * @property {string} [sensorName] EvenlySampledChannel sensorName.
     * @property {number} [sampleRateHz] EvenlySampledChannel sampleRateHz.
     * @property {number|Long} [firstSampleTimestampEpochMicrosecondsUtc] EvenlySampledChannel firstSampleTimestampEpochMicrosecondsUtc.
     * @property {BytePayload$Properties} [bytePayload] EvenlySampledChannel bytePayload.
     * @property {UInt32Payload$Properties} [uint32Payload] EvenlySampledChannel uint32Payload.
     * @property {UInt64Payload$Properties} [uint64Payload] EvenlySampledChannel uint64Payload.
     * @property {Int32Payload$Properties} [int32Payload] EvenlySampledChannel int32Payload.
     * @property {Int64Payload$Properties} [int64Payload] EvenlySampledChannel int64Payload.
     * @property {Float32Payload$Properties} [float32Payload] EvenlySampledChannel float32Payload.
     * @property {Float64Payload$Properties} [float64Payload] EvenlySampledChannel float64Payload.
     * @property {Array.<number>} [valueMeans] EvenlySampledChannel valueMeans.
     * @property {Array.<number>} [valueStds] EvenlySampledChannel valueStds.
     * @property {Array.<number>} [valueMedians] EvenlySampledChannel valueMedians.
     * @property {Array.<string>} [metadata] EvenlySampledChannel metadata.
     */

    /**
     * Constructs a new EvenlySampledChannel.
     * @exports EvenlySampledChannel
     * @constructor
     * @param {EvenlySampledChannel$Properties=} [properties] Properties to set
     */
    function EvenlySampledChannel(properties) {
        this.channelTypes = [];
        this.valueMeans = [];
        this.valueStds = [];
        this.valueMedians = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * EvenlySampledChannel channelTypes.
     * @type {Array.<ChannelType>|undefined}
     */
    EvenlySampledChannel.prototype.channelTypes = $util.emptyArray;

    /**
     * EvenlySampledChannel sensorName.
     * @type {string|undefined}
     */
    EvenlySampledChannel.prototype.sensorName = "";

    /**
     * EvenlySampledChannel sampleRateHz.
     * @type {number|undefined}
     */
    EvenlySampledChannel.prototype.sampleRateHz = 0;

    /**
     * EvenlySampledChannel firstSampleTimestampEpochMicrosecondsUtc.
     * @type {number|Long|undefined}
     */
    EvenlySampledChannel.prototype.firstSampleTimestampEpochMicrosecondsUtc = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * EvenlySampledChannel bytePayload.
     * @type {BytePayload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.bytePayload = null;

    /**
     * EvenlySampledChannel uint32Payload.
     * @type {UInt32Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.uint32Payload = null;

    /**
     * EvenlySampledChannel uint64Payload.
     * @type {UInt64Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.uint64Payload = null;

    /**
     * EvenlySampledChannel int32Payload.
     * @type {Int32Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.int32Payload = null;

    /**
     * EvenlySampledChannel int64Payload.
     * @type {Int64Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.int64Payload = null;

    /**
     * EvenlySampledChannel float32Payload.
     * @type {Float32Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.float32Payload = null;

    /**
     * EvenlySampledChannel float64Payload.
     * @type {Float64Payload$Properties|undefined}
     */
    EvenlySampledChannel.prototype.float64Payload = null;

    /**
     * EvenlySampledChannel valueMeans.
     * @type {Array.<number>|undefined}
     */
    EvenlySampledChannel.prototype.valueMeans = $util.emptyArray;

    /**
     * EvenlySampledChannel valueStds.
     * @type {Array.<number>|undefined}
     */
    EvenlySampledChannel.prototype.valueStds = $util.emptyArray;

    /**
     * EvenlySampledChannel valueMedians.
     * @type {Array.<number>|undefined}
     */
    EvenlySampledChannel.prototype.valueMedians = $util.emptyArray;

    /**
     * EvenlySampledChannel metadata.
     * @type {Array.<string>|undefined}
     */
    EvenlySampledChannel.prototype.metadata = $util.emptyArray;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * EvenlySampledChannel payload.
     * @name EvenlySampledChannel#payload
     * @type {string|undefined}
     */
    Object.defineProperty(EvenlySampledChannel.prototype, "payload", {
        get: $util.oneOfGetter($oneOfFields = ["bytePayload", "uint32Payload", "uint64Payload", "int32Payload", "int64Payload", "float32Payload", "float64Payload"]),
        set: $util.oneOfSetter($oneOfFields)
    });

    /**
     * Creates a new EvenlySampledChannel instance using the specified properties.
     * @param {EvenlySampledChannel$Properties=} [properties] Properties to set
     * @returns {EvenlySampledChannel} EvenlySampledChannel instance
     */
    EvenlySampledChannel.create = function create(properties) {
        return new EvenlySampledChannel(properties);
    };

    /**
     * Encodes the specified EvenlySampledChannel message. Does not implicitly {@link EvenlySampledChannel.verify|verify} messages.
     * @param {EvenlySampledChannel$Properties} message EvenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    EvenlySampledChannel.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.channelTypes && message.channelTypes.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.channelTypes.length; ++i)
                writer.uint32(message.channelTypes[i]);
            writer.ldelim();
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.sensorName);
        if (message.sampleRateHz != null && message.hasOwnProperty("sampleRateHz"))
            writer.uint32(/* id 3, wireType 1 =*/25).double(message.sampleRateHz);
        if (message.firstSampleTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("firstSampleTimestampEpochMicrosecondsUtc"))
            writer.uint32(/* id 4, wireType 0 =*/32).int64(message.firstSampleTimestampEpochMicrosecondsUtc);
        if (message.bytePayload && message.hasOwnProperty("bytePayload"))
            $root.BytePayload.encode(message.bytePayload, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
        if (message.uint32Payload && message.hasOwnProperty("uint32Payload"))
            $root.UInt32Payload.encode(message.uint32Payload, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
        if (message.uint64Payload && message.hasOwnProperty("uint64Payload"))
            $root.UInt64Payload.encode(message.uint64Payload, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
        if (message.int32Payload && message.hasOwnProperty("int32Payload"))
            $root.Int32Payload.encode(message.int32Payload, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
        if (message.int64Payload && message.hasOwnProperty("int64Payload"))
            $root.Int64Payload.encode(message.int64Payload, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
        if (message.float32Payload && message.hasOwnProperty("float32Payload"))
            $root.Float32Payload.encode(message.float32Payload, writer.uint32(/* id 10, wireType 2 =*/82).fork()).ldelim();
        if (message.float64Payload && message.hasOwnProperty("float64Payload"))
            $root.Float64Payload.encode(message.float64Payload, writer.uint32(/* id 11, wireType 2 =*/90).fork()).ldelim();
        if (message.valueMeans && message.valueMeans.length) {
            writer.uint32(/* id 12, wireType 2 =*/98).fork();
            for (var i = 0; i < message.valueMeans.length; ++i)
                writer.double(message.valueMeans[i]);
            writer.ldelim();
        }
        if (message.valueStds && message.valueStds.length) {
            writer.uint32(/* id 13, wireType 2 =*/106).fork();
            for (var i = 0; i < message.valueStds.length; ++i)
                writer.double(message.valueStds[i]);
            writer.ldelim();
        }
        if (message.valueMedians && message.valueMedians.length) {
            writer.uint32(/* id 14, wireType 2 =*/114).fork();
            for (var i = 0; i < message.valueMedians.length; ++i)
                writer.double(message.valueMedians[i]);
            writer.ldelim();
        }
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 15, wireType 2 =*/122).string(message.metadata[i]);
        return writer;
    };

    /**
     * Encodes the specified EvenlySampledChannel message, length delimited. Does not implicitly {@link EvenlySampledChannel.verify|verify} messages.
     * @param {EvenlySampledChannel$Properties} message EvenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    EvenlySampledChannel.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an EvenlySampledChannel message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    EvenlySampledChannel.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.EvenlySampledChannel();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.channelTypes && message.channelTypes.length))
                    message.channelTypes = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.channelTypes.push(reader.uint32());
                } else
                    message.channelTypes.push(reader.uint32());
                break;
            case 2:
                message.sensorName = reader.string();
                break;
            case 3:
                message.sampleRateHz = reader.double();
                break;
            case 4:
                message.firstSampleTimestampEpochMicrosecondsUtc = reader.int64();
                break;
            case 5:
                message.bytePayload = $root.BytePayload.decode(reader, reader.uint32());
                break;
            case 6:
                message.uint32Payload = $root.UInt32Payload.decode(reader, reader.uint32());
                break;
            case 7:
                message.uint64Payload = $root.UInt64Payload.decode(reader, reader.uint32());
                break;
            case 8:
                message.int32Payload = $root.Int32Payload.decode(reader, reader.uint32());
                break;
            case 9:
                message.int64Payload = $root.Int64Payload.decode(reader, reader.uint32());
                break;
            case 10:
                message.float32Payload = $root.Float32Payload.decode(reader, reader.uint32());
                break;
            case 11:
                message.float64Payload = $root.Float64Payload.decode(reader, reader.uint32());
                break;
            case 12:
                if (!(message.valueMeans && message.valueMeans.length))
                    message.valueMeans = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMeans.push(reader.double());
                } else
                    message.valueMeans.push(reader.double());
                break;
            case 13:
                if (!(message.valueStds && message.valueStds.length))
                    message.valueStds = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueStds.push(reader.double());
                } else
                    message.valueStds.push(reader.double());
                break;
            case 14:
                if (!(message.valueMedians && message.valueMedians.length))
                    message.valueMedians = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMedians.push(reader.double());
                } else
                    message.valueMedians.push(reader.double());
                break;
            case 15:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an EvenlySampledChannel message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    EvenlySampledChannel.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an EvenlySampledChannel message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    EvenlySampledChannel.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        var properties = {};
        if (message.channelTypes != null) {
            if (!Array.isArray(message.channelTypes))
                return "channelTypes: array expected";
            for (var i = 0; i < message.channelTypes.length; ++i)
                switch (message.channelTypes[i]) {
                default:
                    return "channelTypes: enum value[] expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                    break;
                }
        }
        if (message.sensorName != null)
            if (!$util.isString(message.sensorName))
                return "sensorName: string expected";
        if (message.sampleRateHz != null)
            if (typeof message.sampleRateHz !== "number")
                return "sampleRateHz: number expected";
        if (message.firstSampleTimestampEpochMicrosecondsUtc != null)
            if (!$util.isInteger(message.firstSampleTimestampEpochMicrosecondsUtc) && !(message.firstSampleTimestampEpochMicrosecondsUtc && $util.isInteger(message.firstSampleTimestampEpochMicrosecondsUtc.low) && $util.isInteger(message.firstSampleTimestampEpochMicrosecondsUtc.high)))
                return "firstSampleTimestampEpochMicrosecondsUtc: integer|Long expected";
        if (message.bytePayload != null) {
            properties.payload = 1;
            var error = $root.BytePayload.verify(message.bytePayload);
            if (error)
                return "bytePayload." + error;
        }
        if (message.uint32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt32Payload.verify(message.uint32Payload);
            if (error)
                return "uint32Payload." + error;
        }
        if (message.uint64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt64Payload.verify(message.uint64Payload);
            if (error)
                return "uint64Payload." + error;
        }
        if (message.int32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int32Payload.verify(message.int32Payload);
            if (error)
                return "int32Payload." + error;
        }
        if (message.int64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int64Payload.verify(message.int64Payload);
            if (error)
                return "int64Payload." + error;
        }
        if (message.float32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float32Payload.verify(message.float32Payload);
            if (error)
                return "float32Payload." + error;
        }
        if (message.float64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float64Payload.verify(message.float64Payload);
            if (error)
                return "float64Payload." + error;
        }
        if (message.valueMeans != null) {
            if (!Array.isArray(message.valueMeans))
                return "valueMeans: array expected";
            for (var i = 0; i < message.valueMeans.length; ++i)
                if (typeof message.valueMeans[i] !== "number")
                    return "valueMeans: number[] expected";
        }
        if (message.valueStds != null) {
            if (!Array.isArray(message.valueStds))
                return "valueStds: array expected";
            for (var i = 0; i < message.valueStds.length; ++i)
                if (typeof message.valueStds[i] !== "number")
                    return "valueStds: number[] expected";
        }
        if (message.valueMedians != null) {
            if (!Array.isArray(message.valueMedians))
                return "valueMedians: array expected";
            for (var i = 0; i < message.valueMedians.length; ++i)
                if (typeof message.valueMedians[i] !== "number")
                    return "valueMedians: number[] expected";
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates an EvenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     */
    EvenlySampledChannel.fromObject = function fromObject(object) {
        if (object instanceof $root.EvenlySampledChannel)
            return object;
        var message = new $root.EvenlySampledChannel();
        if (object.channelTypes) {
            if (!Array.isArray(object.channelTypes))
                throw TypeError(".EvenlySampledChannel.channelTypes: array expected");
            message.channelTypes = [];
            for (var i = 0; i < object.channelTypes.length; ++i)
                switch (object.channelTypes[i]) {
                default:
                case "MICROPHONE":
                case 0:
                    message.channelTypes[i] = 0;
                    break;
                case "BAROMETER":
                case 1:
                    message.channelTypes[i] = 1;
                    break;
                case "LATITUDE":
                case 2:
                    message.channelTypes[i] = 2;
                    break;
                case "LONGITUDE":
                case 3:
                    message.channelTypes[i] = 3;
                    break;
                case "SPEED":
                case 4:
                    message.channelTypes[i] = 4;
                    break;
                case "ALTITUDE":
                case 5:
                    message.channelTypes[i] = 5;
                    break;
                case "RESERVED_0":
                case 6:
                    message.channelTypes[i] = 6;
                    break;
                case "RESERVED_1":
                case 7:
                    message.channelTypes[i] = 7;
                    break;
                case "RESERVED_2":
                case 8:
                    message.channelTypes[i] = 8;
                    break;
                case "TIME_SYNCHRONIZATION":
                case 9:
                    message.channelTypes[i] = 9;
                    break;
                case "ACCURACY":
                case 10:
                    message.channelTypes[i] = 10;
                    break;
                case "ACCELEROMETER_X":
                case 11:
                    message.channelTypes[i] = 11;
                    break;
                case "ACCELEROMETER_Y":
                case 12:
                    message.channelTypes[i] = 12;
                    break;
                case "ACCELEROMETER_Z":
                case 13:
                    message.channelTypes[i] = 13;
                    break;
                case "MAGNETOMETER_X":
                case 14:
                    message.channelTypes[i] = 14;
                    break;
                case "MAGNETOMETER_Y":
                case 15:
                    message.channelTypes[i] = 15;
                    break;
                case "MAGNETOMETER_Z":
                case 16:
                    message.channelTypes[i] = 16;
                    break;
                case "GYROSCOPE_X":
                case 17:
                    message.channelTypes[i] = 17;
                    break;
                case "GYROSCOPE_Y":
                case 18:
                    message.channelTypes[i] = 18;
                    break;
                case "GYROSCOPE_Z":
                case 19:
                    message.channelTypes[i] = 19;
                    break;
                case "OTHER":
                case 20:
                    message.channelTypes[i] = 20;
                    break;
                case "LIGHT":
                case 21:
                    message.channelTypes[i] = 21;
                    break;
                case "IMAGE":
                case 22:
                    message.channelTypes[i] = 22;
                    break;
                case "INFRARED":
                case 23:
                    message.channelTypes[i] = 23;
                    break;
                }
        }
        if (object.sensorName != null)
            message.sensorName = String(object.sensorName);
        if (object.sampleRateHz != null)
            message.sampleRateHz = Number(object.sampleRateHz);
        if (object.firstSampleTimestampEpochMicrosecondsUtc != null)
            if ($util.Long)
                (message.firstSampleTimestampEpochMicrosecondsUtc = $util.Long.fromValue(object.firstSampleTimestampEpochMicrosecondsUtc)).unsigned = false;
            else if (typeof object.firstSampleTimestampEpochMicrosecondsUtc === "string")
                message.firstSampleTimestampEpochMicrosecondsUtc = parseInt(object.firstSampleTimestampEpochMicrosecondsUtc, 10);
            else if (typeof object.firstSampleTimestampEpochMicrosecondsUtc === "number")
                message.firstSampleTimestampEpochMicrosecondsUtc = object.firstSampleTimestampEpochMicrosecondsUtc;
            else if (typeof object.firstSampleTimestampEpochMicrosecondsUtc === "object")
                message.firstSampleTimestampEpochMicrosecondsUtc = new $util.LongBits(object.firstSampleTimestampEpochMicrosecondsUtc.low >>> 0, object.firstSampleTimestampEpochMicrosecondsUtc.high >>> 0).toNumber();
        if (object.bytePayload != null) {
            if (typeof object.bytePayload !== "object")
                throw TypeError(".EvenlySampledChannel.bytePayload: object expected");
            message.bytePayload = $root.BytePayload.fromObject(object.bytePayload);
        }
        if (object.uint32Payload != null) {
            if (typeof object.uint32Payload !== "object")
                throw TypeError(".EvenlySampledChannel.uint32Payload: object expected");
            message.uint32Payload = $root.UInt32Payload.fromObject(object.uint32Payload);
        }
        if (object.uint64Payload != null) {
            if (typeof object.uint64Payload !== "object")
                throw TypeError(".EvenlySampledChannel.uint64Payload: object expected");
            message.uint64Payload = $root.UInt64Payload.fromObject(object.uint64Payload);
        }
        if (object.int32Payload != null) {
            if (typeof object.int32Payload !== "object")
                throw TypeError(".EvenlySampledChannel.int32Payload: object expected");
            message.int32Payload = $root.Int32Payload.fromObject(object.int32Payload);
        }
        if (object.int64Payload != null) {
            if (typeof object.int64Payload !== "object")
                throw TypeError(".EvenlySampledChannel.int64Payload: object expected");
            message.int64Payload = $root.Int64Payload.fromObject(object.int64Payload);
        }
        if (object.float32Payload != null) {
            if (typeof object.float32Payload !== "object")
                throw TypeError(".EvenlySampledChannel.float32Payload: object expected");
            message.float32Payload = $root.Float32Payload.fromObject(object.float32Payload);
        }
        if (object.float64Payload != null) {
            if (typeof object.float64Payload !== "object")
                throw TypeError(".EvenlySampledChannel.float64Payload: object expected");
            message.float64Payload = $root.Float64Payload.fromObject(object.float64Payload);
        }
        if (object.valueMeans) {
            if (!Array.isArray(object.valueMeans))
                throw TypeError(".EvenlySampledChannel.valueMeans: array expected");
            message.valueMeans = [];
            for (var i = 0; i < object.valueMeans.length; ++i)
                message.valueMeans[i] = Number(object.valueMeans[i]);
        }
        if (object.valueStds) {
            if (!Array.isArray(object.valueStds))
                throw TypeError(".EvenlySampledChannel.valueStds: array expected");
            message.valueStds = [];
            for (var i = 0; i < object.valueStds.length; ++i)
                message.valueStds[i] = Number(object.valueStds[i]);
        }
        if (object.valueMedians) {
            if (!Array.isArray(object.valueMedians))
                throw TypeError(".EvenlySampledChannel.valueMedians: array expected");
            message.valueMedians = [];
            for (var i = 0; i < object.valueMedians.length; ++i)
                message.valueMedians[i] = Number(object.valueMedians[i]);
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".EvenlySampledChannel.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates an EvenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link EvenlySampledChannel.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {EvenlySampledChannel} EvenlySampledChannel
     */
    EvenlySampledChannel.from = EvenlySampledChannel.fromObject;

    /**
     * Creates a plain object from an EvenlySampledChannel message. Also converts values to other types if specified.
     * @param {EvenlySampledChannel} message EvenlySampledChannel
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    EvenlySampledChannel.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.channelTypes = [];
            object.valueMeans = [];
            object.valueStds = [];
            object.valueMedians = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.sensorName = "";
            object.sampleRateHz = 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? "0" : 0;
        }
        if (message.channelTypes && message.channelTypes.length) {
            object.channelTypes = [];
            for (var j = 0; j < message.channelTypes.length; ++j)
                object.channelTypes[j] = options.enums === String ? $root.ChannelType[message.channelTypes[j]] : message.channelTypes[j];
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            object.sensorName = message.sensorName;
        if (message.sampleRateHz != null && message.hasOwnProperty("sampleRateHz"))
            object.sampleRateHz = message.sampleRateHz;
        if (message.firstSampleTimestampEpochMicrosecondsUtc != null && message.hasOwnProperty("firstSampleTimestampEpochMicrosecondsUtc"))
            if (typeof message.firstSampleTimestampEpochMicrosecondsUtc === "number")
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? String(message.firstSampleTimestampEpochMicrosecondsUtc) : message.firstSampleTimestampEpochMicrosecondsUtc;
            else
                object.firstSampleTimestampEpochMicrosecondsUtc = options.longs === String ? $util.Long.prototype.toString.call(message.firstSampleTimestampEpochMicrosecondsUtc) : options.longs === Number ? new $util.LongBits(message.firstSampleTimestampEpochMicrosecondsUtc.low >>> 0, message.firstSampleTimestampEpochMicrosecondsUtc.high >>> 0).toNumber() : message.firstSampleTimestampEpochMicrosecondsUtc;
        if (message.bytePayload != null && message.hasOwnProperty("bytePayload")) {
            object.bytePayload = $root.BytePayload.toObject(message.bytePayload, options);
            if (options.oneofs)
                object.payload = "bytePayload";
        }
        if (message.uint32Payload != null && message.hasOwnProperty("uint32Payload")) {
            object.uint32Payload = $root.UInt32Payload.toObject(message.uint32Payload, options);
            if (options.oneofs)
                object.payload = "uint32Payload";
        }
        if (message.uint64Payload != null && message.hasOwnProperty("uint64Payload")) {
            object.uint64Payload = $root.UInt64Payload.toObject(message.uint64Payload, options);
            if (options.oneofs)
                object.payload = "uint64Payload";
        }
        if (message.int32Payload != null && message.hasOwnProperty("int32Payload")) {
            object.int32Payload = $root.Int32Payload.toObject(message.int32Payload, options);
            if (options.oneofs)
                object.payload = "int32Payload";
        }
        if (message.int64Payload != null && message.hasOwnProperty("int64Payload")) {
            object.int64Payload = $root.Int64Payload.toObject(message.int64Payload, options);
            if (options.oneofs)
                object.payload = "int64Payload";
        }
        if (message.float32Payload != null && message.hasOwnProperty("float32Payload")) {
            object.float32Payload = $root.Float32Payload.toObject(message.float32Payload, options);
            if (options.oneofs)
                object.payload = "float32Payload";
        }
        if (message.float64Payload != null && message.hasOwnProperty("float64Payload")) {
            object.float64Payload = $root.Float64Payload.toObject(message.float64Payload, options);
            if (options.oneofs)
                object.payload = "float64Payload";
        }
        if (message.valueMeans && message.valueMeans.length) {
            object.valueMeans = [];
            for (var j = 0; j < message.valueMeans.length; ++j)
                object.valueMeans[j] = message.valueMeans[j];
        }
        if (message.valueStds && message.valueStds.length) {
            object.valueStds = [];
            for (var j = 0; j < message.valueStds.length; ++j)
                object.valueStds[j] = message.valueStds[j];
        }
        if (message.valueMedians && message.valueMedians.length) {
            object.valueMedians = [];
            for (var j = 0; j < message.valueMedians.length; ++j)
                object.valueMedians[j] = message.valueMedians[j];
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this EvenlySampledChannel message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    EvenlySampledChannel.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this EvenlySampledChannel to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    EvenlySampledChannel.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return EvenlySampledChannel;
})();

$root.UnevenlySampledChannel = (function() {

    /**
     * Properties of an UnevenlySampledChannel.
     * @typedef UnevenlySampledChannel$Properties
     * @type {Object}
     * @property {Array.<ChannelType>} [channelTypes] UnevenlySampledChannel channelTypes.
     * @property {string} [sensorName] UnevenlySampledChannel sensorName.
     * @property {Array.<number|Long>} [timestampsMicrosecondsUtc] UnevenlySampledChannel timestampsMicrosecondsUtc.
     * @property {BytePayload$Properties} [bytePayload] UnevenlySampledChannel bytePayload.
     * @property {UInt32Payload$Properties} [uint32Payload] UnevenlySampledChannel uint32Payload.
     * @property {UInt64Payload$Properties} [uint64Payload] UnevenlySampledChannel uint64Payload.
     * @property {Int32Payload$Properties} [int32Payload] UnevenlySampledChannel int32Payload.
     * @property {Int64Payload$Properties} [int64Payload] UnevenlySampledChannel int64Payload.
     * @property {Float32Payload$Properties} [float32Payload] UnevenlySampledChannel float32Payload.
     * @property {Float64Payload$Properties} [float64Payload] UnevenlySampledChannel float64Payload.
     * @property {number} [sampleIntervalMean] UnevenlySampledChannel sampleIntervalMean.
     * @property {number} [sampleIntervalStd] UnevenlySampledChannel sampleIntervalStd.
     * @property {number} [sampleIntervalMedian] UnevenlySampledChannel sampleIntervalMedian.
     * @property {Array.<number>} [valueMeans] UnevenlySampledChannel valueMeans.
     * @property {Array.<number>} [valueStds] UnevenlySampledChannel valueStds.
     * @property {Array.<number>} [valueMedians] UnevenlySampledChannel valueMedians.
     * @property {Array.<string>} [metadata] UnevenlySampledChannel metadata.
     */

    /**
     * Constructs a new UnevenlySampledChannel.
     * @exports UnevenlySampledChannel
     * @constructor
     * @param {UnevenlySampledChannel$Properties=} [properties] Properties to set
     */
    function UnevenlySampledChannel(properties) {
        this.channelTypes = [];
        this.timestampsMicrosecondsUtc = [];
        this.valueMeans = [];
        this.valueStds = [];
        this.valueMedians = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * UnevenlySampledChannel channelTypes.
     * @type {Array.<ChannelType>|undefined}
     */
    UnevenlySampledChannel.prototype.channelTypes = $util.emptyArray;

    /**
     * UnevenlySampledChannel sensorName.
     * @type {string|undefined}
     */
    UnevenlySampledChannel.prototype.sensorName = "";

    /**
     * UnevenlySampledChannel timestampsMicrosecondsUtc.
     * @type {Array.<number|Long>|undefined}
     */
    UnevenlySampledChannel.prototype.timestampsMicrosecondsUtc = $util.emptyArray;

    /**
     * UnevenlySampledChannel bytePayload.
     * @type {BytePayload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.bytePayload = null;

    /**
     * UnevenlySampledChannel uint32Payload.
     * @type {UInt32Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.uint32Payload = null;

    /**
     * UnevenlySampledChannel uint64Payload.
     * @type {UInt64Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.uint64Payload = null;

    /**
     * UnevenlySampledChannel int32Payload.
     * @type {Int32Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.int32Payload = null;

    /**
     * UnevenlySampledChannel int64Payload.
     * @type {Int64Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.int64Payload = null;

    /**
     * UnevenlySampledChannel float32Payload.
     * @type {Float32Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.float32Payload = null;

    /**
     * UnevenlySampledChannel float64Payload.
     * @type {Float64Payload$Properties|undefined}
     */
    UnevenlySampledChannel.prototype.float64Payload = null;

    /**
     * UnevenlySampledChannel sampleIntervalMean.
     * @type {number|undefined}
     */
    UnevenlySampledChannel.prototype.sampleIntervalMean = 0;

    /**
     * UnevenlySampledChannel sampleIntervalStd.
     * @type {number|undefined}
     */
    UnevenlySampledChannel.prototype.sampleIntervalStd = 0;

    /**
     * UnevenlySampledChannel sampleIntervalMedian.
     * @type {number|undefined}
     */
    UnevenlySampledChannel.prototype.sampleIntervalMedian = 0;

    /**
     * UnevenlySampledChannel valueMeans.
     * @type {Array.<number>|undefined}
     */
    UnevenlySampledChannel.prototype.valueMeans = $util.emptyArray;

    /**
     * UnevenlySampledChannel valueStds.
     * @type {Array.<number>|undefined}
     */
    UnevenlySampledChannel.prototype.valueStds = $util.emptyArray;

    /**
     * UnevenlySampledChannel valueMedians.
     * @type {Array.<number>|undefined}
     */
    UnevenlySampledChannel.prototype.valueMedians = $util.emptyArray;

    /**
     * UnevenlySampledChannel metadata.
     * @type {Array.<string>|undefined}
     */
    UnevenlySampledChannel.prototype.metadata = $util.emptyArray;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * UnevenlySampledChannel payload.
     * @name UnevenlySampledChannel#payload
     * @type {string|undefined}
     */
    Object.defineProperty(UnevenlySampledChannel.prototype, "payload", {
        get: $util.oneOfGetter($oneOfFields = ["bytePayload", "uint32Payload", "uint64Payload", "int32Payload", "int64Payload", "float32Payload", "float64Payload"]),
        set: $util.oneOfSetter($oneOfFields)
    });

    /**
     * Creates a new UnevenlySampledChannel instance using the specified properties.
     * @param {UnevenlySampledChannel$Properties=} [properties] Properties to set
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel instance
     */
    UnevenlySampledChannel.create = function create(properties) {
        return new UnevenlySampledChannel(properties);
    };

    /**
     * Encodes the specified UnevenlySampledChannel message. Does not implicitly {@link UnevenlySampledChannel.verify|verify} messages.
     * @param {UnevenlySampledChannel$Properties} message UnevenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UnevenlySampledChannel.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.channelTypes && message.channelTypes.length) {
            writer.uint32(/* id 1, wireType 2 =*/10).fork();
            for (var i = 0; i < message.channelTypes.length; ++i)
                writer.uint32(message.channelTypes[i]);
            writer.ldelim();
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.sensorName);
        if (message.timestampsMicrosecondsUtc && message.timestampsMicrosecondsUtc.length) {
            writer.uint32(/* id 3, wireType 2 =*/26).fork();
            for (var i = 0; i < message.timestampsMicrosecondsUtc.length; ++i)
                writer.int64(message.timestampsMicrosecondsUtc[i]);
            writer.ldelim();
        }
        if (message.bytePayload && message.hasOwnProperty("bytePayload"))
            $root.BytePayload.encode(message.bytePayload, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
        if (message.uint32Payload && message.hasOwnProperty("uint32Payload"))
            $root.UInt32Payload.encode(message.uint32Payload, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
        if (message.uint64Payload && message.hasOwnProperty("uint64Payload"))
            $root.UInt64Payload.encode(message.uint64Payload, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
        if (message.int32Payload && message.hasOwnProperty("int32Payload"))
            $root.Int32Payload.encode(message.int32Payload, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
        if (message.int64Payload && message.hasOwnProperty("int64Payload"))
            $root.Int64Payload.encode(message.int64Payload, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
        if (message.float32Payload && message.hasOwnProperty("float32Payload"))
            $root.Float32Payload.encode(message.float32Payload, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
        if (message.float64Payload && message.hasOwnProperty("float64Payload"))
            $root.Float64Payload.encode(message.float64Payload, writer.uint32(/* id 10, wireType 2 =*/82).fork()).ldelim();
        if (message.sampleIntervalMean != null && message.hasOwnProperty("sampleIntervalMean"))
            writer.uint32(/* id 11, wireType 1 =*/89).double(message.sampleIntervalMean);
        if (message.sampleIntervalStd != null && message.hasOwnProperty("sampleIntervalStd"))
            writer.uint32(/* id 12, wireType 1 =*/97).double(message.sampleIntervalStd);
        if (message.sampleIntervalMedian != null && message.hasOwnProperty("sampleIntervalMedian"))
            writer.uint32(/* id 13, wireType 1 =*/105).double(message.sampleIntervalMedian);
        if (message.valueMeans && message.valueMeans.length) {
            writer.uint32(/* id 14, wireType 2 =*/114).fork();
            for (var i = 0; i < message.valueMeans.length; ++i)
                writer.double(message.valueMeans[i]);
            writer.ldelim();
        }
        if (message.valueStds && message.valueStds.length) {
            writer.uint32(/* id 15, wireType 2 =*/122).fork();
            for (var i = 0; i < message.valueStds.length; ++i)
                writer.double(message.valueStds[i]);
            writer.ldelim();
        }
        if (message.valueMedians && message.valueMedians.length) {
            writer.uint32(/* id 16, wireType 2 =*/130).fork();
            for (var i = 0; i < message.valueMedians.length; ++i)
                writer.double(message.valueMedians[i]);
            writer.ldelim();
        }
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 17, wireType 2 =*/138).string(message.metadata[i]);
        return writer;
    };

    /**
     * Encodes the specified UnevenlySampledChannel message, length delimited. Does not implicitly {@link UnevenlySampledChannel.verify|verify} messages.
     * @param {UnevenlySampledChannel$Properties} message UnevenlySampledChannel message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    UnevenlySampledChannel.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an UnevenlySampledChannel message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UnevenlySampledChannel.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.UnevenlySampledChannel();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.channelTypes && message.channelTypes.length))
                    message.channelTypes = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.channelTypes.push(reader.uint32());
                } else
                    message.channelTypes.push(reader.uint32());
                break;
            case 2:
                message.sensorName = reader.string();
                break;
            case 3:
                if (!(message.timestampsMicrosecondsUtc && message.timestampsMicrosecondsUtc.length))
                    message.timestampsMicrosecondsUtc = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.timestampsMicrosecondsUtc.push(reader.int64());
                } else
                    message.timestampsMicrosecondsUtc.push(reader.int64());
                break;
            case 4:
                message.bytePayload = $root.BytePayload.decode(reader, reader.uint32());
                break;
            case 5:
                message.uint32Payload = $root.UInt32Payload.decode(reader, reader.uint32());
                break;
            case 6:
                message.uint64Payload = $root.UInt64Payload.decode(reader, reader.uint32());
                break;
            case 7:
                message.int32Payload = $root.Int32Payload.decode(reader, reader.uint32());
                break;
            case 8:
                message.int64Payload = $root.Int64Payload.decode(reader, reader.uint32());
                break;
            case 9:
                message.float32Payload = $root.Float32Payload.decode(reader, reader.uint32());
                break;
            case 10:
                message.float64Payload = $root.Float64Payload.decode(reader, reader.uint32());
                break;
            case 11:
                message.sampleIntervalMean = reader.double();
                break;
            case 12:
                message.sampleIntervalStd = reader.double();
                break;
            case 13:
                message.sampleIntervalMedian = reader.double();
                break;
            case 14:
                if (!(message.valueMeans && message.valueMeans.length))
                    message.valueMeans = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMeans.push(reader.double());
                } else
                    message.valueMeans.push(reader.double());
                break;
            case 15:
                if (!(message.valueStds && message.valueStds.length))
                    message.valueStds = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueStds.push(reader.double());
                } else
                    message.valueStds.push(reader.double());
                break;
            case 16:
                if (!(message.valueMedians && message.valueMedians.length))
                    message.valueMedians = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.valueMedians.push(reader.double());
                } else
                    message.valueMedians.push(reader.double());
                break;
            case 17:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an UnevenlySampledChannel message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    UnevenlySampledChannel.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an UnevenlySampledChannel message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    UnevenlySampledChannel.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        var properties = {};
        if (message.channelTypes != null) {
            if (!Array.isArray(message.channelTypes))
                return "channelTypes: array expected";
            for (var i = 0; i < message.channelTypes.length; ++i)
                switch (message.channelTypes[i]) {
                default:
                    return "channelTypes: enum value[] expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                    break;
                }
        }
        if (message.sensorName != null)
            if (!$util.isString(message.sensorName))
                return "sensorName: string expected";
        if (message.timestampsMicrosecondsUtc != null) {
            if (!Array.isArray(message.timestampsMicrosecondsUtc))
                return "timestampsMicrosecondsUtc: array expected";
            for (var i = 0; i < message.timestampsMicrosecondsUtc.length; ++i)
                if (!$util.isInteger(message.timestampsMicrosecondsUtc[i]) && !(message.timestampsMicrosecondsUtc[i] && $util.isInteger(message.timestampsMicrosecondsUtc[i].low) && $util.isInteger(message.timestampsMicrosecondsUtc[i].high)))
                    return "timestampsMicrosecondsUtc: integer|Long[] expected";
        }
        if (message.bytePayload != null) {
            properties.payload = 1;
            var error = $root.BytePayload.verify(message.bytePayload);
            if (error)
                return "bytePayload." + error;
        }
        if (message.uint32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt32Payload.verify(message.uint32Payload);
            if (error)
                return "uint32Payload." + error;
        }
        if (message.uint64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.UInt64Payload.verify(message.uint64Payload);
            if (error)
                return "uint64Payload." + error;
        }
        if (message.int32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int32Payload.verify(message.int32Payload);
            if (error)
                return "int32Payload." + error;
        }
        if (message.int64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Int64Payload.verify(message.int64Payload);
            if (error)
                return "int64Payload." + error;
        }
        if (message.float32Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float32Payload.verify(message.float32Payload);
            if (error)
                return "float32Payload." + error;
        }
        if (message.float64Payload != null) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            var error = $root.Float64Payload.verify(message.float64Payload);
            if (error)
                return "float64Payload." + error;
        }
        if (message.sampleIntervalMean != null)
            if (typeof message.sampleIntervalMean !== "number")
                return "sampleIntervalMean: number expected";
        if (message.sampleIntervalStd != null)
            if (typeof message.sampleIntervalStd !== "number")
                return "sampleIntervalStd: number expected";
        if (message.sampleIntervalMedian != null)
            if (typeof message.sampleIntervalMedian !== "number")
                return "sampleIntervalMedian: number expected";
        if (message.valueMeans != null) {
            if (!Array.isArray(message.valueMeans))
                return "valueMeans: array expected";
            for (var i = 0; i < message.valueMeans.length; ++i)
                if (typeof message.valueMeans[i] !== "number")
                    return "valueMeans: number[] expected";
        }
        if (message.valueStds != null) {
            if (!Array.isArray(message.valueStds))
                return "valueStds: array expected";
            for (var i = 0; i < message.valueStds.length; ++i)
                if (typeof message.valueStds[i] !== "number")
                    return "valueStds: number[] expected";
        }
        if (message.valueMedians != null) {
            if (!Array.isArray(message.valueMedians))
                return "valueMedians: array expected";
            for (var i = 0; i < message.valueMedians.length; ++i)
                if (typeof message.valueMedians[i] !== "number")
                    return "valueMedians: number[] expected";
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates an UnevenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     */
    UnevenlySampledChannel.fromObject = function fromObject(object) {
        if (object instanceof $root.UnevenlySampledChannel)
            return object;
        var message = new $root.UnevenlySampledChannel();
        if (object.channelTypes) {
            if (!Array.isArray(object.channelTypes))
                throw TypeError(".UnevenlySampledChannel.channelTypes: array expected");
            message.channelTypes = [];
            for (var i = 0; i < object.channelTypes.length; ++i)
                switch (object.channelTypes[i]) {
                default:
                case "MICROPHONE":
                case 0:
                    message.channelTypes[i] = 0;
                    break;
                case "BAROMETER":
                case 1:
                    message.channelTypes[i] = 1;
                    break;
                case "LATITUDE":
                case 2:
                    message.channelTypes[i] = 2;
                    break;
                case "LONGITUDE":
                case 3:
                    message.channelTypes[i] = 3;
                    break;
                case "SPEED":
                case 4:
                    message.channelTypes[i] = 4;
                    break;
                case "ALTITUDE":
                case 5:
                    message.channelTypes[i] = 5;
                    break;
                case "RESERVED_0":
                case 6:
                    message.channelTypes[i] = 6;
                    break;
                case "RESERVED_1":
                case 7:
                    message.channelTypes[i] = 7;
                    break;
                case "RESERVED_2":
                case 8:
                    message.channelTypes[i] = 8;
                    break;
                case "TIME_SYNCHRONIZATION":
                case 9:
                    message.channelTypes[i] = 9;
                    break;
                case "ACCURACY":
                case 10:
                    message.channelTypes[i] = 10;
                    break;
                case "ACCELEROMETER_X":
                case 11:
                    message.channelTypes[i] = 11;
                    break;
                case "ACCELEROMETER_Y":
                case 12:
                    message.channelTypes[i] = 12;
                    break;
                case "ACCELEROMETER_Z":
                case 13:
                    message.channelTypes[i] = 13;
                    break;
                case "MAGNETOMETER_X":
                case 14:
                    message.channelTypes[i] = 14;
                    break;
                case "MAGNETOMETER_Y":
                case 15:
                    message.channelTypes[i] = 15;
                    break;
                case "MAGNETOMETER_Z":
                case 16:
                    message.channelTypes[i] = 16;
                    break;
                case "GYROSCOPE_X":
                case 17:
                    message.channelTypes[i] = 17;
                    break;
                case "GYROSCOPE_Y":
                case 18:
                    message.channelTypes[i] = 18;
                    break;
                case "GYROSCOPE_Z":
                case 19:
                    message.channelTypes[i] = 19;
                    break;
                case "OTHER":
                case 20:
                    message.channelTypes[i] = 20;
                    break;
                case "LIGHT":
                case 21:
                    message.channelTypes[i] = 21;
                    break;
                case "IMAGE":
                case 22:
                    message.channelTypes[i] = 22;
                    break;
                case "INFRARED":
                case 23:
                    message.channelTypes[i] = 23;
                    break;
                }
        }
        if (object.sensorName != null)
            message.sensorName = String(object.sensorName);
        if (object.timestampsMicrosecondsUtc) {
            if (!Array.isArray(object.timestampsMicrosecondsUtc))
                throw TypeError(".UnevenlySampledChannel.timestampsMicrosecondsUtc: array expected");
            message.timestampsMicrosecondsUtc = [];
            for (var i = 0; i < object.timestampsMicrosecondsUtc.length; ++i)
                if ($util.Long)
                    (message.timestampsMicrosecondsUtc[i] = $util.Long.fromValue(object.timestampsMicrosecondsUtc[i])).unsigned = false;
                else if (typeof object.timestampsMicrosecondsUtc[i] === "string")
                    message.timestampsMicrosecondsUtc[i] = parseInt(object.timestampsMicrosecondsUtc[i], 10);
                else if (typeof object.timestampsMicrosecondsUtc[i] === "number")
                    message.timestampsMicrosecondsUtc[i] = object.timestampsMicrosecondsUtc[i];
                else if (typeof object.timestampsMicrosecondsUtc[i] === "object")
                    message.timestampsMicrosecondsUtc[i] = new $util.LongBits(object.timestampsMicrosecondsUtc[i].low >>> 0, object.timestampsMicrosecondsUtc[i].high >>> 0).toNumber();
        }
        if (object.bytePayload != null) {
            if (typeof object.bytePayload !== "object")
                throw TypeError(".UnevenlySampledChannel.bytePayload: object expected");
            message.bytePayload = $root.BytePayload.fromObject(object.bytePayload);
        }
        if (object.uint32Payload != null) {
            if (typeof object.uint32Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.uint32Payload: object expected");
            message.uint32Payload = $root.UInt32Payload.fromObject(object.uint32Payload);
        }
        if (object.uint64Payload != null) {
            if (typeof object.uint64Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.uint64Payload: object expected");
            message.uint64Payload = $root.UInt64Payload.fromObject(object.uint64Payload);
        }
        if (object.int32Payload != null) {
            if (typeof object.int32Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.int32Payload: object expected");
            message.int32Payload = $root.Int32Payload.fromObject(object.int32Payload);
        }
        if (object.int64Payload != null) {
            if (typeof object.int64Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.int64Payload: object expected");
            message.int64Payload = $root.Int64Payload.fromObject(object.int64Payload);
        }
        if (object.float32Payload != null) {
            if (typeof object.float32Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.float32Payload: object expected");
            message.float32Payload = $root.Float32Payload.fromObject(object.float32Payload);
        }
        if (object.float64Payload != null) {
            if (typeof object.float64Payload !== "object")
                throw TypeError(".UnevenlySampledChannel.float64Payload: object expected");
            message.float64Payload = $root.Float64Payload.fromObject(object.float64Payload);
        }
        if (object.sampleIntervalMean != null)
            message.sampleIntervalMean = Number(object.sampleIntervalMean);
        if (object.sampleIntervalStd != null)
            message.sampleIntervalStd = Number(object.sampleIntervalStd);
        if (object.sampleIntervalMedian != null)
            message.sampleIntervalMedian = Number(object.sampleIntervalMedian);
        if (object.valueMeans) {
            if (!Array.isArray(object.valueMeans))
                throw TypeError(".UnevenlySampledChannel.valueMeans: array expected");
            message.valueMeans = [];
            for (var i = 0; i < object.valueMeans.length; ++i)
                message.valueMeans[i] = Number(object.valueMeans[i]);
        }
        if (object.valueStds) {
            if (!Array.isArray(object.valueStds))
                throw TypeError(".UnevenlySampledChannel.valueStds: array expected");
            message.valueStds = [];
            for (var i = 0; i < object.valueStds.length; ++i)
                message.valueStds[i] = Number(object.valueStds[i]);
        }
        if (object.valueMedians) {
            if (!Array.isArray(object.valueMedians))
                throw TypeError(".UnevenlySampledChannel.valueMedians: array expected");
            message.valueMedians = [];
            for (var i = 0; i < object.valueMedians.length; ++i)
                message.valueMedians[i] = Number(object.valueMedians[i]);
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".UnevenlySampledChannel.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates an UnevenlySampledChannel message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link UnevenlySampledChannel.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {UnevenlySampledChannel} UnevenlySampledChannel
     */
    UnevenlySampledChannel.from = UnevenlySampledChannel.fromObject;

    /**
     * Creates a plain object from an UnevenlySampledChannel message. Also converts values to other types if specified.
     * @param {UnevenlySampledChannel} message UnevenlySampledChannel
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UnevenlySampledChannel.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.channelTypes = [];
            object.timestampsMicrosecondsUtc = [];
            object.valueMeans = [];
            object.valueStds = [];
            object.valueMedians = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.sensorName = "";
            object.sampleIntervalMean = 0;
            object.sampleIntervalStd = 0;
            object.sampleIntervalMedian = 0;
        }
        if (message.channelTypes && message.channelTypes.length) {
            object.channelTypes = [];
            for (var j = 0; j < message.channelTypes.length; ++j)
                object.channelTypes[j] = options.enums === String ? $root.ChannelType[message.channelTypes[j]] : message.channelTypes[j];
        }
        if (message.sensorName != null && message.hasOwnProperty("sensorName"))
            object.sensorName = message.sensorName;
        if (message.timestampsMicrosecondsUtc && message.timestampsMicrosecondsUtc.length) {
            object.timestampsMicrosecondsUtc = [];
            for (var j = 0; j < message.timestampsMicrosecondsUtc.length; ++j)
                if (typeof message.timestampsMicrosecondsUtc[j] === "number")
                    object.timestampsMicrosecondsUtc[j] = options.longs === String ? String(message.timestampsMicrosecondsUtc[j]) : message.timestampsMicrosecondsUtc[j];
                else
                    object.timestampsMicrosecondsUtc[j] = options.longs === String ? $util.Long.prototype.toString.call(message.timestampsMicrosecondsUtc[j]) : options.longs === Number ? new $util.LongBits(message.timestampsMicrosecondsUtc[j].low >>> 0, message.timestampsMicrosecondsUtc[j].high >>> 0).toNumber() : message.timestampsMicrosecondsUtc[j];
        }
        if (message.bytePayload != null && message.hasOwnProperty("bytePayload")) {
            object.bytePayload = $root.BytePayload.toObject(message.bytePayload, options);
            if (options.oneofs)
                object.payload = "bytePayload";
        }
        if (message.uint32Payload != null && message.hasOwnProperty("uint32Payload")) {
            object.uint32Payload = $root.UInt32Payload.toObject(message.uint32Payload, options);
            if (options.oneofs)
                object.payload = "uint32Payload";
        }
        if (message.uint64Payload != null && message.hasOwnProperty("uint64Payload")) {
            object.uint64Payload = $root.UInt64Payload.toObject(message.uint64Payload, options);
            if (options.oneofs)
                object.payload = "uint64Payload";
        }
        if (message.int32Payload != null && message.hasOwnProperty("int32Payload")) {
            object.int32Payload = $root.Int32Payload.toObject(message.int32Payload, options);
            if (options.oneofs)
                object.payload = "int32Payload";
        }
        if (message.int64Payload != null && message.hasOwnProperty("int64Payload")) {
            object.int64Payload = $root.Int64Payload.toObject(message.int64Payload, options);
            if (options.oneofs)
                object.payload = "int64Payload";
        }
        if (message.float32Payload != null && message.hasOwnProperty("float32Payload")) {
            object.float32Payload = $root.Float32Payload.toObject(message.float32Payload, options);
            if (options.oneofs)
                object.payload = "float32Payload";
        }
        if (message.float64Payload != null && message.hasOwnProperty("float64Payload")) {
            object.float64Payload = $root.Float64Payload.toObject(message.float64Payload, options);
            if (options.oneofs)
                object.payload = "float64Payload";
        }
        if (message.sampleIntervalMean != null && message.hasOwnProperty("sampleIntervalMean"))
            object.sampleIntervalMean = message.sampleIntervalMean;
        if (message.sampleIntervalStd != null && message.hasOwnProperty("sampleIntervalStd"))
            object.sampleIntervalStd = message.sampleIntervalStd;
        if (message.sampleIntervalMedian != null && message.hasOwnProperty("sampleIntervalMedian"))
            object.sampleIntervalMedian = message.sampleIntervalMedian;
        if (message.valueMeans && message.valueMeans.length) {
            object.valueMeans = [];
            for (var j = 0; j < message.valueMeans.length; ++j)
                object.valueMeans[j] = message.valueMeans[j];
        }
        if (message.valueStds && message.valueStds.length) {
            object.valueStds = [];
            for (var j = 0; j < message.valueStds.length; ++j)
                object.valueStds[j] = message.valueStds[j];
        }
        if (message.valueMedians && message.valueMedians.length) {
            object.valueMedians = [];
            for (var j = 0; j < message.valueMedians.length; ++j)
                object.valueMedians[j] = message.valueMedians[j];
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this UnevenlySampledChannel message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    UnevenlySampledChannel.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this UnevenlySampledChannel to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    UnevenlySampledChannel.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return UnevenlySampledChannel;
})();

$root.RedvoxPacketResponse = (function() {

    /**
     * Properties of a RedvoxPacketResponse.
     * @typedef RedvoxPacketResponse$Properties
     * @type {Object}
     * @property {RedvoxPacketResponse.Type} [type] RedvoxPacketResponse type.
     * @property {number|Long} [checksum] RedvoxPacketResponse checksum.
     * @property {Array.<RedvoxPacketResponse.Error>} [errors] RedvoxPacketResponse errors.
     * @property {Array.<string>} [metadata] RedvoxPacketResponse metadata.
     */

    /**
     * Constructs a new RedvoxPacketResponse.
     * @exports RedvoxPacketResponse
     * @constructor
     * @param {RedvoxPacketResponse$Properties=} [properties] Properties to set
     */
    function RedvoxPacketResponse(properties) {
        this.errors = [];
        this.metadata = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                this[keys[i]] = properties[keys[i]];
    }

    /**
     * RedvoxPacketResponse type.
     * @type {RedvoxPacketResponse.Type|undefined}
     */
    RedvoxPacketResponse.prototype.type = 0;

    /**
     * RedvoxPacketResponse checksum.
     * @type {number|Long|undefined}
     */
    RedvoxPacketResponse.prototype.checksum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    /**
     * RedvoxPacketResponse errors.
     * @type {Array.<RedvoxPacketResponse.Error>|undefined}
     */
    RedvoxPacketResponse.prototype.errors = $util.emptyArray;

    /**
     * RedvoxPacketResponse metadata.
     * @type {Array.<string>|undefined}
     */
    RedvoxPacketResponse.prototype.metadata = $util.emptyArray;

    /**
     * Creates a new RedvoxPacketResponse instance using the specified properties.
     * @param {RedvoxPacketResponse$Properties=} [properties] Properties to set
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse instance
     */
    RedvoxPacketResponse.create = function create(properties) {
        return new RedvoxPacketResponse(properties);
    };

    /**
     * Encodes the specified RedvoxPacketResponse message. Does not implicitly {@link RedvoxPacketResponse.verify|verify} messages.
     * @param {RedvoxPacketResponse$Properties} message RedvoxPacketResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacketResponse.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.type != null && message.hasOwnProperty("type"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.type);
        if (message.checksum != null && message.hasOwnProperty("checksum"))
            writer.uint32(/* id 2, wireType 0 =*/16).int64(message.checksum);
        if (message.errors && message.errors.length) {
            writer.uint32(/* id 3, wireType 2 =*/26).fork();
            for (var i = 0; i < message.errors.length; ++i)
                writer.uint32(message.errors[i]);
            writer.ldelim();
        }
        if (message.metadata && message.metadata.length)
            for (var i = 0; i < message.metadata.length; ++i)
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.metadata[i]);
        return writer;
    };

    /**
     * Encodes the specified RedvoxPacketResponse message, length delimited. Does not implicitly {@link RedvoxPacketResponse.verify|verify} messages.
     * @param {RedvoxPacketResponse$Properties} message RedvoxPacketResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RedvoxPacketResponse.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a RedvoxPacketResponse message from the specified reader or buffer.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacketResponse.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.RedvoxPacketResponse();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.type = reader.uint32();
                break;
            case 2:
                message.checksum = reader.int64();
                break;
            case 3:
                if (!(message.errors && message.errors.length))
                    message.errors = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.errors.push(reader.uint32());
                } else
                    message.errors.push(reader.uint32());
                break;
            case 4:
                if (!(message.metadata && message.metadata.length))
                    message.metadata = [];
                message.metadata.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a RedvoxPacketResponse message from the specified reader or buffer, length delimited.
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RedvoxPacketResponse.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a RedvoxPacketResponse message.
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {?string} `null` if valid, otherwise the reason why it is not
     */
    RedvoxPacketResponse.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.type != null)
            switch (message.type) {
            default:
                return "type: enum value expected";
            case 0:
            case 1:
                break;
            }
        if (message.checksum != null)
            if (!$util.isInteger(message.checksum) && !(message.checksum && $util.isInteger(message.checksum.low) && $util.isInteger(message.checksum.high)))
                return "checksum: integer|Long expected";
        if (message.errors != null) {
            if (!Array.isArray(message.errors))
                return "errors: array expected";
            for (var i = 0; i < message.errors.length; ++i)
                switch (message.errors[i]) {
                default:
                    return "errors: enum value[] expected";
                case 0:
                case 1:
                    break;
                }
        }
        if (message.metadata != null) {
            if (!Array.isArray(message.metadata))
                return "metadata: array expected";
            for (var i = 0; i < message.metadata.length; ++i)
                if (!$util.isString(message.metadata[i]))
                    return "metadata: string[] expected";
        }
        return null;
    };

    /**
     * Creates a RedvoxPacketResponse message from a plain object. Also converts values to their respective internal types.
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     */
    RedvoxPacketResponse.fromObject = function fromObject(object) {
        if (object instanceof $root.RedvoxPacketResponse)
            return object;
        var message = new $root.RedvoxPacketResponse();
        switch (object.type) {
        case "OK":
        case 0:
            message.type = 0;
            break;
        case "ERROR":
        case 1:
            message.type = 1;
            break;
        }
        if (object.checksum != null)
            if ($util.Long)
                (message.checksum = $util.Long.fromValue(object.checksum)).unsigned = false;
            else if (typeof object.checksum === "string")
                message.checksum = parseInt(object.checksum, 10);
            else if (typeof object.checksum === "number")
                message.checksum = object.checksum;
            else if (typeof object.checksum === "object")
                message.checksum = new $util.LongBits(object.checksum.low >>> 0, object.checksum.high >>> 0).toNumber();
        if (object.errors) {
            if (!Array.isArray(object.errors))
                throw TypeError(".RedvoxPacketResponse.errors: array expected");
            message.errors = [];
            for (var i = 0; i < object.errors.length; ++i)
                switch (object.errors[i]) {
                default:
                case "NOT_AUTHENTICATED":
                case 0:
                    message.errors[i] = 0;
                    break;
                case "OTHER":
                case 1:
                    message.errors[i] = 1;
                    break;
                }
        }
        if (object.metadata) {
            if (!Array.isArray(object.metadata))
                throw TypeError(".RedvoxPacketResponse.metadata: array expected");
            message.metadata = [];
            for (var i = 0; i < object.metadata.length; ++i)
                message.metadata[i] = String(object.metadata[i]);
        }
        return message;
    };

    /**
     * Creates a RedvoxPacketResponse message from a plain object. Also converts values to their respective internal types.
     * This is an alias of {@link RedvoxPacketResponse.fromObject}.
     * @function
     * @param {Object.<string,*>} object Plain object
     * @returns {RedvoxPacketResponse} RedvoxPacketResponse
     */
    RedvoxPacketResponse.from = RedvoxPacketResponse.fromObject;

    /**
     * Creates a plain object from a RedvoxPacketResponse message. Also converts values to other types if specified.
     * @param {RedvoxPacketResponse} message RedvoxPacketResponse
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacketResponse.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults) {
            object.errors = [];
            object.metadata = [];
        }
        if (options.defaults) {
            object.type = options.enums === String ? "OK" : 0;
            if ($util.Long) {
                var long = new $util.Long(0, 0, false);
                object.checksum = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
            } else
                object.checksum = options.longs === String ? "0" : 0;
        }
        if (message.type != null && message.hasOwnProperty("type"))
            object.type = options.enums === String ? $root.RedvoxPacketResponse.Type[message.type] : message.type;
        if (message.checksum != null && message.hasOwnProperty("checksum"))
            if (typeof message.checksum === "number")
                object.checksum = options.longs === String ? String(message.checksum) : message.checksum;
            else
                object.checksum = options.longs === String ? $util.Long.prototype.toString.call(message.checksum) : options.longs === Number ? new $util.LongBits(message.checksum.low >>> 0, message.checksum.high >>> 0).toNumber() : message.checksum;
        if (message.errors && message.errors.length) {
            object.errors = [];
            for (var j = 0; j < message.errors.length; ++j)
                object.errors[j] = options.enums === String ? $root.RedvoxPacketResponse.Error[message.errors[j]] : message.errors[j];
        }
        if (message.metadata && message.metadata.length) {
            object.metadata = [];
            for (var j = 0; j < message.metadata.length; ++j)
                object.metadata[j] = message.metadata[j];
        }
        return object;
    };

    /**
     * Creates a plain object from this RedvoxPacketResponse message. Also converts values to other types if specified.
     * @param {$protobuf.ConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RedvoxPacketResponse.prototype.toObject = function toObject(options) {
        return this.constructor.toObject(this, options);
    };

    /**
     * Converts this RedvoxPacketResponse to JSON.
     * @returns {Object.<string,*>} JSON object
     */
    RedvoxPacketResponse.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    /**
     * Type enum.
     * @name Type
     * @memberof RedvoxPacketResponse
     * @enum {number}
     * @property {number} OK=0 OK value
     * @property {number} ERROR=1 ERROR value
     */
    RedvoxPacketResponse.Type = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "OK"] = 0;
        values[valuesById[1] = "ERROR"] = 1;
        return values;
    })();

    /**
     * Error enum.
     * @name Error
     * @memberof RedvoxPacketResponse
     * @enum {number}
     * @property {number} NOT_AUTHENTICATED=0 NOT_AUTHENTICATED value
     * @property {number} OTHER=1 OTHER value
     */
    RedvoxPacketResponse.Error = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "NOT_AUTHENTICATED"] = 0;
        values[valuesById[1] = "OTHER"] = 1;
        return values;
    })();

    return RedvoxPacketResponse;
})();