<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api900.proto

namespace RedvoxPacketResponse;

use UnexpectedValueException;

/**
 * Error types
 *
 * Protobuf type <code>RedvoxPacketResponse.Error</code>
 */
class Error
{
    /**
     * Generated from protobuf enum <code>NOT_AUTHENTICATED = 0;</code>
     */
    const NOT_AUTHENTICATED = 0;
    /**
     * Generated from protobuf enum <code>OTHER = 1;</code>
     */
    const OTHER = 1;

    private static $valueToName = [
        self::NOT_AUTHENTICATED => 'NOT_AUTHENTICATED',
        self::OTHER => 'OTHER',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }


    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(Error::class, \RedvoxPacketResponse_Error::class);

