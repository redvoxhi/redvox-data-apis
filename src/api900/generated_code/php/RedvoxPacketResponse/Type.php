<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api900.proto

namespace RedvoxPacketResponse;

use UnexpectedValueException;

/**
 * Response types
 *
 * Protobuf type <code>RedvoxPacketResponse.Type</code>
 */
class Type
{
    /**
     * Generated from protobuf enum <code>OK = 0;</code>
     */
    const OK = 0;
    /**
     * Generated from protobuf enum <code>ERROR = 1;</code>
     */
    const ERROR = 1;

    private static $valueToName = [
        self::OK => 'OK',
        self::ERROR => 'ERROR',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }


    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(Type::class, \RedvoxPacketResponse_Type::class);

