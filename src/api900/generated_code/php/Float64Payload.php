<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api900.proto

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * An array of float64s
 *
 * Generated from protobuf message <code>Float64Payload</code>
 */
class Float64Payload extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated double payload = 1;</code>
     */
    private $payload;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type float[]|\Google\Protobuf\Internal\RepeatedField $payload
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Api900::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated double payload = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Generated from protobuf field <code>repeated double payload = 1;</code>
     * @param float[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setPayload($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::DOUBLE);
        $this->payload = $arr;

        return $this;
    }

}

