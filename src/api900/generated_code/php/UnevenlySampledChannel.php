<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api900.proto

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * A channel without evenly sampled data. i.e., one with a non-stable sample rate such as barometer or GPS
 * Note: Multiple values can be associated with each timestamp such as in the case of a GPS returning lat, lng, speed, and altitude at the same time
 * For each value, specify a channel type, then in the payload, interleave the values.
 * e.g. channel_types = [LATITUDE, LONGITUDE, SPEED, ALTITUDE], then the payload becomes for each timestamp/sample i
 *  payload = [latitude[0], longitude[0], speed[0], altitude[0], latitude[1], longitude[1], speed[1], altitude[1], ..., latitude[i], longitude[i], speed[i], altitude[i]]
 *
 * Generated from protobuf message <code>UnevenlySampledChannel</code>
 */
class UnevenlySampledChannel extends \Google\Protobuf\Internal\Message
{
    /**
     * Channel types associated with provided timestamps
     *
     * Generated from protobuf field <code>repeated .ChannelType channel_types = 1;</code>
     */
    private $channel_types;
    /**
     * Name of sensor
     *
     * Generated from protobuf field <code>string sensor_name = 2;</code>
     */
    protected $sensor_name = '';
    /**
     * List of timestamps for each sample
     *
     * Generated from protobuf field <code>repeated int64 timestamps_microseconds_utc = 3;</code>
     */
    private $timestamps_microseconds_utc;
    /**
     * Mean of sample internval as determined from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_mean = 11;</code>
     */
    protected $sample_interval_mean = 0.0;
    /**
     * Standard deviation of sample interval from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_std = 12;</code>
     */
    protected $sample_interval_std = 0.0;
    /**
     * Median of sample interval from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_median = 13;</code>
     */
    protected $sample_interval_median = 0.0;
    /**
     * Mean values in payload, one mean per channel
     *
     * Generated from protobuf field <code>repeated double value_means = 14;</code>
     */
    private $value_means;
    /**
     * Standard deviations in payload, one per channel
     *
     * Generated from protobuf field <code>repeated double value_stds = 15;</code>
     */
    private $value_stds;
    /**
     * Medians in payload, one per channel
     *
     * Generated from protobuf field <code>repeated double value_medians = 16;</code>
     */
    private $value_medians;
    /**
     * Extra metadata to associate with this channel
     *
     * Generated from protobuf field <code>repeated string metadata = 17;</code>
     */
    private $metadata;
    protected $payload;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int[]|\Google\Protobuf\Internal\RepeatedField $channel_types
     *           Channel types associated with provided timestamps
     *     @type string $sensor_name
     *           Name of sensor
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $timestamps_microseconds_utc
     *           List of timestamps for each sample
     *     @type \BytePayload $byte_payload
     *     @type \UInt32Payload $uint32_payload
     *     @type \UInt64Payload $uint64_payload
     *     @type \Int32Payload $int32_payload
     *     @type \Int64Payload $int64_payload
     *     @type \Float32Payload $float32_payload
     *     @type \Float64Payload $float64_payload
     *     @type float $sample_interval_mean
     *           Mean of sample internval as determined from timestamps
     *     @type float $sample_interval_std
     *           Standard deviation of sample interval from timestamps
     *     @type float $sample_interval_median
     *           Median of sample interval from timestamps
     *     @type float[]|\Google\Protobuf\Internal\RepeatedField $value_means
     *           Mean values in payload, one mean per channel
     *     @type float[]|\Google\Protobuf\Internal\RepeatedField $value_stds
     *           Standard deviations in payload, one per channel
     *     @type float[]|\Google\Protobuf\Internal\RepeatedField $value_medians
     *           Medians in payload, one per channel
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $metadata
     *           Extra metadata to associate with this channel
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Api900::initOnce();
        parent::__construct($data);
    }

    /**
     * Channel types associated with provided timestamps
     *
     * Generated from protobuf field <code>repeated .ChannelType channel_types = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getChannelTypes()
    {
        return $this->channel_types;
    }

    /**
     * Channel types associated with provided timestamps
     *
     * Generated from protobuf field <code>repeated .ChannelType channel_types = 1;</code>
     * @param int[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setChannelTypes($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::ENUM, \ChannelType::class);
        $this->channel_types = $arr;

        return $this;
    }

    /**
     * Name of sensor
     *
     * Generated from protobuf field <code>string sensor_name = 2;</code>
     * @return string
     */
    public function getSensorName()
    {
        return $this->sensor_name;
    }

    /**
     * Name of sensor
     *
     * Generated from protobuf field <code>string sensor_name = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setSensorName($var)
    {
        GPBUtil::checkString($var, True);
        $this->sensor_name = $var;

        return $this;
    }

    /**
     * List of timestamps for each sample
     *
     * Generated from protobuf field <code>repeated int64 timestamps_microseconds_utc = 3;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getTimestampsMicrosecondsUtc()
    {
        return $this->timestamps_microseconds_utc;
    }

    /**
     * List of timestamps for each sample
     *
     * Generated from protobuf field <code>repeated int64 timestamps_microseconds_utc = 3;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setTimestampsMicrosecondsUtc($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->timestamps_microseconds_utc = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.BytePayload byte_payload = 4;</code>
     * @return \BytePayload
     */
    public function getBytePayload()
    {
        return $this->readOneof(4);
    }

    /**
     * Generated from protobuf field <code>.BytePayload byte_payload = 4;</code>
     * @param \BytePayload $var
     * @return $this
     */
    public function setBytePayload($var)
    {
        GPBUtil::checkMessage($var, \BytePayload::class);
        $this->writeOneof(4, $var);

        return $this;
    }

    /**
     * Generated from protobuf field <code>.UInt32Payload uint32_payload = 5;</code>
     * @return \UInt32Payload
     */
    public function getUint32Payload()
    {
        return $this->readOneof(5);
    }

    /**
     * Generated from protobuf field <code>.UInt32Payload uint32_payload = 5;</code>
     * @param \UInt32Payload $var
     * @return $this
     */
    public function setUint32Payload($var)
    {
        GPBUtil::checkMessage($var, \UInt32Payload::class);
        $this->writeOneof(5, $var);

        return $this;
    }

    /**
     * Generated from protobuf field <code>.UInt64Payload uint64_payload = 6;</code>
     * @return \UInt64Payload
     */
    public function getUint64Payload()
    {
        return $this->readOneof(6);
    }

    /**
     * Generated from protobuf field <code>.UInt64Payload uint64_payload = 6;</code>
     * @param \UInt64Payload $var
     * @return $this
     */
    public function setUint64Payload($var)
    {
        GPBUtil::checkMessage($var, \UInt64Payload::class);
        $this->writeOneof(6, $var);

        return $this;
    }

    /**
     * Generated from protobuf field <code>.Int32Payload int32_payload = 7;</code>
     * @return \Int32Payload
     */
    public function getInt32Payload()
    {
        return $this->readOneof(7);
    }

    /**
     * Generated from protobuf field <code>.Int32Payload int32_payload = 7;</code>
     * @param \Int32Payload $var
     * @return $this
     */
    public function setInt32Payload($var)
    {
        GPBUtil::checkMessage($var, \Int32Payload::class);
        $this->writeOneof(7, $var);

        return $this;
    }

    /**
     * Generated from protobuf field <code>.Int64Payload int64_payload = 8;</code>
     * @return \Int64Payload
     */
    public function getInt64Payload()
    {
        return $this->readOneof(8);
    }

    /**
     * Generated from protobuf field <code>.Int64Payload int64_payload = 8;</code>
     * @param \Int64Payload $var
     * @return $this
     */
    public function setInt64Payload($var)
    {
        GPBUtil::checkMessage($var, \Int64Payload::class);
        $this->writeOneof(8, $var);

        return $this;
    }

    /**
     * Generated from protobuf field <code>.Float32Payload float32_payload = 9;</code>
     * @return \Float32Payload
     */
    public function getFloat32Payload()
    {
        return $this->readOneof(9);
    }

    /**
     * Generated from protobuf field <code>.Float32Payload float32_payload = 9;</code>
     * @param \Float32Payload $var
     * @return $this
     */
    public function setFloat32Payload($var)
    {
        GPBUtil::checkMessage($var, \Float32Payload::class);
        $this->writeOneof(9, $var);

        return $this;
    }

    /**
     * Generated from protobuf field <code>.Float64Payload float64_payload = 10;</code>
     * @return \Float64Payload
     */
    public function getFloat64Payload()
    {
        return $this->readOneof(10);
    }

    /**
     * Generated from protobuf field <code>.Float64Payload float64_payload = 10;</code>
     * @param \Float64Payload $var
     * @return $this
     */
    public function setFloat64Payload($var)
    {
        GPBUtil::checkMessage($var, \Float64Payload::class);
        $this->writeOneof(10, $var);

        return $this;
    }

    /**
     * Mean of sample internval as determined from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_mean = 11;</code>
     * @return float
     */
    public function getSampleIntervalMean()
    {
        return $this->sample_interval_mean;
    }

    /**
     * Mean of sample internval as determined from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_mean = 11;</code>
     * @param float $var
     * @return $this
     */
    public function setSampleIntervalMean($var)
    {
        GPBUtil::checkDouble($var);
        $this->sample_interval_mean = $var;

        return $this;
    }

    /**
     * Standard deviation of sample interval from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_std = 12;</code>
     * @return float
     */
    public function getSampleIntervalStd()
    {
        return $this->sample_interval_std;
    }

    /**
     * Standard deviation of sample interval from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_std = 12;</code>
     * @param float $var
     * @return $this
     */
    public function setSampleIntervalStd($var)
    {
        GPBUtil::checkDouble($var);
        $this->sample_interval_std = $var;

        return $this;
    }

    /**
     * Median of sample interval from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_median = 13;</code>
     * @return float
     */
    public function getSampleIntervalMedian()
    {
        return $this->sample_interval_median;
    }

    /**
     * Median of sample interval from timestamps
     *
     * Generated from protobuf field <code>double sample_interval_median = 13;</code>
     * @param float $var
     * @return $this
     */
    public function setSampleIntervalMedian($var)
    {
        GPBUtil::checkDouble($var);
        $this->sample_interval_median = $var;

        return $this;
    }

    /**
     * Mean values in payload, one mean per channel
     *
     * Generated from protobuf field <code>repeated double value_means = 14;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getValueMeans()
    {
        return $this->value_means;
    }

    /**
     * Mean values in payload, one mean per channel
     *
     * Generated from protobuf field <code>repeated double value_means = 14;</code>
     * @param float[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setValueMeans($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::DOUBLE);
        $this->value_means = $arr;

        return $this;
    }

    /**
     * Standard deviations in payload, one per channel
     *
     * Generated from protobuf field <code>repeated double value_stds = 15;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getValueStds()
    {
        return $this->value_stds;
    }

    /**
     * Standard deviations in payload, one per channel
     *
     * Generated from protobuf field <code>repeated double value_stds = 15;</code>
     * @param float[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setValueStds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::DOUBLE);
        $this->value_stds = $arr;

        return $this;
    }

    /**
     * Medians in payload, one per channel
     *
     * Generated from protobuf field <code>repeated double value_medians = 16;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getValueMedians()
    {
        return $this->value_medians;
    }

    /**
     * Medians in payload, one per channel
     *
     * Generated from protobuf field <code>repeated double value_medians = 16;</code>
     * @param float[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setValueMedians($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::DOUBLE);
        $this->value_medians = $arr;

        return $this;
    }

    /**
     * Extra metadata to associate with this channel
     *
     * Generated from protobuf field <code>repeated string metadata = 17;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Extra metadata to associate with this channel
     *
     * Generated from protobuf field <code>repeated string metadata = 17;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setMetadata($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->metadata = $arr;

        return $this;
    }

    /**
     * @return string
     */
    public function getPayload()
    {
        return $this->whichOneof("payload");
    }

}

