#!/bin/bash

echo "Compiling all .proto files"
cd ./analysis && ./build.sh && cd -         && \
    cd ./api900 && ./build.sh && cd -       && \
    cd ./redvox-web/ && ./build.sh && cd -
#cd ./data-downloader/ && ./build.sh && cd -
