
\documentclass[]{article}

\usepackage[margin=.60in]{geometry}

\usepackage{tabulary}
\usepackage{hyperref}

%opening
\title{RedVox Data Formats\\API 0x800}
\author{Distributed for Public Release\\ISLA, University of Hawaii at Manoa\\Contact: achriste@hawaii.edu}

\begin{document}

\maketitle

%\begin{abstract}

%\end{abstract}
\tableofcontents

\section{Introduction}
The data format we use to transfer infrasound is the \textbf{Waveform Audio File Format} (also known as \textbf{WAVE} or \textbf{WAV}). WAVE is based off of the \textbf{Resource Interchange File Format} (or \textbf{RIFF}) which provides the ability to store data in chunks.
\\\\
Adding a new meta-data chunk titled \textbf{RDVX} to a traditional WAVE formatted file will produce a RedVox encoded file. 
\\\\
This document serves as the API documentation for the entire RedVox file format for API version 0x800.

\section{File Name Convention}
Files are named on the device and by our acquisition server using the following format:\\\\ rdvx\_\textit{deviceId}\_\textit{timestamp}\_\textit{sensorType}\_\textit{numMessageExchanges}.wav where

\begin{itemize}
    \item \textit{deviceId} - 10-digit 0-padded device number
    \item \textit{timestamp} - number of seconds since the epoch UTC (extracted using the device's epoch time of the first sample)
    \item \textit{sensorType} - one of
    \begin{itemize}
        \item \textbf{L} - 80 Hz microphone
        \item \textbf{H} - 800 Hz microphone
        \item \textbf{A} - 8000 Hz microphone
        \item \textbf{B} - barometer
    \end{itemize}
    \item \textit{numMessageExchanges} - is the 2-digit, 0-padded total number of message exchanges (upto 10) that took place during the file construction.
\end{itemize}


\noindent An example filename might look like: \textbf{rdvx\_1000123456\_1488489621\_H\_09.wav}. Filename times from a specific device should be monotonically increasing.

\section{RedVox Microphone API 0x800}
\hspace*{-0.5cm}
\begin{tabulary}{0.9\paperwidth}{cccccC}
    \textbf{Field} & \textbf{Byte Offset} & \textbf{Type} & \textbf{Count} & \textbf{Endian} & \textbf{Comments} \\ 
    \hline 
    headerChunkId & 0 & char & 4 & big &  \textbf{RIFF}\\ 
    \hline 
    headerChunkSize & 4 & int32 & 1 & little &  \\ 
    \hline 
    headerChunkFormat & 8 & char & 4 & big &  \textbf{WAVE}\\ 
    \hline 
    headerSubChunkId & 12 & char & 4 & big &  \textbf{fmt }\\ 
    \hline 
    headerSubChunkSize & 16 & int32 & 1 & little &  \\ 
    \hline 
    headerAudioFormat & 20 & int16 & 1 & little &  \\ 
    \hline 
    headerNumChannels & 22 & int16 & 1 & little &  \\ 
    \hline 
    headerSampleRate & 24 & int32 & 1 & little & \textbf{80} or \textbf{800} or \textbf{8000}\\ 
    \hline 
    headerByteRate & 28 & int32 & 1 & little &  \\ 
    \hline 
    headerBlockAlign & 32 & int16 & 1 & little &  \\ 
    \hline 
    headerBitsPerSample & 34 & int16 & 1 & little & \textbf{16} or \textbf{24} \\ 
    \hline 
    rdvxChunkId & 36 & char & 4 & big & \textbf{RDVX}\\ 
    \hline 
    rdvxChunkSize & 40 & int32 & 1 & little &  \\ 
    \hline 
    rdvxApiVersion & 44 & int32 & 1 & little & \textbf{2048} decimal or \textbf{0x800} hex \\ 
    \hline 
    rdvxServerRecvEpoch & 48 & int64 & 1 & little & Server arrival, ms since epoch UTC \\ 
    \hline 
    rdvxIdForVendor & 56 & uint32 & 1 & little & ``Device id" \\ 
    \hline 
    rdvxTruncatedUuid & 60 & int32 & 1 & little & Set by device, not user definable \\ 
    \hline 
    rdvxLatitude & 64 & float64 & 1 & little & Degrees latitude \\ 
    \hline 
    rdvxLongitude & 72 & float64 & 1 & little & Degrees longitude \\ 
    \hline 
    rdvxAltitude & 80 & float64 & 1 & little & Meters \\ 
    \hline 
    rdvxSpeed & 88 & float64 & 1 & little & Meters / second \\ 
    \hline 
    rdvxHorizontalAccuracy & 96 & float64 & 1 & little & Meters \\ 
    \hline 
    rdvxVerticalAccuracy & 104 & float64 & 1 & little & Meters \\ 
    \hline 
    rdvxCalibrationTrim & 112 & float64 & 1 & little &  \\ 
    \hline 
    rdvxTimeOfSolution & 120 & int32 & 1 & little & ms since epoch UTC \\ 
    \hline 
    rdvxFileStartMach & 124 & int64 & 1 & little & Device ``machine" internal count since app start \\ 
    \hline 
    rdvxFileStartEpoch & 132 & int64 & 1 & little & Device timestamp start of file, $\mu$s since epoch UTC \\ 
    \hline 
    rdvxNumMessageExchanges & 140 & int8 & 1 & little & Each message exchange contains 6 messages \\ 
    \hline 
    rdvxMessageExchanges & 141 & int64 & 60 & little &  Time synch messages, only first $6 * rdvxNumMessageExchanges$ messages are valid\\ 
    \hline 
    rdvxSensorName & 621 & char & 30 & big &  \\ 
    \hline 
    rdvxDeviceName & 651 & char & 20 & big &  \\ 
    \hline 
    rdvxSyncServer & 671 & char & 25 & big & Time synchronization server ip and port \\ 
    \hline 
    rdvxDataServer & 696 & char & 25 & big & Data acquisition server ip and port \\ 
    \hline 
    dataChunkId & 721 & char & 4 & big &  \textbf{data}\\ 
    \hline 
    dataChunkSize & 725 & int32 & 1 & little & \textbf{12288} (24 bit @ 80Hz) or \textbf{98304} (24 bit @ 800Hz) or \textbf{786432} (24 bit @ 8000Hz) \\ 
    \hline 
    waveform & 729 & int16 or int24 & 4096 or 32768 & little & See section \ref{ssec:wav} \\ 
    \hline 
\end{tabulary} 

\subsection{Addendum on Microphone Waveform}\label{ssec:wav}
The data type is one of \textit{int16} or \textit{int24}. The data type is determined by examining the value of \textit{headerBitsPerSample}. The number of samples this field holds is dependent on the sample rate and is summarized below.

\begin{itemize}
    \item 4096 samples - 24 bit @ 80Hz. File time duration of 51.200 s.
    \item 32768 samples - 24 bit @ 800Hz. File time duration of 40.960 s.
    \item 262144 samples - 24 bit @ 8000Hz. File time duration of 32.768 s.
\end{itemize}

\section{RedVox Barometer API 0x800}
\hspace*{-0.5cm}
\begin{tabulary}{0.9\paperwidth}{cccccC}
    \textbf{Field} & \textbf{Byte Offset} & \textbf{Type} & \textbf{Count} & \textbf{Endian} & \textbf{Comments} \\ 
    \hline 
    headerChunkId & 0 & char & 4 & big &  \textbf{RIFF}\\ 
    \hline 
    headerChunkSize & 4 & int32 & 1 & little &  \\ 
    \hline 
    headerChunkFormat & 8 & char & 4 & big &  \textbf{BAR }\\ 
    \hline 
    headerSubChunkId & 12 & char & 4 & big &  \textbf{fmt }\\ 
    \hline 
    headerSubChunkSize & 16 & int32 & 1 & little &  \\ 
    \hline 
    headerAudioFormat & 20 & int16 & 1 & little &  \\ 
    \hline 
    headerNumChannels & 22 & int16 & 1 & little &  \\ 
    \hline 
    headerSampleRate & 24 & int32 & 1 & little & \textbf{1} (closer to $^1/_{1.3}$ Hz in iOS)\\ 
    \hline 
    headerByteRate & 28 & int32 & 1 & little &  \\ 
    \hline 
    headerBlockAlign & 32 & int16 & 1 & little &  \\ 
    \hline 
    headerBitsPerSample & 34 & int16 & 1 & little & \textbf{64} \\ 
    \hline 
    rdvxChunkId & 36 & char & 4 & big & \textbf{RDVX}\\ 
    \hline 
    rdvxChunkSize & 40 & int32 & 1 & little &  \\ 
    \hline 
    rdvxApiVersion & 44 & int32 & 1 & little & \textbf{2048} decimal or \textbf{0x800} hex \\ 
    \hline 
    rdvxServerRecvEpoch & 48 & int64 & 1 & little & Server arrival, ms since epoch UTC \\ 
    \hline 
    rdvxIdForVendor & 56 & uint32 & 1 & little & ``Device id" \\ 
    \hline 
    rdvxTruncatedUuid & 60 & int32 & 1 & little & Set by device, not user definable \\ 
    \hline 
    rdvxLatitude & 64 & float64 & 1 & little & Degrees latitude \\ 
    \hline 
    rdvxLongitude & 72 & float64 & 1 & little & Degrees longitude \\ 
    \hline 
    rdvxAltitude & 80 & float64 & 1 & little & Meters \\ 
    \hline 
    rdvxSpeed & 88 & float64 & 1 & little & Meters / second \\ 
    \hline 
    rdvxHorizontalAccuracy & 96 & float64 & 1 & little & Meters \\ 
    \hline 
    rdvxVerticalAccuracy & 104 & float64 & 1 & little & Meters \\ 
    \hline 
    rdvxCalibrationTrim & 112 & float64 & 1 & little &  \\ 
    \hline 
    rdvxTimeOfSolution & 120 & int32 & 1 & little & ms since epoch UTC \\ 
    \hline 
    rdvxFileStartMach & 124 & int64 & 1 & little & Device ``machine" internal count since app start \\ 
    \hline 
    rdvxFileStartEpoch & 132 & int64 & 1 & little & Device timestamp start of file, $\mu$s since epoch UTC \\ 
    \hline 
    rdvxNumMessageExchanges & 140 & int8 & 1 & little & Each message exchange contains 6 messages \\ 
    \hline 
    rdvxMessageExchanges & 141 & int64 & 60 & little &  Time synch messages, only first $6 * rdvxNumMessageExchanges$ messages are valid\\ 
    \hline 
    rdvxSensorName & 621 & char & 30 & big &  \\ 
    \hline 
    rdvxDeviceName & 651 & char & 20 & big &  \\ 
    \hline 
    rdvxSyncServer & 671 & char & 25 & big & Time synchronization server ip and port \\ 
    \hline 
    rdvxDataServer & 696 & char & 25 & big & Data acquisition server ip and port \\ 
    \hline 
    dataChunkId & 721 & char & 4 & big & \textbf{data} \\ 
    \hline 
    dataChunkSize & 725 & int32 & 1 & little & \textbf{4096} \\ 
    \hline 
    waveform & 729 & float64 & 512 & little & See section \ref{ssec:bar} \\ 
    \hline 
\end{tabulary} 

\subsection{Addendum on Barometer Waveform}\label{ssec:bar}
The \textit{waveform} field of barometer data contains 512 64-bit floating point values (256 timestamps and 256 barometer readings). These values are arranged in time, pressure pairs, and alternate between a 64-bit floating point timestamp in milliseconds and a 64-bit floating point barometer value in kPa.
\\\\
The iOS file time duration is approximately 333 seconds, or 5.5 minutes.
\\\\
Assuming a 0-indexed array, the values should be laid out in the following manner. Every even indexed value should be a timestamp and every odd indexed value should be a barometer reading.
\\ \\ For each index $n$ in [0, 2, 4, ..., $2^n$], $n$ is a timestamp. 
\\ \\ For each index $n$ in [0, 2, 4, ..., $2^n$], $n+1$ is a barometer reading.
\end{document}
