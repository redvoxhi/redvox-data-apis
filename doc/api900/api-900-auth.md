### Performing Authentication

Authentication is provided by utilizing JSON Web Tokens (JWT) (https://jwt.io/introduction/). The tokens are cryptographically signed with elliptic key cryptography.
The tokens themselves do not provide encryption, but rather they prove that the person who holds a JWT is who they say they are. Thus, by including a JWT in each RedVox packet,
we can be certain that that packet is from the owner described in the token. 

The tokens are provided by one of the RedVox servers. Tokens are acquired over HTTPS using the following method:

### Obtaining a token

A user must already have an account on the server they are trying to log into (i.e. https://redvox.io or the dev server at https://milton.soest.hawaii.edu:8000).

1. Send an HTTP POST "form url encoded" request to https://milton.soest.hawaii.edu:8000/login/mobile
    1. The request should contain two key-value pairs, "email" and "password". Email should hold the user's email and password should hold the user's password.
    2. The response will be a JSON body containing key-value pairs
        1. If "status" is 200, success, retrieve the JWT strimg from "jwt". This is the string you should include with every RedVox packet.
        2. If "status" is 400, error, the client created a bad or malformed request (make sure both email and password are included). 
        3. If "status" is 401, error, authentication error, either email or password is incorrect
        4. If "status" other or missing, error, unknown server error
    3. If status is anything other than 200, alert the user to error and specify that the username and/or password is incorrect.
    4. Login details should never be transmitted over http, only https. Moving forward this shouldn't be an issue since https is a prerequisite for api 900 (implemented server side). 

### Private mode

The app should contain a "Private" switch. When flipped, a field in the protobuf is_private is set to true. 

If is_private is set to true, then the following must also be set:
1. authenticated_email
2. auth_token

If either of the above are not set when is_private is true, then the server will reject the packet.

The privacy settings are then handled server side. User's who mark their data private will be the only ones who can access, view, and analyze their data. 

### RedVox Packet Responses

All api 900 protobuf packets sent to the server will receive a protobuf response detailed at: https://bitbucket.org/redvoxhi/redvox-data-apis/src/master/src/api900/api900.proto?at=master&fileviewer=file-view-default

The response contains a type, a checksum, a list of errors (if there are any), and a list of metadata (if there is any).

1. If "type" is "OK" and "checksum" matches, mark packet as sent
2. If "type" is "ERROR", you can find the specific errors in the "errors" field.
    1. If "errors" includes "NOT_AUTHENTICATED" or "OTHER"
        1. Backfill packet
3. If "type" is "OK", but "checksum" is wrong
    1. Backfill packet
4. If you don't receive a response
    1. Backfill packet

### Handling of backfilled data

Data can be backfilled for various reasons relating to network failure to server side errors and authentication issues.
    
TODO

### To Discuss

1. Does turning off the private switch mean backfilled private data can not be sent in the clear?
2. Does receiving a packet response of "NOT_AUTHENTICATED" force the app to "log out" the user?
3. If a user is "logged out", does the private switch automatically turn off?
4. Does backfilled data require the current state of the app (i.e. current private and auth settings) or the state when it was recorded?





