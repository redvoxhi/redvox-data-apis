import com.google.protobuf.InvalidProtocolBufferException;
import io.redvox.apis.Api900;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

/**
 * Gradle Dependencies:
 *      compile group: 'com.google.protobuf', name: 'protobuf-java', version: '3.4.0'
 *      compile group: 'net.jpountz.lz4', name: 'lz4', version: '1.3'
 *
 *  Make sure generated protobuf Java wrapper "Api900.java" is available in your project:
 *      You may need to update the package name of the Api900.java file.
 *      Can be obtained from: https://bitbucket.org/redvoxhi/redvox-data-apis/raw/master/src/api900/generated_code/java/Api900.java
 */

public class Api900Example {
    public static void main(final String[] args) throws InvalidProtocolBufferException {
        // Serialize Api900.RedvoxPacket
        // 1. Create an instance of an Api900.RedvoxPacket.Builder
        final Api900.RedvoxPacket.Builder redvoxPacketBuilder = getRedvoxPacketBuilder();

        // 2. Convert the builder into an instance of an Api900.RedvoxPacket
        final Api900.RedvoxPacket redvoxPacket = redvoxPacketBuilder.build();

        // 3. Serialize instance of RedvoxPacket to bytes
        final byte[] serializedRedvoxPacket = redvoxPacket.toByteArray();

        // 4. Compress with lz4
        final LZ4Factory lz4Factory = LZ4Factory.fastestInstance();
        final LZ4Compressor lz4Compressor = lz4Factory.highCompressor(); // Can also choose fastCompressor() here.
        final byte[] compressedRedvoxPacket = lz4Compress(lz4Compressor, serializedRedvoxPacket);

        // 5. Your compressedRedvoxPacket is now ready to send over websockets to acquisition server

        // Deserialize Api900.RedvoxPacket
        // 1. Decompress if serialized form is compressed
        final LZ4FastDecompressor lz4FastDecompressor = lz4Factory.fastDecompressor();
        final byte[] decompressedRedvoxPacket = lz4Decompress(lz4FastDecompressor, compressedRedvoxPacket);

        // Obtain an instance of a Api900.RedvoxPacket
        try {
            final Api900.RedvoxPacket packet = Api900.RedvoxPacket.parseFrom(decompressedRedvoxPacket);
            System.out.println(packet);

            // If you need to update any of the fields, you'll need to obtain a builder, and then build a new instance
            //.. i.e. ... packet.toBuilder().setIsBackfilled(true).build();

        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        // Deserialize Api900.RedvoxPacketResponse
        // These values will come from the server and will be an array of bytes, here we define them for our
        // toy examples
        final byte[] okResponse = java.util.Base64.getDecoder().decode("EK2BAg==");
        final byte[] errorResponse = java.util.Base64.getDecoder().decode("CAEQrYECGgEA");

        // First, we'll look at an ok response
        final Api900.RedvoxPacketResponse okRedvoxPacketResponse = Api900.RedvoxPacketResponse.parseFrom(okResponse);
        final Api900.RedvoxPacketResponse errorRedvoxPacketResponse = Api900.RedvoxPacketResponse.parseFrom(errorResponse);
        handleRedvoxPacketResponse(okRedvoxPacketResponse);
        handleRedvoxPacketResponse(errorRedvoxPacketResponse);
    }


    //      We will be filling in the fields of API 900 in-order as described at
    //      https://bitbucket.org/redvoxhi/redvox-data-apis/src/master/src/api900/api900.proto?at=master&fileviewer=file-view-default
    private static Api900.RedvoxPacket.Builder getRedvoxPacketBuilder() {
        final Api900.RedvoxPacket.Builder redvoxPacketBuilder = Api900.RedvoxPacket.newBuilder();

        // Scalar fields have setters associated with them
        redvoxPacketBuilder.setApi(900);

        // You can also chain a bunch of set calls together, this is called the builder pattern.
        redvoxPacketBuilder.setUuid("unique-token")
                .setRedvoxId("fancy-redvox-id")
                .setAuthenticatedEmail("john.doe@gmail.com")
                .setAuthToken("json-web-token");

        // Whether you decide to use the builder pattern or call each setter as an individual statement
        //      is up to you and the context that you're working with.
        redvoxPacketBuilder.setIsBackfilled(false);
        redvoxPacketBuilder.setIsPrivate(true);

        redvoxPacketBuilder.setDeviceMake("Motorola");
        redvoxPacketBuilder.setDeviceModel("Nexus 6");
        redvoxPacketBuilder.setDeviceOs("Android");
        redvoxPacketBuilder.setDeviceOsVersion("8.0");
        redvoxPacketBuilder.setAppVersion("1.04");
        redvoxPacketBuilder.setAcquisitionServer("wss://redvox.io:9000/api/900");
        redvoxPacketBuilder.setTimeSynchronizationServer("wss://redvox.io:9875");

        redvoxPacketBuilder.setAppFileStartTimestampEpochMicrosecondsUtc(1504637700000000L);
        redvoxPacketBuilder.setAppFileStartTimestampMachine(123L);

        // This value is filled in by server, you don't even need to set if you don't want to.
        redvoxPacketBuilder.setServerTimestampEpochMillisecondsUtc(0L);

        // Ok, repeated fields (or lists) can have items added from an Iterable (aka List), or one at a time.
        // First, if you already have all of your values in a list, you can add them all at one time like this.
        List<Long> timeSynchronizationMessages = Arrays.asList(1L, 2L, 3L, 4L);
        redvoxPacketBuilder.addAllTimeSynchronizationMessages(timeSynchronizationMessages);

        // It's also possible to add values one at a time to repeated fields like follows
        //      It depends on your personal preferences and context as to how you want to fill in repeated fields
        for(Long l : timeSynchronizationMessages) {
            redvoxPacketBuilder.addTimeSynchronizationMessages(l);
        }

        // Let's create an evenly sampled microphone channel and add it to the evenlySampledChannels
        final Api900.EvenlySampledChannel.Builder microphoneChannelBuilder = Api900.EvenlySampledChannel.newBuilder();
        microphoneChannelBuilder.addChannelTypes(Api900.ChannelType.MICROPHONE);
        microphoneChannelBuilder.setSensorName("internal-mic-...");
        microphoneChannelBuilder.setSampleRateHz(80.0);
        microphoneChannelBuilder.setFirstSampleTimestampEpochMillisecondsUtc(1504637700000L);

        // The Api900 provides flexible payload types for channels. It's up to you to select the payload type
        //      best suited for your data. We'll start with 32 bits and see how far that gets us.
        Api900.Int32Payload.Builder int32PayloadBuilder = Api900.Int32Payload.newBuilder();
        // Here, I'm adding fake data, but this is where you would set the waveform. Again, you can do this
        //      one element at a time, or if you already have the waveform you can pass it in as a single list
        for(int i = 0; i < 100; i++) {
            int32PayloadBuilder.addPayload(i);
        }

        microphoneChannelBuilder.setInt32Payload(int32PayloadBuilder);

        // Next we need to set the DQ fields associated with this payload
        //      We set an array of means and standard deviations for each payload in the channel
        //      The order is determined by the array of ChannelTypes
        //      If the channeltypes only includes a single channel, such as this example, we only set
        //      a single value. We will we see how to set multiple channels and values in the next example.
        microphoneChannelBuilder.addValueMeans(1.0); // This is the mean of the values of this channel
        microphoneChannelBuilder.addValueStds(1.0); // This is the std of values of this channel

        redvoxPacketBuilder.addEvenlySampledChannels(microphoneChannelBuilder);

        // Ok, let's create an unevenly sampled channel that has multiple values associated with a single timestamp
        Api900.UnevenlySampledChannel.Builder gpsChannelBuilder = Api900.UnevenlySampledChannel.newBuilder();
        gpsChannelBuilder.addChannelTypes(Api900.ChannelType.LATITUDE);
        gpsChannelBuilder.addChannelTypes(Api900.ChannelType.LONGITUDE);
        gpsChannelBuilder.addChannelTypes(Api900.ChannelType.ALTITUDE);
        gpsChannelBuilder.addChannelTypes(Api900.ChannelType.SPEED);
        gpsChannelBuilder.setSensorName("gps-name");
        // Since this channel is unevenly sampled, we include an array of timestamps associated with each sample
        gpsChannelBuilder.addAllTimestampsMillisecondsUtc(Arrays.asList(1L, 2L, 3L, 4L));
        // The payload includes a repeating cycle of values:
        //      I.e. for each timestamp "i" out of a total "N" timestamps, the payload looks as follows:
        //      [latitude[i], longitude[i], altitude[i], speed[i],
        //       latitude[i + 1], longitude[i + 1], altitude[i + 1], speed[i + 1]
        //       ...
        //       latitude[n - 1], longitude[n - 1], altitude[n - 1], speed[n - 1]
        // You'll note that the values in the payload follow the same ordering as the channel types
        // Let's go ahead and create a float64 payload for these values
        Api900.Float64Payload.Builder float64PayloadBuilder = Api900.Float64Payload.newBuilder();
        for(int i = 0; i < 100; i += 4) {
            double lat = (double)i;
            double lng = (double)i + 1;
            double alt = (double)i + 2;
            double speed = (double)i + 3;
            float64PayloadBuilder.addAllPayload(Arrays.asList(lat, lng, alt, speed));
        }

        gpsChannelBuilder.setFloat64Payload(float64PayloadBuilder);

        // Sample rate means and stds are calculated from the sample rate array
        // This is currently a repeated field, but will be made into a scalar in the near future
        // So you might not want to set this one quite yet
        gpsChannelBuilder.setSampleRateMeans(1.0);
        gpsChannelBuilder.setSampleRateStds(1.0);

        // The value means and stds is an array of these values, one for each ChannelType
        // So for this example, the values_means array would contain the following:
        //      [mean(lat), mean(lng), mean(altitude), mean(speed)]
        // And for the stds
        //      [std{lat}, std(lng), std(altitude), std(speed)]
        gpsChannelBuilder.addAllValueMeans(Arrays.asList(1.0, 1.0, 1.0, 1.0));
        gpsChannelBuilder.addAllValueStds(Arrays.asList(1.0, 1.0, 1.0, 1.0));

        redvoxPacketBuilder.addUnevenlySampledChannels(gpsChannelBuilder);

        redvoxPacketBuilder.addMetadata("Optional meta data string");

        return redvoxPacketBuilder;
    }

    // The returned payload includes the size of the original uncompressed data as the first 4 bytes
    private static byte[] lz4Compress(final LZ4Compressor lz4Compressor, final byte[] uncompressedBytes) {
        final byte[] uncompressedSize = ByteBuffer.allocate(4).putInt(uncompressedBytes.length).array();
        final byte[] compressedData = lz4Compressor.compress(uncompressedBytes);
        final byte[] payload = new byte[uncompressedBytes.length + compressedData.length];
        System.arraycopy(uncompressedSize, 0, payload, 0, uncompressedSize.length);
        System.arraycopy(compressedData, 0, payload, uncompressedSize.length, compressedData.length);
        return payload;
    }

    private static byte[] lz4Decompress(final LZ4FastDecompressor decompressor, final byte[] bytes) {
        // First four bytes are decompressed size
        final byte[] uncompressedSizeBytes = Arrays.copyOf(bytes, 4);
        final byte[] compressedData = Arrays.copyOfRange(bytes, uncompressedSizeBytes.length, bytes.length);
        final int uncompressedSize = ByteBuffer.wrap(uncompressedSizeBytes).getInt();
        final byte[] result = new byte[uncompressedSize];
        decompressor.decompress(compressedData, result);
        return result;
    }

    private static void handleRedvoxPacketResponse(final Api900.RedvoxPacketResponse redvoxPacketResponse) {
        switch(redvoxPacketResponse.getResposneType()) {
            case OK:
                System.out.format("%s %d\n", "OK resp", redvoxPacketResponse.getChecksum());
                break;
            case ERROR:
                System.out.format("%s %d\n", "ERROR resp", redvoxPacketResponse.getChecksum());
                for(Api900.RedvoxPacketResponse.RedvoxPacketResponseError error : redvoxPacketResponse.getErrorsList()) {
                    System.out.println("\t" + error);
                }
                break;
            case UNRECOGNIZED:
                System.out.format("%s %d\n", "UNRECOGNIZED resp", redvoxPacketResponse.getChecksum());
                break;
        }
    }
}
