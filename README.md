# README #

This repository serves as a central location to document and discuss the RedVox data APIs.

## APIs
* [API 800](https://bitbucket.org/redvoxhi/redvox-data-apis/raw/master/doc/api800/redvox_data_api800.pdf)
* [API 900](https://bitbucket.org/redvoxhi/redvox-data-apis/src/master/doc/api900/api900.md?at=master&fileviewer=file-view-default)